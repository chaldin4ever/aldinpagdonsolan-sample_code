<?php

namespace App\Utils;

use App\Charts\POCChart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use MongoDB\BSON\UTCDateTime;
use Auth;
use Redis;
//use Debugbar;

class Reports {

    public static function chart($type, $label, $dataset, $title, $options, $dataset2=null, $title2=null, $options2=null){

        $chart = new POCChart();
        $chart->dataset($title, $type, $dataset)
            ->options($options);
        $chart->labels($label);

        if($dataset2){
            $chart->dataset($title2, $type, $dataset2)->options($options2);
        }

        return $chart;
//        }

    }


    public static function dateFilter(){
        $months = 12;
        $carbonDate = Carbon::now();

        $date = $carbonDate->endOfDay();

        $fromDate = Toolkit::ConvertStringToCarbon(Input::get('FromDate'));
        $toDate = Toolkit::ConvertStringToCarbon(Input::get('ToDate'));


        if($fromDate && $toDate){
            $start = $fromDate->startOfDay();
            $end = $toDate->endOfDay();
//            dd([$fromDate, $toDate]);
            $dt = Toolkit::CarbonToUTCDateTime($fromDate->startOfDay());
            $et = Toolkit::CarbonToUTCDateTime($toDate->endOfDay());
            $months = $fromDate->diffInMonths($toDate);
            $moyrArr = Toolkit::GetDataArrayForChart($toDate, $months);
        }else{

            $et = Toolkit::CarbonToUTCDateTime($date);
//            dd([ $date]);
            $moyrArr = Toolkit::GetDataArrayForChart($date, $months);
            $oneYearAgo = $date->subMonths($months)->startOfDay() ;
            $dt = Toolkit::CarbonToUTCDateTime($oneYearAgo);

            $start = $oneYearAgo;
            $end = Carbon::now()->endOfDay();
        }

        $dates = [$start, $end];

        return ['dates' => $dates, 'moyrArr'=>$moyrArr, 'dt'=>$dt, 'et'=>$et];
    }

    public static function Options(){

        $option1 = [
            'borderColor' => 'rgb(0, 48, 86, .5)',
            'backgroundColor' => 'rgb(0, 48, 86, .2)',
            'borderWidth' => 1,
            'hoverBackgroundColor' => 'rgb(0, 48, 86, .4)',
            'hoverBorderColor' => 'rgb(0, 48, 86, .8)',
            'hoverBorderWidth' => 1
        ];

        $option2 = [
            'borderColor' => 'rgba(255, 99, 132, 0.2)',
            'backgroundColor' => 'rgba(255, 99, 132, 0.2)',
            'hoverBackgroundColor' => 'rgba(255, 99, 132, 0.5)',
            'hoverBorderColor' => 'rgba(255, 99, 132, 0.5)',
            'borderWidth' => 1,
            'hoverBorderWidth' => 1
        ];

        $option3 =[
            'borderColor' => 'rgb(0, 48, 86, .5)',
            'backgroundColor' => 'rgb(0, 48, 86, .2)',
            'borderWidth' => 1,
            'hoverBackgroundColor' => 'rgb(0, 48, 86, .3)',
            'hoverBorderColor' => 'rgb(0, 48, 86, .8)',
            'hoverBorderWidth' => 1
        ];

        return [
            'option1' => $option1, 'option2' => $option2, 'option3' => $option3,
        ];
    }

}