<?php

namespace App\Utils;

use App\Models\Facility;
use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;
use App\Models\Ticket;
use App\Models\FormToken;
use StateMachine;
use Auth;
use Redis;

class Toolkit {
    public static function GetTodayUTC(){
        $dt = Carbon::now();
        return new UTCDateTime($dt->timestamp*1000);
    }

    // $date is a string in yyyy-MM-dd format
    public static function GetDateUTC($date)
    {
        $carbon = new Carbon($date);
        return new UTCDateTime($carbon->timestamp * 1000);
    }

    public static function UTCDateTimeToFullString($dt){
        if(isset($dt)){
            $email = Auth::user()->email;
            $key_tz = "$email.timezone";
            $key_dt = "$email.date_format";
            $tz = empty(Redis::get($key_tz))? "UTC": Redis::get($key_tz);
            $dt_format = Redis::get($key_dt)." H:i";
            if (empty($dt_format))
                $dt_format = "Y-m-d H:i";
            return $dt->toDateTime()->setTimezone(new \DateTimeZone($tz))->format($dt_format);
        }else
            return '';
    }

    public static function UTCDateTimeToDateString($dt)
    {
        if (isset($dt)) {
            $email = Auth::user()->email;
            $key_tz = "$email.timezone";
            $key_dt = "$email.date_format";
            $tz = empty(Redis::get($key_tz)) ? "UTC" : Redis::get($key_tz);
            $dt_format = Redis::get($key_dt);
            if(empty($dt_format))
                $dt_format = "Y-m-d";
            return $dt->toDateTime()->setTimezone(new \DateTimeZone($tz))->format($dt_format);
        } else
            return '';
    }

    public static function UTCDateTimeToShortDateString($dt)
    {
        if (isset($dt)) {
            $email = Auth::user()->email;
            $key_tz = "$email.timezone";
            $key_dt = "$email.date_format";
            $tz = empty(Redis::get($key_tz)) ? "UTC" : Redis::get($key_tz);
            $dt_format = Redis::get($key_dt);
            return $dt->toDateTime()->setTimezone(new \DateTimeZone($tz))->format($dt_format);
        } else
            return '';
    }

    public static function UTCDateTimeToYYMMDD($dt)
    {
        if (isset($dt)) {
            $email = Auth::user()->email;
            $key_tz = "$email.timezone";
            $tz = empty(Redis::get($key_tz)) ? "UTC" : Redis::get($key_tz);
            $dt_format = "Y-m-d";
            return $dt->toDateTime()->setTimezone(new \DateTimeZone($tz))->format($dt_format);
        } else
            return '';
    }

    public static function GetTicketNumber($project_code){
        $ur = rand(100001, 999999);
        $ticket = $project_code. '-'.((string)$ur);
        for($i = 0; $i < 899998; $i++) {
            $ur = rand(100001, 999999);
            $ticket = $project_code. '-'.((string)$ur);
            // check if this number is being used
            $row = Ticket::where('Ticket', $ticket)->get()->first();
            if(empty($row)){
                break;
            }
        }
        // $ticket = $project_code.'%';
        // $row = Ticket::orderBy('created_at', 'desc')
        //     ->where('Ticket', 'like', $ticket)->get()->first();
        // if(empty($row)){
        //     $ticket = $project_code.'-1';
        // } else {
        //     $ps = explode('-', $row->Ticket);
        //     $n = intval($ps[1]) + 1;
        //     $ticket = $project_code.'-'.$n;
        // }
        return $ticket;
    }


    public static function CarbonToDateFullString($carbon){
        return Toolkit::UTCDateTimeToFullString(new UTCDateTime($carbon->timestamp*1000));
    }

    public static function CarbonToDateString($carbon)
    {
        return Toolkit::UTCDateTimeToShortDateString(new UTCDateTime($carbon->timestamp * 1000));
    }

    public static function GetInitials($name){
        $ps = explode(' ', $name);
        if(sizeof($ps) >= 2 && strlen($ps[0]) > 1 && strlen($ps[1]) > 1){
            return strtoupper(substr($ps[0],0, 1).substr($ps[1],0, 1));
        } else {
            return strtoupper(substr($name,0, 2));
        }
    }

    public static function LogJournal($project, $user, $action, $message, $id){

        /*$project_id = array_get($project, 'id');
        $p = \App\Models\Project::find($project_id);
        $recipients = [];
        if(!empty($p->Members) && is_array($p->Members)){
            foreach ($p->Members as $m) {
                $recipients[] = [
                    'email' => array_get($m, 'email'),
                    'state' => 'new',
                ];
            }
        }

        $j = new \App\Models\Journal();
        $j->Project = $project;
        $j->User = $user;
        $j->Action = $action;
        $j->Message = $message;
        $j->ObjectId = $id;
        $j->Recipients = $recipients;
        $j->save();*/
    }

    public  static function GetDefaultRole($project, $user){
        $role = 'Client';
        if(empty($project->Members))
            return $role;
        $members = $project->Members;
        foreach($members as $m){
            if(array_get($m, 'email') == $user->email){
                $roles = array_get($m, 'roles');
                if(is_array($roles)){
                    if(sizeof($roles) > 0)
                        $role = $roles[0];
                } else {
                    $role = $roles;
                }
                break;
            }
        }
        if(empty($role) && !empty($project->UserRoles)){
            $roles = explode(",", $project->UserRoles);
            if(sizeof($roles) > 0)
                $role = $roles[0];
        }
        if(empty($role))
            $role = 'Client';
        return $role;
    }

    // some project may choose to use fields collected as the ticket title
    public static function GetCustomTitle($project, $ticket)
    {
        $title = "";
        $fields = explode(",", $project->TitleFields);
        $response = $ticket->Response;
        foreach($fields as $fld){
            $val = array_get($response, trim($fld));
            if(!empty($val))
                $title = "$title $val";
        }
        return $title;
    }

    public static function GetTicketDueDate($project, $ticket){
        $duedate = "";
        $fld = $project->DueDateField."_submit";
        $response = $ticket->Response;
        $duedate = array_get($response, $fld);
        if(!empty($duedate)){
            $duedate = Toolkit::GetDateUTC($duedate);
        }
        return $duedate;
    }

    public static function GetProjectStates($project){
        if(empty($project->StateMachine))
            $stateMachine = config('oidesk.default_workflow');
        else
            $stateMachine = $project->StateMachine;
        $smConfig = config("state-machine." . $stateMachine);
        return array_get($smConfig, "states");
    }

    public static function CheckUserStatePermission($project, $currentUserRole, $currentTicketState){
        $hasPermission = false;
        $states = array_get($project->UserStates, $currentUserRole);
        if(!empty($states) && is_array($states)){
            $hasPermission = in_array($currentTicketState, $states);
        }
        return $hasPermission;
    }

    public static function GetFormToken($code, $email){
        $token = new FormToken();
        $token->Token = uniqid($code);
        $token->Owner = $email;
        $token->save();
        return $token;
    }

    // from state to find user role
    // from user role to find members
    public static function FindTeam($project, $state, $owner_email){
        $teams = [];
        $userStates = $project->UserStates;
        if(empty($userStates)) return $teams;
        if(!is_array($userStates)) return $teams;

        $members = $project->Members;
        $roles = [];
        foreach($userStates as $role => $ustates){
            if(!is_array($ustates)) continue;
            if(in_array($state, $ustates)){
                $roles[] = $role;
            }
        }
        foreach($members as $m){
            $found = false;
            foreach($roles as $role){
                if(!is_array(array_get($m, 'roles'))) continue;
                if(in_array($role, array_get($m, 'roles'))){
                    $found = true;
                    break;
                }
            }
            if($found && $owner_email != array_get($m, 'email'))
                $teams[] = array_get($m, 'email');
        }
        return $teams;
    }

    public static function GetTeamMembers($email){
        $projects = \App\Models\Project::orderBy('Name')
            ->where('Members.email', $email)
            ->where('State', 'on')
            ->get();

        $members = [];
        foreach ($projects as $p) {
            foreach ($p->Members as $m) {
                $em = array_get($m, 'email');
                if (!array_key_exists($em, $members)) {
                    $members[$em] = [
                        'name' => ucwords(array_get($m, 'name')),
                        'email' => array_get($m, 'email')
                    ];
                }
            }
        }
        asort($members);
        return $members;
    }

    public static function GetUserProjects($email){
        $projects = \App\Models\Project::orderBy('Description')
            ->orderBy('Name')
            ->where('Members.email', $email)
            ->where('State', 'on')
            ->get();
        $data=[];
        foreach($projects as $p){
            $data[$p->_id] = $p;
        }
        return $data;
    }

    public static function GetUserProjectIdArray($email)
    {
        $projects = Toolkit::GetUserProjects($email);
        $ret = [];
        foreach ($projects as $p) {
            $ret[] = $p->_id;
        }
        return $ret;
    }


    public static function GetTicketCurrentState($project, $ticket_id){
        $ticket = Ticket::find($ticket_id);
        $stateMachine = empty($project->StateMachine) ? config('oidesk.default_workflow') : $project->StateMachine;
        $sm = StateMachine::get($ticket, $stateMachine);
        return $sm->getState();
    }

    public static function GetProjectNewConverdsations($projects , $email){
        // dd($email);
        $results = [];
        foreach ($projects as $p) {
            // need to find out conversation not sent by the user and is still new to you
            $logs = \App\Models\Conversation::whereRaw(
                [
                    'Project.id' => $p->_id,
                    'Recipients' =>
                        array('state' => 'new', 'email' => $email)
                ]
            )->get();
            $results[$p->_id] = sizeof($logs);
        }
        return $results;
    }

    public static function AddChat($sender, $receiver, $note){
        $chat = new \App\Models\TeamChat();
        $chat->sender = $sender;
        $chat->receiver = $receiver;
        $chat->note = $note;
        $chat->State = 'new';
        $chat->save();

        $note = str_replace('[CHAT_ID]', $chat->_id, $note);
        $chat->note = $note;
        $chat->save();
    }

    public static function SendChatToProjectMembers ($project, $ticket, $sender, $note){
        foreach($project->Members as $m){
            if(array_get($m, 'email') == $sender)
                continue;
            Toolkit::AddChat($sender, array_get($m, 'email'), $note);
        }
    }

    public static function ChangeTicketState($ticket_id, $state){
        $ticket = \App\Models\Ticket::find($ticket_id);
        $project = \App\Models\Project::find(array_get($ticket->Project, 'id'));
        if (empty($project))
            return 0;

        $stateMachine = empty($project->StateMachine) ? config('oidesk.default_workflow') : $project->StateMachine;
        $sm = StateMachine::get($ticket, $stateMachine);
        $trans = $sm->getPossibleTransitions();
        // $trans[] = $state; 
        // dd($trans);
        if (in_array($state, $trans)  && $sm->can($state)) {
            $owner = ["name" => Auth::user()->name, "email" => Auth::user()->email];

            $oldState = $ticket->State;
            $sm->apply($state);
            $newState = $ticket->State;
            $note = "Change from [$oldState] to [$newState]";
            $comment = [
                'Category' => 'system',
                'Action' => 'change-state',
                'Comment' => $note,
                'Timestamp' => \App\Utils\Toolkit::GetTodayUTC(),
                'Owner' => $owner,
                'uuid' => uniqid($project->Code),
                'notify_owner' => false,
            ];
            $d = $ticket->Discussions;
            $d[] = $comment;
            $ticket->Discussions = $d;

            $ticket->save();

            // send notification to members of this new state
            // find all members of this state
            
            // $teams = \App\Utils\Toolkit::FindTeam($project, $ticket->State, "");
            // if (sizeof($teams) > 0)
            //     SendNotificationForNewTicket::dispatch($teams, $project, $ticket, 'internal');

            \App\Utils\Toolkit::LogJournal(
                $ticket->Project,
                $owner,
                'change-state',
                'Changes to [' . $state . '] - ' . $ticket->Title . ' (' . $ticket->Ticket . ')',
                $ticket->_id
            );

            // $extraAttr = "action='markAsRead' value='[CHAT_ID]'";
            // $note = '<p>Ticket <a href="/ticket/' . $ticket_id . '" ' . $extraAttr . '>' . $ticket->Title . ' [' . $ticket->Ticket . ']</a> changed to [' . $state . ']</p>';
            // \App\Utils\Toolkit::SendChatToProjectMembers($project, $ticket, Auth::user()->email, $note);

            

        }
        return 2;
    }

    public static function HasPermission($email, $project_id){
        $data = Toolkit::GetUserProjectIdArray($email);
        return in_array($project_id, $data);
    }

    public static function CheckAlerts($email){
        // check message
        $messages = \App\Models\TeamChat::where('receiver', $email)
            ->where('sender', '!=', $email)
            ->where('State', 'new')
            ->get();

        // check replies
        $chats = \App\Models\TeamChat::where('replies.receiver', $email)
            ->where('replies.state', 'new')
            ->get();

        $reply_count = 0;
        foreach ($chats as $chat) {
            if (empty($chat->replies))
                continue;
            $countIt = false;
            foreach ($chat->replies as $reply) {
                if (array_get($reply, 'receiver') == $email && array_get($reply, 'state') == 'new') {
                    $countIt = true;
                    break;
                }
            }
            if ($countIt) {
                $reply_count++;
            }
        }
        $chat_count = sizeof($messages) + $reply_count;

        // check new conversations
        $projects = Toolkit::GetUserProjects($email);
        $conversationCounts = Toolkit::GetProjectNewConverdsations($projects, $email);
        $conversationCount = 0;
        foreach ($conversationCounts as $c) {
            $conversationCount += $c;
        }

        return [
            "message" => $chat_count,
            "converse" => $conversationCount,
        ];
    }

    public static function CheckAlertsByMember($email)
    {
        $results = [];
        // check message
        $messages = \App\Models\TeamChat::where('receiver', $email)
            ->where('sender', '!=', $email)
            ->where('State', 'new')
            ->get();
        // dd($messages);
        foreach($messages as $m){
            $sender = $m->sender;
            // dd($receiver);
            if(empty(array_get($results, $sender)))
            {
                $results[$sender] = 1;
            } else {
                $results[$sender] = $results[$sender] + 1;
            }
        }
        // check replies
        $chats = \App\Models\TeamChat::where('replies.receiver', $email)
            ->where('replies.state', 'new')
            ->get();

        foreach ($chats as $chat) {
            if (empty($chat->replies))
                continue;
            foreach ($chat->replies as $reply) {
                if (array_get($reply, 'receiver') == $email && array_get($reply, 'state') == 'new') {
                    $sender = array_get($reply, 'sender');
                    if (empty(array_get($results, $sender)))
                    {
                        $results[$sender] = 1;
                    } else {
                        $results[$sender] = $results[$sender] + 1;
                    }
                }
            }
        }
        return $results;
    }

    public static function GetDateTimeFormat(){
        $locale = array_get($_COOKIE, 'user_locale');
        if($locale == null || $locale == ''){
            $locale = 'en';
        }
        if($locale=='zh'){
            return "mm/dd/yyyy";
        } else {
            return "dd/mm/yyyy";
        }
    }

    public static function UTCDateTimeToString($dt){
        if(isset($dt)){
            date_default_timezone_set("Australia/Melbourne");
            $locale = array_get($_COOKIE, 'user_locale');
            if($locale == null || $locale == ''){
                $locale = 'en';
            }
            if($locale=='zh'){
                return $dt->toDateTime()->setTimezone(new \DateTimeZone("Asia/Shanghai"))->format('Y-m-d');
            } else {
                return $dt->toDateTime()->setTimezone(new \DateTimeZone("Australia/Melbourne"))->format('d-M-y');
            }
        }else
            return '';
    }

    public static function DateTimeToString($dt, $time=false){
        $locale = array_get($_COOKIE, 'user_locale');
        if($locale == null || $locale == ''){
            $locale = 'en';
        }
        if($locale=='zh'){
            if($time == false)
                return $dt->setTimezone(new \DateTimeZone("Asia/Shanghai"))->format('Y-m-d');
            else
                return $dt->setTimezone(new \DateTimeZone("Asia/Shanghai"))->format('Y-m-d G:i A');
        } else {

            if($time == false)
                return $dt->setTimezone(new \DateTimeZone("Australia/Melbourne"))->format('d-M-y');
            else
                return $dt->setTimezone(new \DateTimeZone("Australia/Melbourne"))->format('d-M-y G:i A');
        }
    }

    public static function ConvertStringToCarbon($str, $eod = false){
        $locale = array_get($_COOKIE, 'user_locale');
        if($locale == null || $locale == ''){
            $locale = 'en';
        }
        $ps = explode('/', $str);
        if(!$ps) return "";
        if(sizeof($ps) < 3) return "";
        if($locale=='zh'){
            $mm = intval($ps[0]);
            $dd = intval($ps[1]);
            $yy = intval($ps[2]);
        } else {
            $mm = intval($ps[0]);
            $dd = intval($ps[1]);
            $yy = intval($ps[2]);
        }
        $dt = Carbon::createFromDate($yy, $mm, $dd);
        if($eod)
            $dt = $dt->endOfDay();
        else
            $dt = $dt->startOfDay();
        return $dt;
    }

    //for Chart Date Time
    public static function ConvertStringDateTimeToCarbon($str, $time){
        $locale = array_get($_COOKIE, 'user_locale');
        if($locale == null || $locale == ''){
            $locale = 'en';
        }
        $ps = explode('/', $str);
        if(!$ps) return "";
        if(sizeof($ps) < 3) return "";
        if($locale=='zh'){
            $mm = intval($ps[0]);
            $dd = intval($ps[1]);
            $yy = intval($ps[2]);
        } else {
            $mm = intval($ps[0]);
            $dd = intval($ps[1]);
            $yy = intval($ps[2]);
        }

        $t = [];
        if(!empty($time)){
            $t = explode(':', $time);
            $hour = array_get($t, 0);
            $minute = array_get($t, 1);
        }else{
            $hour = '00';
            $minute = '00';
        }

        $second = '00';
        $email = Auth::user()->email;
        $key_tz = "$email.timezone";
        $tz = empty(Redis::get($key_tz))? "UTC": Redis::get($key_tz);

        $dt = Carbon::create($yy, $mm, $dd, $hour, $minute, $second, $tz);// Carbon::createFromDate($yy, $mm, $dd);
        $utcDate = Toolkit::CarbonToUTCDateTime($dt);

        $datetime = [
            'timestampp' => $utcDate,
            'datetime' => self::DateTimeToString($dt, true)
        ];
        return $datetime;
    }

    public static function CarbonToUTCDateTime($carbon){
        return new UTCDateTime($carbon->timestamp*1000);
    }

    public static function UTCDateTimeToDatePicker($dt){
        if(isset($dt)){
            return $dt->toDateTime()->format('m/d/Y');
        }else
            return '';
    }

    public static function DateTimeToStatusActivityDate($dt){
        if(isset($dt)){
            return $dt->format('d.m.Y h:i A');
        }else
            return '';
    }

    public static function GetCareDomains()
    {
        $rows = \App\Models\CareDomain::where('Status', 'Active')
            ->get();
        $data = [];
        foreach ($rows as $row) {
            $data[$row->_id] = $row->CareDomain;
        }
        return $data;
    }

    public static function SetupUserSession($facility, $userid){

        $key = 'current.provider.' . $userid;
        $provider_id = array_get($facility->Provider, 'ProviderId');
        Redis::set($key, $provider_id);
        Redis::expire($key, config('poc.expire_in_24hrs'));

        $db = "mongodb";
        if(!empty($facility->Provider)){
            $provider = \App\Models\Provider::find($provider_id);
            if(!empty($provider)){
                if(!empty($provider->database)){
                    $db = $provider->database;
                }
            }
        }
        $key = 'current.db.' . $userid;
        Redis::set($key, $db);
        Redis::expire($key, config('poc.expire_in_24hrs'));
    }

    public static function GetDatabase($userid){
        $key = 'current.db.' . $userid;
        $db = Redis::get($key);
        if(empty($db))
            $db = "mongodb";
        return $db;
    }

    public static function GetDataArrayForChart($carbonDate, $months){

        $date = $carbonDate->addMonth(1);
        $oneYearAgo = $date->subMonths($months);
        $data = [];
        for($i = 0; $i < $months; $i++){
            $key = $oneYearAgo->format('Ym');
            $MonthYear = $oneYearAgo->format('M-Y');
            $month = $oneYearAgo->format('M');
            $year = $oneYearAgo->format('Y');
            $monthLanguage = __("months.$month");
//            $my = $monthLanguage.'-'.$year;
            $my = $month.'-'.$year;
            $data[$key]= ['MonthYear' => $my, 'count' => 0];
            $oneYearAgo->addMonth();
        }
        return $data;
    }

    public static function sortFacility($a, $b)
    {
        $az = $a->NameLong;
        $bz = $b->NameLong;
        if ($az == $bz) return 0;
        return ($az < $bz) ? -1 : 1;
    }

    public static function ArrayToObject($array, $class = 'stdClass', $strict = false) {
        if (!is_array($array)) {
            return $array;
        }

        //create an instance of an class without calling class's constructor
        $object = unserialize(
            sprintf(
                'O:%d:"%s":0:{}', strlen($class), $class
            )
        );

        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name => $value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {

                    if(method_exists($object, 'set'.$name)){
                        $object->{'set'.$name}(Toolkit::ArrayToObject($value));
                    }else{
                        if(($strict)){

                            if(property_exists($class, $name)){

                                $object->$name = Toolkit::ArrayToObject($value);

                            }

                        }else{
                            $object->$name = Toolkit::ArrayToObject($value);
                        }

                    }

                }
            }
            return $object;
        } else {
            return FALSE;
        }
    }

    public static function smart_count($v){
        if(empty($v))
            return 0;
        if(is_array($v))
            return count($v);
        else
            return 0;
    }

    public static function getFacility(){

        $userid = Auth::user()->_id;
        $key = 'selected.facility.' . $userid;

        $facility_id = Redis::get($key);
        //if(empty($facility_id))return redirect(url('facility/select'));
        $facility = Facility::find($facility_id);
        //if(empty($facility)) redirect(url('facility/select'));

        return $facility;
    }
}

