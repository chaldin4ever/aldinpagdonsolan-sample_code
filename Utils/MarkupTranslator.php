<?php

namespace App\Utils;

use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;
use App\Models\Ticket;

class MarkupTranslator{
    public static function Translate($markup){
        $form = [];
        $questions = explode("==", $markup);
        foreach($questions as $q){
            $qtn = [];
            $lines = explode("\r\n", $q);
            foreach($lines as $line){
                $str = trim($line);
                if(empty($str)) continue;
                $prefix = substr($str, 0, 1);
                if($prefix == "?"){
                    $qtn['text'] = (substr($str, 1));
                    if(substr($str, -1, 1) == "*"){
                        $qtn['required'] = true;
                    } else {
                        $qtn['required'] = false;
                    }
                } else if($prefix == ">"){
                    $qtn['field'] = (substr($str, 1));
                } else if ($prefix == "~") {
                    $qtn['prompt'] = (substr($str, 1));
                } else if ($prefix == "#") {
                    $qtn['code'] = (substr($str, 1));
                }
            }
            $form[] = $qtn;
        }
        return $form;
    }
}