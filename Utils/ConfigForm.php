<?php

namespace App\Utils;

use App\Models\FormQuestions;
use Carbon\Carbon;
use Auth;
use Redis;
use App\Models\Config;

class ConfigForm {

    public static function getForm($configID){

        $config = Config::where('ConfigID', $configID)->first();

        if(!empty($config)) return $config->Form;

    }

    public static function getFormQuestionFields($formId, $qcode){

        $question = FormQuestions::where('Form.FormId', $formId)
                        ->where('Code', $qcode)->first();

        if(!empty($question)){

            $code = $question->Code;
            if(!empty($question->Fields)){

                $fields = [];
                foreach($question->Fields as $field){
                    $fcode = $code.'-'.array_get($field, 'code');
                    $fields[$fcode] = array_get($field, 'text');
                }

                return $fields;
            }
        }

    }

}