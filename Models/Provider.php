<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Provider extends Eloquent
{
    protected $collection = 'Provider';

    public function getObjectAttribute(){

        return [
            'ProviderId' => $this->_id,
            'ProviderName' => $this->ProviderName
        ];
    }

    public function getFacilitiesAttribute(){

        $facility = [];
        if(isset($this->Facility)){
            if(is_array($this->Facility))
                foreach($this->Facility as $fac){
                    $facility[] = array_get($fac, 'FacilityName');
                }
        }

        return $facility;
    }

}
