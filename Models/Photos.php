<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Photos extends Eloquent
{
    protected $collection = 'Photos';

}
