<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Rooms extends Eloquent
{
    protected $collection = 'Rooms';
    //

    public function getObjectAttribute(){

        return ['RoomId' => $this->_id, 'RoomName' => $this->RoomName];
    }

    public function getFacilityNameAttribute(){

        $facilityId = $this->Facility;
        $facility = Facility::find($facilityId);

        if(!empty($facility)) return $facility->NameLong;

    }


    public function getAreaNameAttribute(){

        $facilityId = $this->Facility;
        $facility = Facility::find($facilityId);
        $areas = $facility->Areas;

        if(!empty($facility)){
            $areas = $facility->Areas;

            $areaname = [];
            foreach($areas as $area){
                $areaname[array_get($area, 'AreaId')] = array_get($area, 'AreaName');
            }

            return array_get($areaname, $this->Area);
        }

    }

    public function getResidentNameAttribute(){

        return array_get($this->CurrentResident, "ResidentName");


    }

    public function getVacantNameAttribute(){

        if($this->Vacant == 'on') return 'Yes'; else return 'No';

    }
}
