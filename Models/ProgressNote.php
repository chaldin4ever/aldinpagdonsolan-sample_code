<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProgressNote extends Eloquent
{
    protected $collection = 'ProgressNote';

    protected $dates = ['ArchivedDate'];

    public function getObjectAttribute(){

        return [
            'progressNoteId' => $this->_id,
            'Notes' => $this->Notes
        ];

    }

}
