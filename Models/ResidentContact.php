<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ResidentContact extends Eloquent
{
    protected $collection = 'ResidentContact';
    //
    protected $fillable = ['ResidentId','DisplayOrder','Title', 'FirstName', 'LastName',
        'Address', 'Suburb', 'PostCode', 'Relationshp', 'City', 'Mobile', 'Phone', 'Email'];
}
