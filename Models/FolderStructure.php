<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FolderStructure extends Eloquent
{
    protected $collection = 'FolderStructure';
    //

    public function GetObjectAttribute()
    {
        $data = array();
        $data['FolderId'] = $this->_id;
        $data['Parent'] = $this->Parent;
        $data['FolderName'] = $this->FolderName;

        return (Object)$data;
    }

    public function GetParentFolderAttribute()
    {
        $data = array();
        $data['FolderId'] = $this->_id;
        $data['ParentName'] = $this->FolderName;

        return (Object)$data;
    }

}
