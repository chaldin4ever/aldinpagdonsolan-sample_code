<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Facility extends Eloquent
{
    protected $collection = 'Facility';
    //

    public function getObjectAttribute(){

        if(!empty($this->NameLong)){
            return [
                'FacilityId' => $this->_id,
                'FacilityName' => $this->NameLong
            ];
        }

    }

    public function getForResidentObjectAttribute(){

        if(!empty($this->NameLong)){
            return [
                'facilityId' => $this->_id,
                'facilityName' => $this->NameLong
            ];
        }

    }

    public function getObjectContactNameAttribute(){

        if(!empty($this->ContactLastName) && !empty($this->ContactFirstName)){

            return $this->ContactFirstName.' '.$this->ContactLastName;
        }

    }
}
