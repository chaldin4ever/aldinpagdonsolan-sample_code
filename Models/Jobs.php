<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Jobs extends Eloquent
{
    protected $collection = 'Jobs';
    //

//    protected $dates = ['ExpiryDate'];

    public function getObjectAttribute(){

        return [
            'JobId' => $this->_id,
            'JobTitle' => $this->JobTitle
        ];
    }

}
