<?php

namespace App\Models;

use App\Utils\Toolkit;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ResidentBGL extends Eloquent
{
    protected $collection = 'ResidentBGL';
    //

    public function getBGLDateTimeAttribute(){

        if(!empty($this->data)){

            if(array_key_exists('BGL', $this->data)){

                $time = array_get($this->data, 'Time');
                $nt = date("H:i", strtotime($time));
                $date = array_get($this->data, 'Date');

                $dt = Toolkit::ConvertStringDateTimeToCarbon($date, $nt);

                return array_get($dt, 'datetime');
            }

        }

    }
}
