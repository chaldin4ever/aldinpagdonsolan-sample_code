<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class IntranetSetup extends Eloquent
{
    protected $collection = 'POCIntranetSetup';
}
