<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProgressNoteComment extends Eloquent
{
    protected $collection = 'ProgressNoteComment';

}
