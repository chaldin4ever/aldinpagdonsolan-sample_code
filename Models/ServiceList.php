<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ServiceList extends Eloquent
{
    protected $collection = 'ServiceList';

    public function getObjectAttribute(){

        return [
            'ServiceListId' => $this->_id,
            'ServiceName' => $this->ServiceName
        ];
    }

}
