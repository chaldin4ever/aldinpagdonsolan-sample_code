<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SProcess extends Eloquent
{
    protected $collection = 'PointOfCareSProcess';

    public function GetObjectAttribute(){

        return [
          'ProcessId' => $this->_id,
          'ProcessName' => $this->ProcessName
        ];
    }

}
