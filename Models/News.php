<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class News extends Eloquent
{

    protected $collection = 'News';


    public function getObjectAttribute(){

        return [
            'NewsId' => $this->_id,
            'NewsTitle' => $this->NewsTitle
        ];
    }

}
