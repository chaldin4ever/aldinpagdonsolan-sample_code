<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ResourceFile extends Eloquent
{
    protected $collection = 'ResourceFile';

    public static function GetPhoto($id){
        $res = \App\Models\ResourceFile::find($id);
        if (empty($res)) return null;
        $imageBody = $res->fileStorage->getData();
        return base64_encode($imageBody);
    }

}
