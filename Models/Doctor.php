<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Doctor extends Eloquent
{
    protected $collection = 'Doctor';
    //

    public function getObjectAttribute(){

        return [
            'DoctorId' => $this->_id,
            'FirstName' => $this->FirstName,
            'LastName' => $this->LastName,
            'Phone' => $this->Phone
        ];
    }

    public function getFullNameAttribute(){

        return $this->FirstName.' '.$this->LastName;
    }
}
