<?php

namespace App\Models;

use App\Utils\Toolkit;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use \App\Domains\Assessment;

class Resident extends Eloquent
{
    protected $collection = 'Resident';
    //

    public function getObjectAttribute(){

        $room = $this->Room;
        if(!empty($this->CurrentRoom)){
            $room = array_get($this->CurrentRoom, "RoomName");
        }
        return [
            'ResidentId' => $this->_id,
            'ResidentName' => $this->FullName,
            'Room' => $room,
            'URN' => $this->URN
        ];
    }

    /**
     * discharge must be by an assessment / form
     */
    public function Discharge(Assessment $assessment){
        $this->Status = 4;
        $this->save();

        $note = "Resident discharged by " . array_get($assessment->CreatedBy, "FullName");
        \App\Jobs\CreateProgressNoteJob::dispatch($assessment, $note);

    }

    public function GetPhotoAttribute(){

        $resourcefile = ResourceFile::where('Resident.ResidentId', $this->_id)->first();

        if(empty($resourcefile)){
            if(empty($this->ResourceFileId)) return null;
            $res = \App\Models\ResourceFile::find($this->ResourceFileId);
            if (empty($res)) return null;
            $imageBody = $res->fileStorage->getData();
            return base64_encode($imageBody);
        }else{

            return $resourcefile->AWSFile;

        }


    }

    public function GetLastActivityAttribute(){

        $assessment = Assessment::where('Resident.ResidentId', $this->_id)
                        ->orderBy('updated_at', 'desc')->first();

        $document = Documents::where('Resident.ResidentId', $this->_id)
            ->orderBy('updated_at', 'desc')->first();

        if(!empty($assessment) && !empty($document)){

            if($document->updated_at > $assessment->updated_at){
                return [
                    'category' => 'document',
                    'datetime' => Toolkit::DateTimeToStatusActivityDate($document->updated_at)
                ];
            }else{
                return [
                    'category' => array_get($assessment->Form, 'Category'),
                    'datetime' => Toolkit::DateTimeToStatusActivityDate($assessment->updated_at)
                ];
            }

        }elseif(!empty($assessment)){
            return [
                'category' => array_get($assessment->Form, 'Category'),
                'datetime' => Toolkit::DateTimeToStatusActivityDate($assessment->updated_at)
            ];
        }elseif(!empty($document)){

            return [
                'category' => 'document',
                'datetime' => Toolkit::DateTimeToStatusActivityDate($document->updated_at)
            ];
        }
    }

    public function getRoomNameAttribute(){

        $room = Rooms::find($this->Room);

        return $room->RoomName;
    }

    public function getFacilityNameAttribute(){

        return array_get($this->Facility, 'facilityName');
    }

    public function getProviderNameAttribute(){

        $facilityId = array_get($this->Facility, 'facilityId');
        $facility = Facility::find($facilityId);

        if(!empty($facility))
            return array_get($facility->Provider, 'ProviderName');
    }

    public function getDoctorNameAttribute(){

        $name = array_get($this->Doctor, 'FirstName').' '.array_get($this->Doctor, 'LastName');

        return $name;
    }

    public function getPharmacyNameAttribute(){

        $name = array_get($this->Pharmacy, 'PharmacyName');

        return $name;
    }
}
