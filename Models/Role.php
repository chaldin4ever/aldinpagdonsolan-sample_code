<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Role extends Eloquent
{
    protected $collection = 'Role';
    //

    public function getObjectAttribute(){

        return ['roleId' => $this->_id, 'roleName' => $this->roleName];
    }
}
