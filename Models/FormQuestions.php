<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FormQuestions extends Eloquent
{
    protected $collection = 'AssessmentFormQuestion';

}
