<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Domains\Assessment;
use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;
use DB;

class BusinessLogic extends Eloquent
{
    protected $collection = 'BusinessLogic';

    
    /**
     * check assessment.data to see if a rule pass 
     */
    public function IsTrue(Assessment $assessment, $input){
        $field = array_get($input, 'field');
        $use_raw_data = array_get($input, 'use_raw_data');
        $operation = array_get($input, 'operation');
        $values = array_get($input, 'op_values');

        $ret = false;
        if($field == 'FormState'){
            if(!empty($values) && is_array($values)){
                $ret = in_array($assessment->FormState, $values);
            }
        } else {
            if($use_raw_data){
                $val = array_get($assessment->data, $field);
            } else {
                $val = $assessment->GetValue($field);
            }
            // TODO: need to add exception handling here. just in case user adds incorrect logic
            if ($operation == "") {
                $ret = true;
            } else if ($operation == "in"){
                if (!empty($values) && is_array($values)) {
                    $ret = in_array($val, $values);
                }
            } 
        }
        return $ret; 
    }

    public function ApplyLogic(Assessment $assessment){
        $data = $this->json;
        $inputs = array_get($data, "inputs");
        $output = array_get($data, "output");
        $pinboard = array_get($data, "pinboard");
        $references = array_get($data, 'references');

        $output_action = strtolower(array_get($output, 'action'));
        $keys = array_get($output, 'keys');
        $target_class = array_get($output, 'target_class');
        $target_form = array_get($output, 'target_form');
        $copy_fields = array_get($output, 'copy_fields');

        $note = array_get($pinboard, 'note');
        $due_days = array_get($pinboard, 'due_days');
        $pinboard_action = strtolower(array_get($pinboard, 'action'));
        $user_roles = array_get($pinboard, 'user_roles');
        $item_type = array_get($pinboard, 'item_type');
//         dd($inputs);
        if(empty($inputs)) {
//            dd('failed');
            return false;
        }
        $pass = true;
        foreach($inputs as $input){
            if(!$this->IsTrue($assessment, $input)){
                $pass = false;
                break;
            }
        }
        if(!$pass)
            return;

        $data = $assessment->data;
//        dd($data);
        $response =  $this->prepareResponse($assessment, $data, $copy_fields);
        // dd($response);
        // handle PinboardItem
        if(in_array($pinboard_action, ["create", "insert", "add", "new"])){
            if($due_days < 0)
                $action_date = Carbon::createFromDate(1899, 12, 31);
            else
                $action_date = \Carbon\Carbon::now()->startOfDay()->addDays($due_days);
            $note = $this->prepareNote($assessment, $response, $note);
            \App\Jobs\CreatePinboardItemJob::dispatch($assessment, $action_date, $user_roles, $note, $target_form, $item_type);
        }  else if(in_array($pinboard_action, ["remove", "delete", "del", "archive"])){
            $items = \App\Models\PinboardItem::where('Assessment.AssessmentId', $assessment->_id)
                ->where('state', 'new')->get();
            foreach($items as $item){
                $item->state = 'archived';
                $item->save();
            }
        }

        // handle Class
//         dd($output);
        if(!empty($target_class) && !empty($output_action)){
            $className = "\\App\\Models\\".$target_class;

            if(strtolower($target_class) == "resident"){
//                dd($className);
                $this->UpdateResidentDetails($assessment, $output);
            } else {

                if(in_array($output_action, ["create", "insert", "add", "new"])){
                    $result = new $className;
                    $result->Resident = $assessment->Resident;
                    $result->Facility = $assessment->Facility;
                    $result->Form = $assessment->Form;
                    $result->Assessment = $assessment->Object;
                    $result->state = 'new';
                    $result->data = $response;
                    $result->references = $references;
                    $result->save();
                } else if (in_array($output_action, ["upsert"])) {
                    // dd($output_action);
                    // dd($keys);
                    if(!empty($keys) && is_array($keys))
                    {
                        foreach($keys as $key){
                            if(strtolower($key) == 'resident'){
                                if(empty($items))
                                    $items = $className::where('_id', array_get($assessment->Resident, 'ResidentId'));
                                else
                                    $items = $items->where('_id', array_get($assessment->Resident, 'ResidentId'));
                            } else if(strtolower($key) == 'assessment'){
                                if (empty($items))
                                    $items = $className::where('Assessment.AssessmentId', $assessment->id);
                                else
                                    $items = $items->where('Assessment.AssessmentId', $assessment->id);
                            } else {

                                foreach ($copy_fields as $field) {
                                    $from_field = array_get($field, 'from_field');
                                    $to_field = array_get($field, 'to_field');
                                    $use_raw_data = array_get($field, 'use_raw_data');

                                    if($to_field != $key) continue;

                                    if ($use_raw_data) {
                                        $val = array_get($data, $from_field);
                                    } else {
                                        $val = $assessment->GetValue($from_field);
                                    }
                                    if (empty($items))
                                        $items = $className::where("data.$key", $val);
                                    else
                                        $items = $items->where("data.$key", $val);
                                }
                            }
                        }
                        $rows = $items->get();
                        if($className == "Resident"){

                        } else {
                            if(sizeof($rows) == 0){
                                $result = new $className;
                                $result->Resident = $assessment->Resident;
                                $result->Facility = $assessment->Facility;
                                $result->Form = $assessment->Form;
                                $result->Assessment = $assessment->Object;
                                $result->state = 'new';
                                $result->data = $response;
                                $result->references = $references;
                                $result->save();
                            }
                            else {
                                foreach($rows as $result){
                                    $old_response = $result->data;
                                    foreach($response as $k => $v){
                                        $old_response[$k] = $v;
                                    }
                                    $result->state = 'updated';
                                    $result->data = $old_response;
                                    $result->references = $references;
                                    $result->save();
                                }
                            }
                        }

                    }
                } else if (in_array($output_action, ["remove", "delete", "del", "archive"])) {
                    $items = $className::where('Assessment.AssessmentId', $assessment->_id)
                        ->where('state', 'new')->get();
                    foreach ($items as $item) {
                        $item->state = 'archived';
                        $item->save();
                    }
                }

            }
        }
    }

    private function UpdateResidentDetails($assessment, $output){
        $data = $assessment->data;
        $userid = array_get($assessment->CreatedBy, 'UserId');
        $resident_id = array_get($assessment->Resident, 'ResidentId');
        $db = \App\Utils\Toolkit::GetDatabase($userid);
        $resident = DB::connection($db)->collection("Resident")
            ->find($resident_id);
//        dd($resident);
        if(empty($resident)) return;

        $copy_fields = array_get($output, 'copy_fields');
        foreach($copy_fields as $field){
            $from_field = array_get($field, 'from_field');
            $to_field = array_get($field, 'to_field');
            $resident[$to_field] = array_get($data, $from_field);
        }
//        dd($resident);
        DB::connection($db)->collection("Resident")
            ->where('_id', $resident_id)
            ->update($resident, ['upsert' => true]);
    }

    private function prepareNote ($assessment, $response, $note){
        $response['resident_name'] = array_get($assessment->Resident, 'ResidentName');
        $response['form_name'] = array_get($assessment->Form, 'FormName');
        // dd($note);
        foreach($response as $k => $v){
            $fld = "$".$k;
            $note = str_replace($fld, $v, $note);
        }
        return $note;
    }

    
    private function prepareResponse($assessment, $data, $copy_fields){
        $response = [];
        foreach ($copy_fields as $field) {
            $from_field = array_get($field, 'from_field');
            $to_field = array_get($field, 'to_field');
            $use_raw_data = array_get($field, 'use_raw_data');

            if ($use_raw_data) {
                $val = array_get($data, $from_field);
            } else {
                $val = $assessment->GetValue($from_field);
            }
            $response[$to_field] = $val;
        }
        return $response;
    }


    // /**
    //  * check of all business rules pass
    //  */
    // public static function AllLogicsTrue($key, Assessment $assessment){
    //     $logics = BusinessLogic::where("json.key", $key)
    //         ->where('json.form_code', array_get($assessment->Form, 'FormID'))
    //         ->get();
    //     $ret = true;
    //     foreach($logics as $logic){
    //         if(!$logic->IsTrue($assessment)){
    //             $ret = false;
    //             break;
    //         }
    //     }
    //     return $ret;
    // }


    // /**
    //  * assign a value from assessment.data into $movement
    //  * used in handling resident movement for now
    //  */
    // public static function Assign($key, Assessment $assessment, $movement)
    // {
    //     $data = $assessment->data;
    //     $logics = \App\Models\BusinessLogic::where('json.key', $key)
    //         ->get();
    //     foreach ($logics as $logic) {
    //         $data = $logic->json;
    //         $field = array_get($data, 'field');
    //         $use_user_input = array_get($data, 'use_user_input');
    //         $operation = array_get($data, 'operation');
    //         $values = array_get($data, 'op_values');
    //         $target_form = array_get($data, 'target_form');
    //         $target_field = array_get($data, 'target_field');
    //         $data_type = array_get($data, 'data_type');

    //         if ($use_user_input) {
    //             $val = array_get($data, $field);
    //             if ($data_type == "date") {
    //                 $val = Carbon::parse($val);
    //                 $val = new UTCDateTime($val->timestamp * 1000);
    //             }
    //         } else {
    //             $val = $assessment->GetValue($field);
    //         }
    //         if ($operation == "assign") {
    //             $movement[$values] = $val;
    //         }
    //     }
    //     return $movement;
    // }



}
