<?php

namespace App\Models;

use App\Domains\User;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserInbox extends Eloquent
{
    protected $collection = 'UserInbox';

    public function getObjectAttribute(){

        return [
            'InboxId' => $this->_id,
            'UserId' => array_get($this->FromUser, 'UserId'),
            'FullName' => array_get($this->FromUser, 'FullName'),
            'Subject' => $this->Subject
        ];
    }

    public function getFromUserEmailAttribute(){
        $userid = array_get($this->FromUser, 'UserId');
        $user = \App\User::find($userid);

        if(!empty($user)) return $user->email; else return '---';
    }

    public function getTotalReplyAttribute(){

//        $parent = array_get($this->Parent, 'InboxId');
        $inboxReply = UserInboxReply::where('Inbox.InboxId', $this->_id)->get()->count();

        return $inboxReply + 1;
    }

}