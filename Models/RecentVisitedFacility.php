<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RecentVisitedFacility extends Eloquent
{
    protected $collection = 'RecentVisitedFacility';
    //
}
