<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FormPicklistMap extends Eloquent
{
    protected $collection = 'FormPicklistMap';

}
