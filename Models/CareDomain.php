<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CareDomain extends Eloquent
{
    protected $collection = 'CareDomain';

    public function getObjectAttribute(){

        return [
            'caredomainId' => $this->_id,
            'CareDomain' => $this->CareDomain
        ];
    }

}
