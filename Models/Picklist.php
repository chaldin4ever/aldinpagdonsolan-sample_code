<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Picklist extends Eloquent
{
    protected $collection = 'Picklist';

    public function getObjectAttribute(){

        return [
            'picklistId' => $this->_id,
            'picklistName' => $this->PicklistName
        ];
    }

    public function getPicklistsAttribute(){

        if(!empty($this->Lists)){

            $list = [];
            foreach($this->Lists as $k=>$data){

                $code = array_get($data, 'code');
                $text = array_get($data, 'text');
                $list[] = $code.' - '.$text;

            }

            return $list;
        }
    }

}
