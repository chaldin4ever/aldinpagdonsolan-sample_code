<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CarePlan extends Eloquent
{
    protected $collection = 'PointOfCareCarePlan';

}
