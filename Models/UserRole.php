<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserRole extends Eloquent
{
    protected $collection = 'Role';
    //

    public function getObjectAttribute(){

        return [
            'RoleId' => $this->_id,
            'RoleName' =>$this->roleName
        ];
    }
}
