<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pharmacy extends Eloquent
{
    protected $collection = 'Pharmacy';
    //

    public function getObjectAttribute(){

        return [
            'PharmacyId' => $this->_id,
            'PharmacyName' => $this->PharmacyName
        ];
    }
}
