<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SProcessSteps extends Eloquent
{
    protected $collection = 'PointOfCareSProcessSteps';

}
