<?php

namespace App\Models;

use App\Domains\AssessmentForm;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Config extends Eloquent
{
    protected $collection = 'Config';

    public function getObjectAttribute(){

        return [
            'configId' => $this->_id,
            'ConfigName' => $this->ConfigName
        ];
    }

}
