<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RecentVisitedResident extends Eloquent
{
    protected $collection = 'RecentVisitedResident';
    //
    public function GetPhotoAttribute(){

        $resident = Resident::find(array_get($this->Resident, 'ResidentId'));
        if(empty($resident->ResourceFileId)) return null;
        $res = \App\Models\ResourceFile::find($resident->ResourceFileId);
        if (empty($res)) return null;
        $imageBody = $res->fileStorage->getData();
        return base64_encode($imageBody);
    }
}
