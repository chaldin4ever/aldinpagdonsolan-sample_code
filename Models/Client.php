<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Client extends Eloquent
{
    protected $collection = 'Client';

    public function getObjectAttribute(){

        return [
            'clientId' => $this->_id,
            'clientName' => $this->ClientName,
            'nomenclature' => $this->Nomenclature,
            'ExternalIdentifier' => $this->ExternalIdentifier
        ];
    }

}
