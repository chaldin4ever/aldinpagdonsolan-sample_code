<?php

namespace App\Http\Controllers;

use App\Domains\User;
use App\Models\FolderStructure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FolderStructureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $folders = FolderStructure::all();

        $dataArr = $this->prepareFolderData($folders);
        $tree = $this->createParentChild($dataArr);

        return view('folderstructure.index', compact('folders', 'tree'));
    }

    public function prepareFolderData($folders){

        $dataArr = [];
        if(!empty($folders)){
            foreach($folders as $folder){

                $parentId = array_get($folder->Parent, 'FolderId');

                $dataArr[$folder->_id] = [
                    'id' => $folder->_id,
                    'parentid' => $parentId,
                    'name' => $folder->FolderName,
                    'createdby' => array_get($folder->CreatedBy, 'FullName'),
                    'created_at' => $folder->created_at
                ];


            }

        }

        return $dataArr;
    }


    public function createTree(&$list, $parent){
        $tree = array();
        if(!empty($parent)){
            foreach ($parent as $k=>$l){
                if(isset($list[$l['id']])){
                    $l['children'] = $this->createTree($list, $list[$l['id']]);
                }
                $tree[] = $l;
            }
        }

        return $tree;
    }

    public function createParentChild($dataArr){

        $new = array();
        if(!empty($dataArr)){
            foreach ($dataArr as $a){
                $parentid = array_get($a, 'parentid');
                $new[$parentid][] = $a;
            }

            $treeArr = [];
            foreach ($dataArr as $k=>$v) {
                $treeArr[] = $this->createTree($new, array($dataArr[$k]));

            }
        }


        $tree =[];
        if(!empty($treeArr)){
            foreach($treeArr as $dataArr){
                foreach($dataArr as $k=>$data){
                    if(array_get($data, 'parentid' )== ''){
                        $tree[] = (object)$data;
                    }
                }
            }
        }

        return $tree;
    }


    public function store(Request $request){

        $user = User::find(Auth::user()->_id);

        if(!empty($request->_id))
            $folder = FolderStructure::find($request->_id);
        else
            $folder = new FolderStructure();

        $folderParent = FolderStructure::find($request->Parent);
        $folder->FolderName = $request->FolderName;
        $folder->Parent = !empty($folderParent) ? $folderParent->ParentFolder : ['FolderId'=> null, 'ParentName'=> 'root'];
        $folder->CreatedBy = $user->Object;
        $folder->save();

        return redirect('folders')->with('status', 'Successfully saved.');
    }

    public function delete($id){

        FolderStructure::destroy($id);

        FolderStructure::where('Parent.FolderId', $id)->delete();

        return redirect('folders')->with('status', 'Successfully deleted.');
    }

}
