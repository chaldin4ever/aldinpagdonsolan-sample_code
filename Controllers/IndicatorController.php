<?php

namespace App\Http\Controllers;

use App\Charts\POCChart;
use App\Domains\Assessment;
use App\Models\RecentVisitedResident;
use App\Models\Resident;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redis;

class IndicatorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $userid = Auth::user()->_id;
        $key = 'selected.facility.' . $userid;
        $facility_id = Redis::get($key);
        if(empty($facility_id))return redirect(url('facility/select'));

        $residents = $this->mostrecent();
        $facility = \App\Models\Facility::find($facility_id);

        \App\Utils\Toolkit::SetupUserSession($facility, $userid);

        $counts= $this->countAssessment();

//        dd($counts);

        return view('indicator.index', compact('residents', 'facility', 'counts'));
    }

    public function mostrecent(){

//        dd($facilityId);
        $residents = RecentVisitedResident::where('User.UserId', Auth::user()->_id)
//                ->groupBy('Resident')
            ->orderby('created_at', 'desc')
            ->get();

        $data = [];
        foreach($residents as $resident){
            $r = RecentVisitedResident::where('Resident.ResidentId', array_get($resident->Resident, 'ResidentId'))->first();
            $data[array_get($resident->Resident, 'ResidentId')] = $r;
        }

        $residents = collect($data)->take(5);// RecentVisitedResident::whereIn('_id', $grp)->orderby('created_at', 'desc')->get();

        return $residents;
    }

    public function countAssessment(){

        $lastMo_dt = new Carbon('first day of last month');
        $lastMo_et = new Carbon('last day of last month');

        $date = (string) Carbon::now()->format('M G');
        $dt = new Carbon('first day of '.$date);
        $et = new Carbon('last day of '.$date);

        $FormID = config('poc.RESIDENT_INCIDENT_FORM_ID');
        $title = 'Falls and fall related fractures';
        $fallCount = $this->getCountAssessment($lastMo_dt, $lastMo_et, $dt, $et, $FormID );

        $FormID = config('poc.WOUND_FORM_ID');
        $title = 'Pressure Injury';
        $injuryCount  = $this->getCountAssessment($lastMo_dt, $lastMo_et, $dt, $et, $FormID );

        $FormID = config('poc.WEIGHT_FORM_ID');
        $title = 'Unplanned Weight loss';
        $weightCount  = $this->getCountAssessment($lastMo_dt, $lastMo_et, $dt, $et, $FormID );

        $FormID = config('poc.PHYSICAL_RESTRAINT_FORM_ID');
        $title = 'Use of physical straint';
        $straintCount  = $this->getCountAssessment($lastMo_dt, $lastMo_et, $dt, $et, $FormID );


        $indicatorCount = [
           'Falls and fall related fractures'=> $fallCount,
            'Pressure Injury' => $injuryCount,
            'Unplanned Weight loss'=>$weightCount,
            'Use of physical straint'=>$straintCount,
            'Use of nine or more medicines'=> []
        ];

        return $indicatorCount;

    }

    public function getAssessment($dt, $et, $formID){

        $dates = [$dt->startOfDay(), $et->endOfDay()];
        $assessments = Assessment::where('Form.FormID', $formID);

        $assessments = $assessments->whereBetween("created_at", $dates);

        $assessments = $assessments->count();

        return $assessments;

    }

    public function getCountAssessment($lastMo_dt, $lastMo_et, $dt, $et, $formID ){

        $lastMonth = $this->getAssessment($lastMo_dt, $lastMo_et, $formID);
        $currentMonth = $this->getAssessment($dt, $et, $formID);

        $indicator = $currentMonth - $lastMonth;

        $status = '';
        if($indicator > 0)$status = 'up';
        elseif($indicator == 0) $status = 'zero';
        else{
            $status = 'down';
            $num = $indicator * -1;

            $indicator = $num;
        }

        $total = $indicator;

        $count = [
            'LastMonth' => $lastMonth,
            'CurrentMonth' => $currentMonth,
            'Total' => $total,
            'Status' => $status
        ];

        return $count;
    }



    public function fall(){


        $dates = array_get($this->dateFilter(), 'dates');
        $moyrArr =array_get($this->dateFilter(), 'moyrArr');
        $dt = array_get($this->dateFilter(), 'dt');
        $et = array_get($this->dateFilter(), 'et');

        $date_field = config('poc.RESIDENT_INCIDENT_DATE_FIELD');
        $date_field_utc = config('poc.RESIDENT_INCIDENT_DATE_FIELD_UTC');
        $code = config('poc.RESIDENT_INCIDENT_FALL_QUESTION_CODE');
        $fldcode = config('poc.RESIDENT_INCIDENT_FALL_FIELD');
        $fracture_code = config('poc.RESIDENT_INCIDENT_FRACTURE_QUESTION_CODE');
        $fracture_fldcode = config('poc.RESIDENT_INCIDENT_FRACTURE_FIELD');
        $assoc_factors = config('poc.RESIDENT_INCIDENT_ASSOC_FACTOR_FIELDS');

        $assessments = Assessment::where('Form.FormID', config('poc.RESIDENT_INCIDENT_FORM_ID'))
//            ->whereBetween('updated_at', $dates)
            ->whereBetween("data.$date_field_utc", $dates)
            ->get();

        $fracture_data = [];
        $fall_data = [];
        $assocfactor_data = [];


        foreach($assessments as $assm){
            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');
            $fall = array_get($assm->data, $code.'-'.$fldcode);
//            dd($moyr);

            if($fall == 'on'){
                $fall_data[$moyr][] = [
                    'Date' => array_get($assm->data, $date_field),
                    'Fall' => array_get($assm->data, $code.'-'.$fldcode),
                ];
            }

            $fracture = array_get($assm->data, $fracture_code.'-'.$fracture_fldcode);

            if($fracture == 'on'){
                $fracture_data[$moyr][] = [
                    'Date' => array_get($assm->data, $date_field),
                    'Fracture' => array_get($assm->data, $fracture_code.'-'.$fracture_fldcode),
                ];
            }

            if(!empty($assm->data)){
                foreach($assm->data as $k=>$v){
                    if(in_array($k, array_keys($assoc_factors))){
                        if($v == 'on'){

                            $assocfactor_data[$k][] = $v;
                        }
                    }
                }
            }

        }

        $labeldata = []; $falldataset = []; $fracturedataset = []; $falldata=[]; $fracturedata=[];
        foreach($moyrArr as $my=>$val){

          $labeldata[] = array_get($val, 'MonthYear');
          $totalfall = \App\Utils\Toolkit::smart_count(array_get($fall_data, $my));
          $falldataset[] = $totalfall;
          $totalfracture = \App\Utils\Toolkit::smart_count(array_get($fracture_data, $my));
          $fracturedataset[] = $totalfracture;

          $monthyear = array_get($val, 'MonthYear');
          $falldata[$my] = ['count'=>$totalfall, 'monthyear' => $monthyear];
          $fracturedata[$my] = ['count'=>$totalfracture, 'monthyear' => $monthyear];

        }

//        dd($falldataset);
        $label = $labeldata;
        $dataset = $falldataset;
        $options = array_get($this->Options(), 'option1');;

        $dataset2 = $fracturedataset;
        $options2 = array_get($this->Options(), 'option2');


        $fallchart = $this->chart('line', $label, $dataset, 'Total Number of Fall', $options,
            $dataset2, 'Number of fractures resulting from fall', $options2);


        $af_data = [];
        foreach($assoc_factors as $k=>$v){
            $total = \App\Utils\Toolkit::smart_count(array_get($assocfactor_data, $k));
            $af_data[] = $total;
            $af_label[] = $v;
            $associatedFactor[] = [
              'Title'=>$v,
              'Total'=>$total
            ];
        }

//        dd($af_data);
        $label = $af_label;
        $dataset = $af_data;
        $options = array_get($this->Options(), 'option3');;

        $factor = $this->chart('horizontalBar', $label, $dataset, 'Associated Factor', $options);

        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('indicator.fall_and_fracture',
            compact('fallchart', 'measurement', 'factor',
                'falldata', 'fracturedata', 'associatedFactor',
                'fromDate', 'toDate'));
    }


    public function injuries(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $dates = array_get($this->dateFilter(), 'dates');
        $moyrArr =array_get($this->dateFilter(), 'moyrArr');
        $dt = array_get($this->dateFilter(), 'dt');
        $et = array_get($this->dateFilter(), 'et');

        $date_field = config('poc.WOUND_FORM_DATE_FIELD');
        $date_field_utc = config('poc.WOUND_FORM_DATE_FIELD_UTC');
        $code = config('poc.WOUND_QUESTION_CODE');
        $fldcode = config('poc.WOUND_PRESSURE_FIELD_CODE');

        $assessments = Assessment::where('Form.FormID', config('poc.WOUND_FORM_ID'))
//            ->whereBetween('updated_at', $dates)
            ->whereBetween("data.$date_field_utc", $dates)
            ->get();

        $assoc_factors = config('poc.WOUND_ASSOC_FACTOR_FIELDS');
        $classification_code = config('poc.WOUND_CLASSIFICATION_CODE');
        $classification = config('poc.WOUND_CLASSIFICATION_FIELDS');
        $pressure_data = [];
        $assocfactor_data = [];
        $classification_data = [];
        $class_data = [];
        foreach($assessments as $assm){
            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');
            $pressure = array_get($assm->data, $code );

            $field_data_value = array_get($assm->data, $code);
//            dd($field_data_value);
            if($pressure == $field_data_value){
                $pressure_data[$moyr][] = [
                    'Date' => array_get($assm->data, $date_field),
                    'Pressure' => array_get($assm->data, $code.'-'.$fldcode),
                ];
            }


            if(!empty($assm->data)){
                foreach($assm->data as $k=>$v){
                    if(in_array($k, array_keys($assoc_factors))){
                        if($v == 'on'){
                            $assocfactor_data[$k][] = $v;
                        }
                    }

                    if($k == $classification_code){
                            $class_data[$v][] = $v;
                            $classification_data[$moyr][$v][] = $v;
                    }
                }
            }

        }


        $labeldata = []; $pressuredataset = [];
        foreach($moyrArr as $my=>$val){
//            dd($pressure_data);
            $labeldata[] = array_get($val, 'MonthYear');
            $total = \App\Utils\Toolkit::smart_count(array_get($pressure_data, $my));
            $pressuredataset[] = $total;

            $monthyear = array_get($val, 'MonthYear');
            $pressuredata[$my] = ['count'=>$total, 'monthyear' => $monthyear, 'key'=>$my];

        }

        $label = $labeldata;
        $dataset = $pressuredataset;
        $options = array_get($this->Options(), 'option1');

        $pressurechart = $this->chart('line', $label, $dataset, 'Total Number of Pressure Injuries', $options);

        $cf_data = [];
        foreach($classification as $k=>$v){
            $total = \App\Utils\Toolkit::smart_count(array_get($class_data, $k));
            $cf_data[] = $total;
            $cf_label[] = $v;
            $classData[] = [
                'Title'=>$v,
                'Total'=>$total
            ];
        }

        $label = $cf_label;
        $dataset = $cf_data;
        $options = array_get($this->Options(), 'option2');

        $measurements = $this->chart('bar', $label, $dataset, 'Measurements', $options);


        $af_data = [];
        foreach($assoc_factors as $k=>$v){
            $total = \App\Utils\Toolkit::smart_count(array_get($assocfactor_data, $k));
            $af_data[] = $total;
            $af_label[] = $v;
            $associatedFactor[] = [
                'Title'=>$v,
                'Total'=>$total
            ];
        }

//        dd($af_data);
        $label = $af_label;
        $dataset = $af_data;
        $options = array_get($this->Options(), 'option3');

        $factor = $this->chart('horizontalBar', $label, $dataset, 'Associated Factor', $options);


        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('indicator.injuries',
            compact('pressurechart', 'measurements', 'factor',
                'pressuredata',  'associatedFactor', 'classification_data', 'classData',
                'fromDate', 'toDate'));
    }


    public function weight()
    {

        $dates = array_get($this->dateFilter(), 'dates');
        $moyrArr = array_get($this->dateFilter(), 'moyrArr');
        $dt = array_get($this->dateFilter(), 'dt');
        $et = array_get($this->dateFilter(), 'et');

        $weightFormID = config('poc.WEIGHT_FORM_ID'); //1648
        $date_field = config('poc.WEIGHT_FORM_DATE_FIELD');
        $date_field_utc = config('poc.WEIGHT_FORM_DATE_FIELD_UTC');
        $field = config('poc.WEIGHT_FORM_FIELD');

        $assessments = Assessment::where('Form.FormID', $weightFormID)
//            ->whereBetween('updated_at', $dates)
            ->whereBetween("data.$date_field_utc", $dates)
            ->get();

        $weight_data = []; $weight_significant = []; $weight_consecutive = [];
        foreach($assessments as $assm){

            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');
            $weight_value = array_get($assm->data, $field );

            if(!empty($weight_value)){

                $weightDate = new Carbon(array_get($assm->data, $date_field));
                $last3mo_weight = $this->getLast3MoWeight($weightFormID, $field, $date_field_utc, $weightDate->subMonths(3));

                $significant  = (($weight_value - ($last3mo_weight - 1)) >= 3) ? $weight_value : 0;
//                dd($significant);

                if($significant > 0){
                    $weight_significant[$moyr][] = [
                        'Date' => array_get($assm->data, $date_field),
                        'Weight' => $significant,
                    ];
                }

                $weightDate = array_get($assm->data, $date_field_utc);
                $last_weight = $this->getLastWeight($weightFormID, $field, $date_field_utc, $weightDate);
                $consecutive  = (($weight_value - $last_weight) >= 3) ? $weight_value : 0;

                if($consecutive > 0){
                    $weight_consecutive[$moyr][] = [
                        'Date' => array_get($assm->data, $date_field),
                        'Weight' => $consecutive,
                    ];
                }

            }

        }

        $label = []; $dataset1 = [];  $dataset2 = []; $weightdata = [];
        $weight_significant_data =[]; $weight_consecutive_data = [];

        foreach($moyrArr as $my=>$val){
            $monthyear = array_get($val, 'MonthYear');

            $label[] = array_get($val, 'MonthYear');
            $total1 = \App\Utils\Toolkit::smart_count(array_get($weight_significant, $my));
            $dataset1[] = $total1;
            $weight_significant_data[$my] = ['count'=>$total1, 'monthyear' => $monthyear, 'key'=>$my];

            $total2 = \App\Utils\Toolkit::smart_count(array_get($weight_consecutive, $my));
            $dataset2[] = $total2;
            $weight_consecutive_data[$my] = ['count'=>$total2, 'monthyear' => $monthyear, 'key'=>$my];

            $weight_data[$my] = [
                'monthyear' => $monthyear,
                'Significant' => ['count'=>$total1, 'key'=>$my],
                'Consecutive' => ['count'=>$total2,  'key'=>$my]
            ];

        }

        $options = array_get($this->Options(), 'option1');
        $title1 = 'Significant Unplanned Weight loss';

        $options2 = array_get($this->Options(), 'option2');
        $title2 = 'Consecutive Unplanned Weight loss';

        $weightchart = $this->chart('line', $label, $dataset1, $title1, $options,
                        $dataset2, $title2, $options2
        );

        $measurement = $this->chart('bar', $label, $dataset1, $title1, $options,
            $dataset2, $title2, $options2
        );

        $weightPathwayFormID = config('poc.WEIGHT_PATHWAY_FORM_ID'); //1329
        $assoc_factors = config('poc.WEIGHT_PATHWAY_FORM_FACTORS');

        $weightpath = Assessment::where('Form.FormID', $weightPathwayFormID)
            ->whereBetween('updated_at', $dates)
            ->get();

        $assocfactor_data = [];
        foreach($weightpath as $assm){
            if(!empty($assm->data)){
                foreach($assm->data as $k=>$v){
                    if(in_array($k, array_keys($assoc_factors))){
                        if($v == 'on'){
                            $assocfactor_data[$k][] = $v;
                        }
                    }
                }
            }
        }

        $af_data = [];
        foreach($assoc_factors as $k=>$v){
            $total = \App\Utils\Toolkit::smart_count(array_get($assocfactor_data, $k));
            $af_data[] = $total;
            $af_label[] = $v;
            $associatedFactor[] = [
                'Title'=>$v,
                'Total'=>$total
            ];
        }

//        dd($af_data);
        $label = $af_label;
        $dataset = $af_data;
        $options = array_get($this->Options(), 'option3');

        $factor = $this->chart('horizontalBar', $label, $dataset, 'Associated Factor', $options);

        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('indicator.weight',
            compact('weightchart',  'weight_data', 'associatedFactor','factor', 'measurement',
                'fromDate', 'toDate'));

    }

    public function physical_restraint()
    {

        $dates = array_get($this->dateFilter(), 'dates');
        $moyrArr = array_get($this->dateFilter(), 'moyrArr');
        $dt = array_get($this->dateFilter(), 'dt');
        $et = array_get($this->dateFilter(), 'et');

        $FormID = config('poc.PHYSICAL_RESTRAINT_FORM_ID'); //1648

        $assessments = Assessment::where('Form.FormID', $FormID)
            ->whereBetween('updated_at', $dates)
            ->get();


        $intention_list = config('poc.PHYSICAL_RESTRAINT_INTENTION');
        $devices_list = config('poc.PHYSICAL_RESTRAINT_DEVICES');
        $assoc_factors = config('poc.PHYSICAL_RESTRAINT_FORM_FACTORS');
        $physical_restraint = [];
        $assocfactor_data = [];
        $intention_data = [];
        $intention_device_data = [];
        foreach($assessments as $assm){
            $date = new Carbon($assm->updated_at);
            $moyr = $date->format('Ym');

            if(!empty($assm->data)){
                $physical_restraint[$moyr][] = [
                    'Date' => $date,
                    'Restraint' => 1
                ];

                foreach($assm->data as $k=>$v) {
                    if (in_array($k, array_keys($assoc_factors))) {
                        if ($v == 'on') {
                                $assocfactor_data[$k][] = $v;
                        }
                    }

                    if (in_array($v, array_keys($intention_list)) || in_array($v, array_keys($devices_list))) {
                            $intention_device_data[$k][$v][] = 1;
                    }

                }
            }

        }

//        dd($intention_device_data);
        $measurement = []; $intent_total = 0; $devices_total = 0;
        foreach($intention_device_data as $key=>$vArr){

//            dd($vArr);

            foreach($vArr as $k=>$v){
//                dd($v);
                if($key == config('poc.PHYSICAL_RESTRAINT_INTENTION_FIELD')){
                    $day = array_get(config('poc.PHYSICAL_RESTRAINT_INTENTION'), $k);
                    $measurement[$day]['intent'] = \App\Utils\Toolkit::smart_count($v);
                    $intent_total = $intent_total + \App\Utils\Toolkit::smart_count($v);
                }

                if($key == config('poc.PHYSICAL_RESTRAINT_DEVICES_FIELD')){
                    $day = array_get(config('poc.PHYSICAL_RESTRAINT_DEVICES'), $k);
                    $measurement[$day]['devices'] = \App\Utils\Toolkit::smart_count($v);
                    $devices_total = $devices_total + \App\Utils\Toolkit::smart_count($v);
                }
            }


        }

        $measurement_data = [];
        foreach(config('poc.RESTRAINT_DAYS') as $data){
            $measurement_data[$data] = array_get($measurement, $data);
            if($data == 'Total'){
                $measurement_data[$data]['intent'] = $intent_total;
                $measurement_data[$data]['devices'] = $devices_total;
            }
        }

        $label = []; $dataset=[]; $dataset2=[];
        foreach($measurement_data as $k=>$v){
            $label[] = $k;
            $dataset[] = array_get($v, 'intent');
            $dataset2[] = array_get($v, 'devices');
        }
        $options1 = array_get($this->Options(), 'option1');
        $options2 = array_get($this->Options(), 'option2');
        $measurement_chart = $this->chart('bar', $label, $dataset, 'Intent to Restrain', $options1,
            $dataset2, 'Physical Restraint Devices', $options2
            );

//        dd($measurement_data);

        $label = []; $dataset = [];
        foreach($moyrArr as $my=>$val){

            $label[] = array_get($val, 'MonthYear');
            $total_restraint = \App\Utils\Toolkit::smart_count(array_get($physical_restraint, $my));
            $dataset[] = $total_restraint;

            $monthyear = array_get($val, 'MonthYear');
            $restraintdata[$my] = ['count'=>$total_restraint, 'monthyear' => $monthyear];

        }

        $options = array_get($this->Options(), 'option1');;
        $title = 'Total of Physical Restraint';
        $restraintchart = $this->chart('line', $label, $dataset, $title, $options);


        $af_data = [];
        foreach($assoc_factors as $k=>$v){
            $total = \App\Utils\Toolkit::smart_count(array_get($assocfactor_data, $k));
            $af_data[] = $total;
            $af_label[] = $v;
            $associatedFactor[] = [
                'Title'=>$v,
                'Total'=>$total
            ];
        }

        $label = $af_label;
        $dataset = $af_data;
        $options = array_get($this->Options(), 'option3');;
        $factor = $this->chart('horizontalBar', $label, $dataset, 'Associated Factor', $options);

        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('indicator.physical_restraint',
            compact('restraintchart','factor',
                'restraintdata', 'associatedFactor', 'measurement_chart', 'measurement_data',
                'fromDate', 'toDate'));
    }

    public function getLastWeight( $weightFormID, $field, $date_field_utc, $weightDate){

        $assessment = Assessment::where('Form.FormID', $weightFormID)
            ->where("data.$date_field_utc", '<', $weightDate)
            ->orderby("data.$date_field_utc", 'desc')
            ->first();

        if(!empty($assessment))
            return array_get($assessment->data, $field);

    }

    public function getLast3MoWeight( $weightFormID, $field, $date_field_utc, $weightDate){

        $assessment = Assessment::where('Form.FormID', $weightFormID)
            ->where("data.$date_field_utc", '<=', $weightDate)
            ->orderby("data.$date_field_utc", 'desc')
            ->first();

        if(!empty($assessment))
            return array_get($assessment->data, $field);

    }


    public function chart($type, $label, $dataset, $title, $options, $dataset2=null, $title2=null, $options2=null){

            $chart = new POCChart();
            $chart->dataset($title, $type, $dataset)
                ->options($options);
                //->color('rgb(0, 48, 86, .5)');
//                ->background('rgb(0, 48, 86, .5)');
            $chart->labels($label);

            if($dataset2){
                $chart->dataset($title2, $type, $dataset2)->options($options2);
            }

            return $chart;
//        }

    }


    public function dateFilter(){
        $months = 12;
        $carbonDate = Carbon::now();

        $date = $carbonDate->endOfDay();

        $fromDate = Toolkit::ConvertStringToCarbon(Input::get('FromDate'));
        $toDate = Toolkit::ConvertStringToCarbon(Input::get('ToDate'));


        if($fromDate && $toDate){
            $start = $fromDate->startOfDay();
            $end = $toDate->endOfDay();
//            dd([$fromDate, $toDate]);
            $dt = Toolkit::CarbonToUTCDateTime($fromDate->startOfDay());
            $et = Toolkit::CarbonToUTCDateTime($toDate->endOfDay());
            $months = $fromDate->diffInMonths($toDate);
            $moyrArr = Toolkit::GetDataArrayForChart($toDate, $months);
        }else{

            $et = Toolkit::CarbonToUTCDateTime($date);
//            dd([ $date]);
            $moyrArr = Toolkit::GetDataArrayForChart($date, $months);
            $oneYearAgo = $date->subMonths($months)->startOfDay() ;
            $dt = Toolkit::CarbonToUTCDateTime($oneYearAgo);

            $start = $oneYearAgo;
            $end = Carbon::now()->endOfDay();
        }

        $dates = [$start, $end];

        return ['dates' => $dates, 'moyrArr'=>$moyrArr, 'dt'=>$dt, 'et'=>$et];
    }

    public function Options(){

        $option1 = [
            'borderColor' => 'rgb(0, 48, 86, .5)',
            'backgroundColor' => 'rgb(0, 48, 86, .2)',
            'borderWidth' => 1,
            'hoverBackgroundColor' => 'rgb(0, 48, 86, .4)',
            'hoverBorderColor' => 'rgb(0, 48, 86, .8)',
            'hoverBorderWidth' => 1
        ];

        $option2 = [
            'borderColor' => 'rgba(255, 99, 132, 0.2)',
            'backgroundColor' => 'rgba(255, 99, 132, 0.2)',
            'hoverBackgroundColor' => 'rgba(255, 99, 132, 0.5)',
            'hoverBorderColor' => 'rgba(255, 99, 132, 0.5)',
            'borderWidth' => 1,
            'hoverBorderWidth' => 1
        ];

        $option3 =[
            'borderColor' => 'rgb(0, 48, 86, .5)',
            'backgroundColor' => 'rgb(0, 48, 86, .2)',
            'borderWidth' => 1,
            'hoverBackgroundColor' => 'rgb(0, 48, 86, .3)',
            'hoverBorderColor' => 'rgb(0, 48, 86, .8)',
            'hoverBorderWidth' => 1
        ];

        return [
          'option1' => $option1, 'option2' => $option2, 'option3' => $option3,
        ];
    }


}
