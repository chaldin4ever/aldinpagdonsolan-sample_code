<?php

namespace App\Http\Controllers;

use App\Models\Forms;
use Illuminate\Http\Request;
use Redis;

class FormsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $forms = Forms::where('Active', 1)->get();

        return view('forms.index', compact('forms'));

    }

    public function add(Request $request){

        if(isset($request->formId)){

            $form = Forms::find($request->formId);
            $msg = 'Form was successfully updated.';

        }else{
            $form = new Forms();
            $msg = 'Form was successfully added.';
        }

        $form->FormName = $request->FormName;
        $form->FormCode = $request->FormCode;
        $form->FormParent = $request->FormParent;
        $form->Category = $request->Category;
        $form->Language = $request->Language;
        $form->Role = $request->Role;
        $form->Active = 1;
        $form->Archive = 0;

        $form->save();

        return redirect('form')->with('status', $msg);

    }
}