<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Models\Config;
use Illuminate\Http\Request;
use Auth;
use App\Models\Resident;

class FundingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $user = Auth::user();

        // find number of resident has active ACFI

        $config = Config::where('ConfigID', "ACTIVE_ACFI")->get()->first();
        $resident_count = 0;
        $messageBag = [];
        if(empty($config)){
            $messageBag['config'] = "Form to identify Active ACFI must be configured first";
        } else {
            $form_id = $config->FormID;
            $rows = Assessment::distinct('Resident.ResidentId')->where("Form.FormID", $form_id)->get();
            $resident_count = sizeof($rows);
        }


        return view('funding.index', [
            'user' => $user,
            'resident_count' => $resident_count,
        ]);
    }

    public function residents(){
        $config = Config::where('ConfigID', "ACTIVE_ACFI")->get()->first();
        $resident_count = 0;
        $messageBag = [];
        $rows = [];
        if(empty($config)){
            $messageBag['config'] = "Form to identify Active ACFI must be configured first";
        } else {
            $form_id = $config->FormID;
            $rows = Assessment::where("Form.FormID", $form_id)->distinct('Resident.ResidentId')->get('Resident.ResidentId')->toArray();
//            dd($rows);
            $data = [];
            foreach($rows as $row){
//                dd($row[0]);
                $resident = Resident::find($row[0]);
                $data[] = $resident;
            }
            $rows = $data;
//            dd($rows);
        }

        return view('funding.residents',[
            'residents' => $rows,
        ]);
    }
}
