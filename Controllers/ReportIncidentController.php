<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\User;
use App\Utils\Reports;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
//use Debugbar;

class ReportIncidentController extends Controller
{

    public $facility_id;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(){

        $facility = Toolkit::getFacility();
        $facilityId = $facility->_id;

        $dateFilter = Reports::dateFilter();

        $dates = array_get($dateFilter, 'dates');
        $moyrArr =array_get($dateFilter, 'moyrArr');
        $dt = array_get($dateFilter, 'dt');
        $et = array_get($dateFilter, 'et');

        $date_field = config('poc.RESIDENT_INCIDENT_DATE_FIELD');

        $assessments = Assessment::where('Form.FormID', config('poc.RESIDENT_INCIDENT_FORM_ID'))
//            ->whereBetween("data.$date_field_utc", $dates)
            ->whereBetween("updated_at", $dates)
            ->where('Facility.FacilityId', $facilityId)
            ->get();

        $incident_data = [];

        foreach($assessments as $assm){
            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');

            $incident_data[$moyr][] = [
                'Date' => array_get($assm->data, $date_field),
                'Incident' => 1,
            ];

        }

        $label = []; $dataset = []; $incidentdata=[];
        foreach($moyrArr as $my=>$val){

            $label[] = array_get($val, 'MonthYear');
            $totalincident = \App\Utils\Toolkit::smart_count(array_get($incident_data, $my));
            $dataset[] = $totalincident;

            $monthyear = array_get($val, 'MonthYear');
            $incidentdata[$my] = ['count'=>$totalincident, 'monthyear' => $monthyear];

        }

        $options = array_get(Reports::Options(), 'option1');;

        $incidentchart = Reports::chart('bar', $label, $dataset, 'Total Number of Incidents', $options);


        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        $fallchartdata = $this->fall($dates, $moyrArr, $dt, $et, $facilityId);
        $fallchart = array_get($fallchartdata, 'fallchart');
        $falldata = array_get($fallchartdata, 'falldata');

        $bytypechartdata = $this->incident_by_type($dates, $moyrArr, $dt, $et, $facilityId);
        $bytypechart = array_get($bytypechartdata, 'chart');
        $bytypedata = array_get($bytypechartdata, 'data');

        return view('report.incidents', compact('incidentchart','incidentdata', 'fromDate', 'toDate',
            'fallchart', 'falldata', 'bytypechart', 'bytypedata'));
    }

    public function fall($dates, $moyrArr, $dt, $et, $facilityId){

        $date_field = config('poc.RESIDENT_INCIDENT_DATE_FIELD');
        $date_field_utc = config('poc.RESIDENT_INCIDENT_DATE_FIELD_UTC');
        $code = config('poc.RESIDENT_INCIDENT_FALL_QUESTION_CODE');
        $fldcode = config('poc.RESIDENT_INCIDENT_FALL_FIELD');

        $assessments = Assessment::where('Form.FormID', config('poc.RESIDENT_INCIDENT_FORM_ID'))
//            ->whereBetween("data.$date_field_utc", $dates)
            ->whereBetween("updated_at", $dates)
            ->where('Facility.FacilityId', $facilityId)
            ->get();


//        Debugbar($assessments);

        $fall_data = [];

        foreach($assessments as $assm){
            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');
            $fall = array_get($assm->data, $code.'-'.$fldcode);
            if($fall == 'on'){
                $fall_data[$moyr][] = [
                    'Date' => array_get($assm->data, $date_field),
                    'Fall' => array_get($assm->data, $code.'-'.$fldcode),
                ];
            }


        }

        $labeldata = []; $falldataset = []; $falldata=[];
        foreach($moyrArr as $my=>$val){

            $labeldata[] = array_get($val, 'MonthYear');
            $totalfall = \App\Utils\Toolkit::smart_count(array_get($fall_data, $my));
            $falldataset[] = $totalfall;

            $monthyear = array_get($val, 'MonthYear');
            $falldata[$my] = ['count'=>$totalfall, 'monthyear' => $monthyear];

        }

        $label = $labeldata;
        $dataset = $falldataset;
        $options = array_get(Reports::Options(), 'option2');

        $fallchart = Reports::chart('bar', $label, $dataset, 'Total Number of Fall', $options);


        return [
            'fallchart' => $fallchart,
            'falldata' => $falldata
        ];

    }

    public function incident_by_type($dates, $moyrArr, $dt, $et, $facilityId){


        $date_field = config('poc.RESIDENT_INCIDENT_DATE_FIELD');
        $date_field_utc = config('poc.RESIDENT_INCIDENT_DATE_FIELD_UTC');
        $bytype = config('poc.RESIDENT_INCIDENT_BY_TYPE_FIELDS');

        $assessments = Assessment::where('Form.FormID', config('poc.RESIDENT_INCIDENT_FORM_ID'))
//            ->whereBetween('updated_at', $dates)
            ->whereBetween("data.$date_field_utc", $dates)
            ->where('Facility.FacilityId', $facilityId)
            ->get();

        $bytype_data = [];

        foreach($assessments as $assm){
            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');

            if(!empty($assm->data)){
                foreach($assm->data as $k=>$v){
                    if(in_array($k, array_keys($bytype))){
                        if($v == 'on'){
                            $bytype_data[$k][] = $v;
                        }
                    }
                }
            }

        }


        $dataset = [];
        foreach($bytype as $k=>$v){
            $total = \App\Utils\Toolkit::smart_count(array_get($bytype_data, $k));
            $dataset[] = $total;
            $label[] = $v;
            $ByTypeData[] = [
                'Title'=>$v,
                'Total'=>$total
            ];
        }


        $options = array_get(Reports::Options(), 'option3');

        $incidentbytype = Reports::chart('horizontalBar', $label, $dataset, 'Incident By Type', $options);

        return[
           'chart' => $incidentbytype,
           'data' =>  $ByTypeData
        ] ;

    }
}