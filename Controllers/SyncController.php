<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UploadQueue;

class SyncController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data = $request->all();
        
        $q = new UploadQueue();
        $q->data = $data;
        $q->state = 'new';
        $q->save();

    }
}
