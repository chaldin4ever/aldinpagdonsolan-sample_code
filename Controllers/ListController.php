<?php

namespace App\Http\Controllers;

use App\Models\FormQuestions;
use App\Utils\Toolkit;
use Illuminate\Http\Request;
use App\Domains\AssessmentForm;
use App\Models\POCList;
use Redis;
use Auth;
use App\Models\Facility;
use App\Domains\User;
use App\Models\BusinessLogic;
use App\Models\ResidentList;

class ListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if (empty(Redis::get($key))) return redirect(url('facility/select'));
        $facility_id = Redis::get($key);

        $lists = POCList::orderBy('ListName')
            ->where('Facility.FacilityId', $facility_id)
            ->get();

        return view('list.index',[
            'lists' => $lists,
        ]);
    }

    public function edit(Request $request){
        $form_id = $request->input('id');
        $mode = $request->input('mode');
        $fields = $request->input('fields');
        $list_id = $request->input('list_id');

        if(empty($mode)) $mode = 'new';
        if(empty($fields))
            $selected_fields = [];
        else
            $selected_fields = $fields;

        if(empty($list_id))
            $selected_list = [];
        else
            $selected_list = POCList::find($list_id);
        // dd($form_id);
        $forms = AssessmentForm::orderBy('FormName')
            ->where('IsActive', 1)->get();

        if (!empty($form_id)) {
            $selected_form = AssessmentForm::find($form_id);
            $questions = FormQuestions::where('Form.FormId', $form_id)->get();

//            dd($questions);
        } else {
            $selected_form = [];
            $questions = [];
        }
        return view('list.edit', [
            'selected_form' => $selected_form,
            'forms' => $forms,
            'mode' => $mode,
            'selected_fields' => $selected_fields,
            'selected_list' => $selected_list,
            'questions' => $questions
        ]);
    }

    public function store_list(Request $request){
        $key = 'selected.facility.' . Auth::user()->_id;
        if (empty(Redis::get($key))) return redirect(url('facility/select'));
        
        $facility_id = Redis::get($key);
        $facility = Facility::find($facility_id);

        $list_name = $request->input('list_name');
        $source = $request->input('source');
        $destination = $request->input('destination');

        if(!empty($list_name)){
            $userid = Auth::user()->_id;
            $user = User::find($userid);

            $list = new POCList();
            $list->Facility = $facility->Object;
            $list->ListName = $list_name;
            $list->CreatedBy = $user->Object;
            $list->source = $source;
            $list->destination = $destination;
            $list->save();
        }

        return redirect("lists");
    }

    public function edit_header(Request $request)
    {
        $key = 'selected.facility.' . Auth::user()->_id;
        if (empty(Redis::get($key))) return redirect(url('facility/select'));

        $listid = $request->list_id;

        $list = POCList::find($listid);
        $list->ListName = $request->list_name;
        $list->save();

        return redirect("lists")->with('status', 'Successfully saved.');
    }

    public function edit_field_header(Request $request)
    {
        $key = $request->key;
        $listid = $request->list_id;
        $fieldid = $request->field_id;
        $fieldtext = $request->field_name;

//        $list = POCList::where('_id', $listid)->where('Fields', 'elemMatch', ['uid' => $fieldid])->first();

        $list = POCList::where('_id', $listid);

        $list->update(array("Fields.$key.text" => $fieldtext));
//

        $data = POCList::find($listid);
        return $data;
//        return redirect("lists/edit?list_id=".$listid)->with('status', 'Successfully saved.');
    }

    public function delete_field($listid, $key)
    {

       $list  = POCList::find($listid);

       if(!empty($list->Fields)){
           $fields = [];
           foreach($list->Fields as $k=>$data){

               if($key != $k) $fields[] = $data;

           }

           $list->Fields = $fields;
           $list->save();
       }else{
           $list->pull('Fields');
       }


    }


    public function fieldtext($listid, $key){

        $list = POCList::find($listid);

        $field = array_get($list->Fields, $key);

        return $field;
    }

        public function store_fields(Request $request)
    {
        $fields = $request->input('fields');
        $list_id = $request->input('list_id');
        $form_id = $request->input('id');

        $userid = Auth::user()->_id;
        $user = User::find($userid);
        $form = AssessmentForm::find($form_id);
        $list = POCList::find($list_id);

        $rows = $list->Fields;
        if(empty($rows))
            $rows = [];

//        dd($fields);
        foreach($fields as $field){
            $question = FormQuestions::where('Form.FormId', $form->_id)->where('Code', $field)->first();
            //$qtn = $form->GetField($field);
            //$q = array_get($qtn, 'question');
            $text = $question->Question;//array_get($q, 'text');
            $row = [
                'uid' => uniqid(), 
                'form_id' => $form->_id, 'form_code' => $form->FormID, 
                'form_name' => $form->FormName,
                'qtn_code' => $field, 
                'field_type' => $question->Type,
                'text' => $text ];
            $rows[] = $row;
        }
        $list->Fields = $rows;
        $list->save();

        $url = "lists/edit?id=$form_id&list_id=$list_id";
        return redirect($url);
    }
   
    public function create_logics($list_id){
        $facility = Toolkit::getFacility();
        $list = POCList::find($list_id);
        $fields = $list->Fields;
        // get all form_code first
        $forms = [];
        foreach($fields as $field){
            if(in_array(array_get($field, 'form_code'), $forms))
                continue;
            $form_code = array_get($field, 'form_code');
            $forms[$form_code] = array_get($field, 'form_name');
        }
        foreach($forms as $form_code => $form){
            $logic = BusinessLogic::where('json.form_code', $form_code)->get()->first();
            if(empty($logic))
                $logic = new BusinessLogic();
            $logic->Facility = $facility->Object;
            $logic->title = $list->ListName . " (" . $form .")";
            $logic->archived = false;

            $json['form_code'] = $form_code;

            $inputs = [
                "field" => "FormState" ,
                "operation" => "in" ,
                "op_values" => [
                    config('poc.form_completed'), 
                    config('poc.form_updated')]
            ];
            $copy_fields = [];
            foreach ($fields as $field) {
                if (array_get($field, 'form_code') != $form_code)
                    continue;
                $use_raw_data = !in_array(array_get($field, 'field_type'), 
                    ["radio", "dropdown", "checkbox"]);
                $copy_fields[] = [
                    'from_field' => array_get($field, 'qtn_code'),
                    'to_field' => array_get($field, 'uid'),
                    'use_raw_data' => $use_raw_data
                ];
            }
            $output = [
                "action" => "upsert",
                "keys" => ["resident"],
                "target_class" => "ResidentList",
                "target_form" => 0,
                "copy_fields" => $copy_fields
            ];
            $pinboard = [
                "action" => "",
                "note" => "",
                "due_days" => 0,
                "item_type" => "",
                "user_roles" => [],
            ];

            $json['inputs'] = $inputs;
            $json['output'] = $output;
            $json['pinboard'] = $pinboard;
            $json['references'] = [
                'list_id' => $list_id,
                'name' => $list->ListName
            ];

            $logic->json = $json;
            $logic->save();
        }

        $url = "lists/edit?list_id=".$list_id;
        return redirect($url);
    }

    public function view($list_id){
        $list = POCList::find($list_id);

        $rows = ResidentList::where('references.list_id', $list->_id)->get();

        return view('list.view', [
            'list' => $list,
            'rows' => $rows,
        ]);
    }
}
