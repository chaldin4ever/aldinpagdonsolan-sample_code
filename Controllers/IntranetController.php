<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Models\Config;
use App\Models\IntranetSetup;
use App\Models\Jobs;
use App\Models\News;
use App\Models\Photos;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\RecentVisitedResident;
use Auth;
use Redis;

class IntranetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));
        $facility_id = Redis::get($key);

        $rows = IntranetSetup::orderBy('DispOrder', 'asc')
            ->where('Facility.FacilityId', $facility_id)
            ->where('active', true)
            ->get();

        if(sizeof($rows) == 0) return redirect(url('intranet/setup'));

//        dd($rows);
        $cards = [];
        foreach($rows as $row){
            $code = array_get($row->Card, 'Code');
            $name = array_get($row->Card, 'Name');
            if($code == "recent_residents"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => $this->most_recent_resident()];
            } else if($code == "latest_news"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => $this->get_news($facility_id)];
            } else if($code == "jobs"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => $this->get_jobs($facility_id)];
            } else if($code == "photos"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => $this->get_photos($facility_id)];
            } else if($code == "shortcuts"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => []];
            } else if($code == "discharged"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => $this->get_discharged()];
            } else if($code == "leaves"){
                $cards[$code] = [
                    'name' => $name,
                    'rows' => $this->get_onleaves()];
            }
        }

        $currentSetup = $this->getCurrentSetup($facility_id);
        //echo "<pre>"; var_dump($cards);
        return view("intranet.index",[
            'cards' => $cards,
            'currentSetup' => $currentSetup
        ]);
    }



    public function setup(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));
        $facility_id = Redis::get($key);

        $currentSetup = $this->getCurrentSetup($facility_id);

        $configs = config("poc.intranet_setup");

        $rows = IntranetSetup::orderBy('DispOrder', 'asc')
            ->where('Facility.FacilityId', $facility_id)
            ->get();

        return view("intranet.setup",[
            'configs' => $configs,
            'currentSetup' => $currentSetup,
            'rows' => $rows
        ]);
    }

    public function getCurrentSetup($facility_id){

        $rows = IntranetSetup::orderBy('DispOrder')
            ->where('Facility.FacilityId', $facility_id)
            ->where('active', true)
            ->get();

        $list = [];
        foreach($rows as $row){
            $list[] = array_get($row->Card, 'Code');
        }

        return $list;
    }

    public function store_setup_list($facility){

        $setupListArr = collect(config('poc.intranet_setup'));
        $configList = IntranetSetup::where('Facility.FacilityId', $facility->_id)->get();

        if(sizeof($setupListArr) != sizeof($configList)){
            $num=1;
            foreach($setupListArr as $setupList){

                $setupObj = [
                    'Code' =>array_get($setupList, 'code'),
                    'Name' =>array_get($setupList, 'name'),
                    'Desc' =>array_get($setupList, 'desc')
                ];

                $setup = new IntranetSetup();
                $setup->DispOrder = $num;
                $setup->Facility = $facility->Object;
                $setup->Card = $setupObj;
                $setup->active = false;
                $setup->save();

                $num++;
            }
        }
    }

    public function store_setup(Request $request){

        $facility = Toolkit::getFacility();

        $this->store_setup_list($facility);

        $setupList = collect(config('poc.intranet_setup'))->where('code', $request->code)->first();

        $setupObj = [
            'Code' =>array_get($setupList, 'code'),
            'Name' =>array_get($setupList, 'name'),
            'Desc' =>array_get($setupList, 'desc')
        ];

        $code = $request->code;
        $setup = IntranetSetup::where('Card.Code', $code)
            ->where('Facility.FacilityId', $facility->_id)
            ->first();

        if($request->obj == true){
            if(empty($setup)){
                $setup = new IntranetSetup();
            }

            $setup->Facility = $facility->Object;
            $setup->Card = $setupObj;
            $setup->active = true;
            $setup->save();

        }else{

            $setup->active = false;
            $setup->save();

        }

        return $setup;

    }

    public function update_order(Request $request){
        $neworder = array_unique(explode(',',$request->neworder));

        $num = 1;
        foreach($neworder as $code){
            $row = IntranetSetup::where('Card.Code', $code)->first();
            $row->DispOrder = $num;
            $row->save();
            $num++;
        }

        return $neworder;
    }

    private function get_news($facility_id){

        $now = Carbon::now();
        $news = News::where('Facility.FacilityId', $facility_id)
            ->where('Announcement', false)
            ->where('Status', 'Publish')
            ->where('ExpiryDate', '>', $now)
            ->get();

        return $news;
    }

    private function get_jobs($facility_id){

        $now = Carbon::now();
        $jobs = Jobs::where('Facility.FacilityId', $facility_id)
            ->where('Status', 'Publish')
            ->where('ExpiryDate', '>', $now)
            ->get();

        return $jobs;
    }


    private function get_photos($facility_id){

        $photos = Photos::where('Facility.FacilityId', $facility_id)->get();

        return $photos;
    }

    private function get_onleaves(){
        $config = Config::whereIn('ConfigID', ['HOSPITAL_LEAVE_FORM_ID', 'SOCIAL_LEAVE_FORM_ID'])
            ->where('Archive', 0)
            ->get();
        if(empty($config) || sizeof($config) == 0)
            return [];
//        dd($config);
        $FormID = [];
        foreach($config as $c)
            $FormID[] = $c->FormID;

        $today = Carbon::now();
        $before = $today->subDays(7);
        $rows = Assessment::whereIn("Form.FormID", $FormID)
            ->where('updated_at', ">=", $before)
            ->get();
//        dd($rows);
        $data = [];
        foreach($rows as $row){
            $rid = array_get($row->Resident, 'ResidentId');
            $data[$rid] = [
                "resident" => $row->Resident,
                "data" => $row->data
            ];
        }
//        dd($data);
        return $data;
    }

    private function get_discharged(){
        $config = Config::where('ConfigID', 'DISCHARGE_FORM_ID')
            ->where('Archive', 0)
            ->get()
            ->first();
        if(empty($config))
            return [];
//        dd($config);
        $FormID = $config->FormID;

        $today = Carbon::now();
        $before = $today->subDays(7);
        $rows = Assessment::where("Form.FormID", $FormID)
            ->where('updated_at', ">=", $before)
            ->get();
//        dd($rows);
        $data = [];
        foreach($rows as $row){
            $rid = array_get($row->Resident, 'ResidentId');
            $data[$rid] = [
                "resident" => $row->Resident,
                "data" => $row->data
            ];
        }
//        dd($data);
        return $data;
    }

    private function most_recent_resident(){

//        dd($facilityId);
        $residents = RecentVisitedResident::where('User.UserId', Auth::user()->_id)
//                ->groupBy('Resident')
            ->orderby('created_at', 'desc')
            ->take(5)
            ->get();

        $data = [];
        foreach($residents as $resident){
            $r = RecentVisitedResident::where('Resident.ResidentId', array_get($resident->Resident, 'ResidentId'))->first();
            $data[array_get($resident->Resident, 'ResidentId')] = $r;
        }

        $residents = collect($data)->take(5);// RecentVisitedResident::whereIn('_id', $grp)->orderby('created_at', 'desc')->get();

        return $residents;
    }




}
