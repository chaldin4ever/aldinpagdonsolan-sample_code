<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Jobs;
use App\Models\Photos;
use App\User;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PhotosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $facility = Toolkit::getFacility();
        if(empty($facility))return redirect(url('facility/select'));

        $photos = Photos::where('Facility.FacilityId', $facility->_id)->get();
        return view('photos.index', compact('photos'));
    }

    public function upload(Request $request)
    {
        $facility = Toolkit::getFacility();
        if(empty($facility))return redirect(url('facility/select'));

        $files = $request->file('files');

            $num = 0;
            foreach ($files as $file) {

                $title_suffix = '';
                if($num > 0){
                    $title_suffix = '-'.$num;
                }
                $photo = new Photos();

                $filename = $file->getClientOriginalName();
                $fileObj = Storage::disk('s3')->put('upload', $file,
                    [
                        'visibility' => 'public',
                        'Metadata' => [
                            'file_name' => $filename
                        ]
                    ]);

                $photo->Facility = $facility->Object;
                $photo->Title = $request->Title.''.$title_suffix ;
                $photo->FileName = $filename;
                $photo->AWSFile = env('AWS_URL') . '/' . $fileObj;
                $user = User::find(Auth::user()->_id);
                $photo->UploadedBy = $user->Object;
                $photo->save();

                $num++;
            }

            return redirect(url('intranet/photos'))->with('status', 'Successfully Uploaded.');

    }
}