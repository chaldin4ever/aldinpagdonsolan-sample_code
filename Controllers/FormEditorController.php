<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Domains\FormControl;
use App\Domains\User;
use App\Models\CareDomain;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Resident;
use App\Models\Rooms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redis;

class FormEditorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($formId)
    {

        $userid = Auth::user()->_id;
        $key = 'selected.facility.' . $userid;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $facilityId = Redis::get($key);

        $form = AssessmentForm::find($formId);

            $plObj = new PicklistController();
            $picklists = $plObj::getAutocompleteList();

            $cdObj = new CareDomainController();
            $caredomain = $cdObj::getCareDomain();

            $bodymap = config('poc.bodymap');

            $questions = FormQuestions::where('Form.FormId', $formId)
                        ->orderby('DisplayOrder', 'asc')->get();

            $defaultOrderValue = sizeof($questions) + 1;

            $user = User::find(Auth::user()->_id);
            $userfacility = isset($user->Facility) ? json_encode($user->Facility) : json_encode([]);

            $facility = Facility::find($facilityId);
            $areas = !empty($facility->Areas) ? $facility->Areas : [];

            $vacantrooms = Rooms::where('Facility', $facilityId)
            ->where('CurrentResident', null)
            ->get();

//            dd($userfacility);
                return view('formeditor.index', [
                    'form' => $form,
                    'questions' => $questions,
                    'picklists' => $picklists,
                    'caredomain' => $caredomain,
                    'bodymap' => $bodymap,
                    'data' => [],
                    'defaultOrderValue' => (int)    $defaultOrderValue,
                    'userfacilities' => $userfacility,
                    'areas' => json_encode($areas),
                    'vacantrooms' => json_encode($vacantrooms)

                ]);

        }


        public function apiStoreQuestion(Request $request){

            $formid = $request->formId;
            $form = AssessmentForm::find($formid);

            $type = $request->type;

            if(!empty($request->questionId)){
                $frmQuestion = FormQuestions::find($request->questionId);
            }else{
                $frmQuestion = new FormQuestions();
            }

            $frmQuestion->Form = $form->Object;
            $frmQuestion->DisplayOrder = (int)"$request->DisplayOrder";
            $frmQuestion->Type = $type;
            $frmQuestion->Question = $request->question;
//            $frmQuestion->Picklist = $request->picklistId;
            $frmQuestion->Required = $request->required;
            $frmQuestion->Code = $request->code;
            $frmQuestion->Prompt = $request->prompt;

            $cdomain=[];
            if(!empty($request->caredomain)){
                foreach($request->caredomain as $cdid){

                    if($cdid != ''){
                        $caredomain = CareDomain::find($cdid);
                        $cdomain[] = $caredomain->Object;
                    }

                }
            }


            $frmQuestion->CareDomain = $cdomain;
            $frmQuestion->Goal = $request->goal;
            $frmQuestion->Response = $request->response;

            if($type =='checkbox' || $type =='radio' || $type=='dropdown' || $type='facility'){

                $codelist = $request->fld_code;
                $textlist = $request->fld_text;
                $goallist = $request->fld_goal;
                $responselist = $request->fld_response;
                $scorelist = $request->fld_score;

                $fields = [];
                if(!empty($codelist)){
                    foreach($codelist as $k=>$code){

                        $fields[] = [
                            'code'  => $code,
                            'text'  => array_get($textlist, (int)$k),
                            'goal'  => array_get($goallist, (int)$k),
                            'response'  => array_get($responselist, (int)$k),
                            'score' => array_get($scorelist, (int)$k)
                        ];
                    }
                }


                $frmQuestion->Fields = $fields;
                $frmQuestion->FieldsCode = $request->fld_code;
                $frmQuestion->FieldsText = $request->fld_text;
                $frmQuestion->FieldsGoal = $request->fld_goal;
                $frmQuestion->FieldsResponse = $request->fld_response;
                $frmQuestion->FieldsScore = $request->fld_score;

            }


            $frmQuestion->save();

            return $frmQuestion;

        }

        public function apiGetTotalQuestion($formId){
            $questions = FormQuestions::where('Form.FormId', $formId)
                ->orderby('DisplayOrder', 'asc')->get();

            $defaultOrderValue = sizeof($questions) + 1;

            return $defaultOrderValue;
        }

        public function apiDisplayQuestion($formId){

            $questions = FormQuestions::where('Form.FormId', $formId)
                        ->orderby('DisplayOrder', 'asc')->get();

                return view('formeditor.edit_question_list', [
                    'questions' => $questions, 'data'=> []
                ]);
        }


        public function apiDeleteQuestion($questionId){

            FormQuestions::find($questionId)->delete();

        }

        public function apiGetQuestion($questionId){

            return FormQuestions::find($questionId);

        }

        public function apiSaveDisplayOrder(Request $request){

            $num = 1;
            if(!empty($request->qnId)){
                foreach($request->qnId as $id){

                    $formQuestion = FormQuestions::find($id);
                    $formQuestion->DisplayOrder = (int)$num;
                    $formQuestion->save();

                    $num++;
                }
            }

        }

    public function selectform($residentId, $formId)
    {
        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $resident = Resident::find($residentId);

        $form = AssessmentForm::find($formId);

        if(empty($form)) {
            $form = AssessmentForm::where('Status', 'published')->where('FormID', intval($formId))->get()->first();
            if(empty($form))
                return redirect("/form");

            $questions = FormQuestions::where('Form.FormId', $form->_id)->orderBy('DisplayOrder', 'asc')->get();
        }
        else {
            $questions = FormQuestions::where('Form.FormId', $formId)->orderBy('DisplayOrder', 'asc')->get();
        }
        if(empty($form))
            return redirect(url('resident/view/' . $residentId))->with('danger', 'Unable to locate the form');

            $bodymap = config('poc.bodymap');

            return view('formeditor.selectform', [
                'resident' => $resident,
                'form' => $form,
                'questions' => $questions,
                'data' => [],
                'bodymap' => $bodymap
            ]);

    }

    public function viewform($assessmentId)
    {

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $assessment = Assessment::find($assessmentId);
        $residentId = array_get($assessment->Resident, 'ResidentId');


        $form = AssessmentForm::find(array_get($assessment->Form, 'FormId'));

        if(empty($assessment) OR empty($form))
            return redirect(url('resident/view/'.$residentId.'?view=assessment'))->with('warning', 'Assessment or Form does not exist.');


        $resident = Resident::find($residentId);
        $bodymap = config('poc.bodymap');

        $questions = FormQuestions::where('Form.FormId', $form->_id)->orderBy('DisplayOrder', 'asc')->get();

        return view('formeditor.view', [
            'resident' => $resident,
            'assessment' => $assessment,
            'form' => $form,
            'bodymap' => $bodymap,
            'questions' => $questions
        ]);

    }

    public function editform($residentId, $assessmentId)
    {
        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $resident = Resident::find($residentId);

        $assessment = Assessment::find($assessmentId);

        $formId = array_get($assessment->Form, 'FormId');
        $form = AssessmentForm::find($formId);

        if(empty($form))
            $form = AssessmentForm::where('FormID', intval($formId))->get()->first();

        if(empty($form))
            return redirect(url('resident/view/' . $residentId))->with('danger', 'Unable to locate the form');


        $bodymap = config('poc.bodymap');

        $questions = FormQuestions::where('Form.FormId', $form->_id)->orderBy('DisplayOrder', 'asc')->get();

            return view('formeditor.selectform', [
                'resident' => $resident,
                'form' => $form,
                'questions' => $questions,
                'data' => $assessment,
                'bodymap' => $bodymap
            ]);

    }

    public function preview($formId)
    {
        $form = AssessmentForm::find($formId);
        $bodymap = config('poc.bodymap');


        $questions = FormQuestions::where('Form.FormId', $form->_id)->orderBy('DisplayOrder', 'asc')->get();

        return view('formeditor.preview_form', [

            'form' => $form,
            'questions' => $questions,
            'data' => [],
            'bodymap' => $bodymap
        ]);

    }

    public function javascript($formId){

        $form = AssessmentForm::find($formId);

        return view('formeditor.javascript', compact('form'));
    }

    public function javascript_save($formId, Request $request){

        $form = AssessmentForm::find($formId);
        $form->javascript = $request->javascript;
        $form->save();

        return redirect(url('formeditor/javascript/'.$formId))->with('status', 'Successfully saved.');
    }

    public function UpdateTypeOfDisplayOrderField(){

        $questions = FormQuestions::all();

        foreach($questions as $q){

            $question = FormQuestions::find($q->_id);
            $question->DisplayOrder = (int)"$q->DisplayOrder";
            $question->save();
        }

        return 'Successfully updated.';
    }

    public function AddTitleToAllForms(){

        $assessments = AssessmentForm::all();

        foreach($assessments as $a){

            $assessment = AssessmentForm::find($a->_id);
            $assessment->Title = $a->FormID.' - '.$a->FormName;
            $assessment->save();
        }

        return 'Successfully updated.';
    }

    public function migrateQuestions(){

        $forms = AssessmentForm::all();

        $template_json = [];
        foreach($forms as $form){
            if(isset($form->template_json)){
                $template_json[$form->_id] = $form->template_json;
            }
        }

        foreach($template_json as $formid=>$questions){

            foreach($questions as $key=>$data){
                if(array_get($data, 'code') != 'PLEASE_UPDATE_THIS_QUESTION_OR_REMOVE_THIS_AFTER_ADDING_A_NEW_QUESTION'){

                    $form = AssessmentForm::find($formid);
                    $formQuestion = new FormQuestions();
                    $formQuestion->Form = $form->Object;
                    $formQuestion->DisplayOrder = $key;

                    $type = array_get($data, 'field_type');
                    $formQuestion->Type = $type;
                    $question = array_get($data, 'question');
                    $formQuestion->Question = array_get($question, 'text');
                    $formQuestion->Required = array_get($data, 'required');
                    $formQuestion->Code = array_get($data, 'code');

                    $caredomain = CareDomain::find(array_get($data, 'caredomain'));
                    $formQuestion->CareDomain = !empty($caredomain) ? $caredomain->Object : [];

                    $goal = array_get($data, 'care_plan');
                    $formQuestion->Goal = array_get($goal, 'goal');
                    $formQuestion->Response = array_get($goal, 'map_to');

                    if($type == 'radio' || $type == 'checkbox' || $type='dropdown'){

                        $fields = array_get($data, 'fields');
//                        dd($fields);
                        $fld_data=[]; $codelist = []; $textlist = []; $goallist =[]; $responselist = []; $scorelist = [];

                        if(!empty($fields)){
                            foreach($fields as $k=>$fld){
                                $fld_data[] = [
                                    'code' => array_get($fld, 'code'),
                                    'text' => array_get($fld, 'text'),
                                    'goal' => array_get($fld, 'goal'),
                                    'response' => array_get($fld, 'response'),
                                    'score' => array_get($fld, 'score')
                                ];

                                $codelist[] = array_get($fld, 'code');
                                $textlist[] = array_get($fld, 'text');
                                $goallist[] = array_get($fld, 'goal');
                                $responselist[] = array_get($fld, 'response');
                                $scorelist[] = array_get($fld, 'score');
                            }

                            $formQuestion->Fields = $fld_data;
                            $formQuestion->FieldsCode = $codelist;
                            $formQuestion->FieldsText = $textlist;
                            $formQuestion->FieldsGoal = $goallist;
                            $formQuestion->FieldsResponse = $responselist;
                            $formQuestion->FieldsScore = $scorelist;
                        }

                    }

                    $formQuestion->save();

                }
            }
        }
    }


}