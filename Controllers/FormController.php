<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Config;
use App\Models\EvaluationFormLink;
use App\Models\Facility;
use App\Models\Picklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App;
use App\Domains\AssessmentForm;
use App\Domains\TemplateTranslator;
use Carbon\Carbon;
use App\Domains\FormArchive;
use App\Domains\FormControl;
use Illuminate\Support\Facades\Auth;
use App\Domains\User;
use App\Utils\Toolkit;
use Illuminate\Support\Facades\Log;
use Debugbar;
use App\Models\TableColumn;
use MongoDB\BSON\UTCDatetime;
use PDF;
use Redis;

class FormController extends Controller
{
    protected $facility;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listing($showAll=0)
    {

        if ($showAll) {
            $forms = AssessmentForm::orderBy('FormName')
                ->where('owners', null)
                ->get();
        } else {
            $forms = AssessmentForm::orderBy('FormName')
                ->where('owners', null)
                ->where('IsActive', 1)
                ->get();
        }

        $category = Input::get('category');
        $isActive = (int)Input::get('isActive');
        $language = Input::get('language');

        if ($category or $isActive or $language) {
            return view('form.listingvue', [
                'params' => [
                    'category' => $category,
                    'state' => $isActive,
                    'language' => $language
                ]
            ]);
        } else {
            return view('form.listingvue', [
                'params' => [
                    'form-name' => '',
                    'category' => 'all',
                    'state' => 1,
                    'language' => 'en'
                ]
            ]);
        }


//        return view('form.listingvue');
    }



    public function listingSearch()
    {

        $formName = Input::get('formName');
        $category = Input::get('category');
        $isActive = (int)Input::get('isActive');
        $language = Input::get('language');

        $data = [];
        $forms = AssessmentForm::orderBy('FormName')->get();
        foreach ($forms as $form) {
            $addIt = false;
            if ($category != 'all') {
                if ($category == 'assessment' && $form->AssessmentCategory == 1) {
                    $addIt = true;
                } elseif ($category == 'form' && $form->FormCategory == 1) {
                    $addIt = true;
                } elseif ($category == 'charting' && $form->ChartingCategory == 1) {
                    $addIt = true;
                }
            } else {
                $addIt=true;
            }
            if ($addIt) {
                array_push($data, $form);
            }
        }
        $forms = $data;
        $data = [];
        foreach ($forms as $form) {
            if ($isActive != -1) {
                if ($isActive == $form->IsActive) {
                    array_push($data, $form);
                }
            } else {
                array_push($data, $form);
            }
        }
        $forms = $data;
        $data = [];
        foreach ($forms as $form) {
            if ($language != 'all') {
                if ($language == $form->language) {
                    array_push($data, $form);
                }
            } else {
                array_push($data, $form);
            }
        }
        $forms = $data;
        $data = [];
        foreach ($forms as $form) {
            if ($formName != '') {
                if (stripos('-'.$form->FormName, $formName)) {
                    array_push($data, $form);
                }
            } else {
                array_push($data, $form);
            }
        }
        return view('form.listing', [
            'forms' => $data,
            'params' => [
                'form-name' => $formName,
                'category' => $category,
                'state' => $isActive,
                'language' => $language
            ]
        ]);
    }


    public function add()
    {
        $form = (object)[
            '_id' => null,
            'FormID' => AssessmentForm::NextFormCode(),
            'FormName' => '',
            'IsActive' => 1,
            'ResidentRequired' => 0,
            'StaffRequired' => 0,
            'FormCategory' => 1,
            'ChartingCategory' => 0,
            'AssessmentCategory' => 0,
            'language' => 'en',
            'FormCode' => AssessmentForm::NextFormCode(),
            'reference' => '',
            'owners' => [],
            'Status' => 'draft',
            'NeedFor' => 'Resident',
        ];

        $facility = Toolkit::getFacility();
        $providerId = array_get($facility->Provider, 'ProviderId');

        $roles =  Role::orderBy('roleName', 'asc')
            ->where('Provider.ProviderId', $providerId)
            ->get();

        $facilities =  Facility::orderBy('NameLong', 'asc')->get();

        $parents = $this->getFindParent();

        $evaluation_forms = $this->getEvaluationForms();

        $staff_list =  User::where('UserType', '!=', config('poc.system_admin'))->get();

        return view(
            'form.add_edit',
            ['form' => $form, 'roles' => $roles, 'parents'=>$parents, 'evaluation_forms' => $evaluation_forms,
                'selected_evaluation' => [], 'facilities' => $facilities, 'staff_list'=>$staff_list]
        );
    }

    public function edit($formId)
    {

        $form = AssessmentForm::find($formId);
        if ($form->FormID == 0) {
            $form->FormID = AssessmentForm::NextFormCode();
        }

        if (isset($form['ParentFormID'])) {
            $parentForm = AssessmentForm::where('FormID', $form['ParentFormID'])->get()->first();
            $form['ParentFormName'] = $parentForm['FormName'];
        }

        $facility = Toolkit::getFacility();
        if(empty($facility))return redirect(url('facility/select'));
        $providerId = array_get($facility->Provider, 'ProviderId');

        $roles =  Role::orderBy('roleName', 'asc')
            ->where('Provider.ProviderId', $providerId)
            ->get();

        $selectedRoles = $this->selectedRoles($form);

        $selected_staff  = $this->selectedStaff($form);
//        dd($selected_staff);
        $facilities =  Facility::orderBy('NameLong', 'asc')->get();
        $selectedFacilities = $this->selectedFacilities($form);

        $parents = $this->getFindParent();

        $evaluation_forms =  $this->getEvaluationForms();
        $selected_evaluation = $this->getEvaluationFormLink($formId);

        $staff_list =  User::where('UserType', '!=', config('poc.system_admin'))->get();

        return view('form.add_edit', [
            'form' => $form, 'roles' => $roles, 'facilities' => $facilities,
            'selectedRoles' => $selectedRoles,
            'selectedFacilities' => $selectedFacilities,
            'parents' => $parents,
            'evaluation_forms' => $evaluation_forms,
            'selected_evaluation' => $selected_evaluation,
            'staff_list' => $staff_list,
            'selected_staff' => $selected_staff
        ]);
    }

    public function copy($formid){

        $key = 'selected.facility.' . Auth::user()->_id;
        $facility_id = Redis::get($key);
        if(empty($facility_id))return redirect(url('facility/select'));
        $facility = \App\Models\Facility::find($facility_id);
        if(empty($facility)) redirect(url('facility/select'));
        $provider_id = array_get($facility->Provider, 'ProviderId');
        // Retrieve the first task
        $form = AssessmentForm::find($formid);

        $newForm = $form->replicate();
        $newForm->FormName = $form->FormName.' COPY';
        $newForm->Facility = [];
//        if(Auth::user()->IsSystemAdmin)
//            $newForm->Provider = null;
//        else
            $newForm->Provider = $provider_id;
        $newForm->save();

        // copy questions
        $rows = App\Models\FormQuestions::where('Form.FormId', $formid)
            ->get();
        foreach($rows as $row){
            $newQuestion = $row->replicate();
            $newQuestion->Form =  $newForm->Object;
            $newQuestion->save();
        }

        return redirect(url('form'))->with('status', $form->FormName.' was copied successfully!');

    }

    public function selectedRoles($form){

        $selectedRoles = [];
        if(!empty($form->Roles)){
            foreach($form->Roles as $role){
                $selectedRoles[] = array_get($role, 'roleId');
            }
        }

        return $selectedRoles;
    }

    public function selectedStaff($form){

        $selectedStaff = [];
        if(!empty($form->NeedForStaff)){
            foreach($form->NeedForStaff as $staff){
                $selectedStaff[] = array_get($staff, 'UserId');
            }
        }

        return $selectedStaff;
    }

    public function selectedFacilities($form){

        $selectedFacilities = [];
        if(!empty($form->Facility)){
            foreach($form->Facility as $facility){
                $selectedFacilities[] = array_get($facility, 'FacilityId');
            }
        }
        
        return $selectedFacilities;
    }

    public function getEvaluationForms(){

        $forms =  Config::where('ConfigName', 'EVALUATION_FORM')->get();

        return $forms;

    }

    public function getEvaluationFormLink($formid){

        $form = EvaluationFormLink::where('Form.FormId', $formid)->get();

        $selected_eval = [];
        foreach($form as $frm){
            $selected_eval[] = array_get($frm->Config, 'configId');
        }

        return $selected_eval;
    }

    public function setEvaluationFormLink($configid,$formid, $evalformId){

        $config = Config::find($configid);
        $form = AssessmentForm::find($formid);
        $eval = AssessmentForm::find($evalformId);

        $link = new EvaluationFormLink();
        $link->Config = $config->Object;
        $link->Form = $form->Object;
        $link->Evaluation = $eval->Object;
        $link->save();

    }

    public function deleteEvaluationFormLink($configid,$formid, $evalformId){

        EvaluationFormLink::where('Config.configId', $configid)
                            ->where('Form.FormId', $formid)
                            ->where('Evaluation.FormId', $evalformId)
                            ->delete();

    }

    public function template($formId)
    {


        $form = AssessmentForm::find($formId);

        $bodymap = config('poc.bodymap');

        return view('form.template', [
            'form' => $form,
            'validated' => false,
            'content' => '',
            'data' => [],
            'bodymap' => $bodymap
        ]);
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'formName' => 'required',
            'status' => 'required',
            'form_code' => 'required'
        ]);

        $key = 'selected.facility.' . Auth::user()->_id;
        $facility_id = Redis::get($key);
        $facility = \App\Models\Facility::find($facility_id);
        $provider_id = array_get($facility->Provider, 'ProviderId');

        $parent = explode('-', Input::get('parentFormID'));
        $formId = Input::get('formId');
        $formName = Input::get('formName');
        $parentFormID = (int)trim($parent[0]);
        $residentRequired = Input::get('residentRequired');
        $staffRequired = Input::get('staffRequired');
        $category = Input::get('category');
        $status = Input::get('status');
        $CarryDataOver = (int)Input::get('CarryDataOver');
        $language = Input::get('language');
        $formID = (int)Input::get('form_code');
        $reference = Input::get('form_reference');
        $use_dummy_resident = Input::get('use_dummy_resident');

        Log::debug($formID);
        $isNewForm = false;
        if ($formId == '') {
            $form = new AssessmentForm();
            $form->version = 1;
            $isNewForm = true;
        } else {
            $form = AssessmentForm::find($formId);
        }

        $form->Title = $formID.' - '.$formName;
        $form->FormName = $formName;
        $form->ResidentRequired = ($residentRequired=='1'? 1: 0);
        $form->StaffRequired = ($staffRequired=='1'? 1: 0);
        $form->FormCategory = ($category=='form'? 1: 0);
        $form->AssessmentCategory = ($category=='assessment'? 1: 0);
        $form->ChartingCategory = ($category=='charting'? 1: 0);
        $form->EvaluationCategory = ($category=='evaluation'? 1: 0);
//        $form->IsActive = ($status=='active'? 1: 0);
        $form->Status = $status;
        $form->CarryDataOver = $CarryDataOver;
        $form->language = $language;
        $form->reference = $reference;
        $form->use_dummy_resident = $use_dummy_resident;
//        $form->Provider = $provider_id;
        $roleArray = [];
        if(!empty($request->roles)){
            foreach($request->roles as $role){
                $roleData = Role::find($role);
                $roleArray[] = $roleData->Object;
            }
        }

        $form->Roles = $roleArray;
        $form->NeedFor = $request->NeedFor;


        $stafflist = [];
        if($request->NeedFor == 'Staff'){
            foreach($request->Staff as $staff){
                $user = User::find($staff);
                $stafflist[] = $user->Object;
            }
        }

        $form->NeedForStaff = $stafflist;

//        $facilityArray = [];
//        if(!empty($request->facilities)){
//            foreach($request->facilities as $facility){
//                $facilityData = Facility::find($facility);
//                $facilityArray[] = $facilityData->Object;
//            }
//        }
//
//        $form->Facility = $facilityArray;
        $form->FormID = $formID;
//        $form->ParentFormID = $parentFormID;

        $userid = Auth::user()->_id;
        $user = User::find($userid);
        $form->updated_by = $user->Object;

        $form->save();

        return redirect('/formeditor/'.$form->_id);

/*        if ($isNewForm) {
            return redirect('/form/template/'.$form->_id);
        } else {
//            return redirect('/form/listing');
            return redirect('/form/editform/'.$form->_id);
        }*/
    }

    public function validateTemplate(Request $request)
    {
        $this->validate($request, [
            'formId' => 'required',
            'template_content' => 'required'
        ]);

        $formId = $request->input('formId');
        $content = $request->input('template_content');
        $form = AssessmentForm::find($formId);

        if (substr($content, 0, 9)=='template.') {
            $form->template = $content;
            $form->save();
            return redirect('/form/listing');
        }
        // translate content
        $translator = new TemplateTranslator($content);

        $templateString = $translator->Translate();
        $form_controls = json_decode($templateString);

        if ($form_controls == null) {
            $form = AssessmentForm::find($formId);
//            $this->validator->errors()->add('template', 'error in template, please fix and try again.');
            return view('form.template', [
                'form' => $form,
                'validated' => false,
                'content' => $content,
                'templateString' => $templateString
            ]);
        }
        $form->template = $content;
        $form->template_json = $form_controls;


        // convert to array instead of an object here
        $form_controls = json_decode($templateString, true);

        $controls = new FormControl();
        foreach ($form_controls as $cnt) {
            $controls->AddControl($cnt);
        }
//        dd($controls);
        if ($form->version == null) {
            $form->version = 1;
        } else {
            // archive the current version
            $archive = new FormArchive();
            $archive->Archive($form);

            $form->version++;
        }
        $form->save();

        return redirect('form/editform/'.$form->_id);

        /*return view('form.preview_form', [
            'form' => $form,
            'template' => $templateString,
            'controls' => $controls->controls,
            'data' => [],
        ]);*/
    }

    public function preview($formId)
    {
        $form = AssessmentForm::find($formId);
        $bodymap = config('poc.bodymap');

        if (substr($form->template, 0, 9)=='template.') {
            return view(
                'form.fixed_template',
                [
                    'form' => $form,
                    'template' => $form->template,
                    'data' => []]
            );
        } else {
            $form_controls = $form->template_json;
            if (is_array($form_controls)) {
                $controls = new FormControl();
                foreach ($form_controls as $cnt) {
                    $controls->AddControl($cnt);
                }
                return view('form.preview_form', [
                    'form' => $form,
                    'template' => $form->template,
                    'controls' => $controls->controls,
                    'data' => [],
                    'bodymap'=>$bodymap
                ]);
            } else {
                return redirect('/form/template/'.$formId);
            }
        }
    }

    public function printform($formId)
    {
        $form = AssessmentForm::find($formId);

        if (substr($form->template, 0, 9)=='template.') {
            return view(
                'form.fixed_template',
                [
                    'form' => $form,
                    'template' => $form->template,
                    'data' => []]
            );
        } else {
            $form_controls = $form->template_json;
            if (is_array($form_controls)) {
                $controls = new FormControl();
                foreach ($form_controls as $cnt) {
                    $controls->AddControl($cnt);
                }

                $pdf = PDF::loadView('form.print_form', [
                    'form' => $form,
                    'template' => $form->template,
                    'controls' => $controls->controls,
                    'data' => [],
                ]);

                return $pdf->inline();
//                return view();
            } else {
                return redirect('/form/template/'.$formId);
            }
        }
    }

    public function editform($formId)
    {

        $form = AssessmentForm::find($formId);

        if (substr($form->template, 0, 9)=='template.') {
            return view(
                'form.fixed_template',
                [
                    'form' => $form,
                    'template' => $form->template,
                    'data' => []]
            );
        } else {
            $plObj = new PicklistController();
            $picklists = $plObj::getAutocompleteList();

            $cdObj = new CareDomainController();
            $caredomain = $cdObj::getCareDomain();

            $bodymap = config('poc.bodymap');

            $form_controls = $form->template_json;
                if (is_array($form_controls)) {

                    $controls = new FormControl();
                    foreach ($form_controls as $cnt) {
                        $controls->AddControl($cnt);
                    }

                    return view('form.edit_form', [
                        'form' => $form,
                        'template' => $form->template,
                        'controls' => $controls->controls,
                        'data' => [],
                        'picklists' => $picklists,
                        'caredomain' => $caredomain,
                        'bodymap' => $bodymap
                    ]);
            } else {

                    /*$form_controls = [
                        0 => [
                            'qn' => 1,
                            'code' => 'TEST_POINT_OF_CARE',
                            'question' => ['text' => 'TEST_POINT_OF_CARE', 'attr'=>'', 'goal'=>''],
                            'field_type'=> 'text'
                        ]
                    ];*/

                return redirect('/form/template/'.$formId);
            }
        }
    }

    public function editform_load($formId)
    {

        $form = AssessmentForm::find($formId);


            $plObj = new PicklistController();
            $picklists = $plObj::getAutocompleteList();

            $cdObj = new CareDomainController();
            $caredomain = $cdObj::getCareDomain();

            $bodymap = config('poc.bodymap');

            $form_controls = $form->template_json;
            if (is_array($form_controls)) {

                $controls = new FormControl();
                foreach ($form_controls as $cnt) {
                    $controls->AddControl($cnt);
                }

                return view('template.form_controls_edit_load', [
                    'form' => $form,
                    'template' => $form->template,
                    'controls' => $controls->controls,
                    'data' => [],
                    'picklists' => $picklists,
                    'caredomain' => $caredomain,
                    'bodymap' => $bodymap
                ]);
            }

    }

    public function getFindParent()
    {
//        $name = Input::get('name');
        $result = AssessmentForm::orderBy('FormName')
            ->where('IsActive', 1)->get();

        // For <typeahead> to show text on auto-complete dropdown list
        $itemlist=[];
        foreach ($result as $item) {
            $itemlist[$item['FormID']] = $item['FormID'].' - '.$item['FormName'];
        }

        return json_encode($itemlist);
    }

    public function findByName(Request $request)
    {
        $name = Input::get('name');
        $result = AssessmentForm::orderBy('FormName')
            ->where('IsActive', 1)
            ->where('FormName', 'like', '%'.$name .'%')->get();

        // For <typeahead> to show text on auto-complete dropdown list
        foreach ($result as $item) {
            $item['screen_name'] = $item['FormID'].' - '.$item['FormName'];
        }

        return $result;
    }

    public function autocomplete(Request $request)
    {
        $name = Input::get('name');
        $result = AssessmentForm::orderBy('FormName')
            ->where('IsActive', 1)
            ->where('FormName', 'like', '%'.$name .'%')->get();

        $data = array();
        foreach ($result as $item) {
            $d = array(
                'label' => $item['FormName']. ', '.$item['FormID'],
                'id' => $item['_id']
            );
            array_push($data, $d);
        }
        return json_encode($data);
    }

    public function apiAll($category)
    {
        if ($category=='assessment') {
            return App\Utils\HubHelper::GetAssessmentForms();
        } elseif ($category=='form') {  // this will return assessments and forms
            return App\Utils\HubHelper::GetForms();
        } elseif ($category=='chart') {
            return App\Utils\HubHelper::GetCharts();
        } else {
            return (object)[];
        }
    }

    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $assessmentTab = $this->getTab('AssessmentCategory');
        $evaluationTab = $this->getTab('EvaluationCategory');
        $formsTab = $this->getTab('FormCategory');
        $chartTab = $this->getTab('ChartingCategory');

        return view('form.index',compact('assessmentTab', 'evaluationTab', 'formsTab', 'chartTab'));

    }

    public function getTab($category){

        $rows = AssessmentForm::orderBy('FormName')
            ->where($category, 1)
            ->where('IsActive', 1)->paginate(25);

        return $rows;

    }

    // used by GET and POST
    public function find(Request $request)
    {
        $user = Auth::user();
        $key = 'selected.facility.' . $user->_id;
        $facility_id = Redis::get($key);
        if(empty($facility_id))return redirect(url('facility/select'));
        $this->facility = \App\Models\Facility::find($facility_id);
        if(empty($this->facility)) redirect(url('facility/select'));

        $key = $request->input('key');
        $state = $request->input('state');
        $language = $request->input('lang');
        $name = $request->input('name');

        $rkey = "form_search_term.".$user->_id;
        if(empty($name)){
            // check if $name is in Redis
            $name = Redis::get($rkey);
        } else {
            Redis::set($rkey, $name);
            Redis::expire($rkey, 600);
        }

        $rkey = "form_search_status.".$user->_id;
        if(empty($state)){
            // check if $name is in Redis
            $state = Redis::get($rkey);
        } else {
            Redis::set($rkey, $state);
            Redis::expire($rkey, 600);
        }

        $rkey = "form_search_key.".$user->_id;
        if(empty($key)){
            // check if $name is in Redis
            $key = Redis::get($rkey);
        } else {
            Redis::set($rkey, $key);
            Redis::expire($rkey, config('oidesk.expire-in-12hrs'));
        }


        if (empty($key)) {
            $key = 'all';
        }
        if (empty($state)) {
            $state = 'active';
        }
        if (empty($language)) {
            $language = 'all';
        }

        $rows = \App\Domains\AssessmentForm::orderBy('FormName');

        if ($key=='assessment') {
            $rows = $rows->where('AssessmentCategory', 1);
        } elseif ($key=='form') {
            $rows = $rows->where('FormCategory', 1);
        } elseif ($key=='charting') {
            $rows = $rows->where('ChartingCategory', 1);
        }elseif ($key=='evaluation') {
            $rows = $rows->where('EvaluationCategory', 1);
        }

        if ($language != 'all') {
            $rows = $rows->where('language', $language);
        }

        $user = User::find(Auth::user()->_id);
//        dd($user->UserType);
        if($user->UserType === config('poc.system_admin')){
            $rows = $rows->where(function($query){
                $provider_id = array_get($this->facility->Provider, 'ProviderId');
                $query->where('Provider', null)
                    ->orWhere('Provider', $provider_id);
            });
        }else{
            $rows = $rows->where(function($query){
                $provider_id = array_get($this->facility->Provider, 'ProviderId');
                $query->where('Provider', null)
                    ->orWhere('Provider', $provider_id);
            });
        }
        if ($state == "published")
            $rows = $rows->where('Status', 'published');
        else if($state == "draft")
            $rows = $rows->where('Status', 'draft');
        else if($state == "archived")
            $rows = $rows->where('Status', 'archived');
        else
            $rows = $rows->whereIn('Status', ['draft', 'published']);
        if (!empty($name)) {
            if (is_numeric($name)) {
                $rows = $rows->where('FormID', intval($name));
            } else {
                $rows = $rows->where(function($query) use ($name){
                    $query->where('FormName', 'like', "%$name%")
                    ->orWhere("reference", "like", "%$name%");
                });
            }
        }

        /*$roles = Auth::user()->Roles;
        foreach($roles as $role){
            $userRole = array_get($role, 'RoleId');
            $rows = $rows->where('Roles', 'elemMatch', ['RoleId' => $userRole]);
        }*/


        $rows = $rows->paginate(25);



        /*$userActivity = new UserActivitiesController();
        $userActivity->store(__('mycare.admin_menu').' - '.trans_choice('mycare.form',2),__('mycare.view'));*/

        return view('form.find', [
            'rows' => $rows,
            'key' => $key,
            'state' => $state,
            'lang' => $language,
            'name' => $name,
        ]);
    }

    public function checking(Request $request)
    {

        $error = $request->input('error');
        $lang = $request->input('lang');
        if (empty($error)) {
            $error = 0;
        }
        if (empty($lang)) {
            $lang = 'en';
        }

        $results = [];
        $lang2 = 'zh';
        $forms = AssessmentForm::orderBy('FormName')
            ->where('IsActive', 1)
            ->where('language', $lang)->get();
        if (sizeof($forms)==0) {
            $results [] = [
                "Error" => "No forms found in Language $lang"
            ];
        }
        foreach ($forms as $fm) {
            $zhForms = AssessmentForm::where('FormID', $fm->FormID)
                ->where('language', $lang2)->get();
            if (sizeof($zhForms) == 0) {
                $results [] = [
                    "FormID" => $fm->FormID,
                    "FormName" => $fm->FormName,
                    "Category" => $fm->FormCategory==1?'Form':
                        ($fm->AssessmentCategory==1? 'Assessment':
                            ($fm->ChartingCategory==1? 'Chart':'')),
                    "Error" => "Need <span class='form-name'>$fm->FormID $fm->FormName</span> in Language $lang2"
                ];
            } elseif (sizeof($zhForms) > 1) {
                $results [] = [
                    "FormID" => $fm->FormID,
                    "FormName" => $fm->FormName,
                    "Category" => $fm->FormCategory==1?'Form':
                        ($fm->AssessmentCategory==1? 'Assessment':
                            ($fm->ChartingCategory==1? 'Chart':'')),
                    "Error" => "Too many <span class='form-name'>$fm->FormID $fm->FormName</span> in Language $lang2"
                ];
                $results = $this->compareFormQuestions($results, $fm, $zhForms[0]);
            } else {
                $results [] = [
                    "FormID" => $fm->FormID,
                    "FormName" => $fm->FormName,
                    "Category" => $fm->FormCategory==1?'Form':
                        ($fm->AssessmentCategory==1? 'Assessment':
                            ($fm->ChartingCategory==1? 'Chart':'')),
                    "Error" => ""
                ];
                $results = $this->compareFormQuestions($results, $fm, $zhForms[0]);
            }
        }
        return view('form.checking', [
            'results' => $results,
            'error' => $error
        ]);
    }

    private function compareFormQuestions($results, $form1, $form2)
    {
        if (substr($form1->template, 0, 9)=='template.') {
            if ($form1->template==$form2->template) {
                $results [] = [
                    "FormID" => $form1->FormID,
                    "FormName" => $form1->FormName,
                    "Message" => "$form1->template matched",
                    "Error" => ""
                ];
            } else {
                $results [] = [
                    "FormID" => $form1->FormID,
                    "FormName" => $form1->FormName,
                    "Message" => "$form1->template",
                    "Error" => "Temlate different $form1->template vs $form2->template"
                ];
            }
            return $results;
        }
        $qtns1 = $form1->template_json;
        $qtns2 = $form2->template_json;
        if ($qtns1 == null || $qtns2 == null) {
            $results [] = [
                "FormID" => $form1->FormID,
                "FormName" => $form1->FormName,
                "Error" => "Missing template_json "
            ];
            return $results;
        }
        foreach ($qtns1 as $qtn1) {
            $foundit = false;
            $code1 = array_get($qtn1, 'code');
            if (empty($code1)) {
                continue;
            }
            $text1 = array_get(array_get($qtn1, 'question'), 'text');
            $field_type = array_get($qtn1, 'field_type');
            foreach ($qtns2 as $qtn2) {
                if ($code1==array_get($qtn2, 'code')) {
                    $foundit=true;
                    break;
                }
            }
            if($field_type=="radio" || $field_type=="checkbox"){
                $fields1 = array_get($qtn1, 'fields');
                $fields2 = array_get($qtn2, 'fields');
                foreach($fields1 as $fld1){
                    $fieldFound = false;
                    $fld_code1 = array_get($fld1, "code");
                    if(is_array($fields2)){
                        foreach($fields2 as $fld2){
                            $fld_code2 = array_get($fld2, "code");
                            if($fld_code1 == $fld_code2){
                                $fieldFound = true;
                                break;
                            }
                        }
                    }
                    if(!$fieldFound){
                        $results [] = [
                            "FormID" => $form1->FormID,
                            "FormName" => $form1->FormName,
                            "Error" => "Missing <span class='field-code'>$code1-$fld_code1</span> in $text1"
                        ];
                    }
                }
            }
            if ($foundit) {
                if (!is_array($text1)) {
                    $results [] = [
                        "FormID" => $form1->FormID,
                        "FormName" => $form1->FormName,
                        "Message" => "OK <span class='field-code'>$code1</span> $text1",
                        "Error" => ""
                    ];
                }
            } else {
                $results [] = [
                    "FormID" => $form1->FormID,
                    "FormName" => $form1->FormName,
                    "Error" => "Missing <span class='field-code'>$code1</span> $text1"
                ];
            }
        }

        return $results;
    }

    public function GetPainLocation($location)
    {
        return __('forms.'.$location);
    }

    public function savetemplatehtmlform(Request $request)
    {

        $formid = $request->input('formid');
        $form = AssessmentForm::find($formid);
        $form->template_htmlform = $request->input('template_form');
        $form->save();

        return 'Success';

        //echo '<pre>'; print_r($request->input('template_form')); echo '</pre>';
//        return redirect('form/find/');
    }

    public function checkcareplan(Request $request)
    {

        $error = $request->input('error');
        $lang = $request->input('lang');
        if (empty($error)) {
            $error = 0;
        }
        if (empty($lang)) {
            $lang = 'en';
        }

        $results = [];
        $lang2 = 'zh';
        $forms = AssessmentForm::orderBy('FormName')
            ->where('IsActive', 1)
            ->where('language', $lang)->get();
        if (sizeof($forms)==0) {
            $results [] = [
                "Error" => "No forms found in Language $lang"
            ];
        }
        $results=[];
        foreach ($forms as $fm) {
            if ($fm->IsCustomTemplate) {
                continue;
            }
            $template_json = $fm->template_json;

            if (!is_array($template_json)) {
                $results [] = [
                    "FormID" => $fm->FormID,
                    "FormName" => $fm->FormName,
                    "Message" => "Missing template_json",
                    "Error" => ""
                ];
                continue;
            }
            $results = $this->checkCarePlanEntries($results, $fm);
        }

        return view('form.careplan', [
            'results' => $results,
            'error' => $error
        ]);
    }

    private function checkCarePlanEntries($results, $form)
    {
        $formZh = AssessmentForm::where('FormID', $form->FormID)
            ->where('language', 'zh')
            ->get()->first();
        if ($formZh != null) {
            $controlsZh = $formZh->template_json;
        }

        $template_json = $form->template_json;
        foreach ($template_json as $qtn) {
            $careplans = array_get($qtn, 'care_plan');
            $question = array_get($qtn, 'question');
            if (!is_array($careplans)) {
                continue;
            }
            $fieldType = array_get($qtn, 'field_type');
            $code = array_get($qtn, 'code');
            $qText = array_get($question, 'text');
            foreach ($careplans as $cp) {
                $domain = array_get($cp, 'domain');
                $mapTo = array_get($cp, 'map_to');
                if ($fieldType=='radio' || $fieldType=='checkbox') {
                    $fields = array_get($qtn, 'fields');
                    foreach ($fields as $fld) {
                        $field_text = array_get($fld, 'text');
                        $field_code = array_get($fld, 'code');
                        $field_goal = array_get($fld, 'goal');
                        if (!empty($field_goal)) {
                            $results [] = [
                                "FormID" => $form->FormID,
                                "FormName" => $form->FormName,
                                "QuestionText" => "$field_text",
                                "QuestionCode" => "$code-$field_code",
                                "FieldType" => $fieldType,
                                "Domain" => $domain,
                                "map_to" => $mapTo,
                                "Goal" => ($mapTo == "obs" ? $field_text : $field_goal),
                                "Message" => "",
                                "Error" => ""
                            ];

                            // check if the same care plan stuff is in Chinese version
                            if ($formZh != null) {
                                $results = $this->CheckInZhVersionCheckboxRadio(
                                    $form,
                                    $results,
                                    $controlsZh,
                                    $qtn,
                                    $domain,
                                    $mapTo,
                                    $code,
                                    $field_code,
                                    $field_text
                                );
                            }
                        }
                    }
                } else {
                    $goal = array_get($question, 'goal');
                    $results [] = [
                        "FormID" => $form->FormID,
                        "FormName" => $form->FormName,
                        "QuestionText" => "$qText",
                        "QuestionCode" => "$code",
                        "FieldType" => $fieldType,
                        "Domain" => $domain,
                        "map_to" => $mapTo,
                        "Goal" => ($mapTo == "obs"? $qText: $goal),
                        "Message" => "",
                        "Error" => ""
                    ];

                    // check if the same care plan stuff is in Chinese version
                    if ($formZh != null) {
                        $results = $this->CheckInZhVersionText($form, $results, $controlsZh, $qtn, $domain, $mapTo, $code);
                    }
                }
            }
        }
        return $results;
    }

    private function CheckInZhVersionCheckboxRadio(
        $form,
        $results,
        $controlsZh,
        $qtn,
        $domain,
        $mapTo,
        $code,
        $field_code,
        $field_text
    ) {
        foreach ($controlsZh as $qtnZh) {
            $codeZh = array_get($qtnZh, 'code');
            if ($codeZh == $code) {
                $careplansZh = array_get($qtnZh, 'care_plan');
                if (!is_array($careplansZh)) {
                    $results [] = [
                        "FormID" => $form->FormID,
                        "FormName" => $form->FormName,
                        "QuestionCode" => "$code-$field_code",
                        "Error" => "Missing care_plan in ZH for $code-$field_code => $domain => $mapTo"
                    ];
                    continue;
                }
                $foundDomain = false;
                $questionZh = array_get($qtn, 'question');
                foreach ($careplansZh as $cpZh) {
                    $domainZh = array_get($cpZh, 'domain');
                    $mapToZh = array_get($cpZh, 'map_to');
                    // ignore the domain name because they are in Chinese
                    //if ($domainZh == $domain && $mapToZh = $mapTo) {
                    if ( $mapToZh = $mapTo) {
                        $foundIt = false;
                        $fieldsZh = array_get($qtnZh, 'fields');
                        foreach ($fieldsZh as $fldZh) {
                            if (array_get($fldZh, 'code')==$field_code) {
                                $foundIt = true;
                                $goal = array_get($fldZh, 'goal');
                                $field_textzh = array_get($fldZh, 'text');
                                if (!empty($goal)) {
                                    $results [] = [
                                        "FormID" => $form->FormID,
                                        "FormName" => $form->FormName,
                                        "QuestionCode" => "$code-$field_code",
                                        "Domain" => $domainZh,
                                        "map_to" => $mapToZh,
                                        "Goal" => ($mapTo == "obs"? $field_textzh: $goal),
                                    ];
                                }
                                break;
                            }
                        }
                        if (!$foundIt) {
                            $results [] = [
                                "FormID" => $form->FormID,
                                "FormName" => $form->FormName,
                                "QuestionCode" => "$code-$field_code",
                                "Domain" => $domain,
                                "map_to" => $mapTo,
                                "Error" => "Missing in ZH $code-$field_code => $domain => $mapTo"
                            ];
                        }
                        $foundDomain=true;
                        break;
                    }
                }
                if (!$foundDomain) {
                    $results [] = [
                        "FormID" => $form->FormID,
                        "FormName" => $form->FormName,
                        "QuestionCode" => "$code-$field_code",
                        "Error" => "Unable to locate domain in ZH for $code-$field_code => $domain => $mapTo"
                    ];
                }
            }
        }
        return $results;
    }

    private function CheckInZhVersionText($form, $results, $controlsZh, $qtn, $domain, $mapTo, $code)
    {
        foreach ($controlsZh as $qtnZh) {
            $codeZh = array_get($qtnZh, 'code');
            if ($codeZh == $code) {
                $careplansZh = array_get($qtnZh, 'care_plan');
                if (!is_array($careplansZh)) {
                    $results [] = [
                        "FormID" => $form->FormID,
                        "FormName" => $form->FormName,
                        "QuestionCode" => "$code",
                        "Error" => "Missing care_plan in ZH for $code => $domain => $mapTo"
                    ];
                    continue;
                }
                $foundIt = false;
                $questionZh = array_get($qtnZh, 'question');
                foreach ($careplansZh as $cpZh) {
                    $domainZh = array_get($cpZh, 'domain');
                    $mapToZh = array_get($cpZh, 'map_to');
                    // ignore the domain name because they are in Chinese
                    //if ($domainZh == $domain && $mapToZh = $mapTo) {
                    if ($mapToZh = $mapTo) {
                        $foundIt = true;
                        $goal = array_get($questionZh, 'goal');
                        $results [] = [
                            "FormID" => $form->FormID,
                            "FormName" => $form->FormName,
                            "QuestionCode" => "$code (zh)",
                            "Domain" => $domainZh,
                            "map_to" => $mapToZh,
                            "Goal" => $goal,
                        ];
                        break;
                    }
                }
                if (!$foundIt) {
                    $results [] = [
                        "FormID" => $form->FormID,
                        "FormName" => $form->FormName,
                        "QuestionCode" => "$code",
                        "Domain" => $domain,
                        "map_to" => $mapTo,
                        "Error" => "Missing in ZH $code => $domain => $mapTo"
                    ];
                }
            }
        }
        return $results;
    }


    public function listforms(Request $request)
    {

        $error = $request->input('error');
        $lang = $request->input('lang');
        if (empty($error)) {
            $error = 0;
        }
        if (empty($lang)) {
            $lang = 'en';
        }

        $results = [];
        $lang2 = 'zh';
        $forms = AssessmentForm::orderBy('FormID')
            ->where('IsActive', 1)
            ->where('language', $lang)->get();
        if (sizeof($forms)==0) {
            $results [] = [
                "Error" => "No forms found in Language $lang"
            ];
        }
        foreach ($forms as $fm) {
            $results [] = [
                "FormID" => $fm->FormID,
                "FormName" => $fm->FormName,
                "Category" => $fm->FormCategory==1?'Form':
                    ($fm->AssessmentCategory==1? 'Assessment':
                        ($fm->ChartingCategory==1? 'Chart':'')),
                "Error" => ""
            ];
            $results = $this->listQuestions($results, $fm);
        }
        return view('form.listforms', [
            'results' => $results,
            'error' => $error
        ]);
    }

    private function listQuestions($results, $form1)
    {
        $qtns1 = $form1->template_json;
        if (!is_array($qtns1)) {
            return $results;
        }
        //dd($qtns1);
        foreach ($qtns1 as $qtn1) {
            $code1 = array_get($qtn1, 'code');
            $fieldType = array_get($qtn1, 'field_type');
            if (empty($code1)) {
                continue;
            }
            $text1 = array_get(array_get($qtn1, 'question'), 'text');

            $results [] = [
                "FormID" => $form1->FormID,
                "FormName" => $form1->FormName,
                "Text" => $text1,
                "Code" => $code1,
                "Type" => $fieldType,
            ];

            if ($fieldType=='radio' || $fieldType == 'checkbox') {
                $fields = array_get($qtn1, 'fields');
                foreach ($fields as $fld) {
                    $code = array_get($fld, 'code');
                    $text = array_get($fld, 'text');
                    $results [] = [
                        "FormID" => '',
                        "FormName" => '',
                        "Text" => $text,
                        "Code" => $code1.'-'.$code,
                        "Type" => '',
                    ];
                }
            }
        }

        return $results;
    }

    public function versions($formId)
    {
        $formArchive = FormArchive::where('formId', $formId)
            ->orderBy('version', 'desc')
            ->get();

        return view('form.versions', compact('formArchive'));
    }

    public function previewarchive($formId)
    {

        $form = FormArchive::find($formId);

        if (substr($form->template, 0, 9)=='template.') {
            return view(
                'form.fixed_template',
                [
                    'form' => $form,
                    'template' => $form->template,
                    'data' => []]
            );
        } else {
            $form_controls = $form->template_json;
            if (is_array($form_controls)) {
                $controls = new FormControl();
                foreach ($form_controls as $cnt) {
                    $controls->AddControl($cnt);
                }
//                Debugbar::debug('this is working?');
                return view('form.preview_archive', [
                    'form' => $form,
                    'template' => $form->template,
                    'controls' => $controls->controls,
                    'data' => [],
                ]);
            } else {
                return redirect('/form/template/'.$formId);
            }
        }
    }

    public function restorearchive($formId, $formArchiveId)
    {

        $form = AssessmentForm::find($formId);
        $formArchive = FormArchive::find($formArchiveId);

        $form->template = $formArchive->template;
        $form->template_json = $formArchive->template_json;
        $form->save();

        return redirect('form/preview/'.$formId.'?msg=restored');
    }

    public function compare($formId)
    {
        $form1 = AssessmentForm::find($formId);
        $formID = $form1->FormID;
        $lang = $form1->language;
        if (empty($lang)) {
            $lang = "zh";
        }
        if ($lang == "en") {
            $lang = "zh";
        }

        $form2 = AssessmentForm::where('FormID', $formID)
            ->where('language', $lang)
            ->get()->first();


        return view('form.compare', [
            'form1' => $form1,
            'form2' => $form2,
        ]);
    }
}
