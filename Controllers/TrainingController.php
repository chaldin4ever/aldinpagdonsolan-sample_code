<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Auth;

class TrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user = Auth::user();
        $client = new Client();
        $res = $client->post(env('MD_SERVER_URL').'/oauth/token', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'username' => env("MD_TRUST_USER_ACCT"),
                'password' => env("MD_TRUST_USER_PWD"),
                'grant_type' => 'password',
                'client_id' => env('MD_OAUTH_CLIENT_ID'),
                'client_secret' => env('MD_OAUTH_CLIENT_SECRET')
            ],
            "http_errors" => false
        ]);
        if($res->getStatusCode()==200) {
            $body = $res->getBody();
            $accessToken = '';
            while (!$body->eof()) {
                $accessToken = $accessToken. $body->read(1024);
            }
            $json = json_decode($accessToken);
            $token = $json->access_token;
            $url = env("MD_SERVER_URL")."/OAuth/Signin?u=".$user->email."&t=".$token;
            return redirect($url);
        }
    }
}
