<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Domains\FormControl;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Resident;
use App\Models\SProcess;
use App\Models\SProcessSteps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\BinaryOp\Spaceship;
use Redis;

class SProcessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        $facility_id = Redis::get($key);
        if(empty($facility_id))return redirect(url('facility/select'));
        $facility =  Facility::find($facility_id);
        if(empty($facility)) redirect(url('facility/select'));
        $provider_id = array_get($facility->Provider, 'ProviderId');

        if(Auth::user()->UserType == config('poc.provider_admin') || Auth::user()->UserType == config('poc.system_admin')){
            $processAdmin = SProcess::orderby('ProcessName', 'asc')
                        ->where('Category', 'Admin')
                        ->where('Archive', false)->get();
            $processCare = SProcess::orderby('ProcessName', 'asc')
                        ->where('Category', 'Care')
                        ->where('Archive', false)->get();
        }else{
            $processAdmin = SProcess::orderby('ProcessName', 'asc')
                ->where('Category', 'Admin')
                ->where('State', 'published')
                ->where('Provider', $provider_id)
                ->where('Archive', false)->get();
            $processCare = SProcess::orderby('ProcessName', 'asc')
                ->where('Category', 'Care')
                ->where('State', 'published')
                ->where('Provider', $provider_id)
                ->where('Archive', false)->get();
        }

        return view('sprocess.index', compact('processAdmin', 'processCare'));
    }

    public function store(Request $request){

        if(!empty($request->sprocessId)){
            $sprocess = SProcess::find($request->sprocessId);
        }else{
            $sprocess = new SProcess();
        }

        $sprocess->ProcessName = $request->ProcessName;
        $sprocess->Description = $request->Description;
        $sprocess->Category  = $request->ProcessCategory;
        $sprocess->State = 'draft';
        $sprocess->Archive = false;

        $sprocess->save();

        if($request->Redirect == 'true'){
            return redirect(url('sprocess/editstep/'.$request->sprocessId))->with('status', 'Successfully Saved.');
        }else{
            return redirect(url('sprocess'))->with('status', 'Successfully Saved.');
        }



    }

    public function archive($processId)
    {

            $sprocess = SProcess::find($processId);
            $sprocess->Archive = true;
            $sprocess->save();

        return redirect(url('sprocess'))->with('status', 'Successfully Archived.');

    }

    public function editstep($processId){

        $sprocess = SProcess::find($processId);

        $key = 'current.provider.process' . Auth::user()->_id;
        $provider_id = Redis::get($key);
        if(!empty($provider_id)){
            $providerData = ["$provider_id", null];
        }else{
            $key = 'selected.facility.' . Auth::user()->_id;
            $facility = Facility::find(Redis::get($key));
            $provider_id = array_get($facility->Provider, 'ProviderId');
            $providerData = ["$provider_id"];
        }

        $forms = AssessmentForm::whereOr('IsActive', 1)->whereOr('Status', 'published')
            ->whereIn('Provider',  $providerData)
            ->orderBy('FormName')
            ->get();
//        dd($providerData);

        $steps = SProcessSteps::where('Process.ProcessId', $processId)->orderby('DisplayOrder', 'asc')->get();
//        dd($steps);
        return view('sprocess.editstep', compact('sprocess', 'forms', 'steps'));
    }

    public function viewprocess($processId, $residentId=null){

        $sprocess = SProcess::find($processId);

        $forms = AssessmentForm::where('IsActive', 1)->orderBy('FormName')->get();

        $steps = SProcessSteps::where('Process.ProcessId', $processId)->orderby('DisplayOrder', 'asc')->get();
//        dd($steps);

        $obj = new IndicatorController();
        $residents = $obj->mostrecent();


        $bodymap = config('poc.bodymap');

        return view('sprocess.viewprocess',
            compact('sprocess', 'forms', 'steps',
                'residents', 'bodymap', 'residentId'));
    }

    public function addstep(Request $request){

        if(!empty($request->stepId)){
            $newstep = SProcessSteps::find($request->stepId);
            $num = $request->DisplayOrder;
        }else{
            $newstep = new SProcessSteps();
            $num = SProcessSteps::all()->count() + 1;
        }


        $process = SProcess::find($request->sprocessId);

        $newstep->DisplayOrder = $num;
        $newstep->Process = $process->Object;
        $newstep->Title = $request->Title;
        $newstep->Description = $request->Description;

        $forms = [];

        if(!empty($request->forms)){
            foreach($request->forms as $formid){
                $form = AssessmentForm::find($formid);
                $forms[] = $form->Object;
            }
        }


        $newstep->Forms = $forms;
        $newstep->save();

        return redirect(url('sprocess/editstep/'.$request->sprocessId))->with('status', 'Successfully Saved.');

    }

    public function deletestep($stepId, $sprocessId){

        SProcessSteps::find($stepId)->delete();

        return redirect(url('sprocess/step/'.$sprocessId))->with('status', 'Successfully Deleted.');
    }

    public function displayorder(Request $request){

        if(!empty($request->OrderList)){
            foreach($request->OrderList as $k=>$stepId){
                $process = SProcessSteps::find($stepId);
                $num = $k + 1;
                $process->DisplayOrder = (int)$num;
                $process->save();
            }

            return true;
        }

    }

    public function selectform($residentId, $formId, $processId)
    {
        $resident = Resident::find($residentId);

        $form = AssessmentForm::find($formId);
        $bodymap = config('poc.bodymap');

        $questions = FormQuestions::where('Form.FormId', $form->_id)->orderBy('DisplayOrder', 'asc')->get();

            return view('sprocess.selectform', [
                'resident' => $resident,
                'form' => $form,
                'data' => [],
                'bodymap' => $bodymap,
                'processId' => $processId,
                'questions' => $questions
            ]);

    }

    public function copy($processId){

        $process = SProcess::find($processId);
        $newProcess = $process->replicate();

        $key = 'selected.facility.' . Auth::user()->_id;
        $facility_id = Redis::get($key);
        if(empty($facility_id))return redirect(url('facility/select'));
        $facility =  Facility::find($facility_id);
        if(empty($facility)) redirect(url('facility/select'));
        $provider_id = array_get($facility->Provider, 'ProviderId');

        $newProcess->ProcessName = $process->ProcessName.' Copy';
        $newProcess->Provider = $provider_id;
        $newProcess->State = 'draft';
        $newProcess->save();

        $rows = SProcessSteps::where('Process.ProcessId', $processId)->get();

        if(!empty($rows)){
            foreach($rows as $row){
                $newStep = $row->replicate();
                $newStep->Process =  $newProcess->Object;
                $newStep->save();
            }
        }


        return back()->with('status', 'Successfully Copied.');

    }

    public function publish($processId, $state){

        $key = 'selected.facility.' . Auth::user()->_id;
        $facility_id = Redis::get($key);
        if(empty($facility_id))return redirect(url('facility/select'));
        $facility =  Facility::find($facility_id);
        if(empty($facility)) redirect(url('facility/select'));
        $provider_id = array_get($facility->Provider, 'ProviderId');

        $process = SProcess::find($processId);
        $process->Provider = $provider_id;
        $process->State = $state;
        $process->save();

        return back()->with('status', 'Successfully Saved.');
    }
}
