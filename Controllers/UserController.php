<?php

namespace App\Http\Controllers;

use App\Domains\User;
use App\Models\Facility;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redis;

class UserController extends Controller
{
    public $users;

    public function __construct()
    {
        $this->middleware('auth');


    }

    public function index(){

        $user = Auth::user();
        if(!in_array($user->UserType, [config('poc.system_admin'),
            config('poc.facility_admin'), config('poc.provider_admin')]))
            return redirect('user/notsystemadmin');

        if($user->UserType == config('poc.system_admin'))
            $users =  \App\User::all();
        else if(in_array($user->UserType , [config('poc.provider_admin'), config('poc.facility_admin')])){
            // find out default facility of this user
            $key = 'selected.facility.' . $user->_id;
            if(empty(Redis::get($key)))return redirect(url('facility/select'));
            $facilityId = Redis::get($key);
            $users = \App\User::whereIn('Facility.FacilityId', [$facilityId])->get();

        }
        return view('user.index', compact('users'));
    }

    public function notsystemadmin(){
        return view('user.notsystemadmin');
    }

    public function storeuser(Request $request){

        $email = $request->email;

        $newuser = User::where('email', $email)->first();

        if(empty($newuser)) return redirect('user')->with('warningstatus', 'Email Address does not exist.');

        $userid = Auth::user()->_id;
        $user = User::find($userid);

        $newuser->Parent = $user->Object;
        $newuser->save();

        return redirect('user')->with('status', 'Successfully added.');

    }

    public function facility($userid){

        $current_userid = Auth::user()->_id;
        $current_user = User::find($current_userid);
        $facility = (!empty($current_user->Facility)) ? $current_user->Facility : []; //Facility::where('Archive', '!=', 1)->get();

        $user = User::find($userid);
        $userfacilities = [];
        if(isset($user->Facility)){
            if(!empty($user->Facility)){
                foreach($user->Facility as $data){
                    $userfacilities[] = array_get($data, 'FacilityId');
                }
            }
        }

        return view('user.facility', compact('user', 'facility', 'userfacilities'));
    }

    public function role($userid){

        $key = 'selected.facility.' .Auth::user()->_id;

        $facilityId = Redis::get($key);
        $facility = Facility::find($facilityId);

        $providerId = isset($facility->Provider) ? array_get($facility->Provider, 'ProviderId') : '';

        $role = UserRole::where('Provider.ProviderId', $providerId)->get(); //(!empty($current_user->Roles)) ? $current_user->Roles : []; //Facility::where('Archive', '!=', 1)->get();

        $user = User::find($userid);
        $userroles = [];
        if(isset($user->Roles)){
            if(!empty($user->Roles)){
                foreach($user->Roles as $data){
                    $userroles[] = array_get($data, 'RoleId');
                }
            }
        }

        return view('user.role', compact('user', 'role', 'userroles'));
    }

    public function assignfacility(Request $request){

        $user = User::find($request->userid);

        $assignFacility = [];
        if(!empty($request->facilityIDs)){
            foreach($request->facilityIDs as $facility){

                $facility = Facility::find($facility);
                $assignFacility[] = $facility->Object;

            }
        }

        $user->Facility = $assignFacility;
        $user->save();
    }


    public function assignrole(Request $request){

        $user = User::find($request->userid);

        $assignRole = [];
        if(!empty($request->roleIDs)){
            foreach($request->roleIDs as $role){

                $role = UserRole::find($role);
                $assignRole[] = $role->Object;

            }
        }

        $defaultRole = UserRole::find($request->defaultRole);

        $user->Roles = $assignRole;
        $user->DefaultRole = $defaultRole->Object;
        $user->save();
    }

    public function profile(Request $request){
        return view('user.profile');
    }

    public function store(Request $request)
    {
        $email = $request->input('email');
        $name = $request->input('name');
        $timezone = $request->input('timezone');
        $date_format = $request->input('date_format');

        if(!empty($email) && !empty($name)){
            $user = \App\User::where('email', $email)->get()->first();
            if(!empty($user)){
                $user->name = $name;
                $user->timezone = $timezone;
                $user->date_format = $date_format;
                $user->save();

                $key = "$email.timezone";
                Redis::set($key, $timezone);
                Redis::expire($key, config('oidesk.expire-in-24hrs')); 

                $key = "$email.date_format";
                Redis::set($key, $date_format);
                Redis::expire($key, config('oidesk.expire-in-24hrs')); 

                return redirect('/home');
            }
        }
        return redirect('user/profile');
    }


    public function updateusers(){

        $users = User::whereIn('email', ['aldin.pagdonsolan@gmail.com', 'chang388@gmail.com'])->get();

        foreach($users as $user){

            $u = User::find($user->_id);
            $u->UserType = 'SystemAdmin';
            $u->save();

        }
    }


    public function admin(){

        $key = 'selected.facility.' .Auth::user()->_id;

        $facilityId = Redis::get($key);
        $facility = Facility::find($facilityId);

        $providerId = isset($facility->Provider) ? array_get($facility->Provider, 'ProviderId') : '';

        $users = User::where('UserType', '!=', config('poc.system_admin'))
            ->where('UserType', 'exists', false)
            ->get();

        $useradmins = User::whereIn('UserType',[config('poc.facility_admin'), config('poc.provider_admin')])->get();

//        dd($useradmins);
        return view('user.admin', compact('users', 'useradmins'));
    }

    public function assignadmin(Request $request){

        $admin = $request->admin;
        foreach($request->users as $k=>$v){

            $user = \App\User::find($v);
            $user->UserType = array_get($admin, $k);
            $user->save();
        }
    }

}
