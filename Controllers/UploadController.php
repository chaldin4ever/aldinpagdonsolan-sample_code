<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;
use Storage;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

class UploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * use by Froala editor to upload files
     *
     * @return \Illuminate\Http\Response
     */
    public function image(Request $request)
    {
        $file = $request->file('file_name');
        $originalFileName = $file->getClientOriginalName();
        $path = Storage::disk('s3')->put('upload', $request->file('file_name'), 
            [
                'visibility' => 'public',
                'Metadata' => [
                    'file_name' => $originalFileName
                ]
            ]);

        return ["link" => '/upload/get/'.substr($path,7)];
        // return ["link" => env('AWS_URL').'/'.$path];
    }

    public function get($key2){
        $key = "upload/".$key2;
        if(Storage::disk('s3')->exists($key)){
            $metadata = Storage::disk('s3')->getMetaData($key);
            // dd($metadata);
            $mimetype = array_get($metadata, 'mimetype');
            $filesize = array_get($metadata, 'size');
            $filename = array_get(array_get($metadata, 'metadata'), 'file_name');
            header('Content-Type: '.$mimetype);
            if(substr($mimetype,0,5) == 'image'){

            } else {
                header('Content-Disposition: attachment; filename="'.$filename.'"');
            }
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . $filesize);
            print (Storage::disk('s3')->get($key));
        } else {
            return [];
        }
    }

    // used by meltingpin
    public function store(Request $request)
    {
        $file = $request->file('file');
        // dd($file);
        // dd($file->getSize());
        if ($file == null || !$file->getSize()) {
            return "ERR";
        }
        //Display File Name
        $originalFileName = $file->getClientOriginalName();

        $data = file_get_contents($file);
        $encoded_data = base64_encode($data);

        $usedPins = Cache::get("pins");
        if (empty($usedPins)) {
            $usedPins = [];
        }


        $max = 1800;
        for ($n = 0; $n < $max; $n++) {
            $key = mt_rand(100001, 999999);
            if (!in_array($key, $usedPins)) {
                break;
            }
        }
        array_push($usedPins, $key);
        Cache::put("pins", $usedPins, $max * 2);

        // store if in Redis
        Redis::set($key, $encoded_data);
        Redis::expire($key, $max);

        $filename = $key . "_filename";
        Redis::set($filename, $originalFileName);
        Redis::expire($filename, $max);

        return $key;
    }

    public function download($key)
    {
        $string_content = Redis::get($key);
        if (empty($string_content)) {
            return "ERR";
        }
        $contents = base64_decode($string_content);

        $key = $key . "_filename";
        $originalFileName = Redis::get($key);
        // Storage::put($originalFileName, $contents);
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $originalFileName . "\"");
        return $contents;
        // Storage:download($originalFileName, $originalFileName);
        // readfile(storage_path("app/" . $originalFileName));

        // Storage::delete($originalFileName);
    }

    public function meltingpin(){
        return view('upload.meltingpin');
    }
}
