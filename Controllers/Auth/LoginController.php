<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Facility;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use Redis;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function validateLogin(Request $request)
    {

//        dd($request);
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ? : redirect()->intended($this->redirectPath());
    }

    public function authenticated(Request $request, $user)
    {
        if ($user->activation_code) {
            auth()->logout();
            return back()->with('message', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }

/*        $key = 'selected.facility.' . $user->_id;
//        dd($key);
        $facilityId = $request->facility;
        if(empty(Redis::get($key))){
            Redis::set($key, $facilityId);
            Redis::expire($key, 43200);
        }*/

        return redirect()->intended($this->redirectPath());
    }

    public function showLoginForm()
    {

//        $facilities = Facility::where('Archive', '!=', 0)->get();

        return view('auth.login'/*, compact('facilities')*/);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->user();

         $user->name;
    }

    public function redirect($service) {
        return Socialite::driver ( $service )->redirect ();
    }

    public function callback($service) {

        $user = Socialite::with ( $service )->user ();


        $authUser = $this->findOrCreateUser($user, $service);

        if(!$authUser) return redirect(url('login'))->with('status', 'Email already exist.');

        Auth::login($authUser, true);

        return redirect ( 'home' );
    }

    public function findOrCreateUser($user, $provider)
    {

        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }

        $checkemail = User::where('email', $user->email)->first();
        if(!empty($checkemail)) return false;

        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }
}
