<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Resident;
use App\User;
use App\Utils\Reports;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

//use Debugbar;

class ReportWeightController extends Controller
{

    public $facility_id;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(){

        $facility = Toolkit::getFacility();
        $facilityId = $facility->_id;

        $dateFilter = Reports::dateFilter();

        $dates = array_get($dateFilter, 'dates');
        $moyrArr =array_get($dateFilter, 'moyrArr');
        $dt = array_get($dateFilter, 'dt');
        $et = array_get($dateFilter, 'et');

        $residents = Resident::orderby('CurrentRoom.Room', 'asc')
                ->where('Facility.facilityId', $facilityId)->get();

//        dd($residents);
        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('report.weight', compact('residents', 'fromDate', 'toDate'));

    }

    public function weight($residentId)
    {

        $moyrArr = array_get(Reports::dateFilter(), 'moyrArr');

        $weightFormID = config('poc.WEIGHT_FORM_ID'); //1648
        $date_field = config('poc.WEIGHT_FORM_DATE_FIELD');
        $date_field_utc = config('poc.WEIGHT_FORM_DATE_FIELD_UTC');
        $field = config('poc.WEIGHT_FORM_FIELD');

        $assessments = Assessment::where('Form.FormID', $weightFormID)
            ->where('Resident.ResidentId', $residentId)
            ->get();

        $weight_data = []; $weight_significant = []; $weight_consecutive = [];
        foreach($assessments as $assm){

            $date = new Carbon(array_get($assm->data, $date_field));
            $moyr = $date->format('Ym');
            $weight_value = array_get($assm->data, $field );

            if(!empty($weight_value)){

                $weightDate = new Carbon(array_get($assm->data, $date_field));
                $last3mo_weight = $this->getLast3MoWeight($residentId, $weightFormID, $field, $date_field_utc, $weightDate->subMonths(3));

                $significant  = (($weight_value - ($last3mo_weight - 1)) >= 3) ? $weight_value : 0;
//                dd($significant);

                if($significant > 0){
                    $weight_significant[$moyr][] = [
                        'Date' => array_get($assm->data, $date_field),
                        'Weight' => $significant,
                    ];
                }

                $weightDate = array_get($assm->data, $date_field_utc);
                $last_weight = $this->getLastWeight($residentId, $weightFormID, $field, $date_field_utc, $weightDate);
                $consecutive  = (($weight_value - $last_weight) >= 3) ? $weight_value : 0;

                if($consecutive > 0){
                    $weight_consecutive[$moyr][] = [
                        'Date' => array_get($assm->data, $date_field),
                        'Weight' => $consecutive,
                    ];
                }

            }

        }

        $label = []; $dataset1 = [];  $dataset2 = [];
        $weight_significant_data =[]; $weight_consecutive_data = [];

        foreach($moyrArr as $my=>$val){
            $monthyear = array_get($val, 'MonthYear');

            $label[] = array_get($val, 'MonthYear');
            $total1 =  Toolkit::smart_count(array_get($weight_significant, $my));
            $dataset1[] = $total1;
            $weight_significant_data[$my] = ['count'=>$total1, 'monthyear' => $monthyear, 'key'=>$my];

            $total2 =  Toolkit::smart_count(array_get($weight_consecutive, $my));
            $dataset2[] = $total2;
            $weight_consecutive_data[$my] = ['count'=>$total2, 'monthyear' => $monthyear, 'key'=>$my];

            $weight_data[$my] = [
                'monthyear' => $monthyear,
                'Significant' => ['count'=>$total1, 'key'=>$my],
                'Consecutive' => ['count'=>$total2,  'key'=>$my]
            ];

        }

        $options = array_get(Reports::Options(), 'option1');
        $title1 = 'Significant Unplanned Weight loss';

        $options2 = array_get(Reports::Options(), 'option2');
        $title2 = 'Consecutive Unplanned Weight loss';

        $weightchart = Reports::chart('line', $label, $dataset1, $title1, $options,
            $dataset2, $title2, $options2
        );

        $resident = Resident::find($residentId);
        $residentName = $resident->FullName;
        return view('report.residentWeight', compact('weightchart', 'residentName'));

    }

    public function getLastWeight($residentId, $weightFormID, $field, $date_field_utc, $weightDate){

        $assessment = Assessment::where('Form.FormID', $weightFormID)
            ->where('Resident.ResidentId', $residentId)
            ->where("data.$date_field_utc", '<', $weightDate)
            ->orderby("data.$date_field_utc", 'desc')
            ->first();

        if(!empty($assessment))
            return array_get($assessment->data, $field);

    }

    public function getLast3MoWeight($residentId, $weightFormID, $field, $date_field_utc, $weightDate){

        $assessment = Assessment::where('Form.FormID', $weightFormID)
            ->where('Resident.ResidentId', $residentId)
            ->where("data.$date_field_utc", '<=', $weightDate)
            ->orderby("data.$date_field_utc", 'desc')
            ->first();

        if(!empty($assessment))
            return array_get($assessment->data, $field);

    }

}