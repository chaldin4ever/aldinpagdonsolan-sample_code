<?php

namespace App\Http\Controllers;

use App\Domains\User;
use App\Models\Facility;
use App\Models\PinboardItem;
use App\Models\ProgressNote;
use App\Models\ProgressNoteComment;
use App\Models\Resident;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redis;

class ProgressNoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getLast10($residentId){

        $progressnotes = ProgressNote::orderby('created_at', 'desc')
                    ->where('Resident.ResidentId', $residentId)
                    ->take(10)->get();

        return $progressnotes;

//
    }


    public static function getAll($residentId, $dt, $et, $category, $user, $endofshift){

        $progressnotes = ProgressNote::orderby('created_at', 'desc')
            ->where('Resident.ResidentId', $residentId)
            ->whereBetween('created_at', [$dt, $et]);

        if($category){
            $progressnotes = $progressnotes->whereIn('Form.Category', $category);
        }

        if($user){
            $progressnotes = $progressnotes->where('User.FullName', 'LIKE', "%$user%");
        }

//        dd($endofshift);
        if($endofshift == 'true'){
            $progressnotes = $progressnotes->where('EndOfShift', $endofshift);
        }

        $progressnotes = $progressnotes->take(config('poc.progress_note_limit'))->get();

        return $progressnotes;

//
    }

    public function store(Request $request){

        if(isset($request->pnoteId)){
            if(!empty($request->pnoteId)){
                $pnote = ProgressNote::find($request->pnoteId);
            }
        }else{
            $pnote = new ProgressNote();
        }


        $userid = Auth::id();
        $user = User::find($userid);
        $pnote->User = $user->Object;
        $residentId = $request->residentId;
        $resident = Resident::find($residentId);
        $pnote->Resident = $resident->Object;
        $pnote->Notes = $request->notes;
        $pnote->Condition = $request->condition;
        $pnote->ActionTaken = $request->actionTaken;
        $pnote->Response = $request->response;
        $pnote->EndOfShift = $request->endofshift;
        $pnote->Archive = 0;
        $pnote->save();

        if($request->quicknote == 'true') return $pnote;

        $progressnotes = self::getLast10($residentId);

        $fromDate = $request->fromDate;
        $toDate = $request->toDate;

        return view('assessment.tabs.progressnotes',
            compact('progressnotes','fromDate', 'toDate'));
    }

    public function storepinboard(Request $request){

        $key = 'selected.facility.' . Auth::user()->_id;
        $facilityId = Redis::get($key);
        $facility = Facility::find($facilityId);
        $user = User::find(Auth::user()->_id);
        $resident = Resident::find($request->residentId);

        $item = new PinboardItem();
        $item->Facility = $facility->Object;
        $item->Resident = $resident->Object;
        $item->CreatedBy = $user->Object;
        $item->action_date = Toolkit::CarbonToUTCDateTime(Carbon::now());
//        $item->user_roles = $this->user_roles;
        $item->note = $request->note;
        $item->item_type = 'progress note';
        $item->state = 'new';
        $item->save();

    }

    public function commentstore(Request $request){

        $pnId = $request->progressNoteId;
        $comment = $request->comment;

        $pnote = ProgressNote::find($pnId);

        $pnoteComment = new ProgressNoteComment();
        $pnoteComment->ProgressNote = $pnote->Object;
        $pnoteComment->Resident = $pnote->Resident;
        $pnoteComment->Comment = $comment;

        $userid = Auth::id();
        $user = User::find($userid);
        $pnoteComment->CreatedBy = $user->Object;
        $pnoteComment->save();

        return $pnoteComment;

    }

    public static function getComments($pnId){

        $comments = ProgressNoteComment::where('ProgressNote.progressNoteId', $pnId)->get();

        return $comments;

    }

    public static function commentshow($pnId){

        $comments = self::getComments($pnId);
        return view('assessment.tabs.progressnotes_comment', compact('pnId', 'comments'));
    }

    public function archive(Request $request){

        $pnId = $request->pnId;

        $note = ProgressNote::find($pnId);
        $note->Archive = 1;
        $note->ArchiveReason = $request->reason;
        $user = User::find(auth::id());
        $note->ArchivedBy = $user->Object;
        $note->ArchivedDate = Carbon::now();
        $note->save();

        $archive = [
            'user' => array_get($note->ArchivedBy, 'FullName'),
            'date'  => Toolkit::CarbonToDateFullString($note->ArchivedDate),
            'reason' => $note->ArchiveReason
        ];

        return json_encode($archive);
    }

}
