<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Resident;
use App\Models\Rooms;
use App\User;
use App\Utils\Toolkit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Redis;

class RoomsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($facilityId){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $facility = Facility::find($facilityId);
        $rooms = Rooms::where('Facility', $facilityId)->get();

        return view('rooms.index', compact('rooms', 'facilityId', 'facility'));
    }

    public function add($facilityId){


        $facility = Facility::where('Archive', '!=', 1)->get();

        $residents = Resident::where('Facility.facilityId', $facilityId)->get();

        $order = (Rooms::where('Facility', $facilityId)->count()) + 1;
        $selected_facility = Facility::find($facilityId);

        return view('rooms.add', compact('facility', 'facilityId', 'residents', 'order', 'selected_facility'));
    }

    public function edit($roomId){

        $key = 'selected.facility.' . Auth::user()->_id;
        $facilityId = Redis::get($key);
        $facility = Facility::where('Archive', '!=', 1)->get();

        $residents = Resident::where('Facility.facilityId', $facilityId)->get();

        $room = Rooms::find($roomId);
//            dd($room);
        return view('rooms.add', compact('facility', 'facilityId', 'residents', 'room'));
    }

    public function store(Request $request){

        $room = new Rooms();
        $room->Facility = $request->Facility;
        $room->Area = $request->Area;
        $room->RoomName = $request->RoomName;
        $room->DisplayOrder = $request->DisplayOrder;
        $room->Vacant = $request->Vacant;
        $room->CurrentResident = $request->CurrentResident;
        $room->save();

        if(!empty($request->CurrentResident)){
            $resident = Resident::find($request->CurrentResident);
            $resident->Room = $room->_id;
            $resident->save();
        }


        return redirect(url('rooms/'.$request->Facility))->with('status', 'Successfully Saved.');

    }

    public function getarea($facilityid, $areaId){

        $facility = Facility::find($facilityid);
        $areas = isset($facility->Areas) ? $facility->Areas : [];
//        dd($areas);

        return view('rooms.area', compact('areas', 'areaId'));
    }

    public function vacantrooms(){

        $facility = Toolkit::getFacility();

        $rooms = Rooms::where('Facility', $facility->_id)
                ->where('CurrentResident', null)
                ->get();

        return view('rooms.vacantrooms', compact('rooms', 'facility'));

    }

}