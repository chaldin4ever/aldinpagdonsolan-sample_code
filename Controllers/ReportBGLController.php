<?php

namespace App\Http\Controllers;

use App\Charts\POCChart;
use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Resident;
use App\Models\ResidentBGL;
use App\User;
use App\Utils\Reports;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

//use Debugbar;

class ReportBGLController extends Controller
{

    public $facility_id;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(){

        $facility = Toolkit::getFacility();
        $facilityId = $facility->_id;

        $dateFilter = Reports::dateFilter();

        $dates = array_get($dateFilter, 'dates');
        $moyrArr =array_get($dateFilter, 'moyrArr');
        $dt = array_get($dateFilter, 'dt');
        $et = array_get($dateFilter, 'et');

        $residents = Resident::orderby('CurrentRoom.Room', 'asc')
            ->where('Facility.facilityId', $facilityId)->get();

//        dd($residents);
        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('report.bgl', compact('residents', 'fromDate', 'toDate'));

    }

    public function bgl($residentId){

        $dateFilter = Reports::dateFilter();

        $moyrArr =array_get($dateFilter, 'moyrArr');


        $resident = Resident::find($residentId);
        $residentName = $resident->FullName;
        $formID = config('poc.BGL_CHART_FORM_ID');
        $date_submit_utc = config('poc.BGL_CHART_DATE_FIELD_UTC');

        $bgl = Assessment::where('Form.FormID', $formID)
                ->where('Resident.ResidentId', $residentId)
                ->orderby('data.'.$date_submit_utc, 'asc')
                ->get();

        $bglchart = $this->bgldataset($bgl);

        return view('report.residentBGL', compact('residentName',  'bglchart'));

    }

    public function bgldataset($dataArr){

        $date_field = config('poc.BGL_CHART_DATE_FIELD');
        $time_field = config('poc.BGL_CHART_TIME_FIELD');
        $bgl = config('poc.BGL_CHART_FIELD');

        $dataset = []; $label = [];
        if(!empty($dataArr)){

//            dd($dataArr);
            foreach($dataArr as $data){

                $time = array_get($data->data, $time_field);
                $nt = date("H:i", strtotime($time));
                $date = Toolkit::ConvertStringDateTimeToCarbon(array_get($data->data, $date_field), $nt);

                $label[] = array_get($date, 'datetime'); //$date.' '.array_get($data->data, 'Time');
                $dataset[] = array_get($data->data, $bgl);
            }

            $chart = new POCChart();
            $chart->dataset('BGL', 'line', $dataset);
            $chart->labels($label);

            return $chart;
        }

    }

}