<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\Jobs;
use App\User;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $facility = Toolkit::getFacility();

        $jobs = Jobs::orderby('created_at', 'desc')
            ->orderby('JobTitle', 'asc')
            ->where('Facility.FacilityId', $facility->_id)
            ->get();

        $expiryDate = Carbon::now()->addMonth(1)->format('m/d/Y');

        return view('jobs.index', compact('jobs', 'expiryDate'));
    }

    public function store(Request $request)
    {

        $facility = Toolkit::getFacility();

        if (!empty($request->jobId)) {
            $jobs = Jobs::find($request->jobId);
        } else {
            $jobs = new Jobs();
        }

        $jobs->Facility = $facility->Object;

        $jobs->JobTitle = $request->JobTitle;
        $jobs->Description = $request->Description;
        $jobs->ExpiryDateFormatted = $request->ExpiryDate;
        $expydate = new Carbon($request->ExpiryDate_submit);
        $jobs->ExpiryDate = Toolkit::CarbonToUTCDateTime($expydate);
        $jobs->ExpiryDate_submit = $request->ExpiryDate_submit;
        $jobs->Status = $request->Status;
        $user = User::find(Auth::user()->_id);
        $jobs->CreatedBy = $user->Object;
        $jobs->save();

        return redirect(url('intranet/jobs'))->with('status', 'Saved Successfully.');

    }

    public function getdata($id){

        $jobs = Jobs::find($id);

        return $jobs;

    }

    public function view($id){

        $job = Jobs::find($id);

        return view('jobs.view', compact('job'));

    }

    public function archive($id){

        $jobs = Jobs::find($id);
        $jobs->Archive = true;
        $jobs->save();

        return redirect(url('intranet/jobs'))->with('status', 'Archived Successfully.');


    }
}