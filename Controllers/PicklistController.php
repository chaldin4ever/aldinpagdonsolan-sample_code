<?php

namespace App\Http\Controllers;

use App\Domains\AssessmentForm;
use App\Models\FormPicklistMap;
use App\Models\Picklist;
use App\Models\PicklistParent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redis;

class PicklistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $picklists = Picklist::all();

        return view('picklist.index', compact('picklists'));
    }

    public function store(Request $request){

//        dd($request->all());
        if($request->picklistId == null){
            $picklist = new Picklist();
        }else{

            $picklist = Picklist::find($request->picklistId);

//            $this->dynamicupdate($request->picklistId, $request->Picklists);
//            $this->updateformpicklistmap($request->picklistId, $request->Picklists);

        }

        if($request->Archive == 'Yes'){
            $picklist->Archive = 'Yes';
        }else{
            $picklist->PicklistName = $request->PicklistName;



            $lists = []; $codelist = $request->Code;

            if(!empty($request->Picklists)){
                foreach($request->Picklists as $k=>$pl){
//                    $randnum = rand ( 10000 , 99999 );
//                    $code = strtoupper(substr($request->PicklistName, 0, 3)).'-'.$randnum;
                    $lists[] = [
                        'code' => $codelist[$k],
                        'text' => $pl
                    ];
                }
            }


            $picklist->Lists  = $lists;
            $picklist->Archive = 'No';
        }

        $picklist->save();

        return redirect('picklist')->with('status', 'Successfully Saved.');

    }

    public static  function getAutocompleteList(){

        $picklists = Picklist::where('Archive', 'No')->get();

        $list = [''];
        if(!empty($picklists)){
            foreach($picklists as $pl){
                $list[$pl->_id] = $pl->PicklistName;
            }
        }

        return json_encode($list);
    }

    public function getpicklist($picklistid){

        $picklist = Picklist::find($picklistid);

        return json_encode($picklist);
    }

    public function storeformpicklistmap(Request $request){

        $plId = $request->picklistid;
        $form = AssessmentForm::find($request->formid);

                $picklist = Picklist::find($plId);
                $map = new FormPicklistMap();
                $map->Form = $form->Object;
                $map->Picklist = !empty($picklist) ? $picklist->Object : [];
                $map->Lists = !empty($picklist) ?  $picklist->Lists : [];
                $map->save();

    }

    public function updateformpicklistmap($picklistid, $picklists){

        $map = FormPicklistMap::where('Picklist.picklistId', $picklistid);

        $plist = Picklist::find($picklistid);
        $picklistmap = FormPicklistMap::where('Picklist.picklistId', $picklistid)->get();


        if(sizeof($picklists) != sizeof($plist->Lists)){

            foreach($picklistmap as $p){

                $updatemap = FormPicklistMap::find($p->_id);
                $updatemap->Lists = $picklists;
                $updatemap->save();
            }

            /*$forms = AssessmentForm::find(array_get($p->Form, 'FormId'));
            $forms_update = AssessmentForm::where('Form.FormId', array_get($p->Form, 'FormId'));
            $code = strtoupper(substr($forms->FormName, 0, 3));

            foreach($forms->template_json as $key=>$questions){

                $fields = array_get($questions, 'fields');

                if(!empty($fields)){
                    foreach($picklists as $k=>$v){
                        $randnum = rand ( 10000 , 99999 );
                        $data[] = [
                            'picklist' => $picklistid,
                            'code' => $code.'-'.$randnum,
                            'text' => $v,
                            'goal' => ''
                        ];
                    }

                    //to be continued
                    if($picklistid == array_get($fields, 'picklist')){
                        $forms_update->update(array("template_json.$key.fields" => $data));
                    }

                }

            }*/



        }else{
            foreach($picklists as $k=>$v){
                $map->update(array("Lists.$k" => $v));
            }
        }

    }

    public function dynamicupdate($picklistid, $picklistArr){

        $forms = AssessmentForm::all();

        $fieldskey= ['picklist', 'text'];

        foreach($forms as $form){

            $dynamicupdate = AssessmentForm::where('_id',$form->_id);

            $formjson = $form->template_json;

            foreach($picklistArr as $index=>$text){

                foreach($formjson as $key=>$fj){

                    $fields = array_get($fj, 'fields');

                        if(!empty($fields)){

                                foreach($fields as $field=>$dataArr){

                                    $id = $picklistid.'-'.$index;

                                    if(array_get($dataArr, 'picklist') == $id) {

                                        foreach ($dataArr as $k => $v) {

                                            if (in_array($k, $fieldskey)) {

                                                if ($k == 'text') {
                                                    $dynamicupdate->update(array("template_json.$key.fields.$field.$k" => $text));
                                                }

                                            }
                                        }
                                    }
                                }
                         }
                    }
                }

        }
    }

}
