<?php

namespace App\Http\Controllers;

use App\Mail\NotifyFacilityUser;
use App\Mail\NotifyProviderUser;
use App\Models\Doctor;
use App\Models\Facility;
use App\Models\Journal;
use App\Models\MedicationProfile;
use App\Models\MedicationProfileTransaction;
use App\Models\Patient;
use App\Models\Resident;
use App\Models\UserInbox;
use App\Models\UserInboxReply;
use App\Models\UserSent;
use App\User;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Redis;

class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $users = User::all();

        foreach($users as $user){
            $name = $user->email;
            if(Auth::user()->_id != $user->_id) $userdata[$user->_id] = $name;
        }

        $userid =  Auth::user()->_id;
        $user = User::find($userid);

        $facility = Facility::all();
        $currentFacility = Facility::find(Redis::get($key));

        $totalinbox = UserInbox::where('ToUser.UserId', $user->_id)
            ->where('Parent', 'exists', false)
            ->where('Status', '!=', 'sent')
            ->get()->count();

        $totalsent = UserInbox::orderby('created_at', 'desc')
            ->where('Status','sent')
            ->where('ToUser.UserId', $userid)->count();

        $inboxMessages = UserInbox::orderby('created_at', 'desc')
            ->where('Status',  '!=', 'sent')
                        ->where('ToUser.UserId', $userid)->get();

        $sentMessages = UserInbox::orderby('created_at', 'desc')
            ->where('Status', 'sent')
            ->where('ToUser.UserId', $userid)->get();

//dd($sentMessages);
        return view('messages.index',
            compact('userdata',
                'totalinbox', 'totalsent',
                'inboxMessages',
                'facility', 'currentFacility', 'sentMessages'));

    }

    public function getfacilityresident($facilityId){

        $residents = Resident::where('Facility.facilityId', $facilityId)->get();

        return view('messages.facilityresident', compact('residents'));
    }

    public function store(Request $request){

//        $currentInbox = UserInbox::find($request->InboxId);
//        $touser = User::find(array_get($currentInbox->FromUser, 'UserId'));
//        dd($touser);
        $touser = User::where('email', $request->User)->first();

        $userid =  Auth::user()->_id;
        $user = User::find($userid);
//        dd($request->all());
        $inbox = new UserInbox();
        $inbox->ToUser = $touser->Object; //!empty($touser->Object) ? $touser->Object : [];
        $inbox->FromUser = $user->Object;
        $inbox->Subject = $request->Subject;
        $inbox->Message = $request->Message;

        $facility = Facility::find($request->Facility);
        $inbox->Facility = !empty($facility) ? $facility->Object : null;

        $residentArr = [];
        if(isset($request->residents)){
            foreach($request->residents as $r){
                $resident = Resident::find($r);
                $residentArr[] = $resident->Object;
            }
        }
        $inbox->Residents = $residentArr;
        $files = $request->file('files');
        $inbox->Documents =  $this->upload_document($files);
        $inbox->Archive = 0;
        $inbox->Read = 0;
        $inbox->Status = 'inbox';
        $inbox->save();

        $sent = new UserInbox();
        $sent->FromUser =  $touser->Object;
        $sent->ToUser = $user->Object;
        $sent->Subject = $request->Subject;
        $sent->Message = $request->Message;
        $sent->Facility = $request->Facility;
        $sent->Residents = $request->Residents;
        $files = $request->file('files');
        $sent->Documents =  $this->upload_document($files);
        $sent->Archive = 0;
        $sent->Status = 'sent';
        $sent->Inbox = $inbox->Object;
        $sent->save();

        $updateInbox = UserInbox::find($inbox->_id);
        $updateInbox->Inbox = $sent->Object;
        $updateInbox->save();

        return redirect('messages')->with('status', 'Message Sent.');

    }

    public function upload_document($files){

//        dd($files);
        if(!empty($files)){
            foreach ($files as $file) {

                $filename = $file->getClientOriginalName();
                Storage::disk('s3')->put('upload', $file,
                    [
                        'visibility' => 'public',
                        'Metadata' => [
                            'file_name' => $filename
                        ]
                    ]);

                $document = file_get_contents($file);
                $base64File = base64_encode($document);
                $ext = $file->getClientOriginalExtension();

                $filedocs[] = [
                    'FileName' => $filename,
                    'File' => $base64File,
                    'FileType' => $file->getMimeType()];

            }

            return $filedocs;

        }
    }

    public function getinbox(){
        $userid =  Auth::user()->_id;
        $inboxMessages = $inboxMessages = UserInbox::orderby('updated_at', 'desc')
            ->where('Status',  '!=', 'sent')
            ->where('ToUser.UserId', $userid)->get();

        return view('messages.inbox', compact('inboxMessages'));
    }

    public function sentinbox(){
        $userid =  Auth::user()->_id;
        $sentMessages = UserInbox::orderby('updated_at', 'desc')
            ->where('Status', 'sent')
            ->where('ToUser.UserId', $userid)->get();

        return view('messages.sent', compact('sentMessages'));
    }

    public function getmessage($id){
       $message = UserInbox::find($id);

       return $message;
    }

    public function file($id, $k){
        $message = UserInbox::find($id);
        $document = array_get($message->Documents, $k);

        return view('messages.file', ['document'=>(object)$document]);

    }

    //get message with all replies
    public function replies($inboxId){

        $this->updateread($inboxId, 1);

        $messages = UserInboxReply::where('Inbox.InboxId', $inboxId)->get();

        return view('messages.replies', compact('messages'));
    }

    public function updateread($inboxId, $v){

        $inbox = UserInbox::find($inboxId);
        $inbox->Read = $v;
        $inbox->save();
    }

    public function store_reply(Request $request){

        $inboxid = $request->InboxId;
        $userinbox = UserInbox::find($inboxid);
//        $this->updateread($inboxid, 0);

        $touserid = array_get($userinbox->FromUser, 'UserId');
        $userid =  Auth::user()->_id;
        $user = User::find($userid);

        $touser = User::find($touserid);

        $inbox = new UserInboxReply();
        $inbox->Inbox = $userinbox->Object;
        $inbox->ToUser = $touser->Object;
        $inbox->FromUser = $user->Object;
        $inbox->Subject = $request->Subject;
        $inbox->Message = $request->Message;
        $inbox->Archive = 0;
        $inbox->Read = 0;
        $inbox->save();

        $sentinbox = new UserInbox();
        $sentinbox->Inbox = $userinbox->Object;
        $sentinbox->ToUser = $user->Object;
        $sentinbox->FromUser = $touser->Object;
        $sentinbox->Subject = $request->Subject;
        $sentinbox->Message = $request->Message;
        $sentinbox->Status = 'sent';
        $sentinbox->Archive = 0;
        $sentinbox->Read = 0;
        $sentinbox->save();

        $senderinbox = UserInbox::where('Inbox.InboxId', $inboxid)->first();
        $newinbox = $senderinbox->replicate();
        $newinbox->save();
        $senderinbox->Status = 'inbox';
        $senderinbox->Read = 0;
        $senderinbox->save();

        $inbox = new UserInboxReply();
        $inbox->Inbox = $senderinbox->Object;
        $inbox->ToUser = $touser->Object;
        $inbox->FromUser = $user->Object;
        $inbox->Subject = $request->Subject;
        $inbox->Message = $request->Message;
        $inbox->Archive = 0;
        $inbox->Read = 0;
        $inbox->save();

    }

    public function archive($inboxId){

        $inbox = UserInbox::find($inboxId);
        $inbox->Archive = 1;
        $inbox->save();

        return $inboxId;
    }

    public function request_access(Request $request){
        // TODO: complete this function

        $requested_facility_id = $request->facility_id;
        $requested_facility_name = $request->facilityName;
        $fromEmail =$request->fromEmail;
        $fromUser = User::where('email', $fromEmail)->first();
        $userFullName = $request->userFullName;
        // find facility admin, if any
        // if there is no facility admin, it means this facility is yet to subscribe to ECCA, ignore the request

        $users = User::whereIn('UserType', [config('poc.facility_admin'), config('poc.provider_admin')])->get();

        $facilityAdmin = [];
        foreach($users as $user){

            if(!empty($user->Facility)){
                foreach($user->Facility as $fa){
                    $facilityId = array_get($fa, 'FacilityId');
                    $facility = Facility::find($facilityId);
                    if(isset($facility->ServiceList)){
                        if(!empty($facility->ServiceList)){
                            $serviceListId = array_get($facility->ServiceList, 'ServiceListId');

                            if($serviceListId == $requested_facility_id){
                                //send request
                                // post a message to this facility admin
                                $this->send_request_email($user->email, $userFullName, $fromEmail, $facility);

                            }
                        }
                    }
                }
            }
        }
    }

    public function send_request_email($toEmail, $userFullName, $fromEmail, $facility){

        $touser = User::where('email', $toEmail)->first();
        $inbox = new UserInbox();
        $inbox->ToUser = $touser->Object; //!empty($touser->Object) ? $touser->Object : [];
        $inbox->FromUser = ['UserId'=> 'system', 'FullName'=> 'System Admin'];
        $inbox->Subject = 'Facility Request';

        $fromuser = User::where('email', $fromEmail)->first();
        $url = url('messages/grantaccess/'.$fromuser->_id.'/'.$touser->_id.'/'.$facility->_id);
        $access_btn = "<a href='$url' target='_blank' class='btn mdb-color btn-sm'>Click here to give access to user</a>";
        $system_message ="$userFullName would like to request access to $facility->NameLong facility<br /> $access_btn ";
        $inbox->Message = $system_message;
        $inbox->save();

        // send email to Provider
        Mail::send(new NotifyProviderUser($fromuser, $touser, $facility));
    }

    public function grant_access($from_userid, $to_userid, $facilityId){

        $newuser = User::find($from_userid);
        $touser = User::find($to_userid);

        $facility = Facility::find($facilityId);

        $flist = [];
        if(!empty($newuser->Facility)){
            foreach($newuser->Facility as $f){
                $flist[] = array_get($f, 'FacilityId');
            }
        }

        if(!in_array($facilityId, $flist)){
            $newuser->Parent = $touser->Object;

            if(!empty($newuser->Facility)){
                $fa = [];
                foreach($newuser->Facility as $fac){
                    $fa[] = $fac;
                }
                $num = count($newuser->Facility) + 1;
                $fa[$num] = $facility->Object;
            }else{
                $fa[] = $facility->Object;
            }

            $newuser->Facility = $fa;
            $newuser->save();

            $this->send_granted_email($newuser->email, $newuser->name, $touser->email, $facility);

            $info = 'Facility access was successfully granted.';
            return view('messages.grantaccess', compact('info'));
        }else{
            $info = 'Facility already exist.';
            return view('messages.grantaccess', compact('info'));
        }

    }

    public function send_granted_email($toEmail, $userFullName, $fromEmail, $facility){

        $touser = User::where('email', $toEmail)->first();
        $inbox = new UserInbox();
        $inbox->ToUser = $touser->Object; //!empty($touser->Object) ? $touser->Object : [];
        $inbox->FromUser = ['UserId'=> 'system', 'FullName'=> 'System Admin'];
        $inbox->Subject = 'Facility request have been granted.';

//        $notifyuser = User::where('email', $fromEmail)->first();

        Mail::send(new NotifyFacilityUser($touser, $facility));

        $system_message ="Hi $userFullName, your request to access $facility->NameLong facility has been granted. Congratulations!";
        $inbox->Message = $system_message;
        $inbox->save();

//        return[$notifyuser, $facility];
    }

}