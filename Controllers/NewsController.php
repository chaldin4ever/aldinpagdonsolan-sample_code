<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\Models\News;
use App\User;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){

        $facility = Toolkit::getFacility();

        $news = News::orderby('created_at', 'desc')->orderby('NewsTitle', 'asc')
            ->where('Facility.FacilityId', $facility->_id)
            ->get();

        $expiryDate = Carbon::now()->addMonth(1)->format('m/d/Y');

        return view('news.index', compact('news', 'expiryDate'));
    }

    public function store(Request $request){

        $facility = Toolkit::getFacility();

        if (!empty($request->newsId)) {
            $news = News::find($request->newsId);
        } else {
            $news = new News();
        }
        $news->Facility = $facility->Object;
        $news->NewsTitle = $request->NewsTitle;
        $news->Content = $request->Content;
        $news->Announcement = !empty($request->Announcement) ? true : false;
        $news->ExpiryDateFormatted = $request->ExpiryDate;
        $expydate = new Carbon($request->ExpiryDate_submit);
        $news->ExpiryDate = Toolkit::CarbonToUTCDateTime($expydate);
        $news->ExpiryDate_submit = $request->ExpiryDate_submit;
        $news->Status = $request->Status;
        $user = User::find(Auth::user()->_id);
        $news->CreatedBy = $user->Object;
        $news->save();

        return redirect(url('intranet/news'))->with('status', 'Saved Successfully.');

    }

    public function getdata($id){

        $news = News::find($id);

        return $news;

    }

    public function view($id){

        $news = News::find($id);

        return view('news.view', compact('news'));

    }


}