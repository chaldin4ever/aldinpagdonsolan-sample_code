<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\User;
use App\Utils\ConfigForm;
use App\Utils\Reports;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
//use Debugbar;

class ReportInfectionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(){

        $facility = Toolkit::getFacility();
        $facilityId = $facility->_id;

        $dateFilter = Reports::dateFilter();

        $dates = array_get($dateFilter, 'dates');
        $moyrArr =array_get($dateFilter, 'moyrArr');
        $dt = array_get($dateFilter, 'dt');
        $et = array_get($dateFilter, 'et');

        $infection = $this->infections($dates, $moyrArr, $facilityId);
        $infectionchart = array_get($infection, 'chart');
        $infectiondata = array_get($infection, 'data');
        $infectionbytypechart = array_get($infection, 'bytype_chart');
        $infectionbytypedata = array_get($infection, 'bytype_data');
        $message = array_get($infection, 'message');

        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        return view('report.infections', compact('infectionchart','infectiondata',
            'infectionbytypechart', 'infectionbytypedata','message',
            'fromDate', 'toDate'));
    }

    public function infections($dates, $moyrArr, $facilityId){


        $question_code = config('poc.INFECTION_QUESTION_CODE');
        $form = ConfigForm::getForm('INCIDENT_FORM_ID');
        $formID = array_get($form, 'FormID');

        $assessments = Assessment::where('Form.FormID', $formID)
            ->whereBetween("updated_at", $dates)
            ->where('Facility.FacilityId', $facilityId)
            ->get();

        $formId = array_get($form, 'FormId');
        $fields = ConfigForm::getFormQuestionFields($formId, $question_code);
//        dd($fields);
        $infection_data = [];
        foreach($assessments as $assm){

            $date = $assm->updated_at;
            $moyr = $date->format('Ym');

            foreach($assm->data as $k=>$v){

                if(in_array($k, array_keys($fields))){

                    $infection = $v;

                    if($infection == 'on'){
                        $infection_data[$moyr][] = [
                            'Date' => $date,
                            'Infection' => $infection,
                        ];


                        $bytype_data[$k][] = $v;
                    }

                }
            }
        }

        $label = []; $dataset = []; $infectiondata=[];
        foreach($moyrArr as $my=>$val){

            $label[] = array_get($val, 'MonthYear');
            $total = \App\Utils\Toolkit::smart_count(array_get($infection_data, $my));
            $dataset[] = $total;

            $monthyear = array_get($val, 'MonthYear');
            $infectiondata[$my] = ['count'=>$total, 'monthyear' => $monthyear];

        }

        $options = array_get(Reports::Options(), 'option1');

        $chart = Reports::chart('bar', $label, $dataset, 'Total Number of Infections', $options);

        $ByTypeData = []; $label_bytype = []; $dataset_bytype =[];
        $config_msg = '';
        if(!empty($fields)){
            foreach($fields as $k=>$v){
                $total = \App\Utils\Toolkit::smart_count(array_get($bytype_data, $k));
                $dataset_bytype[] = $total;
                $label_bytype[] = $v;
                $ByTypeData[] = [
                    'Title'=>$v,
                    'Total'=>$total
                ];
            }
        }else{

            $config_msg = "Please add infection form to config forms.";
        }

        $options = array_get(Reports::Options(), 'option2');
        $infectionbytype = Reports::chart('horizontalBar', $label_bytype, $dataset_bytype, 'Infection By Type', $options);

        return [
            'chart' => $chart,
            'data' => $infectiondata,
            'bytype_chart' => $infectionbytype,
            'bytype_data' => $ByTypeData,
            'message' => $config_msg
        ];

    }
}