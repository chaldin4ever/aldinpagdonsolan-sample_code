<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Redis;
use App\Models\PinboardItem;

class PinboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $user = Auth::user();
        $key = 'selected.facility.' . $user->_id;
        if(empty(Redis::get($key)))
            return redirect(url('facility/select'));

        $facility_id = Redis::get($key);

        $items = PinboardItem::where('Facility.FacilityId', $facility_id)
            ->where('state', 'new')
            ->get();

        return view("pinboard.index", [
            "items" => $items
        ]);
    }

    public function view($pinboardId){

        $item = PinboardItem::find($pinboardId);

        return view('pinboard.view', compact('item'));

    }

    public function close($pinboardId){

        $user = User::find(Auth::user()->_id);
        $item = PinboardItem::find($pinboardId);
        $item->state = 'done';
        $item->ClosedBy = $user->Object;
        $item->save();

        return redirect(url('pinboard/view/'.$pinboardId))->with('status', 'Successfully Saved.');

    }
}
