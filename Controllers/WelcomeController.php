<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class WelcomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('welcome.about');    
    }

    public function pricing()
    {
        return view('welcome.pricing');    
    }

    public function tour()
    {
        return view('welcome.tour');    
    }

    public function get_started()
    {
        return view('welcome.get_started');    
    }

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);
        try {
            $validatedData['password'] = bcrypt(str_random(12));
            $validatedData['activation_code'] = str_random(30).time();
            $user = app(User::class)->create($validatedData);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return redirect()->back()->with('message', 'Unable to create new user.');
        }
        $user->notify(new \App\Notifications\UserRegisteredSuccessfully($user));
        return redirect()->back()->with('message', 'Successfully created a new account. Activation email has been sent.');

    }

    public function features(){
        return view('welcome.features');

    }

    public function signup(){

    }

}
