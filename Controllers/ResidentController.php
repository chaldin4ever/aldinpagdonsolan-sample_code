<?php

namespace App\Http\Controllers;

use App\Charts\POCChart;
use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Domains\FormControl;
use App\Domains\User;
use App\Models\Client;
use App\Models\Doctor;
use App\Models\Documents;
use App\Models\EvaluationFormLink;
use App\Models\Facility;
use App\Models\FolderStructure;
use App\Models\Pharmacy;
use App\Models\PinboardItem;
use App\Models\Provider;
use App\Models\RecentVisitedResident;
use App\Models\Resident;
use App\Models\ResidentBGL;
use App\Models\ResidentContact;
use App\Models\ResourceFile;
use App\Models\Role;
use App\Models\Rooms;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Redis;
use DB;

class ResidentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request, $page=null){
//        dd($request->all());
        $userid = Auth::user()->_id;
        $key = 'selected.facility.' . $userid;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $fObj = new FacilityController();
        if($fObj->checkfacility() == true ) return $fObj->nofacility();

        $facilityId = Redis::get($key);
        $db = \App\Utils\Toolkit::GetDatabase($userid);
        $residents = DB::connection($db)->collection("Resident")
            ->orderBy('FullName', 'asc')
            ->where('Archived', false);

        if($request->Facility){
            $residents = $residents->where('Facility.facilityId', $request->Facility);
        }else{
            $residents = $residents->where('Facility.facilityId',$facilityId);
        }

        if($request->Area){
            $residents = $residents->where('Area.AreaId', $request->Area);
        }

        if($request->ResidentName){

            $string = $request->ResidentName;
            $residents = $residents->where(function($q) use ($string) {
                $q->where(function($residents) use ($string){
                    $residents->where('FullName', 'LIKE', '%'.$string.'%');
                })->orWhere(function($residents) use ($string) {
                    $residents->where('PreferredName', 'LIKE', '%'.$string.'%');
                })->orWhere(function($residents) use ($string) {
                    $residents->where('CurrentRoom.RoomName', 'LIKE', '%'.$string.'%');
                });
            });

        }

        if(!empty($request->StatusFilter)){
            foreach($request->StatusFilter as $status){
                $statusArr[] = $status;
            }
        } else {
            $statusArr = ['permanent', 'respite', 'transitional'];
        }
        $residents = $residents->whereIn('Status', $statusArr);

        $residents = $residents->paginate(25);

        $user = User::find($userid);
        $facility = self::getFacility();
        $doctors = self::getDoctor();
        $pharmacy = self::getPharmacy();
        $clients = self::getClient();
        $facility_select = $user->Facilities;//Facility::all();
        $rooms = Rooms::where('Facility', $facilityId)
                ->where('CurrentResident', null)
                ->get();

        $currentFacility = Facility::find($facilityId);

        if($page == 'quicknote' || $request->page  == 'quicknote'){
            $view = 'indicator.quicknote';
        }else if($page == 'weight' || $request->page  == 'weight') {
            $view = 'indicator.weight';
        }else if($page == 'bgl' || $request->page  == 'bgl') {
            $view = 'indicator.bgl';
        }else{
            $view = 'resident.index';
        }


        $selectedStatus = $statusArr;
        return view($view, compact(
            'residents', 'facility', 'doctors',
            'pharmacy', 'clients', 'facility_select', 'facilityId', 'page', 'selectedStatus', 'currentFacility', 'rooms'
        ));
    }


    public function edit($id){

        $userid = Auth::user()->_id;
        $key = 'selected.facility.' . $userid;

        $facilityId = Redis::get($key);

        $resident = Resident::find($id);

        $rooms = Rooms::where('Facility', $facilityId )
            ->where('Vacant', 'on')
            ->get();

        $residentfile = ResourceFile::where('Resident.ResidentId', $id)->first();

        $residentContacts = ResidentContact::where('ResidentId', $id)->get();

        $resident_contact = $this->getContacts($residentContacts);

        return view('resident.edit', compact('resident', 'rooms', 'residentfile', 'resident_contact'));
    }

    public function getContacts($residentContacts){
        $resident_contact = []; $num = 1;
        foreach($residentContacts as $contact){
            $resident_contact[$contact->DisplayOrder] = $contact;
            $num++;
        }

        return $resident_contact;
    }

    public function contact_store(Request $request, $residentId){

//        dd($request->all());
        $data = $request->except('_token', '_id');
//        dd($data);
        if(!empty($request->_id)){
            ResidentContact::where('_id', $request->_id)->update($data);
        }else{
           ResidentContact::create($data);
        }

        return redirect(url('resident/edit/'.$residentId.'?view=contact'))->with('status', 'Sucessfully Saved.');

    }

    public function preview_photo($id){

        $residentfile = ResourceFile::where('Resident.ResidentId', $id)->first();

        return view('resident.preview_photo', compact( 'residentfile'));
    }

    public static function getFacility(){

        $facility = Facility::where('Archive', '!=', 1)->get();

        $f = [];
        if(!empty($facility)){
            foreach($facility as $fa){

                $f[] = ['id' => $fa->_id, 'FacilityName' => $fa->NameLong];

            }

        }

        return json_encode($f);
    }

    public static function getDoctor(){

        $doctors = Doctor::all();

        $doctor = [];
        if(!empty($doctors)){
            foreach($doctors as $doc){

                $doctor[] = ['id' => $doc->_id, 'DoctorName' => $doc->FullName];

            }
        }


        return json_encode($doctor);
    }

    public static function getPharmacy(){

        $pharmacy = Pharmacy::all();

        foreach($pharmacy as $pharma){

            $pharmaName = explode('(', $pharma->PharmacyName);
            $name = str_replace("'", '', $pharmaName[0]);
            $phar[] = ['id' => $pharma->_id, 'PharmacyName' => "$name"];

        }

        return json_encode($phar);
    }

    public static function getClient(){

        $client = Client::all();

        foreach($client as $cl){

            $clientName = explode('(', $cl->ClientName);
            $name = str_replace("'", '', $clientName[0]);
            $clients[] = ['id' => $cl->_id, 'ClientName' => $name];

        }

        return json_encode($clients);
    }

    // only update is possible
    // admission must be from a form
    public function store(Request $request, $page=null){

//        dd($request->all());
        $db = \App\Utils\Toolkit::GetDatabase(Auth::user()->_id);

        if(!empty($request->_id)){
//            $resident = Resident::find($request->_id);
            $residentId = $request->_id;
            $resident = DB::connection($db)->table("Resident")->where('_id', $residentId);
            $residentData = $this->getRequestData($request);
            $resident->update($residentData, ['upsert' => true]);
//            dd($resident);
//            $resident = Toolkit::ArrayToObject($resident, '\App\Models\Resident');
//            dd($resident);
//            $resident->setConnection($db);

//            dd($resident);
        }else{

            $resident = new Resident();
            $resident->setConnection($db);


//        $resident->Archive = false;
//        $provider = Provider::find($request->input('Provider'));
//        dd($resident);
//        $resident->Provider = !empty($provider) ? $provider->Object : '';
            $pharma = Pharmacy::find($request->PharmacyId);
            $resident->Pharmacy = !empty($pharma) ? $pharma->Object : '';
            $doctor = Doctor::find($request->DoctorId);

            $resident->Doctor = !empty($doctor) ?$doctor->Object : '';
//        $facility = Facility::find($request->Facility);
//        $resident->Facility = !empty($facility) ? $facility->ForResidentObject : '';
//        $resident->Area =
            $resident->Status = $request->ResidentStatus;
            $resident->FirstName = $request->FirstName;
            $resident->LastName = $request->LastName;
            $resident->FullName = $request->FirstName.' '.$request->LastName;
            $resident-> PreferredName = $request->PreferredName;
            $resident->DOB = $request->DOB;
            $resident->DOB_submit = $request->DOB_submit;
            $Born = new Carbon(Toolkit::ConvertStringToCarbon($request->DOB_submit));
//        dd($Born);
            $Age = $Born->diff(Carbon::now())->format('%y');
            $resident->Age = $Age;
            $resident->Address = $request->Address;
            $resident->Suburb = $request->Suburb;
            $resident->Postcode = $request->PostCode;
            if(!empty($request->Room)){
                $room = \App\Models\Rooms::find($request->Room);
                if(!empty($room))
                    $resident->CurrentRoom = $room->Object;
            }
            $resident->URN = $request->URN;
            $resident->Gender = $request->Gender;
            $resident->MedicareNumber = $request->MedicareNumber;
            $resident->PensionNumber = $request->PensionNumber;
            $resident->DVANumber = $request->DVANumber;
            $resident->Notes = $request->Notes;
            $resident->DateOfEntry = Carbon::now();
            $resident->save();

            $residentId = $resident->_id;
        }

//        if(!empty($request->Photo)) $this->uploadPhoto($residentId, $request->Photo);
        if(!empty($request->Photo_values)) $this->uploadPhoto($residentId, $request->Photo_values);

        if(!empty($request->Room)){
            $resident = Resident::find($residentId);
            $room = Rooms::find($request->Room);
            $room->CurrentResident = $resident->Object;
            $room->Vacant = 'null';
            $room->save();
        }

        if($page == null)
            return redirect('resident')->with('status', 'Successfully Saved');
        else
            return redirect('resident/edit/'.$residentId)->with('status', 'Successfully Saved');

    }

    public function getRequestData($request){

        $CurrentRoom = [];
        if(!empty($request->Room)){
            $room = \App\Models\Rooms::find($request->Room);
            if(!empty($room)){
                $CurrentRoom = $room->Object;
            }

        }

        $Born = new Carbon($request->DOB_submit);
//        dd($Born);
        $Age = $Born->diff(Carbon::now())->format('%y');

        $residentData = [

        'Status' => $request->ResidentStatus,
        'FirstName' => $request->FirstName,
        'LastName' => $request->LastName,
        'FullName' => $request->FirstName.' '.$request->LastName,
        'PreferredName' => $request->PreferredName,
        'DOB' => $request->DOB,
        'DOB_submit' => $request->DOB_submit,
        'Age' => $Age,
        'CurrentRoom' => $CurrentRoom,
        'Address' => $request->Address,
        'Suburb' => $request->Suburb,
        'Postcode' => $request->PostCode,
        'URN' => $request->URN,
        'Gender' => $request->Gender,
        'MedicareNumber' => $request->MedicareNumber,
        'PensionNumber' => $request->PensionNumber,
        'DVANumber' => $request->DVANumber,
        'Notes' => $request->Notes
        ];

        return $residentData;
    }

    public function uploadPhoto($residentId, $photo_base64)
    {

//        dd($photo_file);
        $resident = Resident::find($residentId);
        $resourceFile = ResourceFile::where('Resident.ResidentId', $residentId)->first();

        if(empty($resourceFile)){
            $resource = new ResourceFile();
        }else{
            $resource = $resourceFile;
        }

        $imageFile=json_decode($photo_base64);
        $image_parts = explode(";base64,", $imageFile->data);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $filename = 'upload/'.uniqid() . '.jpg';

        $img = Storage::disk('s3')->put($filename, $image_base64,
            [
                'visibility' => 'public',
                'Metadata' => [
                    'file_name' => $filename
                ]
            ]);

        $resource->Resident = $resident->Object;
        $resource->File = $photo_base64;
        $resource->AWSFile =  env('AWS_URL').'/'.$filename;
        $resource->save();

/*        if (request()->has($fieldName)) {
            $file = request()->input($fieldName);*/
            /*$file = $fieldName;
            if (!empty($file)) {
                $filename = $file->getClientOriginalName();
                Storage::disk('s3')->put('upload', $file,
                    [
                        'visibility' => 'public',
                        'Metadata' => [
                            'file_name' => $filename
                        ]
                    ]);

                $resident = Resident::find($residentId);
                $resourceFile = ResourceFile::where('Resident.ResidentId', $residentId)->first();

                if(empty($resourceFile)){
                    $resource = new ResourceFile();
                }else{
                    $resource = $resourceFile;
                }

                $document = file_get_contents($file);
                $base64File = base64_encode($document);
                $ext = $file->getClientOriginalExtension();

                $resource->Resident = $resident->Object;
                $resource->FileName = $filename;
                $resource->File = $base64File;
                $resource->FileType = $file->getMimeType(); //File::mimeType($path);
                $resource->FileExtension = $ext;
                $resource->save();
            }*/
//        }
    }

    public function getReminder($residentId){

        $pinboard = PinboardItem::where('Resident.ResidentId', $residentId)
                        ->where('state', 'new')->count();


        return $pinboard;
    }

    public function view($residentId, Request $request){

//        dd($request->all());

//        dd(Input::all());
        $resident = Resident::find($residentId);

        $fromDate = Toolkit::ConvertStringToCarbon(Input::get('FromDate'));
        $toDate = Toolkit::ConvertStringToCarbon(Input::get('ToDate'));
        $category = $request->category;
        $endofshift =  $request->endofshift;
        $user = $request->user;

//        dd([$fromDate, $toDate]);
        if($fromDate && $toDate){

            $dt = Toolkit::CarbonToUTCDateTime($fromDate->startOfDay());
            $et = Toolkit::CarbonToUTCDateTime($toDate->endOfDay());

        }else{

            $today = Carbon::now()->endOfDay();

            $dt = Toolkit::CarbonToUTCDateTime($today->subMonth(1)->startOfDay());
            $et = Toolkit::CarbonToUTCDateTime(Carbon::now()->endOfDay());
        }


        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);

        $pObj = new ProgressNoteController();
        $progressnotes = $pObj::getAll($residentId, $dt, $et, $category, $user, $endofshift);

        $aObj = new AssessmentController();
        $last10assessment = $aObj::getlast10assessment($residentId, 'assessment', 10);
        $last10evaluation = $aObj::getlast10assessment($residentId, 'evaluation', 10);
        $last10charting = $aObj::getlast10assessment($residentId, 'charting', 10);
        $last10forms = $aObj::getlast10assessment($residentId, 'form', 10);
//        $evaluation_forms = $this->evaluation($residentId);

        $pinboard_items = \App\Models\PinboardItem::orderBy('action_date')
            ->where('Resident.ResidentId', $residentId)
            ->where('state', 'new')
            ->get();

        $folders = $this->getResidentFolder();

        // load care plan & care domains
        $rows = \App\Models\CarePlan::orderBy('updated_at', 'desc')
            ->where('Resident.ResidentId', $residentId)
            ->where('state', 'active')
            ->get();
        if(sizeof($rows) == 0)
            $careplan = [];
        else
            $careplan = $rows[0];
        $care_domains = \App\Utils\Toolkit::GetCareDomains();

        $key = Auth::user()->_id . 'resident.active.tab';
        $active_tab = Redis::get($key);
        if(empty($active_tab)) 
            $active_tab = 'pnote_link';

        $this->StoreRecentVisitedResident($residentId);

        $residentfile = ResourceFile::where('Resident.ResidentId', $residentId)->first();

        return view('resident.view', compact('resident', 'progressnotes', 'last10assessment',
            'last10charting', 'last10evaluation', 'last10forms', 'pinboard_items',
            'fromDate', 'toDate', 'folders', 'category', 'user', 'careplan', 'care_domains',
            'active_tab', 'endofshift', 'residentfile'
            ));
    }

    public function details($residentId)
    {

        $resident = Resident::find($residentId);

        $residentContacts = ResidentContact::where('ResidentId', $residentId)->get();

        $resident_contact = $this->getContacts($residentContacts);

        return view('resident.details', compact('resident', 'resident_contact'));

    }

    //Resident Folder Structure
    public function getResidentFolder(){

        $folders = FolderStructure::all();

        $objFolder = new FolderStructureController();

        $dataArr = $objFolder->prepareFolderData($folders);
        $tree = $objFolder->createParentChild($dataArr);

        return $tree;

    }

    //Resident assessment history page

    public function assessment($residentId){

        $assessment = Assessment::where('Resident.ResidentId', $residentId)
            ->where('Form.Category', 'assessment');

        $fromDate = Toolkit::ConvertStringToCarbon(Input::get('FromDate'));
        $toDate = Toolkit::ConvertStringToCarbon(Input::get('ToDate'));

        if($fromDate && $toDate){

            $dt = Toolkit::CarbonToUTCDateTime($fromDate->startOfDay());
            $et = Toolkit::CarbonToUTCDateTime($toDate->endOfDay());

        }else{

            $today = Carbon::now()->endOfDay();

            $dt = Toolkit::CarbonToUTCDateTime($today->subMonth(1)->startOfDay());
            $et = Toolkit::CarbonToUTCDateTime(Carbon::now()->endOfDay());
        }

        $selectedrole = Input::get('role');
        if($selectedrole){
            $assessment = $assessment->where('CreatedBy.Roles', 'elemMatch', ['RoleId' => $selectedrole]);
        }

        $user = Input::get('user');
        if($user){
            $assessment = $assessment->where('CreatedBy.FullName', 'LIKE', "%$user%");
        }

        $title = Input::get('title');
        if($title){
            $assessment = $assessment->where('Form.FormName', 'LIKE', "%$title%");
        }

        $assessment = $assessment->orderby('updated_at', 'desc')
            ->whereBetween('updated_at', [$dt, $et])
            ->get();

        $fromDate = Toolkit::UTCDateTimeToDatePicker($dt);
        $toDate = Toolkit::UTCDateTimeToDatePicker($et);


        $resident = Resident::find($residentId);

        $userid = Auth::user()->_id;
        $key = 'selected.facility.' . $userid;
        $facilityId = Redis::get($key);
        $facility = Facility::find($facilityId);
        $roles = Role::where('Provider.ProviderId', array_get($facility->Provider, 'ProviderId'))->get();

        return view('resident.assessments', compact('assessment', 'resident',
            'roles', 'selectedrole','fromDate', 'toDate', 'title', 'user'));
    }

    public function evaluation($residentId){

        $assessment = Assessment::where('Resident.ResidentId', $residentId)
            ->where('Form.Category', 'evaluation')
            ->orderby('updated_at', 'desc')
            ->get();

        $resident = Resident::find($residentId);

        return view('resident.evaluation', compact('assessment', 'resident'));
    }

    public function charting($residentId){

        $assessment = Assessment::where('Resident.ResidentId', $residentId)
            ->where('Form.Category', 'charting')
            ->orderby('updated_at', 'desc')
            ->get();

        $resident = Resident::find($residentId);

        return view('resident.charting', compact('assessment', 'resident'));
    }

    public function form($residentId){

        $assessment = Assessment::where('Resident.ResidentId', $residentId)
            ->where('Form.Category', 'form')
            ->orderby('updated_at', 'desc')
            ->get();

        $resident = Resident::find($residentId);

        return view('resident.forms', compact('assessment', 'resident'));
    }

    public function settab(Request $request){
        $tab = $request->input('tab');
        if(!empty($tab)){
            $key = Auth::user()->_id.'resident.active.tab';
            Redis::set($key, $tab);
            Redis::expire($key, 83200);
        }
    }

    public function charts($residentId){

        $resident = Resident::find($residentId);

        $bgl = ResidentBGL::where('Resident.ResidentId', $residentId)
                ->orderby('data.Date', 'asc')->orderby('data.Time', 'asc')->get();

        $bglchart = $this->bgldataset($bgl);

        return view('resident.charts', compact('resident', 'bgl', 'bglchart'));

    }

    public function bgldataset($dataArr){

        $dataset = []; $label = [];
        if(!empty($dataArr)){

            foreach($dataArr as $data){

                $time = array_get($data->data, 'Time');
                $nt = date("H:i", strtotime($time));
                $date = Toolkit::ConvertStringDateTimeToCarbon(array_get($data->data, 'Date'), $nt);

//                dd($date);

                $label[] = array_get($date, 'datetime'); //$date.' '.array_get($data->data, 'Time');
                $dataset[] = array_get($data->data, 'BGL');
            }

            $chart = new POCChart();
            $chart->dataset('BGL', 'line', $dataset);
            $chart->labels($label);

            return $chart;
        }

    }

    public function archive($residentId){

        $resident = Resident::find($residentId);
        $resident->Archive = true;
        $resident->save();

        return redirect('resident')->with('status', 'Successfully Archived.');

    }

    public function StoreRecentVisitedResident($residentId){

        $user = User::find(Auth::user()->_id);

        $resident = Resident::find($residentId);
        $vresident = new RecentVisitedResident();
        $vresident->User = $user->Object;
        $vresident->Resident = $resident->Object;
        $vresident->save();

    }

    public function updateStatus(){

        $residents = Resident::all();

        foreach($residents as $resident){

            $statusArr = [1=>'permanent',
                2=> 'respite',
                3=> 'transitional',
                4=> 'discharge',
                5=> 'archive'];

            $r = Resident::find($resident->_id);

            if(is_int($resident->Status)){
                $r->Status = array_get($statusArr, $resident->Status);
                $r->save();
            }

        }
    }

}
