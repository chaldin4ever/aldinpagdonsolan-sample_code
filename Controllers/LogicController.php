<?php

namespace App\Http\Controllers;

use App\Utils\HubHelper;
use App\Utils\Toolkit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Models\BusinessLogic;
use App\Domains\AssessmentForm;
use Redis;

class LogicController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth');
    }

    public function index()
    {

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $facility_id = Redis::get($key);
        // get all business rules
        $rules = BusinessLogic::orderBy('title')
            ->whereIn('archived', [null, false])
            ->where('Facility.FacilityId', $facility_id)
            ->get();

        return view('logic.index', [
            'rules' => $rules
        ]);
    }

    public function edit($ruleId)
    {
        $rule = BusinessLogic::find($ruleId);
        
        if(empty($rule->json)){
            $rule = new BusinessLogic();
            $input["field"] = "";
            $input["operation"] = "in";
            $input["op_values"] = [];

            $output["action"] = "";
            $output["keys"] = [];
            $output["target_class"] = "";
            $output["target_form"] = 0;
            $output["copy_fields"] = [];

            $pinboard["action"] = "";
            $pinboard["note"] = "";
            $pinboard["due_days"] = 0;
            $pinboard["item_type"] = "";
            $pinboard["user+roles"] = [];

            $json["form_code"] = 0;
            $json["inputs"] = [$input];
            $json["output"] = $output;
            $json["pinboard"] = $pinboard;

            $rule->json =  json_encode($json);
        } else {
            $rule->json = json_encode($rule->json);
        }
        // dd($rows);
        return view('logic.edit', [
            'rule' => $rule,
            'mode' => 'edit',
        ]);
    }

    public function archive($ruleId)
    {
        $rule = BusinessLogic::find($ruleId);
        $rule->archived = true;
        $rule->save();

        return redirect('logic');
    }

    public function store(Request $request)
    {
        $facility = Toolkit::getFacility();
        $rule_id = $request->input('rule_id');
        $title = $request->input('title');
        $data_json = $request->input('data');

        if(!empty($title))
        {
            $rule = BusinessLogic::find($rule_id);
            if(empty($rule)){
                $rule = new BusinessLogic();
            }
            $rule->Facility = $facility->Object;
            $rule->title = $title;
            $rule->json = json_decode($data_json);
            $rule->save();
            return $rule;
        }
    }

    public function copy($ruleId)
    {
        $facility = Toolkit::getFacility();
        $rule = BusinessLogic::find($ruleId);
        if (empty($rule)) {
            return redirect('/businessrules');
        }

        $newRule = new BusinessLogic();
        $rule->Facility = $facility->Object;
        $newRule->title = $rule->title.' COPY';
        $newRule->json = $rule->json;

        $newRule->save();

        return redirect('/logic');
    }

    private function doOthers($lang, $results, $rule)
    {
        $subRules = $rule->Rules;
        $form = array_get($rule, 'Form');
        if (empty($form)) {
            $results[] = [
                'Code' => $rule->Code,
                'Process' => $rule->Process,
                'Title' => $rule->Title,
                'Error'=> 'Missing form',
                'Language' => $lang
            ];
        } else {
            $formID = array_get($form, 'FormID');
            $fm = \App\Domains\AssessmentForm::orderBy('language')
                ->where('IsActive', 1)
                ->where('FormID', $formID)->get();
            foreach ($fm as $f) {
                if (!empty($lang) && $f->language != $lang) {
                    continue;
                }
                if (!is_array($subRules)) {
                    continue;
                }
                foreach ($subRules as $sr) {
                    $if = array_get($sr, 'If');
                    $then = array_get($sr, 'Then');

                    $ifField = array_get($if, 'Field');
                    $action = array_get($then, 'Action');
                    $sourceField = array_get($then, 'SourceField');
                    $targetField = array_get($then, 'TargetField');

                    if ($ifField=="") {
                        $ifField="Always";
                    }

                    if ($action=="create-advanced-assessment") {
                        $results = $this->doAdvancedAssessment($lang, $results, $f, $sr);
                    } elseif ($action=="followup-assessment") {
                        $results = $this->doFollowupAssessment($lang, $results, $f, $sr);
                    } elseif ($action=="prompt-assessment") {
                        $results = $this->doPromptAssessment($lang, $results, $f, $sr);
                    } elseif ($action=="update-pressurecare" || $action=="update-toileting") {
                        $results = $this->doSimpleUpdate($lang, $results, $f, $sr);
                    } else {
                        $qtn = $f->GetField($sourceField);
                        $qtnText = array_get(array_get($qtn, 'question'), 'text');
                        $error = '';
                        if (empty($qtnText)) {
                            $error = "Missing question <span class='field-code'>$sourceField</span>";
                        }
                        $ThenField = "<p class='source-field'>$qtnText =></p><p class='target-field'>$targetField</p>";
                        $results[] = [
                            'Form' => $f->FormID,
                            'FormName' => $f->FormName,
                            'Process' => $action,
                            'If' => $ifField,
                            'Then' => $ThenField,
                            'Fields' => "$sourceField => <p class='target-field'>$targetField</p>",
                            'Error' => $error,
                            'Language' => $lang
                        ];
                    }
                }
            }
        }
        return $results;
    }

    private function doPromptAssessment($lang, $results, $f, $sr)
    {
        $if = array_get($sr, 'If');
        $then = array_get($sr, 'Then');

        $ifField = array_get($if, 'Field');
        $ifOperand = array_get($if, 'Operand');
        $ifValue = array_get($if, 'Value');
        $action = array_get($then, 'Action');

        $error="";
        {
            $ifFieldField = $f->GetField($ifField);
            $ifValueQtn = $f->GetQuestion($ifValue);
            $ifFieldFieldText = array_get(array_get($ifFieldField, 'question'), 'text');
            $ifValueQtnText = array_get($ifValueQtn, 'text');
            if (empty($ifFieldFieldText) || empty($ifValueQtnText)) {
                $error = "Missing question $ifField $ifValue";
            }
        }
        $targetFormID = array_get($then, 'FormID');
        $targetForms = \App\Domains\AssessmentForm::orderBy('language')
            ->where('IsActive', 1)
            ->where('FormID', $targetFormID)->get();
        if (sizeof($targetForms)==0) {
            $error = "Missing form $targetFormID, $error";
        }
        foreach ($targetForms as $tfm) {
            if (!empty($lang) && $tfm->language != $lang) {
                continue;
            }
            $results[] = [
                'Form' => $f->FormID,
                'FormName' => $f->FormName,
                'Process' => $action,
                'If' => "$ifFieldFieldText $ifOperand <p class='target-field'>$ifValueQtnText</p>",
                'Then' => $action,
                'Fields' => "$tfm->FormID - $tfm->FormName",
                'Error' => $error,
                'Language' => $lang
            ];
        }
        return $results;
    }

    private function doFollowupAssessment($lang, $results, $f, $sr)
    {
        $if = array_get($sr, 'If');
        $then = array_get($sr, 'Then');

        $ifField = array_get($if, 'Field');
        $action = array_get($then, 'Action');
        $sourceField = array_get($then, 'SourceField');
        $targetField = array_get($then, 'TargetField');

        if ($ifField=="") {
            $ifField="Always";
        }
        $error="";
        $ifOperand = array_get($if, 'Operand');
        $ifValue = array_get($if, 'Value');
        if ($ifValue=="on") {
            $ifFieldField = $f->GetQuestion($ifField);
            $ifFieldFieldText = array_get($ifFieldField, 'text');
            $ifValueQtnText = "ticked";
            if (empty($ifFieldFieldText)) {
                $error = "Missing question $ifField";
            }
        } elseif ($ifValue=="empty") {
            $ifFieldField = $f->GetField($ifField);
            $ifFieldFieldType = array_get($ifFieldField, 'field_type');
            $ifFieldFieldText = array_get(array_get($ifFieldField, 'question'), 'text');
            $ifValueQtnText = "empty";
            if (empty($ifFieldFieldText)) {
                $error = "Missing question $ifField";
            } elseif ($ifFieldFieldType!='text') {
                $error = "Field type mismatch $ifField ($ifFieldFieldType)";
            }
        } else {
            $ifFieldField = $f->GetField($ifField);
            $ifValueQtn = $f->GetQuestion($ifValue);
            $ifFieldFieldText = array_get(array_get($ifFieldField, 'question'), 'text');
            $ifValueQtnText = array_get($ifValueQtn, 'text') ." <span class='field-code'>($ifValue)</span>";
//            if (empty($ifFieldFieldText) || empty($ifValueQtnText))
//                $error = "Missing question <span class='field-code'>$ifField $ifValue</span>";
        }
        $targetFormID = array_get($then, 'FormID');
        $targetForms = \App\Domains\AssessmentForm::orderBy('language')
            ->where('IsActive', 1)
            ->where('FormID', $targetFormID)->get();
        if (sizeof($targetForms)==0) {
            $error = "Missing form $targetFormID, $error";
        }
        foreach ($targetForms as $tfm) {
            if (!empty($lang) && $tfm->language != $lang) {
                continue;
            }

            $results[] = [
                'Form' => $f->FormID,
                'FormName' => $f->FormName,
                'Process' => $action,
                'If' => "$ifFieldFieldText <span class='field-code'>($ifField)</span> $ifOperand <p class='target-field'>$ifValueQtnText</p>",
                'Then' => $action,
                'Fields' => "$tfm->FormID - $tfm->FormName",
                'Error' => $error,
                'Language' => $lang
            ];
        }
        return $results;
    }
    private function doAdvancedAssessment($lang, $results, $f, $sr)
    {
        $if = array_get($sr, 'If');
        $then = array_get($sr, 'Then');

        $ifField = array_get($if, 'Field');
        $action = array_get($then, 'Action');
        $sourceField = array_get($then, 'SourceField');
        $targetField = array_get($then, 'TargetField');
        $fieldType = array_get($then, 'FieldType');

        if ($ifField=="") {
            $ifField="Always";
        }

        if ($action=="create-advanced-assessment") {
            if ($fieldType=='radio') {
                $qtn = $f->GetField($sourceField);
                $qtnText = array_get(array_get($qtn, 'question'), 'text');
            } else {
                $qtn = $f->GetQuestion($sourceField);
                $qtnText = array_get($qtn, 'text');
            }
            $targetFormID = array_get($then, 'TargetForm');
            $targetForms = \App\Domains\AssessmentForm::orderBy('language')
                ->where('IsActive', 1)
                ->where('FormID', $targetFormID)->get();
            if (sizeof($targetForms)==0) {
                $error = "Missing form $targetFormID";
                $results[] = [
                    'Form' => $f->FormID,
                    'FormName' => $f->FormName,
                    'Process' => $action,
                    'If' => $ifField,
                    'Error' => $error,
                    'Language' => $lang
                ];
            }
            foreach ($targetForms as $tfm) {
                if (!empty($lang) && $tfm->language != $lang) {
                    continue;
                }

                if ($fieldType=='radio') {
                    $targetQtn = $tfm->GetField($targetField);
                    $targetText = array_get(array_get($targetQtn, 'question'), 'text');
//                    dd($targetQtn);
                } else {
                    $targetQtn = $tfm->GetQuestion($targetField);
                    $targetText = array_get($targetQtn, 'text');
                }
                $error = '';
                if (empty($qtnText) || empty($targetText)) {
                    $error = "Missing question <span class='field-code'>$sourceField or $targetField</span>";
                }
                $ThenField = "<p class='source-field'>$qtnText => </p><p class='target-field'>$targetText ($targetFormID - $tfm->FormName)</p>";
                $results[] = [
                    'Form' => $f->FormID,
                    'FormName' => $f->FormName,
                    'Process' => $action,
                    'If' => $ifField,
                    'Then' => $ThenField,
                    'Fields' => "$sourceField => <p class='target-field'>$targetField $fieldType</p>",
                    'Error' => $error,
                    'Language' => $lang
                ];
            }
//            $results[] = [];
        }
        return $results;
    }

    private function doAdmission($lang, $results, $rule)
    {
        $subRules = $rule->Rules;
        foreach ($subRules as $sr) {
            $results[] = [
                'SubTitle' => array_get($sr, 'Title')
            ];
            $forms = array_get($sr, 'Forms');
            foreach ($forms as $formID) {
                $fm = \App\Domains\AssessmentForm::where('FormID', $formID)->get();
                foreach ($fm as $f) {
                    if (!empty($lang) && $f->language != $lang) {
                        continue;
                    }

                    $results[] = [
                        'Form' => $f->FormID,
                        'FormName' => $f->FormName,
                        'Language' => $lang
                    ];
                }
            }
        }
        return $results;
    }

    private function doSimpleUpdate($lang, $results, $f, $sr)
    {
        $if = array_get($sr, 'If');
        $then = array_get($sr, 'Then');

        $ifField = array_get($if, 'Field');
        $action = array_get($then, 'Action');
        $sourceField = array_get($then, 'SourceField');
        $targetField = array_get($then, 'TargetField');

        if ($ifField=="") {
            $ifField="Always";
        }

        $qtn = $f->GetQuestion($sourceField);
        $qtnText = array_get($qtn, 'text');

        $error = '';
        if (empty($qtnText)) {
            $error = "Missing question <span class='field-code'>$sourceField</span>";
        }
        $ThenField = "<p class='source-field'>$qtnText => </p><p class='target-field'>$targetField</p>";
        $results[] = [
            'Form' => $f->FormID,
            'FormName' => $f->FormName,
            'Process' => $action,
            'If' => $ifField,
            'Then' => $ThenField,
            'Fields' => "$sourceField => <p class='target-field'>$targetField</p>",
            'Error' => $error,
            'Language' => $lang
        ];
        return $results;
    }
}
