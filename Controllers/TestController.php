<?php

namespace App\Http\Controllers;

use App\Domains\Assessment;
use App\Domains\AssessmentForm;
use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\FormQuestions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test1(){

/*        $user = User::all()->first();
        $facility = Facility::all()->first();

        Mail::send(new NotifyFacilityUser($user, $facility));*/
//        return view('test.mdb');

        $question = FormQuestions::where('Form.FormID', config('poc.RESIDENT_INCIDENT_FORM_ID'))
            ->where('Code', config('poc.RESIDENT_INCIDENT_FALL_QUESTION_CODE'))
            ->first();

        $flds = $question->Fields;
        $fldcode = config('poc.RESIDENT_INCIDENT_FALL_FIELD');

        $field = [];
        foreach($flds as $k=>$fld){
            if($fldcode === array_get($fld, 'code')){
                $field = $fld;
            }
        }

//        return $flds;
        return array_get($field, 'text');
    }

    public function test2(){

        $assessments = Assessment::where('Form.FormID', (int)config('poc.RESIDENT_INCIDENT_ID'))->get();

        $data = []; $dataArr = [];
        foreach($assessments as $assm){

            $dataArr = $assm->data;

            foreach($dataArr as $key=>$value){
//                $field = $assm->GetField($key, $value);
                $data[] = $assm->GetValue($key);
            }
        }

        dd($data);
    }

   
}
