<?php

namespace App\Http\Controllers;

use App\Mail\NotifyFacilityUser;
use App\Models\Facility;
use App\Models\Provider;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserRole;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Redis;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $key = 'selected.facility.' . Auth::user()->_id;
        if(empty(Redis::get($key)))return redirect(url('facility/select'));

        $facilityId = Redis::get($key);

        $facility = Facility::find($facilityId);

        $provider = array_get($facility->Provider, 'ProviderId');

        $roles = $this->getRoles($provider);

        return view('role.index', compact('roles', 'provider'));
    }

    public function getRoles($provider){
        $roles = UserRole::where('Provider.ProviderId', $provider)
            ->orderby('Archive', 'asc')->orderby('roleName', 'asc')->get();
        return $roles;
    }

    public function store(Request $request){

        if(!empty($request->_id)){
            $role = UserRole::find($request->_id);
        }else{
            $role = new UserRole();
        }

        $role->roleName = $request->roleName;

        $provider = Provider::find($request->providerId);
        $role->Provider = $provider->Object;
        $role->save();

        return redirect(url('role'))->with('status', 'Successfully Saved.');
    }

    public function archive($id){

        $role = UserRole::find($id);

        $role->Archive = 1;
        $role->save();

    }

    public function permission($id){

        $role = UserRole::find($id);

        $permissions = RolePermission::where('Role.roleId', $id)->first();
//dd($permissions);
        return view('role.permission', compact('role', 'permissions'));

    }

    public function storepermission(Request $request){

//        dd($request->all());
        $roleId = $request->roleId;
        $role = Role::find($roleId);

        $permission = RolePermission::where('Role.roleId', $roleId)->first();
        if(empty($permission))$permission = new RolePermission();

        $permission->Role = $role->Object;
        $permission->Create = $request->Create;
        $permission->Read = $request->Read;
        $permission->Update = $request->Update;
        $permission->Delete = $request->Delete;
        $permission->save();

        return redirect(url('role/permission/'.$roleId))->with('status', 'Successfully Saved.');


    }


}
