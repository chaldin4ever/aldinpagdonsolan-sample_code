<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redis;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $provider = Provider::all();
        $facility = Facility::all();

        return view('provider.index', compact('provider', 'facility'));

    }

    public function store(Request $request){

        if(!empty($request->_id)){
            $provider = Provider::find($request->_id);
        }else{
            $provider = new Provider();
        }

        $provider->ProviderName = $request->ProviderName;
        $provider->ContactName = $request->ContactName;
        $provider->ContactEmail = $request->ContactEmail;
        $provider->ContactPhone = $request->ContactPhone;
        $provider->State = $request->State;
        $provider->Suburb = $request->Suburb;
        $provider->PostCode = $request->PostCode;

        $provider->save();

        $facility = [];
        if(!empty($request->Facility)){
            foreach($request->Facility as $f){

                $fa = Facility::find($f);
                $fa->Provider = $provider->Object;
                $fa->save();
                $facility[] = $fa->Object;
            }

        }

        $provider->Facility = $facility;
        $provider->save();

        return redirect(url('provider'))->with('status', 'Successfully Saved.');


    }

    public function archive($id){

        $provider = Provider::find($id);
        $provider->Archive = true;
        $provider->save();

        return redirect(url('provider'))->with('status', 'Successfully Archived.');

    }

    public function config($id){

        $provider = Provider::find($id);

        return view('provider.config', compact('provider'));

    }

    public function setconfig(Request $request)
    {

        $provider = Provider::find($request->ProviderId);

        $configName = $request->ConfigName;
        $provider->$configName = $request->ConfigValue;
        $provider->save();


        if ($request->ConfigName == 'AllowPublicForms') {
            $key1 = 'current.provider.forms' . Auth::user()->_id;
            if ($request->ConfigValue == true) {
                Redis::set($key1, $provider->_id);
                Redis::expire($key1, config('poc.expire_in_24hrs'));   // expires in 12 hours
            } else {
                Redis::del($key1);
            }

        }

        if ($request->ConfigName == 'AllowPublicProcesses') {
            $key2 = 'current.provider.process' . Auth::user()->_id;
            if ($request->ConfigValue == true) {
                Redis::set($key2, $provider->_id);
                Redis::expire($key2, config('poc.expire_in_24hrs'));   // expires in 12 hours
            } else {
                Redis::del($key2);
            }
        }

    }


}
