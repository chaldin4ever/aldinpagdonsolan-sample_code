<div class="container">
    <nav class="navbar navbar-expand-lg navbar-default">
        <a class="navbar-brand" href="/">
            <img src="/images/ecca-logo-24.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Product</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/welcome/pricing">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/welcome/features">Features</a>
                </li>
            </ul>
            <form class="form-inline">
                <a class="btn login-btn my-2 my-sm-0 login-btn" type="submit" href="{{url('login')}}">LOGIN</a>
                <button class="btn btn-get-started my-2 my-sm-0" type="submit">GET STARTED FOR FREE </button>
            </form>
        </div>
    </nav>
</div>