<!-- Include external CSS. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Fonts -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
{{--    <link href="{{ asset('css/jquery.tagsinput.min.css') }}" rel="stylesheet">--}}

    <link rel="stylesheet" href="{{url('css/alertify/alertify.core.css')}}">
    <link rel="stylesheet" href="{{url('css/alertify/alertify.default.css')}}">
    <link rel="stylesheet" href="{{url('css/alertify/alertify.bootstrap.css')}}">
    <link href="{{ asset('mdb/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mdb/css/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mdb/css/style.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/dragula.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/emma-style.css') }}" rel="stylesheet">
    <link href="{{asset('jsoneditor/jsoneditor.min.css')}}" rel="stylesheet" type="text/css">