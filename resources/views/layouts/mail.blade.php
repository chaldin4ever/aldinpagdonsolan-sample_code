
<html>

<head>
        <style>
                body{
                        font-family: 'arial',
                        font-size: 14px;
                }
                .top-bar{
                        display: block;
                        position: relative;
                        top:35px;
                       width:600px; 
                       margin:0 auto;
                       text-align: center;
                       font-size:1.35em;
                       line-height:64px;
                       height:64px;
                }
                .box {
                        display: block;
                        position: relative;
                        top:70px;
                        padding: 15px 15px;
                        width:600px;
                        margin:0 auto;
                        box-shadow: 0 4px 24px 0 rgba(37, 38, 94, 0.1);
                        text-align: center;
                        height: 300px;
                }

                .footer{
                        display: block;
                        position: relative;
                        top: 120px;
                }

                .action {
                        display: block;
                        position: relative;
                        top:50px;
                        width: 163px;
                        height: 54px;
                        border-radius: 10px;
                        background-color: rgba(117, 64, 238, 0.1);
                        margin: 0 auto;
                        line-height: 54px;
                }

                a:link {
                        text-decoration: none;
                }

                .logo-text{
                        display: inline-block;
                        line-height: 25px;
                        height:64px;
                        vertical-align: middle;
                        margin-left: 20px;
                }
        </style>
</head>

<body>


<div class="box">
    
@yield('content')

        <span class="footer">
                <img src="{{asset('images/ecca-logo.png')}}" height="50px" style="position:relative:top=25px"/>
                <br />
                From ECCA Health
                <br />
                <a href="{{url('/')}}" style="font-size:0.75em">Visit Web Site</a>
        </span>
</div>

</body>

</html>

