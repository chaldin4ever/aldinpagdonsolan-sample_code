<div class="side-nav fixed">
    <ul class="custom-scrollbar ">

        <li>
            @auth
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a href="{{url('resident')}}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-users" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="{{url('admin')}}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-edit"></i></a>
                    </li>
                    <li><a href="{{url('reports')}}"  class="collapsible-header waves-effect arrow-r"><i class="fa fa-bar-chart "></i></a>
                    </li>
                    <li>
                        <a href="{{url('messages')}}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-envelope"></i></a>
                    </li>

                    <li>
                        <a href="{{url('helpdesk')}}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-info-circle "></i></a>
                    </li>

                    <li><a class="collapsible-header waves-effect arrow-r" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            @endauth
        </li>
        <!--/. Side navigation links -->
    </ul>
</div>