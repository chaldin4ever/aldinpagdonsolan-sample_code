<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyCare') }}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- Styles -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/assessment.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{url('css/jquery.timepicker.css')}}">
    
    <!-- custom scrollbar stylesheet -->
    <link rel="stylesheet" href="{{url('css/jquery.mCustomScrollbar.css')}}"  type="text/css" media="all" >
    <link rel="stylesheet" href="{{url('css/magic-check.css')}}">
    


    <!-- pick a date -->
    <link rel="stylesheet" href="{{url('css/default.css')}}">
    <link rel="stylesheet" href="{{url('css/default.date.css')}}">

    {{-- Bootstrap 3 --}}
    <link rel="stylesheet" href="{{url('css/bootstrap-switch.css')}}"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link rel="stylesheet" href="{{url('css/bulma-docs.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <link rel="stylesheet" href="{{url('css/media-query.css')}}">


    @yield('style')

    <!-- put the style specific to each view(xxx.blade.php) here-->

    <!-- remove the gray background of progress note view -->
    <style>

        .modal-backdrop {display:none !important;}

        .tooltipclass {background: #000; color: #fff; opacity: 9;}

        @media screen and (min-width: 768px) {
            .modal-dialog {
                width: 700px; /* New width for default modal */
            }
            .modal-sm {
                width: 350px; /* New width for small modal */
            }
        }
        @media screen and (min-width: 992px) {
            .modal-lg {
                width: 950px; /* New width for large modal */
            }
        }

    </style>
    
</head>

<body class="">
    <div id="app">

            <!-- End of Top Menu -->
            <div id="app_vue">
                @yield('content')
            </div>

    </div>


<!-- Scripts -->

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


    
    @yield('script')
    <!-- put the script specific to each view(xxx.blade.php) here-->






</body>

</html>