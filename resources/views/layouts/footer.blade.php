<section id="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img src="/images/ecca-logo-24.png" class="ml-4"/>
                <div>
                    <ul class="nav mt-2">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><img src="/images/facebook-logo.png"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><img src="/images/twitter-logo.png"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><img src="/images/youtube-logo.png"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><img src="/images/google-plus-logo.png"/></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-2">
                <div class="footer-block">
                    <p class="link-title">
                        <a class="text-white d-block" data-toggle="collapse" href="#features-nav" aria-expanded="true" aria-controls="features-nav">FEATURES <i class="fa fa-angle-down float-right arrow-down d-block d-lg-none"></i></a>
                    </p>
                    <ul id="features-nav" class="nav flex-column collapse">
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 1</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 2</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 3</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 4</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="footer-block">
                    <p class="link-title">
                        <a class="text-white d-block" data-toggle="collapse" href="#benefits-nav" aria-expanded="true" aria-controls="benefits-nav">BENEFITS <i class="fa fa-angle-down float-right arrow-down d-block d-lg-none"></i></a>
                    </p>
                    <ul id="benefits-nav" class="nav flex-column collapse">
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 1</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 2</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 3</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 4</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="footer-block">
                    <p class="link-title">
                        <a class="text-white d-block" data-toggle="collapse" href="#downloads-nav" aria-expanded="true" aria-controls="downloads-nav">DOWNLOADS<i class="fa fa-angle-down float-right arrow-down d-block d-lg-none"></i></a>
                    </p>
                    <ul id="downloads-nav" class="nav flex-column collapse">
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 1</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 2</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 3</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 4</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="footer-block">
                    <p class="link-title">
                        <a class="text-white d-block" data-toggle="collapse" href="#resources-nav" aria-expanded="true" aria-controls="resources-nav">RESOURCES<i class="fa fa-angle-down float-right arrow-down d-block d-lg-none"></i></a>
                    </p>
                    <ul id="resources-nav" class="nav flex-column collapse">
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 1</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 2</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 3</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 4</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="footer-block">
                    <p class="link-title">
                        <a class="text-white d-block" data-toggle="collapse" href="#companies-nav" aria-expanded="true" aria-controls="companies-nav">COMPANIES<i class="fa fa-angle-down float-right arrow-down d-block d-lg-none"></i></a>
                    </p>
                    <ul id="companies-nav" class="nav flex-column collapse">
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 1</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 2</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 3</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="#">Link 4</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-6 text-white">
                <small>&copy; 2018 ECCA Health</small>
            </div>
            <div class="col-6 text-white">
                <small class="float-right build">Build 1808.20.05</small>
            </div>
        </div>
    </div>
</section>