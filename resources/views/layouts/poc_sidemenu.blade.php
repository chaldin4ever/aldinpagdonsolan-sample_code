<!-- Sidebar navigation -->
@php
    if(\Illuminate\Support\Facades\Auth::check()){
        $userid = \Illuminate\Support\Facades\Auth::user()->_id;
        $user = \App\User::find($userid);
    }

@endphp

    <div id="slide-out" class="side-nav   mdb-sidenav"  style="transform: translateX(-100%);">
        <ul class="custom-scrollbar ">
            <!-- Logo -->
            <li style="margin-bottom: 50px;">
                <div class="logo-wrapper waves-light waves-effect waves-light">

                    <a href="#"><img src="{{asset('images/ecca-logo.png')}}" class="img-fluid flex-center"></a>
                </div>
            </li>
            <!--/. Logo -->
            <!--Social-->

            <!--Search Form-->
            @auth
                <li>
                    <form class="search-form" role="search">
                        <div class="form-group md-form mt-0 pt-1 waves-light waves-effect waves-light">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                    </form>
                </li>
            @endauth
            <!--/.Search Form-->
            <!-- Side navigation links -->
            <li>
                @auth
                    <ul class="collapsible collapsible-accordion">
                        @if(!empty($user->Facility))
                            {{--<li>--}}
                                {{--<a href="{{url('indicator')}}" class="collapsible-header waves-effect arrow-r "><i class="fa fa-list" aria-hidden="true"></i> {{__('Indicators')}}</a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{url('resident')}}" class="collapsible-header waves-effect arrow-r "><i class="fa fa-users" aria-hidden="true"></i> {{__('Residents')}}</a>
                            </li>
                            <li>
                                <a href="{{url('funding')}}" class="collapsible-header waves-effect arrow-r "><i class="fa fa-money" aria-hidden="true"></i> {{__('Funding')}}</a>
                            </li>
                            <li>
                                <a href="{{url('sprocess')}}" class="collapsible-header waves-effect arrow-r "><i class="fa fa-check-square-o" aria-hidden="true"></i> {{__('Processes')}}</a>
                            </li>
                            <li>
                                <a href="{{url('lists')}}" class="collapsible-header waves-effect arrow-r "><i class="fa fa-bars" aria-hidden="true"></i> {{__('Lists')}}</a>
                            </li>
                            <li>
                                <a href="{{url('messages')}}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-envelope"></i> {{__('Messages')}}</a>
                            </li>
                            <li>
                                <a  class="collapsible-header waves-effect arrow-r"><i class="fa fa-globe"></i> {{__('Intranet')}} <i class="fa fa-angle-down rotate-icon"></i></a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="{{url('intranet/news')}}" class="waves-effect"><i class=" fa fa-bullhorn "></i> {{__('News')}}</a></li>
                                        <li><a href="{{url('intranet/jobs')}}" class="waves-effect"><i class=" fa fa-flask "></i> {{__('Jobs')}}</a></li>
                                        <li><a href="{{url('intranet/photos')}}" class="waves-effect"><i class=" fa fa-camera "></i> {{__('Photos')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a  class="collapsible-header waves-effect arrow-r"><i class="fa fa-bar-chart"></i> {{__('Reports')}} <i class="fa fa-angle-down rotate-icon"></i></a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="{{url('report/incident')}}" class="waves-effect"><i class=" fa fa-building "></i> {{__('Incidents')}}</a></li>
                                        <li><a href="{{url('report/infection')}}" class="waves-effect"><i class=" fa fa-home "></i> {{__('Infections')}}</a></li>
                                        <li><a href="{{url('report/weight')}}" class="waves-effect"><i class=" fa fa-home "></i> {{__('Weights')}}</a></li>
                                        <li><a href="{{url('report/bgl')}}" class="waves-effect"><i class=" fa fa-home "></i> {{__('BGL')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a  class="collapsible-header waves-effect arrow-r"><i class="fa fa-paw"></i> {{__('Admin')}} <i class="fa fa-angle-down rotate-icon"></i></a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="{{url('provider')}}" class="waves-effect"><i class=" fa fa-building "></i> {{__('Provider')}}</a></li>
                                        <li><a href="{{url('facility')}}" class="waves-effect"><i class=" fa fa-home "></i> {{__('Facility')}}</a></li>
                                        <li><a href="{{url('caredomain')}}" class="waves-effect"><i class=" fa fa-user-md "></i> {{__('Care Domains')}}</a></li>
                                        <li><a href="{{url('role')}}" class="waves-effect"><i class=" fa fa-user "></i> {{__('User Role')}}</a></li>
                                        <li><a href="{{url('user')}}" class="waves-effect"><i class=" fa fa-users "></i> {{__('Users')}}</a></li>
                                        <li><a href="{{url('form')}}" class="waves-effect"><i class=" fa fa-file-text-o "></i> {{__('Forms')}}</a></li>
                                        <li><a href="{{url('logic')}}" class="waves-effect"><i class=" fa fa-legal "></i> {{__('Logics')}}</a></li>
                                        <li><a href="{{url('folders')}}" class="waves-effect"><i class=" fa fa-folder "></i> {{__('Folders')}}</a></li>
                                        <li><a href="{{url('picklist')}}" class="waves-effect"><i class=" fa fa-file-text-o "></i> {{__('Picklist')}}</a></li>
                                        <li><a href="{{url('config')}}" class="waves-effect"><i class=" fa fa-cog "></i> {{__('Config Forms')}}</a></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a href="{{url('training')}}" target="_blank" class="collapsible-header waves-effect arrow-r"><img src="{{asset('images/mede-logo.png')}}" height="15px"/> <span style="color:#8AC66E">MedeHealth</span></a>
                            </li>
                            <li>
                                <a href="{{url('helpdesk')}}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-info-circle "></i> {{__('Helpdesk')}}</a>
                            </li>
                        @endif

                        <li><a class="collapsible-header waves-effect arrow-r" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <li>
                            <a href="">Build {{env('BUILD_NUMBER')}}</a>
                        </li>
                    </ul>
                @endauth
            </li>
            <!--/. Side navigation links -->
        </ul>
        <div class="sidenav-bg mask-strong"></div>
    </div>
<!--/. Sidebar navigation -->



<!-- Navbar -->
<nav class="navbar navbar-main navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
    <!-- SideNav slide-out button -->

    <div class="float-left ml-3">
        <a href="#" data-activates="slide-out" class="button-collapse text-white" id="nav-slideout-icon">
            <i class="fa fa-bars" style="font-size: 28px; display: block; text-align: center;"></i>
        </a>
    </div>

    <!-- Breadcrumb-->
    <div class="breadcrumb-dn mr-4">
        <p>
            <a href="{{url('/intranet')}}" class="p-0 m-0"><img src="{{asset('images/logo-white-text.png')}}" class="img-fluid flex-center" style="height:50px;"></a>
        </p>
    </div>

    @include('news.announcement_carousel')

    @php

        $facility = \App\Utils\Toolkit::getFacility();

    @endphp
    @if(!empty($facility))
        <div class="">
            @auth
                @php
                    $user = Auth::user();
                    $key = 'selected.facility-name.' . $user->_id;
                    $name = Redis::get($key);
                @endphp
                    <div style="width:35px;display: inline-block">
                        <a href="{{url('messages')}}"><div class="bell-top bell-orange" id="alert_message">0</div></a>
                    </div>
                    <div style="width:35px;display: inline-block">
                        <a href="{{url('pinboard')}}"><div class="bell-top bell-purple" id="alert_pinboard">0</div></a>
                    </div>
                    <div style="display: inline-block"><a href="{{url('resident')}}">{{$name}}</a></div>
                    <div style="width:35px;display: inline-block"><a href="{{url('facility/select')}}"><i class="fa fa-caret-down"></i></a></div>
                </ul>
            @endauth
        </div>
    @endif
    <ul class="nav navbar-nav nav-flex-icons ml-auto">
        
        <li class="nav-item">
            @auth
            <a class="nav-link waves-effect waves-light" href="{{url('user/profile')}}"><i class="fa fa-user"></i> <span class="clearfix d-none d-sm-inline-block">{{Auth::user()->name}}</span></a>
            @endauth
        </li>

    </ul>
</nav>
<!-- /.Navbar -->