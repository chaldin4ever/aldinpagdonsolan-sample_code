<!doctype html>
<html lang="{{ app()->getLocale() }}" class="full-height">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'OI Clinical') }}</title>

        <!-- Fonts -->
        <link href="{{ asset('mdb/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('mdb/css/mdb.min.css') }}" rel="stylesheet">
        <link href="{{ asset('mdb/css/style.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/mdb.style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
    </head>
    <body>

<!--Main Navigation-->
<header>

    <nav class="navbar fixed-top navbar-expand-lg navbar-light scrolling-navbar ">
        <a class="navbar-brand" href="#">
                <img class="brand-logo" src="{{asset('images/Logo-short.png')}}" height="30px"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('login')}}">Login</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="view intro-2" style="">
        <div class="full-bg-img ">
            <div class="mask flex-center">
                <div class="container text-center white-text wow fadeInUp">

                    

                </div>
            </div>
        </div>
    </div>

</header>
<!--Main Navigation-->

<!--Main Layout-->
<main class="text-center py-5">

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @yield('content')

            </div>
        </div>
    </div>

</main>
<!--Main Layout-->

        

        
    <script src="{{asset('mdb/js/jquery-3.2.1.min.js')}}" ></script>
    <script src="{{asset('mdb/js/popper.min.js')}}" ></script>
    
    <script src="{{asset('mdb/js/bootstrap.min.js')}}" ></script>
    <script src="{{asset('mdb/js/mdb.min.js')}}" ></script>

    </body>
</html>
