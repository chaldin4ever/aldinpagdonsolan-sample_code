@push('styles')
<link href="{{ asset('css/front.css') }}" rel="stylesheet">
@endpush

@extends('layouts.main')

@section('content')
<!--header section-->
<section id="header-section" class="front-header">
    @include('layouts.menu')
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-none d-lg-block" >
                    <img src="/images/group-11.png" width="100%"/>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="header-right-content text-white">
                        <h1 class="mb-2">#1 Software for <br>Care and Clinical <br>Documentation</h1>
                        <p>ECCA comes with features that you would expect in a care & clinical software plus a lot more. Check it out now!</p>
                        <p class="mt-5">
                            <a class="btn btn-try-for-free ml-5 ml-md-0 mr-md-3 mr-5">TRY IT FOR FREE</a>
                            <a href="" class="how-it-works text-white"><img src="/images/play.png"/> <span>Watch How it Works</span></a>
                        </p>
                    </div>
                </div>
                <div class="col-7 mt-5 offset-md-5 d-none d-md-block d-lg-none">
                    <img src="/images/group-11.png" width="100%"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end of header section-->

<!-- second section-->
<section id="second-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 mt-lg-6">
                <h2 class="mb-3 txt-black">You are in <br> total control</h2>
                <p class="mb-3">ECCA provides three core technologies that allow an organisation to create organisation-wide standarised processes and forms.</p>
                <p><a class="btn btn-danger" href="">GET STARTED FOR FREE</a></p>
            </div>
            <div class="col-12 col-lg-4 mt-7">
                <div class="card">
                    <div class="card-body">
                    <br/><br/><br/>
                    <img src="/images/pie-chart.png" class="mb-3"/>
                    <h5 class="card-title txt-black-2">Forms</h5>
                    <p class="card-text">ECCA comes almost 100+ ready-to-use forms & assessment tools. It also has a visual form editor allowing you to customise existing forms and build new ones.</p>
                    <a href="#" class="card-link learn-more">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 mt-5 mt-lg-0">
                <div class="card">
                    <div class="card-body">
                    <br/><br/><br/>
                    <img src="/images/goal.png" class="mb-3"/>
                    <h5 class="card-title txt-black-2">Processes</h5>
                    <p class="card-text">You can use 10+ standarised processes come with ECCA or create your own processes that are tailored for your organisation</p>
                    <a href="#" class="card-link learn-more">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-4 offset-lg-4">
                <div class="card mt-5">
                    <div class="card-body">
                        <br/><br/><br/>
                        <img src="/images/puzzle.png" class="mb-3"/>
                        <h5 class="card-title txt-black-2">Logics</h5>
                        <p class="card-text">Using Logics in ECCA, you can decide how to handle or process data collected from Forms.</p>
                        <a href="#" class="card-link learn-more">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="offset-md-5 offset-lg-0 col-md-7 col-lg-4 d-none d-md-block text-center">
                <img src="/images/group-19.png" width="50%" class="mt-5"/>
            </div>
        </div>
    </div>
</section>
<!-- end of second section-->    

<!-- third section -->
<section id="third-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 staff-training">
                <h2 class="mb-3">Staff training included</h2>
                <p class="mb-3">ECCA is partnering with MedeHealth to provide free online training courses for your staff.</p>
                <p><a class="btn btn-danger mt-md-4" href="">GET STARTED FOR FREE</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="card card-competent-staff">
                    <div class="card-body">
                        <br/><br/><br/>
                        <img src="/images/group-26.png" class="mb-3"/>
                        <h5 class="card-title txt-black-2">Competent Staff</h5>
                        <p class="card-text">All staff will have access to online training to ensure they are competent in using ECCA</p>
                        <a href="#" class="card-link learn-more">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-access-everywhere">
                    <div class="card-body">
                        <br/><br/><br/>
                        <img src="/images/group-25.png" class="mb-3"/>
                        <h5 class="card-title txt-black-2">Access Everywhere</h5>
                        <p class="card-text">Online training can be accessed via Wed or mobile phone.</p>
                        <a href="#" class="card-link learn-more">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-4 d-none d-lg-block text-center mt-20">
                <img src="/images/group-22.png" width="50%" />
            </div>
        </div>
    </div>
    <div class="banner">
        <img src="/images/group-12.png"/>
    </div>
</section>
<!-- end of third section -->

<!-- fourth section -->
<section id="fourth-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <img src="/images/group-28.png" width="100%"/>
            </div>
        </div>
        <div class="row first-row">
            <div class="col-md-6 col-lg-4">
                <br/><br/><br/>
                <img src="/images/id.png" class="mb-3"/>
                <h5 class="card-title">Eliminate Missed Signatures</h5>
                <p class="card-text">EMMA ensures staff must sign for every medication administered. Missing signature issue is a thing of the past from day one.</p>
            </div>
            <div class="col-md-6 col-lg-4">
                <br/><br/><br/>
                <img src="/images/notification.png"  class="mb-3"/>
                <h5 class="card-title">Prompts, Alerts & Indicators</h5>
                <p class="card-text">EMMA is full of prompt and alerts to ensure staff remember to take INR, BGL, etc.</p>
            </div>
            <div class="col-md-6 col-lg-4">
                <br/><br/><br/>
                <img src="/images/clock.png" width="43" class="mb-3"/>
                <h5 class="card-title">Extensive up to the Minute Reporting</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
            </div>
            <div class="col-md-6 col-lg-4">
                <br/><br/><br/>
                <img src="/images/globe.png" class="mb-3"/>
                <h5 class="card-title">Remote Accessibility for all Stakeholders</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
            </div>
            <div class="col-md-6 col-lg-4">
                <br/><br/><br/>
                <img src="/images/data.png" class="mb-3"/>
                <h5 class="card-title">Real Time Medicine Data</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
            </div>
            <div class="col-md-6 col-lg-4">
                <br/><br/><br/>
                <img src="/images/cloud.png" width="54" class="mb-3"/>
                <h5 class="card-title">Access Everywhere</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
            </div>
        </div>
    </div>
    <div class="right-banner d-block d-md-none d-lg-block">
        <div class="card mt-5 mt-md-0">
            <div class="card-body">
                <br/><br/>
                <h2 class="card-title">Medication Management Made Easy</h2>
                <p class="card-text">ECCA is integrated with Compact's Electronic Medication Management Administration (EMMA). You can choose to implement medication management from day one or at a later day. The choice is yours.</p>
                <a href="#" class="btn btn-danger">LEARN MORE</a>
            </div>
        </div>
    </div>
</section>
<!-- end of fourth section -->
@endsection