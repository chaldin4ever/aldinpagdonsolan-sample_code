<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="full-height">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120436498-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120436498-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OI DESK') }}</title>

    <!-- Include external CSS. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 
    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
  
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"> -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css">

    <!-- DevExtreme -->
    <link rel="stylesheet" type="text/css" href="{{asset('devextreme/dist/css/dx.common.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('devextreme/dist/css/dx.light.css')}}" />

    <!-- Styles -->
    <link href="{{ asset('css/jquery.tagsinput.css') }}" rel="stylesheet">

    <link href="{{ asset('mdb/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mdb/css/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mdb/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.style.css') }}" rel="stylesheet">
    
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dropzone.min.css') }}" rel="stylesheet" type="text/css">

    @yield('style')
    
</head>
<body>
<div id="app">
    <!--Main Layout-->
    <div class="container p-2">
        <div class="content">
        @yield('content')
        </div>
    </div>
    <!--Main Layout-->

</div>

    <!-- Scripts -->
    <script src="{{asset('mdb/js/jquery-3.2.1.min.js')}}" ></script>
    <script src="{{asset('mdb/js/popper.min.js')}}" ></script>
    
    <script src="{{asset('mdb/js/bootstrap.min.js')}}" ></script>
    <script src="{{asset('mdb/js/mdb.min.js')}}" ></script>
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->

    <script src="{{asset('axios/dist/axios.min.js')}}"></script>

    <!-- Include external JS libs. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/js/froala_editor.pkgd.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    
    <!-- DecExtreme -->
    <script type="text/javascript" src="{{asset('jszip/dist/jszip.js')}}"></script>
    <script type="text/javascript" src="{{asset('devextreme/dist/js/dx.all.js')}}"></script>

    <!-- https://github.com/underovsky/jquery-tagsinput-revisited -->
    <script src="{{asset('js/jquery.tagsinput.js')}}" ></script>
    <script src="{{asset('js/autosize.min.js')}}" ></script>
    <script src="{{asset('js/dropzone.min.js')}}" ></script>
    <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
    

    <!-- Initialize the editor. -->
    <script> 
    $(function () {
        checkNew();
        
            $("textarea[type!='plain']").froalaEditor({
                    toolbarInline: false,
                    charCounterCount: false,
                    toolbarButtons: [
                        'emoticons', 'fontFamily', 'fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough',
                        'align', 'formatOL', 'formatUL', 'indent', 'outdent', 
                        'insertTable', 
                        'undo', 'redo', 
                        'insertImage', 'insertFile', 'insertLink', 
                        'html', 'clearFormatting'
                    ],
                    toolbarVisibleWithoutSelection: true,

                    fileUploadParam: 'file_name',
                    fileUploadURL: '/upload/image',
                    // Set the image upload parameter.
                    imageUploadParam: 'file_name',

                    // Set the image upload URL.
                    imageUploadURL: '/upload/image',

                    // Set request type.
                    imageUploadMethod: 'POST',

                    // Set max image size to 5MB.
                    imageMaxSize: 5 * 1024 * 1024,
                    imageDefaultWidth: 350,
                    imageDefaultDisplay: 'inline',
                    imageStyles: {
                        "img-fluid":"img-fluid"
                    },

                    // Allow to upload PNG and JPG.
                    imageAllowedTypes: ['jpeg', 'jpg', 'png']
                })
                .on('froalaEditor.image.beforeUpload', function (e, editor, images) {
                    // Return false if you want to stop the image upload.
                    // console.log('beforeUpload');
                })
                .on('froalaEditor.image.uploaded', function (e, editor, response) {
                    // Image was uploaded to the server.
                    // console.log('uploaded');

                })
                .on('froalaEditor.image.inserted', function (e, editor, $img, response) {
                    // Image was inserted in the editor.
                    // console.log('inserted');
                })
                .on('froalaEditor.image.replaced', function (e, editor, $img, response) {
                    // Image was replaced in the editor.
                    // console.log('replaced');
                })
                .on('froalaEditor.image.error', function (e, editor, error, response) {
                    console.log('error' + error.code);
                    // Bad link.
                    if (error.code == 1) {}

                    // No link in upload response.
                    else if (error.code == 2) {}

                    // Error during image upload.
                    else if (error.code == 3) {}

                    // Parsing response failed.
                    else if (error.code == 4) {}

                    // Image too text-large.
                    else if (error.code == 5) {}

                    // Invalid image type.
                    else if (error.code == 6) {}

                    // Image can be uploaded only to same domain in IE 8 and IE 9.
                    else if (error.code == 7) {}

                    // Response contains the original server response to the request if available.
                });

        });
         </script>

        @if(url('')=='http://oidesk.devp')
            <script id="fr-fek">
                try {
                    (function (k) {
                        localStorage.FEK = k;
                        t = document.getElementById('fr-fek');
                        t.parentNode.removeChild(t);
                    })('qC1C2B1qB2C1D6B5E1F5H5A1B3A10dvsF-11ynuE2ddts==')
                } catch (e) {}
            </script>
        @endif

        @if(url('')=='https://www.oidesk.com' || url('')=='http://www.oidesk.com')
            <script id="fr-fek">
                try {
                    (function (k) {
                        localStorage.FEK = k;
                        t = document.getElementById('fr-fek');
                        t.parentNode.removeChild(t);
                    })('TA14A7D9jC5D4G3F4J2A7A8C4A4A2A-9utsC8inllyh1F4nab==')
                } catch (e) {}
            </script>
        @endif

        <script>
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }

            
            $(function(){
                setInterval(checkNew, 5000);
            });

            function checkNew(){
                var url = "{{url('chat/checkNew')}}";
                axios.post(url)
                    .then(function(response){
                        // console.log(response.data);
                        if(response.data.message == 0){
                            $('#new-message-counter').removeClass('bell-orange');
                            $('#new-message-counter').html('<i class="fa fa-envelope" aria-hidden="true"></i>');
                        }
                        else{
                            $('#new-message-counter').addClass('bell-orange');
                            var count = response.data.message;
                            if(count >= 100)
                                $('#new-message-counter').addClass('small-font').html(response.data.message);
                            else
                                $('#new-message-counter').removeClass('small-font').html(response.data.message);
                        }
                        if(response.data.converse == 0){
                            $('#new-journal-counter').removeClass('bell-purple');
                            $('#new-journal-counter').html('<i class="fa fa-comment" aria-hidden="true"></i>');
                        }
                        else{
                            $('#new-journal-counter').addClass('bell-purple');
                            var count = response.data.converse;
                            if(count >= 100)
                                $('#new-journal-counter').addClass('small-font').html(count);
                            else
                                $('#new-journal-counter').removeClass('small-font').html(count);
                        }
                });
            }
        </script>

    @yield('script')

    
   
</body>
</html>
