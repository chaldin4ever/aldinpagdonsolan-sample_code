<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="full-height">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ECCA') }}</title>
    @include('layouts.poc_stylesheets')
    @yield('style')


</head>
<body class="hidden-sn  fixed-sn blue-skin white-skin p-0">
<div id="app">

    <header>
        @include('layouts.poc_sidemenu')
    </header>
    <!--Main Navigation-->
<!--Main Layout-->
    <main>

        @yield('content')

    </main>
    <!--Main Layout-->

    <!-- Footer -->
    {{--<footer class="page-footer font-small pt-0 mt-0 bg-white">

        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3 bg-white text-dark">© 2018 Copyright: ECCA Health
        </div>
        <!-- Copyright -->

    </footer>--}}
    <!-- Footer -->

</div>
<script src="{{ asset('js/app.js') }}"></script>
<!-- Scripts -->
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>

<script src="{{asset('mdb/js/popper.min.js')}}" ></script>

<script src="{{asset('mdb/js/mdb.js')}}" ></script>

<script src="{{ asset('js/dragula.js') }}"></script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<!-- Include external JS libs. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>


<!-- Include Editor JS files. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.0/js/froala_editor.pkgd.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{ asset('js/alertify.min.js') }}"></script>
<!-- https://github.com/underovsky/jquery-tagsinput-revisited -->
{{--<script src="{{asset('js/jquery.tagsinput.min.js')}}" ></script>--}}
<script src="{{asset('jsoneditor/jsoneditor.min.js')}}"></script>

<script>

    // SideNav Button Initialization
    $(".button-collapse").sideNav('close');
    // SideNav Scrollbar Initialization
    var sideNavScrollbar = document.querySelector('.custom-scrollbar');
    Ps.initialize(sideNavScrollbar);

</script>
<div class="drag-target" style="left: 0px; touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
<div class="hiddendiv common"></div>
<!-- Initialize the editor. -->
<script>
    $(function () {
        $('textarea.froala-editor').froalaEditor({
            toolbarInline: false,
            charCounterCount: false,
            toolbarButtons: ['fontFamily', 'fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough',
                'paragraphStyle',
                'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                'insertTable',
                'undo', 'redo',
                'insertImage', 'insertLink', 'insertFile',
                'html', 'clearFormatting'
            ],
            toolbarVisibleWithoutSelection: true,

            fileUploadParam: 'file_name',
            fileUploadURL: '/upload/image',
            // Set the image upload parameter.
            imageUploadParam: 'file_name',

            // Set the image upload URL.
            imageUploadURL: '/upload/image',

            // Set request type.
            imageUploadMethod: 'POST',

            // Set max image size to 5MB.
            imageMaxSize: 5 * 1024 * 1024,
            imageDefaultWidth: 350,
            imageDefaultDisplay: 'inline',
            imageStyles: {
                "img-fluid":"img-fluid"
            },

            // Allow to upload PNG and JPG.
            imageAllowedTypes: ['jpeg', 'jpg', 'png']
        })
            .on('froalaEditor.image.beforeUpload', function (e, editor, images) {
                // Return false if you want to stop the image upload.
                // console.log('beforeUpload');
            })
            .on('froalaEditor.image.uploaded', function (e, editor, response) {
                // Image was uploaded to the server.
                // console.log('uploaded');

            })
            .on('froalaEditor.image.inserted', function (e, editor, $img, response) {
                // Image was inserted in the editor.
                // console.log('inserted');
            })
            .on('froalaEditor.image.replaced', function (e, editor, $img, response) {
                // Image was replaced in the editor.
                // console.log('replaced');
            })
            .on('froalaEditor.image.error', function (e, editor, error, response) {
                console.log('error' + error.code);
                // Bad link.
                if (error.code == 1) {}

                // No link in upload response.
                else if (error.code == 2) {}

                // Error during image upload.
                else if (error.code == 3) {}

                // Parsing response failed.
                else if (error.code == 4) {}

                // Image too text-large.
                else if (error.code == 5) {}

                // Invalid image type.
                else if (error.code == 6) {}

                // Image can be uploaded only to same domain in IE 8 and IE 9.
                else if (error.code == 7) {}

                // Response contains the original server response to the request if available.
            });


            $('textarea.froala-editor-sm').froalaEditor({
            toolbarInline: false,
            charCounterCount: false,
            toolbarButtons: ['fontSize','color', 'bold', 'italic', 'underline', 'strikeThrough',
                'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                'undo', 'redo',
                'html', 'clearFormatting'
            ],
            toolbarVisibleWithoutSelection: true,

            fileUploadParam: 'file_name',
            fileUploadURL: '/upload/image',
            // Set the image upload parameter.
            imageUploadParam: 'file_name',

            // Set the image upload URL.
            imageUploadURL: '/upload/image',

            // Set request type.
            imageUploadMethod: 'POST',

            // Set max image size to 5MB.
            imageMaxSize: 5 * 1024 * 1024,
            imageDefaultWidth: 350,
            imageDefaultDisplay: 'inline',
            imageStyles: {
                "img-fluid":"img-fluid"
            },

            // Allow to upload PNG and JPG.
            imageAllowedTypes: ['jpeg', 'jpg', 'png']
        })
            .on('froalaEditor.image.beforeUpload', function (e, editor, images) {
                // Return false if you want to stop the image upload.
                // console.log('beforeUpload');
            })
            .on('froalaEditor.image.uploaded', function (e, editor, response) {
                // Image was uploaded to the server.
                // console.log('uploaded');

            })
            .on('froalaEditor.image.inserted', function (e, editor, $img, response) {
                // Image was inserted in the editor.
                // console.log('inserted');
            })
            .on('froalaEditor.image.replaced', function (e, editor, $img, response) {
                // Image was replaced in the editor.
                // console.log('replaced');
            })
            .on('froalaEditor.image.error', function (e, editor, error, response) {
                console.log('error' + error.code);
                // Bad link.
                if (error.code == 1) {}

                // No link in upload response.
                else if (error.code == 2) {}

                // Error during image upload.
                else if (error.code == 3) {}

                // Parsing response failed.
                else if (error.code == 4) {}

                // Image too text-large.
                else if (error.code == 5) {}

                // Invalid image type.
                else if (error.code == 6) {}

                // Image can be uploaded only to same domain in IE 8 and IE 9.
                else if (error.code == 7) {}

                // Response contains the original server response to the request if available.
            });
    });
</script>

@if(url('')=='https://www.eccahealth.com')
    <script id="fr-fek">
        try {
            (function (k) {
                localStorage.FEK = k;
                t = document.getElementById('fr-fek');
                t.parentNode.removeChild(t);
            })('UC11A5A6kF4A3G3A3C6B7B7A3E3F3C-8fgfgomhfD-8dA3md1C-13==')
        } catch (e) {}
    </script>
@endif


<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $(document).ready(function () {

        setInterval(checkAlerts, {{env('CHECK_ALERT_INTERVAL')}});
        function checkAlerts() {
            var url = "{{url('alert/check')}}";
            axios.post(url)
                .then(function(resp){
                    var resp = resp.data;
                    // console.log(resp);
                    // console.log(resp);
                    $("#alert_message").html(resp.message);
                    $("#alert_pinboard").html(resp.pinboard);
                });
        }

    });
</script>

@yield('script')



</body>
</html>
