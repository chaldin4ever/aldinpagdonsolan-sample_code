<!doctype html>
<html lang="{{ app()->getLocale() }}" class="full-height">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ config('app.name', 'OI Clinical') }}</title>

        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link href="{{ asset('mdb/css/mdb.min.css') }}" rel="stylesheet">
        <link href="{{ asset('mdb/css/style.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/mdb.style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        @stack('styles')
        <link href="{{ asset('css/media-query.css') }}" rel="stylesheet">
    </head>
    <body>
    
    <!-- content -->
    @yield('content')
    <!-- end of content-->

    <!-- footer section -->
    @include('layouts.footer')
    <!-- end of footer section-->
    <script src="{{asset('mdb/js/jquery-3.2.1.min.js')}}" ></script>
    <script src="{{asset('mdb/js/popper.min.js')}}" ></script>
    
    <script src="{{asset('mdb/js/bootstrap.min.js')}}" ></script>
    <script src="{{asset('mdb/js/mdb.min.js')}}" ></script>

    </body>
</html>
