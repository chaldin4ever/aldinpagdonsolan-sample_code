<!-- Modal -->
<div class="modal fade" id="archivePicklistModal" tabindex="-1" role="dialog" aria-labelledby="archivePicklistModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <form method="post" action="{{url('picklist/store')}}" id="picklist" >
            {{--@csrf--}}
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-0">
                    <h5 class="modal-title">{{__('Archive Picklist')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="picklistId" id="picklistId" value="" />
                            <input type="hidden" name="Archive" id="Archive" value="Yes" />
                            <div class="md-form">
                                <input required type="text" id="PicklistName" name="PicklistName" value="" autocomplete="off" class="form-control" placeholder=" ">
                                <label for="PicklistName">{{__('Picklist Name')}}</label>
                            </div>

                        </div>

                    </div>



                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn btn-primary mr-3 ">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>