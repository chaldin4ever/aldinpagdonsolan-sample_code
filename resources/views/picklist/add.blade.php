<!-- Modal -->
<div class="modal fade" id="addPicklistModal" tabindex="-1" role="dialog" aria-labelledby="addPicklistModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" action="{{url('picklist/store')}}" id="picklist" >
            {{--@csrf--}}
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-0">
                    <h5 class="modal-title">{{__('Add Picklist')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col">

                            <div class="md-form">
                                <input type="hidden" name="picklistId" id="picklistId" value="" />
                                <input type="hidden" name="Archive" id="Archive" value="No" />
                                <input required type="text" id="PicklistName" name="PicklistName" value="" autocomplete="off" class="form-control" placeholder=" ">
                                <label for="PicklistName">{{__('Picklist Name')}}</label>
                            </div>
                            <div class="list">
                                <h5 class="subpage-title">{{__('Lists')}} <button type="button" class="add-picklist btn btn-sm btn-blue-grey btn-rounded p-2"
                                    onclick="
                                    $('ol.picklist').append('<li>'+$('ol.picklist li:first').html()+'</li>');
                                    $('ol.picklist li:not(:first-child) button').removeClass('invisible');
                                    "><i class="fa fa-plus"></i></button></h5>
                                <ol class="picklist pl-1">
                                    <li>
                                        <div class="row">
                                        <div class="md-form mt-1 col-11 pr-0">
                                            <input required type="text" id="Code" name="Code[]" value=""  autocomplete="off" autofocus class=" form-control md-input float-left w-25 "  placeholder="Code">
                                            <input required type="text" id="Picklists" name="Picklists[]" value="" autocomplete="off" autofocus class="ml-2 form-control md-input float-left " style="width: 73%;"   placeholder="Text">

                                        </div>

                                        <div class="col-1 delete pl-0"><button  type="button" class="btn btn-sm btn-danger btn-rounded p-1 mt-3 float-left invisible"
                                                                    onclick="$(this).closest('li').remove();"><i class="fa fa-minus"></i></button></div>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>

                    </div>



                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn btn-primary mr-3 ">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>