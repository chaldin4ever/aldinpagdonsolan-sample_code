@extends('layouts.poc')

@section('style')
<style>
    .white-skin input[type=checkbox].filled-in:checked+label:after {
        background-color: rgb(90, 118, 147);
        border-color: rgb(90, 118, 147);
    }
        .white-skin body{
        'Helvetica Light', Helvetica, Arial, sans-serif
        }

    /*ol.picklist :first-child div.delete button {visibility: hidden !important;}*/

</style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2"><strong><i class="fa fa-list"></i> {{__('Picklists')}}</strong></h3>
            </div>
            {{--<div class="col-4 pl-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item "><a href="{{url('landingpage')}}" ><i class="fa fa-arrow-circle-left "></i> {{__('Go Back')}}</a></li>
                    <li class="pl-5 pr-4">

                    </li>
                </ol>
            </div>--}}

            <div class="fixed-action-btn" style="top:70px; right: 105px; height: 140px;">
                <a data-toggle="modal" data-target="#addPicklistModal" class="btn-floating  btn-grey"
                    onclick=" $('div.list').show(); $('#addPicklistModal').modal('show');
                                $('#addPicklistModal input#PicklistName').removeClass('disabled');
                               $('#addPicklistModal input[name^=Picklists]').removeClass('disabled');
                               $('#addPicklistModal ol li button, #addPicklistModal h5 button.add-picklist').removeClass('disabled');
                                $('#addPicklistModal div.md-form input').val('');
                                $('#addPicklistModal ol li:not(:first)').remove();
                            "
                   style=" color: #fff;">
                    <i class="fa fa-plus"  data-toggle="tooltip" data-placement="bottom" title="{{__('Add Picklist')}}"></i>
                </a>

            </div>

        </div>
    </div>

    <div class="container-fluid">

                <div class="row bg-white">
                        <div class="col pt-3">
                            <?php /*echo $residents->render(); */?>
                            <table class="mt-3 table table-hover table-bordered table-striped">
                                <thead class="blue-grey text-white font-weight-bold" style="background: #9bb1c7 !important;">
                                    {{--<th style="width: 180px;">{{__('Facility')}}</th>--}}
                                    <tr>

                                    <th class="w-25">{{__('Name')}}</th>
                                    <th > {{__('Lists')}}</th>

                                    <th  style="width: 170px; !important">{{--{{__('Edit')}}--}}</th>
{{--                                    <th  style="width: 70px;">{{__('Archive')}}</th>--}}

                                    </tr>
                        </thead>
                        <tbody>
                        @foreach($picklists as $pl)

                            <tr @if($pl->Archive == 'Yes') style="text-decoration: line-through" @endif>
                                <td>{{$pl->PicklistName}}</td>
                                <td><ol><li>{!! implode('<li>',$pl->Picklists) !!}</ol></td>
                                <td>
                                    @if($pl->Archive == 'No')
                                        <a href="#" class="editpicklist btn btn-blue-grey btn-lg p-2 pl-3 pr-3" data-toggle="modal" data-target="#editPicklistModal"
                                           onclick="$('div.list').show();
                                                   $('#addPicklistModal ol li:not(:first)').remove();
                                                    $('#addPicklistModal').modal('show');
                                                    $('#addPicklistModal .modal-title').text('Edit Picklist');
                                                    $('#addPicklistModal input#picklistId').val('{{$pl->_id}}');
                                                    $('#addPicklistModal input#PicklistName').val('{{$pl->PicklistName}}');
                                                   $('#addPicklistModal input[name^=Picklists]').removeClass('disabled');
                                                   $('#addPicklistModal ol li button, #addPicklistModal h5 button.add-picklist').removeClass('disabled');

                                                    if($('#addPicklistModal input[name^=Picklists]').length === 1){
                                                           @for($x=0; $x <sizeof($pl->Lists)-1; $x++)
                                                                   $('#addPicklistModal button.add-picklist').trigger('click');
                                                           @endfor
                                                    }

                                                   var listdata = []; var codelist = [];
                                                   @foreach($pl->Lists as $k=>$list)
                                                            listdata.push('{{array_get($list, 'text')}}');
                                                            codelist.push('{{array_get($list, 'code')}}');
                                                    @endforeach

                                                        $('#addPicklistModal input[name^=Picklists]').each(function(key, val) {
                                                            $(this).val(listdata[key]);
                                                        });
                                                       $('#addPicklistModal input[name^=Code]').each(function(key, val) {
                                                            $(this).val(codelist[key]);
                                                       });
                                                        "><i class="fa fa-edit"></i> </a>

                                        <a href="#"
                                           onclick="
                                                   /*$('div.list').hide();
                                                   $('#addPicklistModal').modal('hide');*/
                                                   $('#archivePicklistModal').modal('show');
                                                   $('#archivePicklistModal .modal-title').text('Archive Picklist');
                                                   $('#archivePicklistModal input#picklistId').val('{{$pl->_id}}');
                                                   $('#archivePicklistModal input#PicklistName').val('{{$pl->PicklistName}}');

                                                   $('#archivePicklistModal  input#Archive').val('Yes');
                                                   $('#archivePicklistModal input#PicklistName').addClass('disabled');

                                                   //$('#addPicklistModal  textarea#Description').addClass('disabled');

                                                   "
                                           class="btn btn-blue-grey btn-lg p-2 pl-3 pr-3">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>

<!--                    --><?php //echo $picklists->render(); ?>
                </div>
            </div>
        </div>
    </div>

    @include('picklist.add')
    @include('picklist.archive')
@endsection

@section('script')
    <script>

        $('#myModal').modal('show');

        @if(session('status'))
            toastr.success('{{session('status')}}').css('width', 'auto');
        @endif

    </script>
    @endsection

