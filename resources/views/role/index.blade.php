@extends('layouts.poc')

@section('style')
    <style>

        tr.archive {text-decoration: line-through !important;};
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-user"></i> {{__('User Roles')}}</strong></h3>
                <a data-toggle="modal" onclick="reset()" data-target="#addRoleModal" class="float-right btn btn-blue-grey pl-3 pr-3"
                   style=" color: #fff;">{{__('Add New Role')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">
                @if(!empty($roles))
                    @if(sizeof($roles) > 0)
                        <table class="table table-striped table-hover w-50">
                            <thead>
                            <tr>
                                <th>{{__('Roles')}}</th>

                                <th style="width: 250px;"></th>
                            </tr>
                            </thead>
                            <tbody>

                                       @foreach($roles as $role)
                                           <tr id="{{$role->_id}}" class=" @if(isset($role->Archive)) @if($role->Archive == 1) archive @endif @endif ">
                                                <td>{{$role->roleName}}</td>

                                                <td class="action">
                                                    @if(isset($role->Archive)) @if($role->Archive == 1)

                                                    @endif

                                                        @else

                                                        <a href="{{url('role/permission/'.$role->_id)}}" class=" mt-0 mb-1 btn btn-sm mdb-color text-white p-1" >
                                                            <i class="fa fa-key " ></i> Permission
                                                        </a>

                                                            <a href="#" data-toggle="modal" data-target="#addRoleModal" onclick="getRole('{{$role->_id}}', '{{$role->roleName}}')">
                                                                <i class="fa fa-edit "  style="font-size: 21px;"></i>
                                                            </a>

                                                            <a href="#" data-toggle="modal" onclick="archive('{{$role->_id}}')">
                                                                <i class="fa fa-trash " style="font-size: 21px;"></i>
                                                            </a>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach

                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info">{{__('No record(s) found.')}}</div>
                    @endif
                @endif
            </div>
        </div>

        @include('role.addrole');
    </div>



@endsection

@section('script')
    <script>

        function getRole(id, roleName){

            $('#addRoleModal h5.modal-title').text('Edit Role');
            $('#addRoleModal input#_id').val(id);
            $('#addRoleModal input#RoleName').val(roleName);

        }

        function reset(){
            $('#addRoleModal h5.modal-title').text('New Role');
            $('#addRoleModal input#_id') .val('');
            $('#addRoleModal input#RoleName') .val('');
        }

        function archive(id){

            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
            alertify.confirm('Are you sure?', function(e){

                if(e){
                    var url = '{{url('role/archive/')}}/'+id;

                    axios.get(url).then(function(){

                        $('table.table tr#'+id).addClass('archive');
                        $('table.table tr#'+id+' td.action a').remove();
                        $('.modal-backdrop').fadeOut("slow");
                        toastr.success('Successfully Saved.');
                    })
                }else{

                    $('.modal-backdrop').fadeOut("slow");
                }
            })
        }

        $(document).ready(function() {


            @if(session('status'))
            toastr.success('{{session('status')}}');
            @endif

            $('.mdb-select').material_select();

        });


    </script>
@endsection

