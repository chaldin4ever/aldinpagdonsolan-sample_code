

<!-- Modal -->
<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog" aria-labelledby="addRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{url('role/store')}}">
            {{ csrf_field() }}
            <input type="hidden" name="_id" id="_id" />
            <input type="hidden" name="providerId" id="providerId" value="{{$provider}}" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newuser">New Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class=" md-form form-group">
                        <input required class="form-control" type="text" id="RoleName"  name="roleName" autocomplete="off" placeholder=" " >
                        <label for="RoleName">Role Title</label>
                    </div>

                 {{--   <div class=" md-form form-group">
                        <input class="form-control" type="text" id="RoleDescription"  name="roleDescription" autocomplete="off" >
                        <label for="RoleDescription">Description</label>
                    </div>--}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"   >Save</button>
                </div>
            </div>
        </form>
    </div>
</div>