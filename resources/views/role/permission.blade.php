@extends('layouts.poc')

@section('style')
    <style>
        [type=checkbox][class*=filled-in]:not(:checked)+label:after {border-color: #fff!important;}
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-user"></i> {{$role->roleName}} Role(s) Permission</strong></h3>
                {{--<a data-toggle="modal" data-target="#addUserModal" class="float-right btn btn-blue-grey pl-3 pr-3"--}}
                {{--style=" color: #fff;">{{__('Add New User')}}</a>--}}
                <a href="{{url('role')}}"  class="float-right btn btn-blue-grey pl-3 pr-3"
                   style=" color: #fff;">{{__('Back')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col-9 pt-3">
{{--                <pre>{{print_r($permissions->Create)}}</pre>--}}
                <form method="POST" action="{{url('role/storepermission')}}" >
                    @csrf
                    <input type="hidden" name="roleId" value="{{$role->_id}}" />
                    <button type="submit" class="btn mdb-color float-right pl-4 pr-4"><i class="fa fa-floppy-o"></i> Save</button>
                    {{--<button type="button" class="btn btn-primary float-right pl-4 pr-4"><i class="fa fa-check"></i> Select All</button>--}}
                    <div class="form-check float-left mt-3 mr-0" >
                        <input type="checkbox" name="select_all" class="form-check-input" id="select_all" >
                        <label class="form-check-label pr-2" for="select_all"><span style="margin-left: -15x;">Check All</span></label>
                    </div>
                <table class="table table-striped table-hover">
                    <thead class="bg-dark">
                    <tr>
                        <th class="w-50">{{__('Menu Access')}}</th>
                        <th>
                            <div class="form-check float-left mt-0 pl-0" >
                                <input type="checkbox" name="select_all_create" class="form-check-input" id="select_all_create" >
                                <label class="form-check-label pr-1 " for="select_all_create"><span style="margin-left: -15x;">Create</span></label>
                            </div>
                        </th>
                        <th>
                            <div class="form-check float-left mt-0 pl-0" >
                                <input type="checkbox" name="select_all" class="form-check-input" id="select_all_read" >
                                <label class="form-check-label pr-1" for="select_all_read"><span style="margin-left: -15x;">Read</span></label>
                            </div>
                        </th>
                        <th><div class="form-check float-left mt-0 pl-0" >
                                <input type="checkbox" name="select_all_update" class="form-check-input" id="select_all_update" >
                                <label class="form-check-label pr-1" for="select_all_update"><span style="margin-left: -15x;">Update</span></label>
                            </div></th>
                        <th><div class="form-check float-left mt-0 pl-0" >
                                <input type="checkbox" name="select_all_delete" class="form-check-input" id="select_all_delete" >
                                <label class="form-check-label pr-1" for="select_all_delete"><span style="margin-left: -15x;">Delete</span></label>
                            </div></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(config('poc.MENU_LISTS') as $name=>$menu)
                        @if(is_array($menu))
                            <tr>
                                <th class="p-2 pl-4"><strong>{{$name}}</strong></th>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" name="Create[{{$name}}]" class="form-check-input create" id="Create_{{$name}}" @if(!empty($permissions->Create))@if(in_array($name, array_keys($permissions->Create))) checked @endif @endif>
                                        <label class="form-check-label" for="Create_{{$name}}"></label>
                                    </div>
                                </td>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" class="form-check-input read" name="Read[{{$name}}]" id="Read_{{$name}}"  @if(!empty($permissions->Read))@if(in_array($name, array_keys($permissions->Read))) checked @endif @endif>
                                        <label class="form-check-label" for="Read_{{$name}}"></label>
                                    </div>
                                </td>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" class="form-check-input update" name="Update[{{$name}}]" id="Update_{{$name}}"  @if(!empty($permissions->Update))@if(in_array($name, array_keys($permissions->Update))) checked @endif @endif>
                                        <label class="form-check-label" for="Update_{{$name}}"></label>
                                    </div>
                                </td>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" class="form-check-input delete" name="Delete[{{$name}}]" id="Delete_{{$name}}"  @if(!empty($permissions->Delete))@if(in_array($name, array_keys($permissions->Delete))) checked @endif @endif>
                                        <label class="form-check-label" for="Delete_{{$name}}"></label>
                                    </div>
                                </td>
                            </tr>
                            @foreach($menu as $list)
                                <tr>
                                    <td class="pl-lg-5 p-2"> <i class="fa fa-arrow-circle-right"></i> {{$list}}</td>
                                    <td class="p-2">
                                        <div class="form-check" style="height: 22px;">
                                            <input type="checkbox" name="Create[{{$list}}]" class="form-check-input create" id="Create_{{$list}}"  @if(!empty($permissions->Create))@if(in_array($list, array_keys($permissions->Create))) checked @endif @endif>
                                            <label class="form-check-label" for="Create_{{$list}}"></label>
                                        </div>
                                    </td>
                                    <td class="p-2">
                                        <div class="form-check" style="height: 22px;">
                                            <input type="checkbox" class="form-check-input read" name="Read[{{$list}}]"  id="Read_{{$list}}"  @if(!empty($permissions->Read))@if(in_array($list, array_keys($permissions->Read))) checked @endif @endif>
                                            <label class="form-check-label" for="Read_{{$list}}"></label>
                                        </div>
                                    </td>
                                    <td class="p-2">
                                        <div class="form-check" style="height: 22px;">
                                            <input type="checkbox" class="form-check-input update" name="Update[{{$list}}]" id="Update_{{$list}}"  @if(!empty($permissions->Update))@if(in_array($list, array_keys($permissions->Update))) checked @endif @endif>
                                            <label class="form-check-label" for="Update_{{$list}}"></label>
                                        </div>
                                    </td>
                                    <td class="p-2">
                                        <div class="form-check" style="height: 22px;">
                                            <input type="checkbox" class="form-check-input delete" name="Delete[{{$list}}]" id="Delete_{{$list}}"  @if(!empty($permissions->Delete))@if(in_array($list, array_keys($permissions->Delete))) checked @endif @endif>
                                            <label class="form-check-label" for="Delete_{{$list}}"></label>
                                        </div>
                                    </td>
                                </tr>
                             @endforeach
                        @else
                            <tr>
                                <th class="p-2 pl-4"><strong>{{$menu}}</strong></th>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" name="Create[{{$menu}}]" class="form-check-input create" id="Create_{{$menu}}"  @if(!empty($permissions->Create))@if(in_array($menu, array_keys($permissions->Create))) checked @endif @endif>
                                        <label class="form-check-label" for="Create_{{$menu}}"></label>
                                    </div>
                                </td>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" class="form-check-input read" name="Read[{{$menu}}]" id="Read_{{$menu}}"  @if(!empty($permissions->Read))@if(in_array($menu, array_keys($permissions->Read))) checked @endif @endif>
                                        <label class="form-check-label" for="Read_{{$menu}}"></label>
                                    </div>
                                </td>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" class="form-check-input update" name="Update[{{$menu}}]" id="Update_{{$menu}}"  @if(!empty($permissions->Update))@if(in_array($menu, array_keys($permissions->Update))) checked @endif @endif>
                                        <label class="form-check-label" for="Update_{{$menu}}"></label>
                                    </div>
                                </td>
                                <td class="p-2">
                                    <div class="form-check" style="height: 22px;">
                                        <input type="checkbox" class="form-check-input delete" name="Delete[{{$menu}}]" id="Delete_{{$menu}}"  @if(!empty($permissions->Delete))@if(in_array($menu, array_keys($permissions->Delete))) checked @endif @endif>
                                        <label class="form-check-label" for="Delete_{{$menu}}"></label>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                    <button type="submit" class="btn mdb-color float-right pl-4 pr-4">Save</button>
                </form>
            </div>
        </div>

    </div>



@endsection

@section('script')
    <script>

        $(document).ready(function(){
            @if(session('status'))
                toastr.success('{{session('status')}}');
            @endif
        })

        $("#select_all").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        $("#select_all_create").change(function () {
            $("input.create").prop('checked', $(this).prop("checked"));
        });

        $("#select_all_read").change(function () {
            $("input.read").prop('checked', $(this).prop("checked"));
        });

        $("#select_all_update").change(function () {
            $("input.update").prop('checked', $(this).prop("checked"));
        });

        $("#select_all_delete").change(function () {
            $("input.delete").prop('checked', $(this).prop("checked"));
        });

    </script>
@endsection

