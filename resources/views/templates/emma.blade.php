@php
    if(isset($ticket) && isset($ticket->Params)){
        $params = $ticket->Params;
    } else {
        $params = [];
    }
@endphp
<div class="form-inline">
            <div class="form-group">
                <span for="med-round" style="width:120px;">
                    Time: 
                </span>
                <input class="form-control"  id="p1" name="p1" required value="{{array_get($params, 'p1')}}"/>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1" onclick="$('#p1').val('N/A')">
                    <label class="form-check-label" for="defaultCheckbox1">N/A</label>
                </div>
            </div>
        </div>

        <div class="form-inline">
            <div class="form-group">
                <span for="resident"  style="width:120px;">
                    Resident Name:
                </span>
                <input class="form-control"  id="p2" name="p2" style="width:500px" required value="{{array_get($params, 'p2')}}"/>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox2" onclick="$('#p2').val('N/A')">
                    <label class="form-check-label" for="defaultCheckbox2">N/A</label>
                </div>
            </div>
        </div>

        <div class="form-inline">
            <div class="form-group">
                <span for="medication" style="width:120px;" >
                    Medication:
                </span>
                <input class="form-control"  id="p3" name="p3" style="width:500px" required value="{{array_get($params, 'p3')}}"/>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox3" onclick="$('#p3').val('N/A')">
                    <label class="form-check-label" for="defaultCheckbox3">N/A</label>
                </div>
            </div>
        </div>

        <div class="form-inline">
            <div class="form-group">
                <span for="facility" style="width:120px;" >
                    Facility/Area:
                </span>
                <input class="form-control"  id="p4" name="p4" style="width:500px"  required value="{{array_get($params, 'p4')}}"/>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox4" onclick="$('#p4').val('N/A')">
                    <label class="form-check-label" for="defaultCheckbox4">N/A</label>
                </div>
            </div>
        </div>