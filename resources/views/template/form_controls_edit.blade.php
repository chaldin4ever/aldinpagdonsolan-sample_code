{{--<pre>{{print_r($controls)}}</pre>--}}
<ol id="questions" class="questions" >
    @php $num=0; @endphp
@foreach($controls as $key=>$cnt)
    @php
        $required = '';
        $requiredClass = '';
        if(isset($cnt->required))
        {
            $required='required';
            $requiredClass = 'is-required';
        }

     $type = ['text', 'memo', 'number', 'date', 'time', 'radio', 'checkbox', 'message', 'dropdown', 'bodymap'];
     sort($type);

     $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;

     $careplan['goal'] = ''; $careplan['obs'] = ''; $careplan['intv'] = '';

     if (!empty($cnt->care_plan)) { $cplan = $cnt->care_plan; } else {$cplan = [];}

     if(sizeof($cplan) > 0){

         foreach($cnt->care_plan as $k=>$v){

            if($v['map_to'] == 'goal'){
                $careplan['goal'] = array_get($v, 'goal');
            }

            if($v['map_to'] == 'obs'){
                $careplan['obs'] = array_get($v, 'goal');
            }

            if($v['map_to'] == 'intv'){
                $careplan['intv'] = array_get($v, 'goal');
            }
         }

     }


    $code = '';
    $req='';
    if($cnt->required){
        $req = '*';
    }

    if($cnt->type == 'message'){

        $code .= "=\n";
        $code .= "$cnt->question\n";
        $code .= "=\n^^\n";

    }else{

        $code .= "#$qn$req\n";
        $code .= "$$cnt->code\n";
        $code .= "?$cnt->question\n";
        $code .= "@$cnt->type\n";

        if(!empty($cnt->fields)){
         //echo '<pre>';print_r($cnt->fields); echo '</pre>';
            foreach($cnt->fields as $fld){
                $fcode = $fld['code'];
                $tmpcode = explode('-', $fcode);
                /* $tgoal = $fld['goal'] ? '|'.$fld['goal'] : '';
                $ftxt = $fld['text'].$tgoal; */

                $score = $fld['score'] ? '|'.$fld['score'] : '';
                $ftxt = $fld['text'].$score;


                if($cnt->type == 'radio' || $cnt->type == 'dropdown'){
                    $tmp = "(".$fcode.")"; // "(".$tmpcode[2].")";
                }else{
                    $tmp = "(".$fcode.")";  // "[".$tmpcode[2]."]";
                }

                $code .= "$tmp $ftxt\n";
            }
        }

        if(sizeof($careplan) > 0){

                    if($careplan['goal']){
                        $code .= ">goal=".$careplan['goal']."\n";
                    }

                    if($careplan['obs']){
                        $code .= ">obs=".$careplan['obs']."\n";
                    }

                    if($careplan['intv']){
                        $code .= ">intv=".$careplan['intv']."\n";
                    }
                }

        $code .= "^^\n";

    }

    $showCode = trim($code);

    @endphp

    {{--<pre>{{print_r($cnt)}}</pre>--}}

    @if($cnt->type == 'text')
        <li id="qn{{$qn}}">
            <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
               {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
               data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
            <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
            <div class="columns">
                <div class="field col-9">
                    <label id="label_{{$qn}}"  class="{{$requiredClass}} label_qn" qname="{{$cnt->code}}"
                           rq="{{$cnt->required}}" typeofq="{{$cnt->type}}"
                           goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                           caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                    >{{$cnt->question}}</label>
                    <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                    <p class="control">
                        <input class="input form-control" type="text" id="{{$cnt->code}}"  {{$required}}
                        name="{{$cnt->code}}"  value="{{array_get($data, $cnt->code)}}" style="width:100%" />
                    </p>
                </div>
                <div class="field column is-6 precode" >
                    {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                    {{--<pre id="showCode_{{$qn}}"  class="_collapse">
                     {{$showCode}}
                    </pre>--}}
                </div>
            </div>


        </li>

    @elseif($cnt->type == 'bodymap')
            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
                   onclick="getQuestionData({{$key}})"
                   data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
                <div class="columns">
                    <div class="field col-9">
                        <label id="label_{{$qn}}"  class="{{$requiredClass}} label_qn" qname="{{$cnt->code}}"
                               rq="{{$cnt->required}}" typeofq="{{$cnt->type}}"
                               goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                               caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                        >{{$cnt->question}}</label>
                        <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                        <p class="control">
                            <img src="{{asset('images/bodymap_v2.png')}}" style="width: 50%;"/>
                        </p>
                    </div>
                    <div class="field column is-6 precode" >
                        {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                        {{--<pre id="showCode_{{$qn}}"  class="_collapse">
                         {{$showCode}}
                        </pre>--}}
                    </div>
                </div>


            </li>
    @elseif($cnt->type == 'memo')
            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
                <div class="columns">
                    <div class="field  col-9">
                        <label id="label_{{$qn}}"  class="{{$requiredClass}} label_qn"
                               goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                               caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                               qname="{{$cnt->code}}" rq="{{$cnt->required}}" typeofq="{{$cnt->type}}">{{$cnt->question}}</label>
                        <p class="control">
                            <textarea class="textarea" id="froala-editor" {{$required}} name="{{$cnt->code}}"  >{{array_get($data, $cnt->code)}}</textarea>
                        </p>
                    </div>
                    <div class="field col-9 precode">
                        {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                        <pre id="showCode_{{$qn}}" class="_collapse">
                            {{$showCode}}
                        </pre>
                    </div>
                </div>
        </li>


    @elseif($cnt->type == 'number')
            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
            <div class="columns">
                <div class="field col-9">
                    <label id="label_{{$qn}}"  class="{{$requiredClass}} label_qn"
                           goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                           caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                           typeofq="{{$cnt->type}}" qname="{{$cnt->code}}" rq="{{$cnt->required}}">{{$cnt->question}}</label>
                    <p class="control">
                        <input class="input" type="number" {{$required}} name="{{$cnt->code}}" step="any" value="{{array_get($data, $cnt->code)}}"
                               onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46'  style="width:150px"/>
                    </p>
                </div>
                <div class="field col-9 precode">
                    {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                    <pre id="showCode_{{$qn}}" class="_collapse">
                        {{$showCode}}
                    </pre>
                </div>
            </div>
    </li>


    @elseif($cnt->type == 'date')
            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit  fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
                <div class="columns">
                    <div class="field  col-9">
                        <label class="label_qn" id="label_{{$qn}}"  qname="{{$cnt->code}}"
                               goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                               caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                               rq="{{$cnt->required}}" typeofq="{{$cnt->type}}" >{{$cnt->question}}</label>
                        <p class="control">
                            <input class="input datepicker form-control" id="datepicker" name="{{$cnt->code}}" value="{{array_get($data, $cnt->code)}}"  style="width:150px"/>
                        </p>
                    </div>

                    <div class="field col-9 precode">
                        {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                        <pre id="showCode_{{$qn}}"  class="_collapse">
                            {{$showCode}}
                        </pre>
                    </div>
                </div>
    </li>
            {{--@include('template.editform.date', ['cnt'=> (object)$cnt, 'type'=> $type])--}}
    @elseif($cnt->type == 'time')
            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"  ></i></a>

            <div class="columns">
                <div class="field  col-9">
                    <label id="label_{{$qn}}" class="{{$requiredClass}} label_qn" qname="{{$cnt->code}}"
                           goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                           caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                           rq="{{$cnt->required}}" typeofq="{{$cnt->type}}" >{{$cnt->question}}</label>
                    <p class="control">
                        <input class="input form-control  " id="timepicker" {{$required}} name="{{$cnt->code}}" value="{{array_get($data, $cnt->code)}}"  style="width:150px"/>
                    </p>
             </div>
                <div class="field col-9 precode">
                    {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                    <pre id="showCode_{{$qn}}"  class="_collapse">
                        {{$showCode}}
                    </pre>
                </div>
            </div>
    </li>

    @elseif($cnt->type == 'checkbox')
            {{--{{print_r($cnt->fields)}}--}}
            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
                <div class="columns">
                    <div class="field  col-9">
                    @foreach($cnt->fields as $fld1) @php
                            $fldcode1 = explode('-',$fld1['code']);
                            //$tooltip = $fld1['goal'] ? '|'.$fld1['goal'] : '';
                            $score = !empty($fld1['score']) ? '|'.$fld1['score'] : '';
                            $goal = !empty($fld1['goal']) ? '***'.$fld1['goal'] : '';
                            $response = !empty($fld1['response']) ? '***'.$fld1['response'] : '';
                            $picklist = $fld1['picklist'] ? '***'.$fld1['picklist']  : '';
                            $caredomain = !empty($fld1['caredomain']) ? '***'.$fld1['caredomain'] : '';
                            $fields1[$qn][] = '['.$fld1['code'].']'.$fld1['text'].$picklist.$caredomain.$goal.$response.$score;
                            //$fields1[$qn][] = '['.substr($fld1['code'],-2).']'.$fld1['text'];
                    @endphp @endforeach
                    @php $fields_text1 = implode("\n",$fields1[$qn]);  @endphp
                        {{--<pre>{{print_r($fields1)}}</pre>--}}
                    <label index="{{$qn}}"  id="label_{{$qn}}" qname="{{$cnt->code}}"
                           goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                           typeofq="{{$cnt->type}}"  picklist="{{isset($cnt->picklist) ? $cnt->picklist : ''}}" class="forcheckbox {{$requiredClass}} label_qn"
                           rq="{{$cnt->required}}"
                           fields_option ="{{$fields_text1}}">{{$cnt->question}}</label>
                        <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                        <div class="options">


                            @php $picklist = \App\Models\Picklist::find($cnt->picklist); @endphp

                            @if(!empty($picklist))

                                    @foreach($picklist->Lists as $fld)
                                        <div class="form-row form-check" id="checkbox_{{$fld['code']}}">
                                            <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                                            <input type="checkbox"  fld_name="{{$fld['code']}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"  id="{{$fld['code']}}" name="{{$fld['code']}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                            <label class="form-check-label" id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}" >
                                                {{$fld['text']}}
    {{--                                         @if(array_get($fld, 'score'))<a href="#" data-toggle="tooltip" data-placement="right" title="Score: {{$fld['score']}}"><i class="fa fa-asterisk"></i></a>@endif--}}
                                            </label>
                                        {{--<a href="#" data-toggle="tooltip" id="tooltip_{{$fld['code']}}" style="@if($fld['goal']=='') display: none; @else @endif" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>--}}
                                        </div>
                                    @endforeach


                            @else

                                @foreach($cnt->fields as $fld)
                                    <div class="form-row form-check" id="checkbox_{{$fld['code']}}">
                                        <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                                        <input type="checkbox"  fld_name="{{$fld['code']}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"  id="{{$fld['code']}}" name="{{$fld['code']}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                        <label class="form-check-label" id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}" >
                                            {{$fld['text']}}
                                            {{--                                         @if(array_get($fld, 'score'))<a href="#" data-toggle="tooltip" data-placement="right" title="Score: {{$fld['score']}}"><i class="fa fa-asterisk"></i></a>@endif--}}
                                        </label>
                                        {{--<a href="#" data-toggle="tooltip" id="tooltip_{{$fld['code']}}" style="@if($fld['goal']=='') display: none; @else @endif" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>--}}
                                    </div>
                                @endforeach

                            @endif

                        {{--@foreach($cnt->fields as $fld)
                            <div class="form-row" id="checkbox_{{$fld['code']}}">
                                <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                                <input type="checkbox"  fld_name="{{$cnt->code}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"  id="{{$fld['code']}}" name="{{$fld['code']}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                <label id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}" >
                                    @php $ft = explode('***', array_get($fld, 'text')); @endphp
                                    {{array_get($ft, 0)}}
                                    --}}{{--@if($fld['goal']!='')<a href="#" data-toggle="tooltip" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>@endif--}}{{--
                                </label>
                                <a href="#" data-toggle="tooltip" id="tooltip_{{$fld['code']}}" style="@if($fld['goal']=='') display: none; @else @endif" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>
                            </div>
                        @endforeach--}}
                    </div>
                 </div>
                    <div class="field col-9 precode">
                {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                <pre id="showCode_{{$qn}}" class="_collapse">
                    {{$showCode}}
                </pre>
                    </div>

                </div>
        </li>


    @elseif($cnt->type == 'radio')
        <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>

            <div class="columns">
            <div class="field  col-9">
                @foreach($cnt->fields as $fld0) @php
                    $fldcode = explode('-',$fld0['code']);
                    //$tooltip = $fld0['goal'] ? '|'.$fld0['goal'] : '';
                    $goal = !empty($fld0['goal']) ? '***'.$fld0['goal'] : '';
                    $response = !empty($fld0['response']) ? '***'.$fld0['response'] : '';
                     $score = !empty($fld0['score']) ? '|'.$fld0['score'] : '';
                    $picklist = !empty($fld0['picklist']) ? '***'.$fld0['picklist']  : '';
                    $caredomain = !empty($fld0['caredomain']) ? '***'.$fld0['caredomain'] : '';
                    $fields[$qn][] = '('.$fld0['code'].')'.$fld0['text'] .$picklist.$caredomain.$goal.$response.$score;
                @endphp @endforeach

                @php $fields_text = implode("\n",$fields[$qn]); @endphp

                    {{--<pre>{{print_r($fields)}}</pre>--}}
                <label  index="{{$qn}}" id="label_{{$qn}}" qname="{{$cnt->code}}"  typeofq="{{$cnt->type}}"
                       goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                       class="forcheckbox {{$requiredClass}} label_qn" rq="{{$cnt->required}}"  picklist="{{isset($cnt->picklist) ? $cnt->picklist : ''}}"
                       caredomain="{{isset($cnt->caredomain) ? $cnt->caredomain : ''}}"
                       fields_option="{{$fields_text}}">{{$cnt->question}}</label>
                    <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                <div class="options">
                    @php $picklist = \App\Models\Picklist::find($cnt->picklist); @endphp

                    @if(!empty($picklist))
                        {{--<pre>{{print_r($picklist->Lists)}}</pre>--}}
                        @foreach($picklist->Lists as $fld)
                            <div class="form-row" id="checkbox_{{$fld['code']}}">
                                <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                                <input type="radio"  fld_name="{{$fld['code']}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"  id="radio-{{$fld['code']}}" name="{{$cnt->code}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                <label   id="{{$fld['code']}}" for="radio-{{$fld['code']}}" index="{{$num}}" >
                                    {{$fld['text']}}
                                    {{--@if($fld['goal']!='')<a href="#" data-toggle="tooltip" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>@endif--}}{{----}}{{----}}
                                </label>
                                {{--<a href="#" data-toggle="tooltip" id="tooltip_{{$fld['code']}}" style="@if($fld['goal']=='') display: none; @else @endif" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>--}}
                            </div>
                        @endforeach

                    @else

                        @foreach($cnt->fields as $fld)
                            <div class="form-row " id="radio{{$fld['code']}}">
                                <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                                <input type="radio"  fld_name="{{$fld['code']}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"  id="{{$fld['code']}}" name="{{$cnt->code}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                <label  id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}" >
                                    {{$fld['text']}}
                                    {{--                                         @if(array_get($fld, 'score'))<a href="#" data-toggle="tooltip" data-placement="right" title="Score: {{$fld['score']}}"><i class="fa fa-asterisk"></i></a>@endif--}}
                                </label>
                                {{--<a href="#" data-toggle="tooltip" id="tooltip_{{$fld['code']}}" style="@if($fld['goal']=='') display: none; @else @endif" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>--}}
                            </div>
                        @endforeach

                    @endif

                    {{--@foreach($cnt->fields as $fld)
                        <div class="form-row" id="radio_{{$fld['code']}}">
                            <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                            <input type="radio"  {{$required}} fld_name="{{$cnt->code}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"   name="{{$fld['code']}}" id="{{$fld['code']}}" value="{{$fld['code']}}" class="regular-radio" @if(array_get($data, $cnt->code)==$fld['code'])checked @endif />
                            <label id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}">&nbsp;
                                @php $ft = explode('***', array_get($fld, 'text')); @endphp
                                {{array_get($ft, 0)}}
                                --}}{{--@if($fld['goal']!='')<a href="#" data-toggle="tooltip" id="_goal_{{$fld['code']}}" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>@endif--}}{{--
                            </label>
                            <a href="#" data-toggle="tooltip" id="tooltip_{{$fld['code']}}" style="@if($fld['goal']=='') display: none; @else @endif" data-placement="right" title="{{$fld['goal']}}"><i class="fa fa-asterisk"></i></a>
                        </div>
                    @endforeach--}}
                </div>
            </div>
                <div class="field col-9 precode">
            {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
            <pre id="showCode_{{$qn}}" class="_collapse">
                    {{$showCode}}
                </pre>
                </div>
            </div>
        </li>


    @elseif($cnt->type == 'dropdown')
            @foreach($cnt->fields as $fld0) @php
                $fldcode = explode('-',$fld0['code']);
                $picklist =  $fld0['picklist'] ? '***'.$fld0['picklist']  : '';
                $caredomain = isset($fld0['caredomain']) ? '***'.$fld0['caredomain'] : '';
                $goal = $fld0['goal'] ? '|'.$fld0['goal'] : '';
                $response = $fld0['response'] ? '|'.$fld0['response'] : '';
                 $score = $fld0['score'] ? '|'.$fld0['score'] : '';
                $fields[$qn][] = '('.$fld0['code'].')'.$fld0['text'].$picklist.$caredomain.$goal.$response.$score;
            @endphp @endforeach
            {{--<pre>{{print_r($fields)}}</pre>--}}
            @php $fields_text = implode("\n",$fields[$qn]); @endphp

            <li id="qn{{$qn}}">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData({{$key}})"
                   data-backdrop="static"  data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>
                <div class="columns" >
                    <div class="field column  col-9">
                <label id="label_{{$qn}}" qname="{{$cnt->code}}" typeofq="{{$cnt->type}}"
                       goal="{{$careplan['goal']}}" picklist="{{isset($cnt->picklist) ? $cnt->picklist : ''}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                       class="forcheckbox {{$requiredClass}} label_qn" rq="{{$cnt->required}}"
                        fields_option="{{$fields_text}}"
                        >{{$cnt->question}}
            </label>
            <p class="control">
                <span class="select fordropdown">
                    <select name="{{$cnt->code}}"  style="display: block !important;" class="form-control" >

                        @php $picklist = \App\Models\Picklist::find($cnt->picklist); @endphp

                        @if(!empty($picklist))
                            @foreach($picklist->Lists as $fld)
                                <option name="{{$fld['code']}}"  value="{{$fld['code']}}" @if(array_get($data, $cnt->code)==$fld['code'])selected @endif>{{$fld['text']}}</option>
                            @endforeach

                         @else
                            @foreach($cnt->fields as $fld)
                                <option name="{{$fld['code']}}"  value="{{$fld['code']}}" @if(array_get($data, $cnt->code)==$fld['code'])selected @endif>{{$fld['text']}}</option>
                            @endforeach
                        @endif
                    {{--@foreach($cnt->fields as $fld)
                            @php $ft = explode('***', array_get($fld, 'text')); @endphp

                            <option name="{{$cnt->code}}"  value="{{$fld['code']}}" @if(array_get($data, $cnt->code)==$fld['code'])selected @endif>{{array_get($ft, 0)}}</option>
                        @endforeach--}}
                    </select>
                </span>
            </p>

        </div>
                    <div class="field col-9 precode">
                {{--<a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                <pre id="showCode_{{$qn}}" class="_collapse">
                    {{$showCode}}
                </pre></div>
                </div>
    </li>
    @elseif($cnt->type="message")
            <li id="qn{{$qn}}" style="list-style: none;" value="0" class="message">
                <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$qn}}--}}" class="qn"
                   {{--onclick="showOption_{{$qn}}()"--}}
               onclick="getQuestionData('{{$key}}')"
                   data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                <a href="#" class="qn_trash" id="qn_trash_{{$qn}}"><i class="fa fa-trash"></i></a>

                <div class="columns">
                <div class="field col-9">
                    <div class="content">
                        {!!$cnt->question!!}
                    </div>
                    <label class="content label_qn" id="label_{{$qn}}"
                           goal="{{$careplan['goal']}}" obs="{{$careplan['obs']}}" intv="{{$careplan['intv']}}"
                         typeofq = "{{$cnt->type}}" style="visibility: hidden; height: 0px !important;">
                        {!!$cnt->question!!}
                    </label>
                    <div class="msgpoint">

                    </div>
                </div>
                    <div class="field col-9 precode">
{{--                <a href="#" data-toggle="collapse" data-target="#showCode_{{$qn}}" class="qn_show_code"><i class="fa fa-code"></i> show code</a>--}}
                <pre id="showCode_{{$qn}}" class="_collapse">
                    {{$showCode}}
                </pre></div>
                </div>
            </li>

        @endif
@php $num++ @endphp
@endforeach
</ol>