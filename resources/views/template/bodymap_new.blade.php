
<div class="field col-12 bodymap m-0 p-0">
    {{--<label>Location</label>--}}
    <p class="control">{{--<input type="text"  class="form-control" name="Location" placeholder="Location" id="Location" value=""  />--}}
    <div id="Location" class="alert alert-primary font-weight-bold" role="alert">{{__('Select Location')}}</div>
    <input type="hidden"  class="form-control" name="Location" id="Location" value=""  />
    <input type="hidden"  class="form-control" name="Bodymap" id="Bodymap" value=""  />
    </p>
</div>

<img src="{{asset('map/bodymap.png')}}" width="400" height="372" class="bodymap" border="0" usemap="#map" />

<map name="map">

    <area shape="circle" coords="96,41,22" alt="head" id="Head" onclick="selectbodymap('Head')" nohref="nohref"   />
    <area shape="circle" coords="42,125,23" alt="Right arm" id="Right_arm"  onclick="selectbodymap('Right_arm')"  nohref="nohref" />
    <area shape="circle" coords="29,196,22" alt="Right palm" id="Right_palm"    onclick="selectbodymap('Right_palm')"   nohref="nohref" />
    <area shape="circle" coords="153,123,22" alt="Left arm" id="Left_arm"    onclick="selectbodymap('Left_arm')"  nohref="nohref" />
    <area shape="circle" coords="164,197,22" alt="Left palm" id="Left_palm"    onclick="selectbodymap('Left_palm')"  nohref="nohref" />
    <area shape="circle" coords="95,109,22" alt="Chest" id="Chest"   onclick="selectbodymap('Chest')"  nohref="nohref" />
    <area shape="circle" coords="96,162,22" alt="Abdomen" id="Abdomen"  onclick="selectbodymap('Abdomen')"  nohref="nohref" />
    <area shape="circle" coords="73,220,22" alt="Right leg" id="Right_leg"  onclick="selectbodymap('Right_leg')"  nohref="nohref" />
    <area shape="circle" coords="119,220,22" alt="Left leg" id="Left_leg"  onclick="selectbodymap('Left_leg')"  nohref="nohref" />
    <area shape="circle" coords="73,343,22" alt="Right foot" id="Right_foot"  onclick="selectbodymap('Right_foot')"  nohref="nohref" />
    <area shape="circle" coords="119,343,22" alt="Left foot" id="Left_foot"  onclick="selectbodymap('Left_foot')"  nohref="nohref" />
    <area shape="circle" coords="71,271,22" alt="Right knee" id="Right_knee"  onclick="selectbodymap('Right_knee')"  nohref="nohref" />
    <area shape="circle" coords="120,271,22" alt="Left knee" id="Left_knee"  onclick="selectbodymap('Left_knee')"  nohref="nohref" />
    <area shape="circle" coords="366,197,22" alt="Right hand" id="Right_hand"  onclick="selectbodymap('Right_hand')"  nohref="nohref" />
    <area shape="circle" coords="228,197,22" alt="Left hand" id="Left_hand"  onclick="selectbodymap('Left_hand')"  nohref="nohref" />
    <area shape="circle" coords="296,39,22" alt="head (back)" id="Head_back"  onclick="selectbodymap('Head_back')"  nohref="nohref" />
    <area shape="circle" coords="350,118,22" alt="Right arm (back)" id="Right_arm_back"  onclick="selectbodymap('Right_arm_back')"  nohref="nohref" />
    <area shape="circle" coords="241,123,22" alt="Left arm (back)" id="Left_arm_back"  onclick="selectbodymap('Left_arm_back')"  nohref="nohref" />
    <area shape="circle" coords="296,110,22" alt="Upper back" id="Upper_back"  onclick="selectbodymap('Upper_back')"  nohref="nohref" />
    <area shape="circle" coords="59,81,23" alt="Right shoulder" id="Right_shoulder"  onclick="selectbodymap('Right_shoulder')"  nohref="nohref" />
    <area shape="circle" coords="132,81,23" alt="Left shoulder" id="Left_shoulder"  onclick="selectbodymap('Left_shoulder')"  nohref="nohref" />
    <area shape="circle" coords="329,77,23" alt="Right shoulder (back)" id="Right_shoulder_back"  onclick="selectbodymap('Right_shoulder_back')"  nohref="nohref" />
    <area shape="circle" coords="260,81,23" alt="Left shoulder (back)" id="Left_shoulder_back"  onclick="selectbodymap('Left_shoulder_back')"  nohref="nohref" />
    <area shape="circle" coords="296,154,18" alt="Lower back" id="Lower_back"  onclick="selectbodymap('Lower_back')"  nohref="nohref" />
    <area shape="circle" coords="244,161,17" alt="Left elbow" id="Left_elbow"  onclick="selectbodymap('Left_elbow')"  nohref="nohref" />
    <area shape="circle" coords="351,159,17" alt="Right elbow" id="Right_elbow"  onclick="selectbodymap('Right_elbow')"  nohref="nohref" />
    <area shape="circle" coords="295,204,22" alt="Sacrum" id="Sacrum"  onclick="selectbodymap('Sacrum')"  nohref="nohref" />
    <area shape="circle" coords="319,250,22" alt="Right leg (back)"  onclick="selectbodymap('Right_leg_back')"  id="Right_leg_back" nohref="nohref" />
    <area shape="circle" coords="274,252,22" alt="Left leg (back)"  onclick="selectbodymap('Left_leg_back')"  id="Left_leg_back" nohref="nohref" />
    <area shape="circle" coords="278,322,18" alt="Left heel" id="Left_heel" onclick="selectbodymap('Left_heel')"  nohref="nohref" />
    <area shape="circle" coords="314,322,18" alt="Right heel" id="Right_heel"  onclick="selectbodymap('Right_heel')"  nohref="nohref" />
    <area shape="circle" coords="280,354,14" alt="Left foot (back)" id="Left_foot_back"  onclick="selectbodymap('Left_foot_back')"  nohref="nohref" />
    <area shape="circle" coords="309,354,14" alt="Right foot (back)" id="Right_foot_back"  onclick="selectbodymap('Right_foot_back')"  nohref="nohref" />
</map>
