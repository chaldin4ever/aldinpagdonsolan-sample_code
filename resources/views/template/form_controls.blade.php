@foreach($controls as $cnt)
    @php
        $required = '';
        $requiredClass = '';
        if(isset($cnt->required))
        {
            $required='required';
            $requiredClass = 'is-required';
        }
    @endphp

    @if($cnt->code != 'PLEASE_UPDATE_THIS_QUESTION_OR_REMOVE_THIS_AFTER_ADDING_A_NEW_QUESTION')
        @if($cnt->type == 'text')
            <div class="field col-9">
                <label class="{{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                @if($cnt->Goal!='' && $cnt->Goal != '-')<a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}"><i class="fa fa-asterisk"></i></a>@endif
                <div class="pl-4 ml-2">
                    <p class="control">
                        <input class="input form-control" type="text"  {{$required}}
                               name="{{$cnt->code}}" value="{{array_get($data, $cnt->code)}}"  />
                    </p>
                </div>
            </div>
        @elseif($cnt->type == 'bodymap')
            <div class="field col-9">
                <label class="{{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                @if($cnt->Goal!='' && $cnt->Goal != '-')<a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}"><i class="fa fa-asterisk"></i></a>@endif
                <div class="pl-4 ml-2">
                    <p class="control">
                        @include('template.bodymap')
                    </p>
                </div>
            </div>
        @elseif($cnt->type == 'memo')
            <div class="field col-9">
                <label class="{{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                <div class="pl-4 ml-2">
                <p class="contorl">
                    <textarea class="textarea  form-control froala-editor" id="froala-editor" {{$required}} name="{{$cnt->code}}">{{array_get($data, $cnt->code)}}</textarea>
                </p>
                </div>
            </div>
        @elseif($cnt->type == 'number')
            <div class="field col-9">
                <label class="{{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                <div class="pl-4 ml-2">
                <p class="contorl">
                    <input class="input  form-control" type="number" {{$required}} name="{{$cnt->code}}" step="any" value="{{array_get($data, $cnt->code)}}"
                           onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46' style="width:150px"/>
                </p>
                </div>
            </div>
        @elseif($cnt->type == 'date')
            <div class="field col-9">
                <label>{{$cnt->qn}}. {{$cnt->question}}</label>
                <div class="pl-4 ml-2">
                <p class="contorl">
                    <input class="input  form-control datepicker" id="datepicker" name="{{$cnt->code}}" value="{{array_get($data, $cnt->code)}}"  style="width:150px"/>
                </p>
                </div>
            </div>
        @elseif($cnt->type == 'time')
            <div class="field col-9">
                <label class="{{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                <div class="pl-4 ml-2">
                <p class="contorl">
                    <input class="input  form-control  timepicker" id="timepicker" {{$required}} name="{{$cnt->code}}" value="{{array_get($data, $cnt->code)}}"  style="width:150px"/>
                </p>
                </div>
            </div>
        @elseif($cnt->type == 'checkbox')
            <div class="field col-9">
                <label  class="forcheckbox {{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                <div class="pl-4 ml-2">

                @foreach($cnt->fields as $fld)
                    @php
                        $fld_code = $cnt->code.'-'.array_get($fld, 'code');
                    @endphp
                <div class="form-row">
                    <input type="checkbox" id="{{$fld['code']}}" name="{{$cnt->code.'-'.$fld['code']}}" class=" form-controlregular-checkbox" @if(array_get($data, $fld_code)=='on')checked @endif />
                    <label for="{{$fld['code']}}">
                        @php $ft = explode('***', array_get($fld, 'text')); @endphp
                        {{array_get($ft, 0)}}
                        @if($fld['score']!='')<a href="#" data-toggle="tooltip" data-placement="right" title="Score: {{$fld['score']}}"><i class="fa fa-asterisk"></i></a>@endif
                    </label>
                </div>
                @endforeach
                </div>
            </div>

        @elseif($cnt->type == 'radio')
            <div class="field col-9">

                <label class="forcheckbox {{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}</label>
                <div class="pl-4 ml-2">

                @foreach($cnt->fields as $fld)
                @php
                    $fld_code = $cnt->code.'-'.array_get($fld, 'code');
                @endphp
                <div class="form-row">
                    <input type="radio" {{$required}}  name="{{$cnt->code}}" id="{{$fld_code}}" value="{{$fld_code}}" class="regular-radio" @if(array_get($data, $cnt->code)==$fld_code)checked @endif />
                    <label for="{{$cnt->code.'-'.$fld['code']}}">&nbsp;
                        @php $ft = explode('***', array_get($fld, 'text')); @endphp
                        {{array_get($ft, 0)}}
                        @if($fld['score']!='')<a href="#" data-toggle="tooltip" data-placement="right" title="Score: {{$fld['score']}}"><i class="fa fa-asterisk"></i></a>@endif
                    </label>
                </div>
                @endforeach
                </div>
            </div>
        @elseif($cnt->type == 'dropdown')
            <div class="field col-9">
                <label class="forcheckbox {{$requiredClass}}">{{$cnt->qn}}. {{$cnt->question}}
                </label>
                <div class="pl-4 ml-2">
                <p class="control">
                    <span class="select fordropdown  ">
                        <select name="{{$cnt->code}}" class="form-control" style="display: block !important;">
                            @foreach($cnt->fields as $fld)
                            @php
                                $fld_code = $cnt->code.'-'.array_get($fld, 'code');
                                $ft = explode('***', array_get($fld, 'text'));
                            @endphp
                                <option name="{{$cnt->code}}" value="{{$cnt->code.'-'.$fld['code']}}" @if(array_get($data, $cnt->code)==$fld_code)selected @endif>{{array_get($ft, 0)}}</option>
                            @endforeach
                        </select>
                    </span>
                </p>
                </div>

            </div>
        @elseif($cnt->type="message")
            <div class="content col-9 pl-4 ml-2">
                <h3 style="background-color: #153c52;color: whitesmoke;padding: 4px 4px 4px 4px">{!!$cnt->question!!}</h3>
            </div>
        @endif
    @endif
@endforeach