
    @php $type = ['text', 'memo', 'number', 'date', 'time', 'radio', 'checkbox', 'message', 'dropdown', 'bodymap']; sort($type); @endphp

    @include('template.editform.add_question', ['controls'=> $controls, 'type'=> $type])

    {{--
    @foreach($controls as $cnt)
        @php

            $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;

        $careplan['goal'] = ''; $careplan['obs'] = ''; $careplan['intv'] = '';

        if (!empty($cnt->care_plan)) { $cplan = $cnt->care_plan; } else {$cplan = [];}

        if(sizeof($cplan) > 0){
             foreach($cnt->care_plan as $k=>$v){

                if($v['map_to'] == 'goal'){
                    $careplan['goal'] = $v['domain'];
                }

                if($v['map_to'] == 'obs'){
                    $careplan['obs'] = $v['domain'];
                }

                if($v['map_to'] == 'intv'){
                    $careplan['intv'] = $v['domain'];
                }
             }

         }

        @endphp

        @if($cnt->type == 'text')

            @include('template.editform.text', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'memo')

            @include('template.editform.memo', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'number')

            @include('template.editform.number', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'date')

            @include('template.editform.date', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'time')

            @include('template.editform.time', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'checkbox')


            @include('template.editform.checkbox', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'radio')

            @include('template.editform.radio', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type == 'dropdown')

            @include('template.editform.dropdown', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @elseif($cnt->type="message")

            @include('template.editform.message', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])
        @elseif($cnt->type="message")

            @include('template.editform.bodymap', ['cnt'=> (object)$cnt, 'type'=> $type, 'careplan' => $careplan])

        @endif

    @endforeach
--}}