@php
    $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;
@endphp
<!-- Modal Text -->
<div id="Modal_{{$qn}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$cnt->type}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" style="padding-top: 10px; padding-left: 25px;">
                <div class="field">
                    <label>Type</label>
                    <p class="control">

                        <select name="type" id="type" class=" form-control" style="display: block !important;">
                            <option>Select</option>
                            @foreach($type as $t)
                                <option value="{{$t}}" @if($cnt->type == $t) selected @endif >{{ucwords($t)}}</option>
                            @endforeach
                        </select>
                    </p>
                </div>

                <div id="question_message" style="display: none;">
                    <div class="field">
                        <label>Content</label>
                        <p class="control" >
                            <textarea  name="question" class="message form-control" style="resize: vertical !important;"  >{{$cnt->question}}</textarea>
                        </p>
                    </div>
                    <div class="field" id="message_point">
                        <label style="width: 90%;">Point <a href="#" id="add_point"
                                                            onclick="
                                                            var modalId = $(this).closest('.modal-dialog').parent('id');
                                                            console.log(modalId);
                                                            add_msg_point(modalId);"
                                                            class="pull-right">
                                <i class="fa fa-plus"></i> Add </a></label>
                        <p class="control" id="point" >
                            <input type="text" class="form-control" name="point[]" />
                        </p>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="button is-primary" {{--id="save_checkbox_{{$qn}}"--}}
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '');"
                        data-dismiss="modal">Save Question</button>

                {{-- <button type="button" class="button is-primary" --}}{{--id="save_checkbox_{{$qn}}"--}}{{--
                 onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                         save_edit_qn(modalId, '', '{{$qn}}', '', '1');"
                         data-dismiss="modal">Duplicate/Copy</button>--}}

                <button type="button" class="button is-link" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
    @include('template.editform.add_template')
</div>