@php
    $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;
@endphp
<!-- Modal Text -->
<div id="Modal_{{$qn}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$cnt->question}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" style="padding-top: 10px; padding-left: 25px;">
                <div class="field">
                    <label>Type</label>
                    <p class="control">

                        <select name="type" id="type" class=" form-control" style="display: block !important;">
                            <option>Select</option>
                            @foreach($type as $t)
                                <option value="{{$t}}" @if($cnt->type == $t) selected @endif >{{ucwords($t)}}</option>
                            @endforeach
                        </select>
                    </p>
                </div>
                <div class="field">
                    <label>Required</label>
                    <p class="control">

                        <select name="required" id="required" class=" form-control" style="display: block !important;">
                            <option value="0" @if($cnt->required == 0) selected @endif>No</option>
                            <option value="1" @if($cnt->required == 1) selected @endif>Yes</option>
                        </select>
                    </p>
                </div>
                <div class="field">
                    <label>Code</label>
                    <p class="control"><input class=" form-control"  name="code" id="code" value="{{$cnt->code}}"/></p>
                </div>
                <div class="field">
                    <label>Question</label>
                    <p class="control"><input class=" form-control"  name="question" id="question"
                                              value="{{$cnt->question}}"  /></p>
                </div>
                <div class="field">
                    <label>Goal</label>
                    <p class="control"><input class=" form-control"  name="goal" id="goal" value="{{$careplan['goal']}}"  /></p>
                </div>
                <div class="field">
                    <label>Observation</label>
                    <p class="control"><input class=" form-control" name="observation" id="observation" value="{{$careplan['obs']}}"  /></p>
                </div>
                <div class="field">
                    <label>Intervention</label>
                    <p class="control"><input class=" form-control"  name="intervention" id="intervention" value="{{$careplan['intv']}}"  /></p>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="button is-primary" {{--id="save_checkbox_{{$qn}}"--}}
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '');"
                        data-dismiss="modal">Save Question</button>

                {{--<button type="button" class="button is-primary" --}}{{--id="save_checkbox_{{$qn}}"--}}{{--
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '1');"
                        data-dismiss="modal">Duplicate/Copy</button>--}}

                <button type="button" class="button is-link" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
    @include('template.editform.add_template')
</div>
