<div id="Modal_add" class="modal fade"  tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" style="display: none;" >
    <div class="modal-dialog modal-fluid" role="document" >
            <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Add Question</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" style="padding-top: 10px; padding-left: 25px;">

                <div class="tabs mb-4">
                    <ul class="nav md-pills  pills-secondary">
                        <li class="nav-item"  >
                            <a onclick="
                            var modalId = $('#Modal_add .modal-dialog').attr('id');
                            console.log($(this).parent().attr('id'));
//                            $(this).attr('class','is-active');
                            $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','');
                            $('#'+modalId+' div.tabs ul li:first').attr('class','is-active');
                            $('#'+modalId+' #question_fields').show();
                            $('#'+modalId+' .fieldsOptions').hide();
                            $('#'+modalId+' div.type').show();
                        " a class="nav-link active" data-toggle="tab" href="#panel4" role="tab">Fields</a>
                        </li>
                        <li class="nav-item " id="displayFieldOption" style="display: none;" onclick="
                        var modalId = $('#Modal_add .modal-dialog').attr('id');
                         console.log('option: '+modalId);
//                            $(this).attr('class','is-active');
                            $('#'+modalId+' div.tabs ul li:first').attr('class','');
                            $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','is-active');
                            $('#'+modalId+' #question_fields').hide();
                            $('#'+modalId+' .fieldsOptions').show();
                            $('#'+modalId+' div.type').hide();
                            // displayPicklist();
                        ">
                            <a class="nav-link" data-toggle="tab" href="#panel4" role="tab">Options</a>
                        </li>

                    </ul>
                </div>


                    <div class="field type col-6 pr-5">
                        <label>Type</label>
                        <p class="control">

                            <select class="form-control" name="type" id="type"  style="display: block !important;">
                                <option value="default" >Select</option>
                               @foreach($type as $t)
                                    <option value="{{$t}}">{{ucwords($t)}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="type_hidden" id="type_hidden" />
                        </p>
                    </div>

                <div id="question_fields">
                    <div class="row">
                        <div class="col-6 ">
                            <div class="field col">
                                <label>Question</label>
                                <p class="control"><input  class="form-control" name="question" id="question" value=""  /></p>
                            </div>
                            {{--<div class="field col bodymap">
                                <label>Location</label>
                                <p class="control"><input  class="form-control" name="location" id="location" value=""  /></p>
                            </div>--}}
                            <div class="field col selectpicklist" style="display: none;">
                                <label>Choose Picklist</label>
                                <p class="control"><input  class="form-control" name="selectpicklist" id="selectpicklist" value=""  /></p>
                            </div>
                            <div class="field col">
                                <label>Required</label>
                                <p class="control">

                                    <select style="display: block !important;" class="form-control" name="required" id="required" onchange="$('option[value='+this.value+']').attr('selected', 'selected'); " >
                                        <option value="0" >No</option>
                                        <option value="1" >Yes</option>
                                    </select>
                                </p>
                            </div>

                            <div class="field col">
                                <label>Code</label>
                                <p class="control"><input class="form-control question_code"  name="code" id="code" value=""/></p>
                            </div>

                        </div>

                        <div class="col bg-light pt-4 pb-4 mr-4 goal" style="margin-top: -110px;">

                            <div class="field col">
                                <label>Care Domain</label>
                                <p class="control"><input class="form-control"  name="caredomain" id="caredomain" value=""  /></p>
                            </div>
                            <div class="field col">
                                <label>Goal</label>
                                <p class="control"><input class="form-control"  name="parent_goal" id="parent_goal" value=""
                                    {{--onkeyup="validateGoal(this)"--}} onfocus="$(this).select();"
                                    /></p>
                            </div>
                            <div class="field col"   >
                                <label>Response</label>
                                <select class="form-control" name="parent_response" id="parent_response"  style="display: block !important;">
                                    <option value="" selected>Select</option>
                                    <option value="goal" selected>Goal</option>
                                    <option value="intv">Intervention</option>
                                    <option value="obs">Observation</option>
                                </select>
                            </div>
                            {{--<div class="field col">
                                <div class="form-check form-check-inline">
                                    <input style="display: block !important; visibility: visible;" type="radio" class="form-check-input" id="intervention" value="intv" name="response"> Intervention
                                    <label class="form-check-label" for="intervention">Intervention</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input style="display: block !important; visibility: visible;" type="radio" class="form-check-input" id="observation" value="obs" name="response"> Observation
                                    <label class="form-check-label" for="observation">Observation</label>
                                </div>
                            </div>--}}
                            {{--<div class="field col">
                                <label>Observation</label>
                                <p class="control"><input class="form-control"  name="observation" id="observation" value=""  /></p>
                            </div>
                            <div class="field col">
                                <label>Intervention</label>
                                <p class="control"><input class="form-control"  name="intervention" id="intervention" value=""  /></p>
                            </div>--}}
                        </div>

                        <div class="col bg-white pt-4 pb-4 mr-4 bodymap " style="margin-top: -110px; display:none;">

                            @include('template.bodymap')
{{--                            <img src="{{asset('images/bodymap_v2.png')}}" style="width: 50%;">--}}
                        </div>
                    </div>

              </div>



                <div class="fields fieldsOptions" id="fields" style="padding-top: 10px; min-height: 60px; background: #f4f4f4; display: none;">
                    <div class="row  pl-3 pb-3"><a href="#" class="add_fld float-left pt-2 pl-3" id="add_fld" onclick="" >
                            <i class="fa fa-plus"></i> Add Options
                        </a></div>

                    <div class="fieldOptionsHeader mb-2"  style="padding: 15px 15px 10px 30px; background: rgb(244, 244, 244);">

                        <div class="row">
                            <div class="col-2 pl-3">
                                <label>Code </label>
                            </div>
                            <div class="col-3 pl-3">
                                <label>Text</label>

                            </div>
                            {{--<div class="col-3 pl-3">
                                <label>Care Domain</label>

                            </div>--}}
                            <div class="col-3 pl-3">
                                <label>Goal</label>

                            </div>
                            <div class="col-2 pl-3">
                                {{--<label>Care Domain</label>--}}

                            </div>
                            <div class="col-1 pl-3">
                                <label>Score</label>

                            </div>
                            <div class="col-1 pt-4 text-center">

                            </div>


                        </div>




                    </div>
                </div>

              <div id="question_message" style="display: none;">
                  <div class="field col-5">
                      <label>Content</label>
                       <p class="control" >
                          <textarea  name="question" class="message form-control"  ></textarea>
                      </p>
                  </div>
                  <div class="field col-5" id="message_point">
                      <label style="width: 90%;">Point <a href="#" id="add_point" class="pull-right"><i class="fa fa-plus"></i> Add </a></label>
                      <p class="control" id="point" >
                          <input class="form-control"  type="text" name="point[]" />
                      </p>

                  </div>
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-grey btn-rounded" id="save_add_qn" onclick="save_add_qn(this, 'add')" >Add Question</button>
                <button type="button" id="close_add_modal" class="btn btn-danger btn-rounded" data-dismiss="modal">Close</button>

            </div>
        </div>

    </div>

    @include('template.editform.add_template')

{{--    <div class="ModalEdit">@include('template.editform.edit_question')</div>--}}
</div>