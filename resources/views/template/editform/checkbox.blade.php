<!-- Modal Checkbox -->
@php
    $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;
@endphp

<div id="Modal_{{$qn}}" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >

    <div class="modal-dialog modal-fluid modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$cnt->question}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" style="padding-top: 10px; padding-left: 25px;">

                @include('template.editform.menu')

                {{--<pre> {{print_r($cnt->fields)}}</pre>--}}
                <div class="field type col-6 pr-5 pl-0">
                    <label>Type</label>
                    <p class="control">

                        <select class="form-control" name="type" id="type"  style="display: block !important;">
                            <option>Select</option>
                            @foreach($type as $t)
                                <option value="{{$t}}" @if($cnt->type == $t) selected @endif >{{ucwords($t)}}</option>
                            @endforeach
                        </select>
                    </p>
                </div>

                <div id="question_fields">

                    <div class="row">
                        <div class="col-6 ">
                            <div class="field">
                                <label>Required</label>
                                <p class="control">

                                    <select name="required" id="required"  class=" form-control" style="display: block !important;">
                                        <option value="0" @if($cnt->required == 0) selected @endif>No</option>
                                        <option value="1" @if($cnt->required == 1) selected @endif>Yes</option>
                                    </select>
                                </p>
                            </div>
                            <div class="field">
                                <label>Code</label>
                                <p class="control"><input  class=" form-control" name="code" id="code" value="{{$cnt->code}}"/></p>
                            </div>
                            <div class="field">
                                <label>Question</label>
                                <p class="control"><input  class=" form-control" name="question" id="question" value="{{$cnt->question}}"  /></p>
                            </div>
                        </div>

                        <div class="col bg-light pt-4 pb-4 mr-4" style="margin-top: -110px;">
                            <div class="field">
                                <label>Goal</label>
                                <p class="control"><input  class=" form-control" name="goal" id="goal" value="{{$careplan['goal']}}"  /></p>
                            </div>
                            <div class="field">
                                <label>Observation</label>
                                <p class="control"><input  class=" form-control" name="observation" id="observation" value="{{$careplan['obs']}}"  /></p>
                            </div>
                            <div class="field">
                                <label>Intervention</label>
                                <p class="control"><input  class=" form-control" name="intervention" id="intervention" value="{{$careplan['intv']}}"  /></p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="fields fieldsOptions" id="fields" style="padding-top: 10px; min-height: 60px; background: #f4f4f4; display: none;">
                    {{--<div class="row  pl-3 pb-3"><a href="#" class="add_fld float-left pt-2 pl-3" id="add_fld" onclick="" >
                            <i class="fa fa-plus"></i> Add Options
                        </a></div>--}}

                    <div class="fieldOptionsHeader mb-2"  style="padding: 15px 15px 10px 30px; background: rgb(244, 244, 244);">

                        <div class="row">
                            <div class="col-2 pl-3">
                                <label>Code </label>
                            </div>
                            <div class="col-5 pl-3">
                                <label>Text</label>

                            </div>
                            <div class="col-4 pl-3">
                                <label>Tooltip</label>

                            </div>
                            <div class="col-1 pt-4 text-center">

                            </div>


                        </div>




                    </div>
                </div>
                <div class="fieldsOptions" style="display: none;">
                    <ul id="disable_dragable_{{$qn}}" class="fields" style="list-style:none; " >
                       {{--<li style="list-style:none; clear:both; margin-bottom: 10px; padding: 25px; margin-top: -25px;">
                           <a href="#" class="add_fld_{{$qn}} pull-left" id="add_fld_{{$qn}}" onclick="
                                var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                                add_fld_option(modalId);"
                                style="padding-left: 10px; ">
                               <i class="fa fa-plus">Add Options</i>
                           </a>
                        </li>--}}

                    @foreach($cnt->fields as $fld)
                        <li class="field" id="{{$fld['code']}}"
                            onmouseover="this.style.border='dashed 1px #666'; /*this.style.cursor='move';*/"
                            onmouseout="this.style.border=''; this.style.cursor='';"
                            style="padding: 5px; background: #f4f4f4; position:relative;">
                            <div class="row pl-3">
                                {{--<a href="#" class="qn_trash pull-right" id="delete_fld"  onclick="
                                        var id = $(this).parent().attr('id');
                                        var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                                        confirmAction('#'+modalId+' ul.fields li', id, '{{__('mycare.are_you_sure_to_remove_fieldoption')}}');
                                        "><i class="fa fa-trash"></i></a>--}}
                                {{--<label>Code </label>--}}
                                <div class="col-2">
                                    {{--<input type="text"  name="code_{{$fld['code']}}" id="code_{{$fld['code']}}" value="{{$fld['code']}}" />--}}
{{--                                    @php $field = explode('-', $fld['code']); @endphp--}}
                                    <input type="text"  class=" form-control"  name="code[]" id="code_{{$fld['code']}}" value="{{$fld['code']}}" />
                                </div>
                                {{--<label>Text</label>--}}
                                <div class="col-5">
                                    {{--<input type="text"  name="text_{{$fld['code']}}" id="text_{{$fld['code']}}" value="{{$fld['text']}}" />--}}
                                    @php $fieldtext = explode('***', $fld['text']); @endphp
                                    <input type="text"  class=" form-control"  name="text[]" id="text_{{$fld['code']}}" value="{{reset($fieldtext)}}" />
                                </div>
                                {{--<label>Tooltip</label>--}}
                                <div class="col-4">
                                    <input  class=" form-control" type="text"  name="tooltip[]"  value="{{$fld['goal']}}"  />
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-grey btn-rounded" {{--id="save_checkbox_{{$qn}}"--}}
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '');"
                data-dismiss="modal">Save Question</button>

               {{-- <button type="button" class="button is-primary" --}}{{--id="save_checkbox_{{$qn}}"--}}{{--
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '1');"
                        data-dismiss="modal">Duplicate/Copy</button>--}}

                <button type="button" class="btn btn-danger btn-rounded" data-dismiss="modal">Close</button>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </div>
        </div>

    </div>

    <div class="fieldOptions" style="display:block; height: 1px !important; overflow: hidden;">
    <li class="field" id=""  style="padding: 25px 25px 25px 45px; background: #f4f4f4;">

        <a href="#" class="qn_trash pull-right" id="delete_fld" onclick="
                var id = $(this).parent().attr('id');
                var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                confirmAction('#'+modalId+' ul.fields li', id, '{{__('mycare.are_you_sure_to_remove_fieldoption')}}', '');
                ">
            <i class="fa fa-trash"></i>
        </a>
        <label>Code </label>
        <p class="control"> <input  class=" form-control" type="text" name="code[]" id="code" placeholder="Code" ></p>
        <label>Text</label>
        <p class="control">  <input  class=" form-control" type="text" name="text[]" id="text" placeholder="Text Option"></p>
        <label>Tooltip</label>
        <p class="control">  <input  class=" form-control" type="text" name="tooltip[]" id="tooltip" placeholder="Tooltip"></p>

    </li>
    </div>

    @include('template.editform.add_template')

</div>