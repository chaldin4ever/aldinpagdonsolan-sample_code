@php
    $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;
@endphp
<!-- Modal Text -->
<div id="Modal_{{$qn}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-fluid">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$cnt->question}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" style="padding-top: 10px; padding-left: 25px;">

                <div class="field type col-6 pr-5">
                    <label>Type</label>
                    <p class="control">

                        <select  class="form-control" name="type" id="type"  style="display: block !important;" disabled>
                            <option>Select</option>
                            @foreach($type as $t)
                                <option value="{{$t}}" @if($cnt->type == $t) selected @endif >{{ucwords($t)}}</option>
                            @endforeach
                        </select>
                    </p>
                </div>

                <div class="row">
                    <div class="col-6 ">
                        <div class="field col">
                            <label>Required</label>
                            <p class="control">

                                <select  style="display: block !important;" class="form-control" name="required" id="required">
                                    <option value="0" @if($cnt->required == 0) selected @endif>No</option>
                                    <option value="1" @if($cnt->required == 1) selected @endif>Yes</option>
                                </select>
                            </p>
                        </div>
                        <div class="field col">
                            <label>Code</label>
                            <p class="control"><input class="form-control" name="code" id="code" value="{{$cnt->code}}"/></p>
                        </div>
                        <div class="field col">
                            <label>Question</label>
                            <p class="control"><input class="form-control" name="question" id="question" value="{{$cnt->question}}"  /></p>
                        </div>

                    </div>

                    <div class="col bg-light pt-4 pb-4 mr-4" style="margin-top: -110px;">

                        <div class="field col">
                            <label>Goal</label>
                            <p class="control"><input class="form-control" name="goal" id="goal" value="{{$careplan['goal']}}"  /></p>
                        </div>
                        <div class="field col">
                            <label>Observation</label>
                            <p class="control"><input class="form-control" name="observation" id="observation" value="{{$careplan['obs']}}"  /></p>
                        </div>
                        <div class="field col">
                            <label>Intervention</label>
                            <p class="control"><input class="form-control" name="intervention" id="intervention" value="{{$careplan['intv']}}"  /></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-rounded btn-grey " {{--id="save_checkbox_{{$qn}}"--}}
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '');"
                        data-dismiss="modal">Save Question</button>

                {{--<button type="button" class="button is-primary" --}}{{--id="save_checkbox_{{$qn}}"--}}{{--
                onclick=" var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                        save_edit_qn(modalId, '', '{{$qn}}', '', '1');"
                        data-dismiss="modal">Duplicate/Copy</button>--}}

                <button type="button" class="btn btn-rounded btn-danger " data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
    @include('template.editform.add_template')
</div>
