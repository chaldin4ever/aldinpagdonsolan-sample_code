<script>

    //parse html form page

    function parse_template(){

        var label=[];
        var template;
        var code=[];
        var type=[];
        var fields =[];
        var required=[];
        var fieldsOption, fieldsOptionRadio, fieldsOptionCheckbox, goal, obs, intv, picklist, caredomain;
        var msgpoint;
        var options = [];

        var question_list = $( "ol#questions li label.label_qn, ol#questions li input:not(:hidden), ol#questions li div.options .form-row input:not(:hidden, :checkbox, :radio)" );

        // console.log(question_list);

        question_list.each(function( index, elem ) {

//                console.log($(elem).children('ul')[0]);

//            console.log(elem);

            qn = $(elem).attr('index');
            codename = $(elem).attr('qname');
            labelname = $.trim(elem.textContent);
            typeofq = $(elem).attr('typeofq');
            isrequired = $(elem).attr('rq');
            fieldsOption = $(elem).attr('fields_option');
            msgpoint = $(elem).attr('msgpoint');
            goal = $(elem).attr('goal');
            obs = $(elem).attr('obs');
            intv = $(elem).attr('intv');
            picklist = $(elem).attr('picklist');
            caredomain = $(elem).attr('caredomain');

            // console.log(fieldsOption);
            code.push(codename);
            label.push([labelname, isrequired, codename, typeofq, fieldsOption, goal, obs, intv, msgpoint, picklist, caredomain]);
            type.push(typeofq);
            required.push(isrequired);
            options.push(fieldsOption);

        })


        var questionData = [];
        label.forEach(function(val, key){
            var str_label = val[0];
            var code = val[2];
            var type = val[3];
            if(str_label.length > 0){
                questionData.push(val);
            }
        })

           // console.log(questionData);

        var template_form='', temp_num, temp_code, temp_label, temp_type, temp_fields, temp_goal, temp_obs, temp_intv, temp_required, temp_picklist, temp_caredomain;
        var fieldArray=[], fldOpt=[],fldOptArr=[], Options;

        questionData.forEach(function(val, key){


            temp_num = key + 1;
            temp_label = val[0];
            temp_required = val[1] > 0 ? '*' : '' ;
            temp_code = val[2];
            temp_type = val[3];

            if(typeof(val[4]) == 'undefined'){
                temp_fields = '';
            }else{

                temp_fields = '\n'+val[4];

            }

            if(val[5] == '' || typeof(val[5]) == 'undefined'){
                temp_goal = '';
            }else{
                temp_goal = '\n>goal='+val[5] ;
            }

            if(val[6] == '' || typeof(val[6]) == 'undefined'){
                temp_obs = '';
            }else{
                temp_obs = '\n>obs='+val[6] ;
            }

            if(val[7] == '' || typeof(val[7]) == 'undefined'){
                temp_intv = '';
            }else{
                temp_intv = '\n>intv='+val[7];
            }

            //picklist
            if(val[9] == '' || typeof(val[9]) == 'undefined'){
                temp_picklist = '';
            }else{
                temp_picklist = '\n^'+val[9];
            }

            //caredomain
            if(val[10] == '' || typeof(val[10]) == 'undefined'){
                temp_caredomain = '';
            }else{
                temp_caredomain = '\n`'+val[10];
            }

            if(temp_type == 'message'){
                var m = $.trim(temp_label);
                template_form += '=\n'+m+'\n';
                template_form += $.trim(val[8])+'\n=';
            }else{
                template_form += '#'+temp_num+temp_required+'\n$'+temp_code+'\n?'+temp_label+'\n@'+temp_type;
                template_form += temp_fields;
                template_form += temp_goal;
                template_form += temp_obs;
                template_form += temp_intv;
                template_form += temp_picklist;
                template_form += temp_caredomain;
            }
            template_form += '\n^^\n';

        })

           console.log(template_form);

        $('textarea#template_content').val(template_form);

    }

</script>