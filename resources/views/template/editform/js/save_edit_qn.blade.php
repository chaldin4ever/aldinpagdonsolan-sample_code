<script>
    //edit save
    function save_edit_qn(mId, flag, qn, ts, dup) {

        var d = new Date();
        var timestamp = d.getTime();

        modalId = '#'+mId;

        var type = $(modalId+' select#type').val();
        var rq = $(modalId+' select#required').val();
        var code = $(modalId+' input#code').val();
        var question = $(modalId+' input#question').val();
        var message = $(modalId+' textarea.message').val();
//            console.log($('#Modal_add textarea.message').val());
        var goal = $(modalId+' input#goal').val();
        var obs = $(modalId+' input#observation').val();
        var intv = $(modalId+' input#intervention').val();

//        console.log([type, rq, code, question, message, goal, obs, intv]);

        var  fieldcodeArr = [];
        $(modalId+' div.fieldsOptions input[name^="code"]').each(function(k, v) {
            val = $(v).val();
            fieldcodeArr.push(val);
        });

        var  fieldtextArr = [];
        $(modalId+' div.fieldsOptions input[name^="text"]').each(function(k, v) {
            val = $(v).val();
            fieldtextArr.push(val);
        });

        var  tooltipArr = [];
        $(modalId+' div.fieldsOptions input[name^="tooltip"]').each(function() {
            val = $(this).val();
            tooltipArr.push(val);
        });

        var  msgPointArr = [];
        $(modalId+' #question_message input[name^="point"]').each(function(k, v) {
            val = $(v).val();
            msgPointArr.push(val);
        });

        var dataCss = modalId+" #add_qn_template li#"+type+" label#"+type;
        var dataCss0 = modalId+" #add_qn_template li#"+type;
        var dataShowCode = modalId+" #add_qn_template li#"+type+" pre";

        //put all data into label question as attributes
        $(dataCss).text(question);
        $(dataCss).attr('name', code);
        $(dataCss).attr('qname', code);
        $(dataCss).attr('typeofq', type);
        $(dataCss).attr('rq', rq);
        $(dataCss).attr('goal', goal);
        $(dataCss).attr('obs', obs);
        $(dataCss).attr('intv', intv);

        //add parse code to pre tag

        var qnCode = '';

        if(type=='message'){

            qnCode += "=\n";
            qnCode += message+"\n";
            qnCode += "=\n";

            msgPointArr.forEach(function(val, key){
                qnCode += "*"+val+"\n";
            })

        }else{
            if(rq){var r='*';}
            qnCode += '#'+qn+r+"\n";
            qnCode += '$'+code+"\n";
            qnCode += '?'+question+"\n";
            qnCode += '@'+type+"\n";

            fieldtextArr.forEach(function (val, key) {
                var field_code = fieldcodeArr[key];
                var tmpcode = field_code.split('-');
                var fcode;
                if(tmpcode.length == 2){
                    fcode = tmpcode[1];
                }else {
                    fcode= tmpcode[0];
                }

                var tooltip = tooltipArr[key];
                var tip = '';

                if(type == 'radio' && type == 'checkbox'){
                    if(tooltip.length > 0){
                        tip = "|"+tooltip;
                    }
                }



                if(type == 'radio' || type == 'dropdown'){
                    qnCode += '('+fcode+") "+val+tip+"\n";
                }else{
                    qnCode += '['+fcode+"] "+val+tip+"\n";
                }
            })

            if(goal.length > 0){
                qnCode += '>goal='+goal+"\n";
            }
            if(obs.length > 0){
                qnCode += '>obs='+obs+"\n";
            }
            if(intv.length > 0){
                qnCode += '>intv='+intv+"\n";
            }
        }

        qnCode += "^^\n";

        $(dataShowCode).text(qnCode);

        if(type=='message'){

            var point = '<ul>';
            msgPointArr.forEach(function(v, k){
                point += '<li>'+v+'<input type="hidden" name="point[]" value="'+v+'" /> </li>';
            })
            point +='</ul>';

            $(dataCss).html(message+point);

            $(dataCss0 +" div.content").html(message+point);
        }

        if(type == 'radio' || type == 'checkbox' || type == 'dropdown') {

            var optionTag = '';

            if(type == 'radio' || type == 'checkbox'){

                fieldcodeArr.forEach(function (val, key) {

                    var tagId = type + '_' + val;
                    var fldLabel = fieldtextArr[key];
                    var tooltip = tooltipArr[key];

                    optionTag += "<div id='" + tagId + "' class='form-row'>";
                    optionTag += "<input type='" + type + "' fld_name='" + val + "' fld_code='" + val + "' fld_label='" + fldLabel + "' class='regular-checkbox' id='" + val + "' />";
                    optionTag += "<label id='" + val + "' for='" + val + "' >";
                    optionTag += fldLabel;
                    optionTag += "</label>";

                    if(tooltip.length > 0){
                        optionTag += "&nbsp; <a href=\"#\" data-toggle=\"tooltip\" data-placement=\"right\" " +
                            "title=\""+tooltip+"\" data-original-title=\""+tooltip+"\"><i class=\"fa fa-asterisk\"></i></a>";
                    }
                    optionTag += "</div>";
                })
            }else{
                if (type == 'dropdown') {
//                    console.log(fieldcodeArr);
                    optionTag += "<span class='select fordropdown'>";
                    optionTag += "<select name="+code+" class='form-control' style='display: block !important;'>";
                    fieldcodeArr.forEach(function (val, key) {
                        var fldLabel = fieldtextArr[key];
                        optionTag += "<option value='" + val + "' name='" + val + "'>";
                        optionTag += fldLabel;
                        optionTag += "</option>";
                    })
                    optionTag += "</select></span>";
                }
            }

            $(dataCss0 + " div.field div.options").html(optionTag);


            var fields_option = '';
            fieldtextArr.forEach(function (val, key) {
                var field_code = fieldcodeArr[key];
                var tooltipDesc = tooltipArr[key];
                var tmpcode = field_code.split('-');
                var fcode;
                if(tmpcode.length == 2){
                    fcode = tmpcode[1];
                }else {
                    fcode= tmpcode[0];
                }

                if(type == 'radio' || type =='dropdown'){
                    var codetype = '('+fcode+')';
                }

                if(type == 'checkbox'){
                    var codetype = '['+fcode+']';
                }

                fields_option +=  codetype + val;
                fields_option +=  '|'+tooltipDesc+'\n';
            })

            $(dataCss).attr('fields_option', fields_option);
        }

        var ModalEdit = '#Modal_edit_'+timestamp;

        if(type=='message'){
            var messageQ =   "$('"+ModalEdit+" .modal-body div#question_fields').hide();";
            var hideQfields = messageQ == 'undefined' ? '' : messageQ;
        }

        var show_opt = "$('"+ModalEdit+" div.tabs ul li:not(:first)').css('display', 'block');\n";
        var show_option = (type == 'checkbox' || type == 'radio' || type == 'dropdown') ? show_opt : '';

        $(dataCss0 + " a:first").attr('data-target', ModalEdit);
        $(dataCss0 + " a:first").attr('onclick', "$('"+ModalEdit+" div.tabs ul li:first').trigger('click');\n "+show_option +
            "\n $('"+ModalEdit+" div.tabs ul li:first').attr('class', 'is-active');\n" +
            "" +hideQfields+"");

        if(flag=='1'){
            var listQn = "li#"+type+ts;
        }else{
            var listQn = "li#qn"+qn;
        }

/*        if(dup.length > 0){
            //do not remove
        }else{
            $(listQn).remove();
        }*/

        $(dataCss0).clone().attr('id', type + timestamp).appendTo(modalId+" #add_qn_template");
        $(dataCss0 + timestamp).insertAfter(listQn).hide().fadeIn(1000);
        $(listQn).remove();
        $(modalId).clone().attr('id', 'Modal_edit_'+timestamp).prependTo("body");

        $(''+ModalEdit+'').attr('class', 'modal fade');
        $(''+ModalEdit+'').css('display', 'none');
        $(''+ModalEdit+' .modal-title').text('Update Question');
        $(''+ModalEdit + ' select#type option[value='+type+']').attr('selected', 'selected');
        $(''+ModalEdit+' .modal-footer button:first').text('Save Question');
        $(''+ModalEdit+' .modal-footer button:first').attr('id', 'save_edit_qn_'+timestamp);
        $(''+ModalEdit+' .modal-footer button:first').attr('onclick',
            "var modalId = $(this).closest('.modal-dialog').parent().attr('id');" +
            "save_edit_qn(modalId, '1', "+qn+", "+timestamp+")");

        //Fields tab Action
        var field_on_click = "var modalId = $('#Modal_edit_"+timestamp+" .modal-dialog').parent().attr('id');\n" +
            "                            $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','');\n" +
            "                            $('#'+modalId+' div.tabs ul li:first').attr('class','is-active');\n" +
            "                            $('#'+modalId+' #question_fields').show();\n" +
            "                            $('#'+modalId+' .fieldsOptions').hide();\n" +
            "                            $('#'+modalId+' div.type').show();";

        $("#Modal_edit_"+timestamp+" div.tabs ul li:first").attr('onclick',field_on_click);

        //Fields tab Action
        var option_on_click = "var modalId = $('#Modal_edit_"+timestamp+" .modal-dialog').parent().attr('id');\n" +
            "                            $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','is-active');\n" +
            "                            $('#'+modalId+' div.tabs ul li:first').attr('class','');\n" +
            "                            $('#'+modalId+' #question_fields').hide();\n" +
            "                            $('#'+modalId+' .fieldsOptions').show();\n" +
            "                            $('#'+modalId+' div.type').hide();";

        $(""+ModalEdit+" div.tabs ul li:not(:first)").attr('onclick',option_on_click);

        var add_options = "$(\""+ModalEdit+" #add_qn_template .fieldOptions\").clone().appendTo('"+ModalEdit+" div#fields').hide().fadeIn(2000);"

        $(""+ModalEdit+" .fieldsOptions .field a#add_fld").attr('onclick',add_options);

        //auto save
        parse_template();
        auto_save();


    }
</script>