<script>

    //save add

    // $( "#save_add_qn" ).click(function() {

    function save_add_qn(btn, action, liqn){

        // alert(liqn);

        var Modal_ID = $(btn).parent().parent().parent().parent().attr('id');
        // if($('#Modal_add input#question').val().length > 0){
            var qnAppend = $('input#addQnType').val();
            // console.log(qnAppend);

            if(qnAppend == 'preppend'){
                var qn = 0;
            }

            if(qnAppend == 'append'){
                var qn = $("ol#questions li").length + 1;
            }

            var d = new Date();
            var timestamp = d.getTime();
            var modal = $(this).closest('.modal-dialog').attr('id');
            var modalId = $('#'+modal).parent().attr('id');
            /*console.log(modal);
            console.log(modalId);*/

            var type = $('#'+Modal_ID+' select#type').val();
            var rq = $('#'+Modal_ID+'  select#required').val();
            var code = $('#'+Modal_ID+' input#code').val();
            var question = $('#'+Modal_ID+' input#question').val();
            var message = $('#'+Modal_ID+' textarea.message').val();
//            console.log($('#Modal_add textarea.message').val());
            var goal = $('#'+Modal_ID+'  input#parent_goal').val();
            var resp = $('#'+Modal_ID+'  select#parent_response').val();

            var obs = $('#'+Modal_ID+'  input#observation:checked').val();
            var intv = $('#'+Modal_ID+'  input#intervention:checked').val();
            // var obs = $('#'+Modal_ID+' input#observation').val();
            // var intv = $('#'+Modal_ID+' input#intervention').val();
            var picklist = $('#'+Modal_ID+' input#selectpicklist').attr('selectpicklist_id');
            var caredomain = $('#'+Modal_ID+' input#caredomain').attr('caredomain_id');

           /* console.log(Modal_ID);
            console.log(goal);
            console.log('response:'+resp);
            console.log('obs:'+obs);*/
            //Edit  - delete the previous then add the new

            var qnfld = Modal_ID.split('_');
            var li_qn_id = qnfld[2];
            // alert(qnfld[1]);
        var liqnid='';
        if(qnfld[2] !== 'undefined'){

                if(qnfld[1] === 'edit') {
                    //$('li#' + li_qn_id).remove();
                    // $('#Modal_edit_'+li_qn_id).remove();
                    liqnid = li_qn_id;
                }
            }
        /**********************************************************************/


            var  fieldcodeArr = []; var fcArr = [];
            $('#'+Modal_ID+'  div#fields input[name^="code"]').each(function() {
                val = $(this).val();
                fcArr.push(val);
            });

            if(fcArr.length === 0){
                fieldcodeArr = ['OPTION'];
            }else{
                fieldcodeArr = fcArr;
            }

            var  fieldtextArr = []; var ftArr = []; var fieldTextListArr = [];
            $('#'+Modal_ID+' div#fields input[name^="text"]').each(function() {
                var id = $(this).attr('picklistid');

                // var fieldTextList =[{id: $(this).val()}] ;
                picklistdata = id+':'+$(this).val();
                fieldTextListArr.push(id);

                val = $(this).val();
                ftArr.push(val);
            });

            if(ftArr.length === 0){
                fieldtextArr = ['Option 1'];
            }else{
                fieldtextArr = ftArr;
                savePicklistMap();
            }

            /*var  tooltipArr = []; var ttArr = [];
            $('#'+Modal_ID+' div#fields input[name^="tooltip"]').each(function() {
                val = $(this).val();
                ttArr.push(val);
            });

            if(ttArr.length === 0){
                tooltipArr = ['Tooltip Option 1'];
            }else{
                tooltipArr = ttArr;
            }*/

            var  goalArr = []; var gArr = [];
            $('#'+Modal_ID+' div#fields input[name^="goal"]').each(function() {
                val = $(this).val();
                gArr.push(val);
            });

            if(gArr.length === 0){
                goalArr = [0];
            }else{
                goalArr = gArr;
            }

            var  responseArr = []; var rArr = [];
            $('#'+Modal_ID+' div#fields select[name^="response"]').each(function() {
                val = $(this).val();
                rArr.push(val);
            });

            if(rArr.length === 0){
                responseArr = [0];
            }else{
                responseArr = rArr;
            }

            var  scoreArr = []; var sArr = [];
            $('#'+Modal_ID+' div#fields input[name^="score"]').each(function() {
                val = $(this).val();
                sArr.push(val);
            });

            if(sArr.length === 0){
                scoreArr = [0];
            }else{
                scoreArr = sArr;
            }


            var  msgPointArr = [];
            $('#'+Modal_ID+' #question_message input[name^="point"]').each(function() {
                val = $(this).val();
                msgPointArr.push(val);
            });

            var caredomainArr =[], cdArr=[];
            $('#'+Modal_ID+' div#fields input[name^="caredomain"]').each(function() {

                var id = $(this).attr('caredomain_id');
                caredomainArr.push(id);
                val = $(this).val();
                cdArr.push(val);
            });

            // console.log(caredomainArr);

            /*if(cdArr.length > 0){

                caredomainArr = cdArr;
            }*/

            var dataCss = '#'+Modal_ID+" #add_qn_template li#"+type+" label#"+type;
            var dataCss0 = '#'+Modal_ID+" #add_qn_template li#"+type;
            var dataCss1 = '#'+Modal_ID+" #add_qn_template li#";
            var dataShowCode = '#'+Modal_ID+" #add_qn_template li#"+type+" pre";

            $(dataCss).text(question);
            $(dataCss).attr('name', code);
            $(dataCss).attr('qname', code);
            $(dataCss).attr('typeofq', type);
            $(dataCss).attr('rq', rq);

            console.log(resp);
            if(resp != null){
                if(resp.length > 0){
                    $(dataCss).attr(resp, goal);
                }
            }

            // if(goal != 'undefined'){
               /* if(goal.length > 0) {
                    if(resp != 'goal'){
                        $(dataCss).attr('goal', goal);
                    }

                }*/
            // }


            // $(dataCss).attr('obs', obs);
            // $(dataCss).attr('intv', intv);
            $(dataCss).attr('picklist', picklist);
            $(dataCss).attr('caredomain', caredomain);

            //add parse code to pre tag

            var qnCode = '';

            if(type=='message'){

                qnCode += "=\n";
                qnCode += message+"\n";
                qnCode += "=\n";

                msgPointArr.forEach(function(val, key){
                    qnCode += "*"+val+"\n";
                })

            }else{

                if(rq){var r='*';}
                qnCode += '#'+qn+r+"\n";
                qnCode += '$'+code+"\n";
                qnCode += '?'+question+"\n";
                qnCode += '@'+type+"\n";

                fieldtextArr.forEach(function (val, key) {
                    var field_code = fieldcodeArr[key];
                    var tmpcode = field_code.split('-');
                    var fcode = field_code;

                    /*if(tmpcode.length == 2){
                        fcode = tmpcode[1];
                    }else {
                        fcode= tmpcode[0];
                    }

                    var tooltip = tooltipArr[key];
                    var tip = '';
                    if(tooltip.length > 0){
                        tip = "|"+tooltip;
                    }*/

                    var score = scoreArr[key];
                    var scr = '';
                    if(score.length > 0){
                        scr = "|"+score;
                    }

                    var picklistid = fieldTextListArr[key];

                    if(type == 'radio' || type == 'dropdown'){
                        qnCode += '('+fcode+") "+val+scr+"\n";
                    }else{
                        qnCode += '['+fcode+"] "+val+scr+"\n";
                    }
                })

                if(goal.length > 0) {
                    qnCode += '>goal=' + goal + "\n";

                    if(resp != null){
                        if(resp.length > 0){
                            qnCode += '>'+resp+'='+goal+"\n";
                        }
                    }

                }

               /* if(obs.length > 0){
                    qnCode += '>obs='+obs+"\n";
                }
                if(intv.length > 0){
                    qnCode += '>intv='+intv+"\n";
                }*/

            }

            qnCode += "^^\n";


            $(dataShowCode).text(qnCode);

            if(type=='message'){

                var point = '<ul>';
                var msgpoint='';
                msgPointArr.forEach(function(v, k){
                    point += '<li>'+v+'<input type="hidden" name="point[]" value="'+v+'" /> </li>';
                    msgpoint += '*'+v+'\n'
                })
                point +='</ul>';

                $(dataCss).attr('msgpoint', msgpoint);

                $(dataCss).html(message);
//            $(dataCss0+'div.field div.msgpoint').html(point);
                $(dataCss0 +" div.content").html(message+point);
            }

            if(type == 'radio' || type == 'checkbox' || type == 'dropdown') {


                var optionTag = '';

                if(type == 'radio' || type == 'checkbox'){

                    fieldcodeArr.forEach(function (val, key) {
                        var tagId = type + '_' + val;
                        var fldLabel = fieldtextArr[key];
                        var score = scoreArr[key];
                        var caredomain = caredomainArr[key];

                        optionTag += "<div id='" + tagId + "' class='form-row'>";
                        optionTag += "<input type='" + type + "' fld_name='" + val + "' fld_caredomain='" + caredomain + "' fld_code='" + val + "' fld_label='" + fldLabel + "' " +
                            " class='regular-checkbox' id='" + val + "'  />";
                        optionTag += "<label id='" + val + "' for='" + val + "' >";
                        optionTag += fldLabel;
                        optionTag += "</label>";
                        if(score.length > 0){
                            optionTag += " &nbsp; <a href=\"#\" data-toggle=\"tooltip\" data-placement=\"right\" " +
                                "title=\"Score: "+score+"\" data-original-title=\"Score: "+score+"\"><i class=\"fa fa-asterisk\"></i></a>";
                        }
                        optionTag += "</div>";
                    })
                }else{
                    if (type == 'dropdown') {
                        // console.log(fieldcodeArr);
                        optionTag += "<span class='select fordropdown'>";
                        optionTag += "<select name="+code+" class='form-control' style='display: block !important;'>";
                        fieldcodeArr.forEach(function (val, key) {
                            var fldLabel = fieldtextArr[key];
                            optionTag += "<option value='" + val + "' name='" + val + "'>";
                            optionTag += fldLabel;
                            optionTag += "</option>";
                        })
                        optionTag += "</select></span>";
                    }
                }

                $(dataCss0 + " div.field div.options").html(optionTag);


                var fields_option = '';
                fieldtextArr.forEach(function (val, key) {
                    var field_code = fieldcodeArr[key];
                    var tooltipDesc = scoreArr[key];
                    var tmpcode = field_code.split('-');
                    var fcode = field_code;

                    /*if(tmpcode.length == 2){
                        fcode = tmpcode[1];
                    }else {
                        fcode= tmpcode[0];
                    }*/

                    if(type == 'radio' || type =='dropdown'){

                        var codetype = '('+fcode+')';
                    }

                    if(type == 'checkbox'){
                        var codetype = '['+fcode+']';
                    }

                    var picklistid = fieldTextListArr[key];
                    // var domain = caredomainArr[key];
                    // fields_option +=  codetype + val +'***'+picklistid+'***'+domain;
                    var fld_goal = goalArr[key], fld_response = responseArr[key];
                    fields_option +=  codetype + val +'***'+picklistid+'***'+fld_goal+'***'+fld_response;

                    fields_option +=  '|'+tooltipDesc+'\n';

                    /*var picklistid = fieldTextListArr[key];
                    picklists_ID += picklistid+'~';*/
                })

                if(fields_option === ''){
                    var fieldOption = '\n[OPTION] Option 1';
                }else{
                    var fieldOption = fields_option;
                }

                $(dataCss).attr('fields_option', fieldOption);
                // $(dataCss).attr('picklists', picklists_ID);

                // console.log(fieldOption);
            }

            var ModalEdit = '#Modal_edit_'+timestamp;

            if(type=='message'){
                var messageQ =   "$('"+ModalEdit+" .modal-body div#question_fields').hide();";
                var hideQfields = messageQ == 'undefined' ? '' : messageQ;
            }

            var show_opt = "$('"+ModalEdit+" div.tabs ul li:not(:first)').css('display', 'block');\n";
            var show_option = (type == 'checkbox' || type == 'radio' || type == 'dropdown') ? show_opt : '';

            $(dataCss0 + " a:first").attr('data-target', ModalEdit);
            $(dataCss0 + " a:first").attr('onclick', "$('"+ModalEdit+" div.tabs ul li:first').trigger('click');\n "+show_option +
                "\n $('"+ModalEdit+" div.tabs ul li:first').attr('class', 'is-active');\n" +
                "" +hideQfields+"");

            //get unique id to li question
            $(dataCss0).attr('id',timestamp);
            // $(dataCss0).attr('id', type + timestamp);
            // alert(dataCss0 + timestamp);

        if(type !== 'default') {
            if ($('#Modal_add input#question').val().length > 0) {

                if((goal == '' && resp == '') || (goal != '' && resp != null)){
                    if (action === 'add') {
                        if (qnAppend == 'append') {
                            $(dataCss1 + timestamp).clone().appendTo("#form_template_question ol#questions").hide().fadeIn(2000);
                        } else {
                            $(dataCss1 + timestamp).clone().prependTo("#form_template_question ol#questions").hide().fadeIn(2000);
                        }
                    } else if (action === 'edit') {


                        var liquestionId = liqnid.length > 0 ? liqnid : liqn;
                        // alert(liquestionId);
                        $(dataCss1 + timestamp).clone().insertAfter("#form_template_question ol#questions li#" + liquestionId).hide().fadeIn(1000);
                        $('#form_template_question ol#questions li#' + liquestionId).remove();

                    }else{
                    }
                }
            }
        }


            $(dataCss1 + timestamp).attr('id', type);
            // alert(type);

        if(liqnid.length > 0){
            $('#Modal_edit_'+liqnid).clone().attr('id', 'Modal_edit_'+timestamp).prependTo("body");
            $('#Modal_edit_'+liqnid).remove();
        }else{
            $("#Modal_add").clone().attr('id', 'Modal_edit_'+timestamp).prependTo("body");
        }


            $(''+ModalEdit+'').attr('class', 'modal fade');
            $(''+ModalEdit+'').css('display', 'none');
            $(''+ModalEdit+' .modal-title').text('Save Question');
            $(''+ModalEdit + ' select#type option[value='+type+']').attr('selected', 'selected');
            $(''+ModalEdit + ' select#type ').addClass(' disabled');
            $(''+ModalEdit + ' select#parent_response ').val(resp);

            // console.log(responseArr);
            if(responseArr != 'undefined' || responseArr != null){

                var response = $(''+ModalEdit+' div#fields select[name^="response"]');
                response.each( function(k, v) {
                    var val = responseArr[k];
                    $(this).val(val);
// console.log(this);
                })
            }

            $(''+ModalEdit+' .modal-footer button:first').text('Save Question');
            $(''+ModalEdit+' button#save_add_qn').attr('onclick', "save_add_qn(this, 'edit','"+timestamp+"')");
//        $(''+ModalEdit+' .modal-footer button:first').attr('id', 'save_edit_qn_'+timestamp);
           /* $(''+ModalEdit+' .modal-footer button:first').attr('onclick',
                "var modalId = $(this).closest('.modal-dialog').parent().attr('id');" +
                "save_edit_qn(modalId, '1', '', "+timestamp+")");*/

            //Fields tab Action
            var field_on_click = "var modalId = $('"+ModalEdit+" .modal-dialog').parent().attr('id');\n" +
                "                            $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','');\n" +
                "                            $('#'+modalId+' div.tabs ul li:first').attr('class','is-active');\n" +
                "                            $('#'+modalId+' #question_fields').show();\n" +
                "                            $('#'+modalId+' .fieldsOptions').hide();\n" +
                "                            $('#'+modalId+' div.type').show();";

            $(""+ModalEdit+" div.tabs ul li:first").attr('onclick',field_on_click);

            //Fields tab Action
            var option_on_click = "var modalId = $('"+ModalEdit+" .modal-dialog').parent().attr('id');\n" +
                "                            $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','is-active');\n" +
                "                            $('#'+modalId+' div.tabs ul li:first').attr('class','');\n" +
                "                            $('#'+modalId+' #question_fields').hide();\n" +
                "                            $('#'+modalId+' .fieldsOptions').show();\n" +
                "                            $('#'+modalId+' div.type').hide();";

            $(""+ModalEdit+" div.tabs ul li:not(:first)").attr('onclick',option_on_click);

            // var add_options = "$(\""+ModalEdit+" #add_qn_template .fieldOptions\").clone().appendTo('"+ModalEdit+" div#fields').hide().fadeIn(2000);"
            var add_options = "$(\"#Modal_add add_qn_template .fieldOptions\").clone().appendTo('"+ModalEdit+" div#fields').hide().fadeIn(2000);"

            $(""+ModalEdit+" .fieldsOptions .field a#add_fld").attr('onclick',add_options);


                if(type !== 'default'){

                    if($('#Modal_add input#question').val().length === 0){

                            $('#'+Modal_ID+'  ul.nav li:first a').trigger('click');
                            $('#'+Modal_ID+'  input#question').focus();
                            toastr.warning('Please fill up Question text field.').css('width', 'auto');
                            return false;


                    }else{

                        // parse_save(action);

                            console.log(resp);

                            var parse_flg = 0;
                            if(goal == '' && resp == ''){
                                parse_flg = 1;
                            }else{

                                if(goal != '' && resp != null){
                                    parse_flg =1;
                                }else{
                                    parse_flg = 0;
                                    toastr.warning('Please fill up Goal and Response field.').css('width', 'auto');
                                }
                            }

                            if(parse_flg > 0){
                                parse_save(action);
                            }


                    }


                    return false;
                }else{
                    toastr.warning('Please choose type of field.').css('width', 'auto');
                    $('#'+Modal_ID+' select#type').focus();
                    return false;
                }

            //auto save


        /*}
        else{
            $('#Modal_add ul.nav li:first a').trigger('click');
            $('#Modal_add input#question').focus();
            toastr.warning('Please fill up Question text field.').css('width', 'auto');
        }*/


    }

    function parse_save(action){

        parse_template(action);
        auto_save();

        $('button#close_add_modal').trigger('click');
        $('.modal-backdrop').remove();

    }


    function savePicklistMap(){


        var url = '{{url('picklist/storeformpicklistmap')}}';

        var picklist = $('#Modal_add input#selectpicklist').attr('picklistid');

        axios.post(url,{
            formid: '{{$formid}}',
            picklistid: picklist
        }).then(function(response){
            // console.log(response.data);
        })
    }
</script>