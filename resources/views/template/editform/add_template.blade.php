<div id="add_qn_template" style="visibility: hidden; height: 0px; overflow: hidden;">
    <li id="bodymap" >
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash_text" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this bodymap?')}}');
                " ><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
                <label class="label_qn" id="bodymap"  >This is Text</label>
                <a href="#" data-toggle="tooltip" data-placement="right" title="" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                <p class="control">
                    <img src="{{asset('images/bodymap_v2.png')}}" style="width: 50%;"/>
                </p>
            </div>
            <div class="field column is-6 precode">
                <pre id="showCode" >

                </pre>
            </div>
        </div>
    </li>

    <li id="text" >
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash_text" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                " ><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
                <label class="label_qn" id="text"  >This is Text</label>
                <a href="#" data-toggle="tooltip" data-placement="right" title="" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                <p class="control">
                    <input class="input form-control" type="text" id="text"  name="" style="width:80%" />
                </p>
            </div>
            <div class="field column is-6 precode">
                <pre id="showCode" >

                </pre>
            </div>
        </div>
    </li>



    <li id="memo">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash " onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
            <label class="label_qn" id="memo" name="" obs="" goal="" intv="" req="">This is memo</label>
            <p class="control">
                <textarea id="memo" class="textarea  form-control" id="froala-editor" ></textarea>
            </p>
            </div>
            <div class="field column is-6 precode">
                <pre id="showCode" >

                </pre>
            </div>
        </div>
    </li>


    <li id="number">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
            <label class="label_qn" id="number" name="" obs="" goal="" intv="" req="">This is number</label>
            <p class="control">
                <input class="input  form-control" id="number" type="number"   step="any" value=""
                       onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46'  style="width:150px"/>
            </p>
            </div>
            <div class="field column is-6 precode">
                <pre id="showCode" >

                </pre>
            </div>
        </div>
    </li>


    <li id="date">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
            <label class="label_qn" id="date" name="" obs="" goal="" intv="" req="">This is Date</label>
            <p class="control">
                <input class="input  form-control datepicker" id="datepicker"  style="width:150px"/>
            </p>
        </div>
        <div class="field column is-6 precode">
                <pre id="showCode" >

                </pre>
        </div>
        </div>
    </li>

    <li id="time">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
            <label class="label_qn" id="time" name="" obs="" goal="" intv="" req="">This is time</label>
            <p class="control">
                <input class="input  form-control timepicker" id="timepicker"   style="width:150px"/>
            </p>
            </div>
            <div class="field column is-6 precode">
                    <pre id="showCode" >

                    </pre>
            </div>
        </div>
    </li>

    <li id="checkbox">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static"  data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>

        <div class="columns">
            <div class="field column col-9">

                <label class="label_qn" id="checkbox" name="" obs="" goal="" intv="" req="">This is checkbox</label>
                <a href="#" data-toggle="tooltip" data-placement="right" title="" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>

                <div class="options">

                </div>
            </div>
            <div class="field column is-6 precode">
                        <pre id="showCode" >

                        </pre>
            </div>
        </div>
    </li>

    <li id="radio">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>

        <div class="columns">
            <div class="field column col-9">

            <label class="label_qn" id="radio" name="" obs="" goal="" intv="" req="">This is Radio</label>
            <a href="#" data-toggle="tooltip" data-placement="right" title="" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
            <div class="options">

            </div>
        </div>

        <div class="field column is-6 precode">
                        <pre id="showCode" >

                        </pre>
        </div>
</div>
    </li>

    <li id="dropdown">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>

        <div class="columns">
            <div class="field column col-9">
                <label class="label_qn" id="dropdown" name="" obs="" goal="" intv="" rq="">This is dropdown</label>
            <div class="options">
                <span class="select fordropdown">
                    <select class="form-control" style="display: block !important;">

                    </select>
                </span>
            </div>

        </div>

        <div class="field column is-6 precode">
                        <pre id="showCode" >

                        </pre>
        </div>
</div>
    </li>

    <li id="message" value="0" style="list-style: none;" class="message">
        <a href="#" data-toggle="modal" data-target="#Modal_edit" class="qn" data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
        <a href="#" class="qn_trash" id="qn_trash" onclick="
                var id = $(this).parent().attr('id');
                confirmAction('li', id, '{{__('Are you sure you want to remove this question?')}}');
                "><i class="fa fa-trash"></i></a>
        <div class="columns">
            <div class="field column col-9">
            <label typeofq="message" id="message" class="label_qn" style="visibility: hidden; height: 1px !important;">Content</label>
            <div class="content">
            </div>
        </div>

        <div class="field column is-6 precode">
                        <pre id="showCode" >

                        </pre>
        </div>
</div>
    </li>

        <div class="fieldOptions" id="fieldOptions" style="padding: 13px 15px 10px 30px !important; background: #f4f4f4;  ">

            <div class="row border-bottom-1 pb-1">
                <div class="col-2 p-1">
                    <label>Code </label>
                    <p class="control mb-1"> <input class=" form-control field_code" type="text" name="code[]" id="code" placeholder="Code"></p>
                </div>
                <div class="col-3 p-1">
                    <label>Text</label>
                    <p class="control mb-1">  <input class=" form-control" type="text" name="text[]" id="text" placeholder="Text Option"></p>

                </div>
                {{--<div class="col-2 p-1 ">
                    <label>Care Domain</label>
                    <p class="control mb-1"  id="caredomain">  <input class=" form-control" type="text" name="caredomain[]" id="caredomain" placeholder="Care Domain"></p>

                </div>--}}
                <div class="col-3 p-1 ">
                    <label>Goal</label>
                    <p class="control mb-1"  id="goal">  <input class=" form-control" type="text" name="goal[]" id="goal" placeholder="Goal"></p>
                </div>
                <div class="col-2 p-1 ">
                    {{--<label>Goal</label>--}}
                    <p class="control mb-1"  id="response">

                        <select class="form-control" name="response[]" id="response"  style="display: block !important;">
                            <option value="" selected>Select</option>
                            <option value="goal">Goal</option>
                            <option value="intv">Intervention</option>
                            <option value="obs">Observation</option>
                        </select>
                    </p>
                </div>
                <div class="col-1 p-1 pr-3">
                    <label>Score</label>
                    <p class="control mb-1">  <input class=" form-control" type="number" min="0" name="score[]" id="score" placeholder="Score"></p>

                </div>
                <div class="col-1 p-1 text-center">
                    <a href="#" class="qn_trash  " id="delete_fld" onclick="
                            var id = $(this).parent().parent().parent().attr('id');
                            var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                            // alert(id);
                            confirmAction('#'+modalId+' div#fields div#'+id, id, '{{__('Are you sure you want to remove this field option?')}}');
                            " style="float: none !important;">
                        <i class="fa fa-trash" style="font-size: 25px;     margin-top: 5px;"></i>
                    </a>
                </div>

            </div>
        </div>


</div>