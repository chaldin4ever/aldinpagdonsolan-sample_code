@extends('layouts.poc')

@section('style')
    <style>
        .process-title {
            font-size: 1.25rem;
            text-decoration: underline;
        }
        .tooltip .tooltiptext {
            width: 120px;
            bottom: 100%;
            left: 50%;
            margin-left: -60px; /* Use half of the width (120/2 = 60), to center the tooltip */
        }

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-cogs"></i> {{__('Standardised Processes')}}</strong></h3>
                <button type="button"  onclick="reset()" href="{{url('sprocess/add')}}" data-toggle="modal" data-target="#addSProcessModal" class="float-right btn btn-mdb-color">Add Process</button>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">

                    <div class="card  "  >
                        <div class="card-header  white-text" style="background: #003056; ">
                            Admin Process
                        </div>
                        <div class="card-body">
                            <ul class="list-group col ">

                                @if(!empty($processAdmin))
                                    @foreach($processAdmin as $admin)
                                      <li class="list-group-item">
                                          <span class="state_{{$admin->_id}}">
                                         @if($admin->State == 'published')
                                                  <i class="fa fa-check-circle-o float-left m-1 mt-2 " style="color: green;" ></i>
                                              @else
                                                  <i class="fa fa-times-circle-o float-left m-1 mt-2 "  style="color: red;" ></i>
                                              @endif
                                          </span>
                                          <a href="{{url('sprocess/view/'.$admin->_id)}}" class="float-left process-title">
                                              {{$admin->ProcessName}}
                                          </a>

                                          <a href="#" class="float-right"  onclick="archive('{{$admin->_id}}')" >
                                              <i class="fa fa-trash" style="font-size: 20px !important;"></i>
                                          </a>
                                          <a href="#" class="float-right mr-2" data-toggle="modal" data-target="#addSProcessModal"  onclick="edit('{{$admin->_id}}', '{{$admin->ProcessName}}', '{{$admin->Category}}')">
                                              <i class="fa fa-edit" style="font-size: 20px !important;"></i>
                                          </a>
                                          <a href="#" class="float-right mr-2" {{--data-toggle="tooltip" data-placement="top" title="Copy"--}}  onclick="copy('{{$admin->_id}}')">
                                              <i class="fa fa-clone" style="font-size: 20px !important;"></i>
                                          </a>

                                          @if($admin->State == 'published')
                                              <a href="#" class="float-right mr-2" {{--data-toggle="tooltip" data-placement="top" title="Copy"--}}  onclick="publish('{{$admin->_id}}', 'draft')">
                                                  <i class="fa fa-times-circle" style="font-size: 20px !important;"></i>
                                              </a>
                                          @else
                                              <a href="#" class="float-right mr-2" {{--data-toggle="tooltip" data-placement="top" title="Copy"--}}  onclick="publish('{{$admin->_id}}', 'published')">
                                                  <i class="fa fa-check-square" style="font-size: 20px !important;"></i>
                                              </a>
                                          @endif
                                          <input type="hidden" id="desc_{{$admin->_id}}" value="{{$admin->Description}}" />
                                          <div class="row w-100 p-3 pt-1  mt-2">
                                             {!! $admin->Description !!}
                                          </div>
                                      </li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>

                    </div>

            </div>
        </div>

        <div class="row bg-white">
            <div class="col pt-3">
                <div class="card  "  >
                    <div class="card-header  white-text" style="background: #003056; ">
                        Care Process
                    </div>
                    <div class="card-body" >
                    <ul class="list-group  col">
                        @if(!empty($processCare))
                            @foreach($processCare as $care)
                                <li class="list-group-item">
                                    <span class="state_{{$care->_id}}">
                                         @if($care->State == 'published')
                                            <i class="fa fa-check-circle-o float-left m-1 mt-2 " style="color: green;" ></i>
                                        @else
                                            <i class="fa fa-times-circle-o float-left m-1 mt-2 "  style="color: red;" ></i>
                                        @endif
                                          </span>
                                    <a href="{{url('sprocess/view/'.$care->_id)}}" class="float-left process-title">
                                        {{$care->ProcessName}}
                                    </a>

                                    <a href="#" class="float-right" onclick="archive('{{$care->_id}}')">
                                        <i class="fa fa-trash" style="font-size: 20px !important;"></i>
                                    </a>
                                    <a href="#" class="float-right mr-2" data-toggle="modal" data-target="#addSProcessModal"  onclick="edit('{{$care->_id}}', '{{$care->ProcessName}}', '{{$care->Category}}')">
                                        <i class="fa fa-edit" style="font-size: 20px !important;"></i>
                                    </a>
                                    <a href="#" class="float-right mr-2" {{--data-toggle="tooltip" data-placement="top" title="Copy"--}}  onclick="copy('{{$care->_id}}')">
                                        <i class="fa fa-clone" style="font-size: 20px !important;"></i>
                                    </a>

                                    @if($care->State == 'published')
                                        <a href="#" class="float-right mr-2" {{--data-toggle="tooltip" data-placement="top" title="Copy"--}}  onclick="publish('{{$care->_id}}', 'draft')">
                                            <i class="fa fa-times-circle" style="font-size: 20px !important;"></i>
                                        </a>
                                    @else
                                        <a href="#" class="float-right mr-2" {{--data-toggle="tooltip" data-placement="top" title="Copy"--}}  onclick="publish('{{$care->_id}}', 'published')">
                                            <i class="fa fa-check-square" style="font-size: 20px !important;"></i>
                                        </a>
                                    @endif
                                    <input type="hidden" id="desc_{{$care->_id}}" value="{{$care->Description}}" />
                                    <div class="row w-100 p-3 pt-1  mt-2">
                                        {!! $care->Description !!}
                                    </div>
                                </li>
                            @endforeach
                        @endif
                        {{--<ul class="list-group-item">Waiting List</ul>
                        <ul class="list-group-item">Admission</ul>
                        <ul class="list-group-item">Transfer</ul>
                        <ul class="list-group-item">Discharge</ul>--}}
                    </ul>
                    </div>
                </div>
            </div>
        </div>

        @include('sprocess.add')
    </div>

@endsection

@section('script')
    <script>

        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            })
        })

        $(document).ready(function(){

            @if(session('status'))
                  toastr.success('{{session('status')}}');
            @endif


            $('.mdb-select').material_select();
        })


        function edit(id, name, category){

            destroyEditor();

            $('input#sprocessId').val(id);
            $('input#ProcessName').val(name);

            var desc = $('input#desc_'+id).val();
            $('textarea#Description').val(desc);

            $(' #addSProcessModal .select-dropdown li:contains(' + category + ' Process)').trigger('click');

            setEditor();
        }

        function reset(){

            $('input#sprocessId').val('');
            $('input#ProcessName').val('');
            $('textarea#Description').val('');
        }

        function setEditor(){

            $(' textarea#Description').froalaEditor({
                toolbarButtons: ['fontSize', 'bold', 'italic', 'underline', 'strikeThrough',
                    'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                    'undo', 'redo',
                    'html', 'clearFormatting'
                ],
            });

        }

        function destroyEditor(){
            $('textarea#Description').froalaEditor('destroy');
        }

        function archive(id){

            var url = '{{url('sprocess/archive')}}/'+id;

            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            alertify.confirm('Are you sure?', function(e){

                if(e){
                    window.location.href = url;
                    $('.modal-backdrop').fadeOut("slow");
                }else{
                    $('.modal-backdrop').fadeOut("slow");
                }

            })
        }

        function copy(id){

            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            alertify.confirm('Do you want to copy this process?', function(e){

                if(e){
                    var url = '{{url('sprocess/copy')}}/'+id;

                    window.location.href = url;
/*
                    axios.get(url).then(function(response){
                        console.log(response );
                        toastr.success('Successfully Copied.');
                    })*/
                }

                $('.modal-backdrop').fadeOut("slow");
            });

        }

        function publish(id, state){

            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            var msg = state === 'draft' ? 'Do you want to unpublish this process?' : 'Do you want to publish this process?';
            alertify.confirm(msg, function(e){

                if(e){
                    var url = '{{url('sprocess/publish')}}/'+id+'/'+state;

                    window.location.href = url;

                    /*axios.get(url).then(function(response){
                        // console.log(response );

                        var html = state === 'draft' ? "<i class=\"fa fa-times-circle-o float-left m-1 mt-2\" style='color: red' ></i>" :
                            "<i class=\"fa fa-check-circle-o float-left m-1 mt-2\" style='color: green' ></i>";
                        $('span.state_'+id).html(html);
                        toastr.success('Successfully Saved.');



                    })*/
                }

                $('.modal-backdrop').fadeOut("slow");
            });

        }


    </script>
@endsection