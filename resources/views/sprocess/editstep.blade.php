@extends('layouts.poc')

@section('style')
    <style>
        .shadow-textarea textarea.form-control::placeholder {
            font-weight: 300;
        }
        .shadow-textarea textarea.form-control {
            padding-left: 0.8rem;
        }

        .multiple-select-dropdown li [type=checkbox]+label {
            top: .2rem !important;
        }

        [type=checkbox][class*=filled-in]:not(:checked)+label:after {border-color: #fff!important;}

        ul#steplists > li:hover  {
            cursor: move; /* fallback if grab cursor is unsupported */
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        .shadow-textarea ul, .shadow-textarea ol {
             margin: 10px;
             padding: 5px;
        }

        .shadow-textarea ul li {
            list-style: disc;
        }

        .shadow-textarea ol li {
            list-style-type: decimal;
        }

        ul#steplists li ul li   { margin-left: 30px  !important; padding-left: 10px  !important;; list-style: disc !important;}
        ul#steplists li ol li  { margin-left: 0px;  padding-left: 10px; list-style-type: decimal !important;}

        ul#steplists, ul#steplists li{
            list-style: disc !important;
        }

        .process-step-title {
            font-weight: bold;
            font-size: 1.25rem;
            line-height: 35px;
            text-decoration: underline;
        }

        .back-to-link {
            font-size:0.85rem;
            line-height:45px
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-cog"></i> {{$sprocess->ProcessName}} </strong>
                    <small><a href="#" onclick="editprocess('{{$sprocess->_id}}', '{{$sprocess->ProcessName}}', '{{$sprocess->Category}}')"  data-toggle="modal" data-target="#addSProcessModal" class="btn btn-sm btn-primary badge p-1 pl-2 pr-2"><i class="fa fa-edit"></i> {{__('Edit')}}</a></small></h3>

                <button type="button"  data-toggle="modal" data-target="#addStepProcessModal" onclick="reset()" tabindex="1" class="float-right btn btn-primary"><i class="fa fa-plus" ></i> Add Step</button>
                <a   href="{{url('sprocess/view/'.$sprocess->_id)}}"  class="float-right back-to-link">Back to process</a>
            </div>


        </div>
    </div>

    <div class="container-fluid mb-4">
        <div class="row bg-white">
            <div class="col p-3 {{--mdb-color text-white--}} m-3 " style="vertical-align: center !important;">
                {{--<i class="fa fa-info-circle fa-2x"></i> --}}{!! $sprocess->Description !!}

                <input type="hidden" id="ProcessDescription" value="{!! $sprocess->Description !!}" />
            </div>
        </div>
        <div class="row bg-white">
            <div class="col p-3">
                <ul class="list-group" id="steplists">
                    @if(!empty($steps))
                        @foreach($steps as $step)
                            <li class="list-group-item list-group-item-action flex-column align-items-start ">
                                    <input type="hidden" name="DisplayOrder[]" value="{{$step->_id}}"/>
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1 process-step-title">{{$step->Title}}</h5>
                                        <small style="font-size: 21px; " >
                                            <input type="hidden" value="{{$step->Description}}" id="stepDesc{{$step->_id}}" />
                                            <a href="#" data-toggle="modal" data-target="#addStepProcessModal" onclick="edit('{{$step->_id}}', '{{$step->Title}}', '{{json_encode($step->Forms)}}', '{{$step->DisplayOrder}}')"><i class="fa fa-edit text-dark"></i></a>
                                            <a href="#" onclick="deletestep('{{$step->_id}}','{{$sprocess->_id}}')"><i class="fa fa-trash text-dark mb-1"></i></a></small>
                                    </div>
                                <div class="row">
                                    <div class="col-8">
                                        {!! $step->Description !!}
                                    </div>

                                    <div class="col-4">
                                        <ul class="list-group   " style="font-size: 15px;">
                                            <li class="list-group-item active mdb-color border-0 p-1 pl-3">Forms</li>
                                            @foreach($step->Forms as $form)
                                                <li class="list-group-item p-1 pl-3">
                                                    {{array_get($form, 'FormID')}} - {{array_get($form, 'FormName')}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                     @else
                        <div class="alert alert-info">
                            <strong>No Steps found.</strong>
                        </div>
                    @endif

                </ul>
            </div>
        </div>

        @include('sprocess.addstep')
        @include('sprocess.add')
    </div>

@endsection

@section('script')
    <script>

        $(function() {
           setEditor();
        });

        $(document).ready(function(){



            @if(session('status'))
            toastr.success('{{session('status')}}');
            @endif


            $('.mdb-select').material_select();

            var drake =  window.dragula();
            drake = dragula([document.querySelector('ul#steplists')]);

            drake.on('drop', function(){

                var displayorder = $('ul#steplists li input[name^=DisplayOrder]');
                var orderArr = [];
                $.each(displayorder, function(k, data){
                    orderArr.push(data.value);
                })

                var url = '{{url('sprocess/displayorder')}}';
                axios.post(url,{
                    OrderList: orderArr
                }).then(toastr.success('Successfully Saved.'));

            })

        })

        function editprocess(id, name, category){

            reset(); destroyEditor();

            $('#addSProcessModal input#Redirect').val('true');
            $('#addSProcessModal input#sprocessId').val(id);
            $('#addSProcessModal input#ProcessName').val(name);

            $(' #addSProcessModal .select-dropdown li:contains(' + category + ' Process)').trigger('click');

            setEditor();

            var ProcessDescription = $('input#ProcessDescription').val();
            $('#addSProcessModal textarea#Description').froalaEditor('html.set', ProcessDescription);

        }

        function setEditor(){

             $(' textarea#Description').froalaEditor({
                toolbarButtons: ['fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough',
                    'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                    'undo', 'redo',
                    'html', 'clearFormatting'
                ],
            });

        }

        function destroyEditor(){
            $('textarea#Description').froalaEditor('destroy');
        }

        function resetDragula(){

            var drake =  window.dragula();


            drake.destroy;
            drake = dragula([document.querySelector('ul#steplists')]);
            // console.log(drake);


        }

        function edit(id, title,  forms, order){

            reset(); destroyEditor();

            $('#addStepProcessModal .formlist').hide();

            $('#addStepProcessModal .modal-title').text('Edit Step');
            var formsArr = jQuery.parseJSON(forms);

            $.each(formsArr, function(k, data){
                $(' #addStepProcessModal table.table label#'+data.FormId+'').trigger('click');
                $(' #addStepProcessModal table.table label#'+data.FormId+'').closest('tr').addClass(' selected');
            })

            var desc = $(' #stepDesc'+id).val();
            $('#addStepProcessModal input#stepId').val(id);
            $('#addStepProcessModal input#DisplayOrder').val(order);
            $('#addStepProcessModal input#Title').val(title);
            $('#addStepProcessModal textarea#Description').val(desc);

             setEditor();
        }

        function reset(){

            $('#addStepProcessModal .modal-title').text('Add Step');

            $('#addStepProcessModal input#stepId').val('');
            $('#addStepProcessModal input#Title').val('');
            $('#addStepProcessModal textarea#Description').val('');
            $('#addStepProcessModal input#showall').prop("checked", false);
            $('#addStepProcessModal table.forms tr').removeClass( 'selected');
            $('#addStepProcessModal table.forms tr').show();
            $("#addStepProcessModal table.forms input:checkbox").prop("checked", false);
            $('#addStepProcessModal ul.selectedForms li:not(:first)').remove();
            /*$('.mdb-select').material_select('destroy');
            $('.mdb-select').val('0').change();
            $('.mdb-select').material_select();*/
        }

        function deletestep(stepid, processId){

            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            var url = '{{url('sprocess/deletestep')}}/'+stepid+'/'+processId;

            alertify.confirm('Are you sure?', function(e){

                if(e){
                    window.location.href = url;
                    $('.modal-backdrop').fadeOut("slow");
                }else{
                    $('.modal-backdrop').fadeOut("slow");
                }

            })
        }

        $('#addStepProcessModal').on('shown.bs.modal', function() {
            $('#addStepProcessModal input#Title').focus();
        })


        $('input[name="searchform"]').on('keyup', function() {

            var input, filter, tr, td, i;

            input  = $(this);
            filter = input.val().toUpperCase();
            tr     = $("table.forms tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0]; // <-- change number if you want other column to search
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        })

        function selectForm(checkbox, formname){

            if(checkbox.checked){
                $('#addStepProcessModal ul.selectedForms').append("<li class=\"list-group-item \" id=\""+checkbox.value+"\" ><i class='mr-3 fa fa-check-square-o'></i>"+formname+"</li>");
                $(checkbox).closest('tr').addClass(' selected');

            }else{
                // alert('#addStepProcessModal ul.selectedForms li#'+checkbox.value);
                $('#addStepProcessModal ul.selectedForms li#'+checkbox.value).remove();
                $(checkbox).closest('tr').removeClass(' selected');
            }

        }

        function showallchecked(checkbox){

            if(checkbox.checked){
                $('#addStepProcessModal table.forms tr').hide();
                $('#addStepProcessModal table.forms tr.selected').show();
            }else{
                $('#addStepProcessModal table.forms tr').show();
            }

        }
    </script>
@endsection

