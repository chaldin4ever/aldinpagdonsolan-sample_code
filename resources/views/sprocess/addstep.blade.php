<!-- Modal -->
<div class="modal fade" id="addStepProcessModal" tabindex="-1" role="dialog"  aria-labelledby="addStepProcessModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fluid" role="document">
        <form method="post" action="{{url('sprocess/addstep')}}" id="sprocess" >
            @csrf
            {{--{{csrf_field()}}--}}
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-2 mdb-color text-white ">
                    <h5 class="modal-title">{{__('Add Step')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col">

                            <div class="form-group mb-4">
                                <input type="hidden" name="sprocessId" id="sprocessId" value="{{$sprocess->_id}}" />
                                <input type="hidden" name="stepId" id="stepId" value="" />
                                <input type="hidden" name="DisplayOrder" id="DisplayOrder" value="" />
                                <label for="Title" class="Title">{{__('Title')}}</label>
                                <input required type="text" id="Title" name="Title" value="" autocomplete="off" class="form-control"  autofocus>

                            </div>

                            <div class="form-group shadow-textarea">
                                <label for="Description">Description</label>
                                <textarea required class="form-control z-depth-1 " name="Description" id="Description" rows="8" placeholder="Write something here..."></textarea>
                            </div>


                        </div>
                        <div class="col border-left pl-4"  >
                            <div class="form-group">
                                <input type="text" class="form-control   "
                                       onfocus="$('.formlist').show()" {{--onfocusout="$('.formlist').hide()"--}}
                                       id="searchform" name="searchform" placeholder="⮟ Search Form" autocomplete="off" />


                            </div>
                            <div class="md-form mb-4 formlist bg-white mr-3 pb-4 "
                                 style=" width: 90% !important; border: 1px solid #45526e!important; height: 370px;
                                  display: none; position:absolute; top: 19px; z-index: 5;">
                                <div class="form-check  checkbox-warning-filled mdb-color p-2 text-white pt-0">
                                    <input type="checkbox" class="form-check-input filled-in" name="show" id="showall" onchange="showallchecked(this)"   >
                                    <label class="form-check-label text-white pl-4" style="font-size: 15px; top: 0;"  id="showall" for="showall"><div style="margin-top: -2px;">Show all selected forms</div></label>
                                    <button style="margin-top: -10px;" type="button" class="float-right btn btn-sm bg-danger  p-1 pl-3 pr-3 mt-0" onclick="$('.formlist').hide()"><i class="fa fa-minus"></i> Hide</button>
                                </div>

                                <div class=" "  style="height: 320px;  overflow: auto; width: 100% !important; ">

                                    <table class="table table-striped forms "  id="" style="font-size: 15px;">

                                        <tbody>
                                        @foreach($forms as $form)
                                        <tr>
                                            <td class="pt-0">
                                                <div class="form-check p-0 m-0">
                                                    <input type="checkbox" name="forms[]" onchange="selectForm(this, '{{$form->FormID.' - '.$form->FormName}}');" value="{{$form->_id}}" class="form-check-input" id="{{$form->_id}}"   >
                                                    <label class="form-check-label"  id="{{$form->_id}}" for="{{$form->_id}}">{{$form->FormID}} - {{$form->FormName}}
                                                        @if(!empty($form->Provider))
                                                            <i class="fa fa-rss-square" style="font-size:1.2rem;color:#1C3356"></i>
                                                        @endif
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>


                            </div>
                            <ul class="list-group selectedForms pr-1" style="font-size: 15px; z-index: 1;">
                                <li class="list-group-item active  border-0 mdb-color ">Selected Forms</li>

                            </ul>
                        </div>

                    </div>



                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2  border-top" style="border-top: 1px solid #dee2e6 !important;">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn btn-primary mr-3 ">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>