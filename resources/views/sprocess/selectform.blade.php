@extends('layouts.poc')

@section('style')
    <style>

        [type=radio]+label, [type=checkbox]+label{
            height: auto !important;
        }

        .white-skin input[type=checkbox].filled-in:checked+label:after {
            background-color: rgb(90, 118, 147);
            border-color: rgb(90, 118, 147);
        }
        .white-skin body{
        'Helvetica Light', Helvetica, Arial, sans-serif
        }

        .nav-link, .navbar {
            padding: 1.2rem .2rem;
            font-weight: normal;
        }

        table.bodymap td  {    font-size: 0rem !important;}
        table.bodymap button{ background: none !important; padding: 0 !important; margin:0!important; border: none !Important; cursor: pointer !important;}

        table.bodymap .circle_selected{
            position: absolute !important;
            background: darkred !important;
            opacity: .2 !important;
            display: none;

        }

        table.bodymap button div#Head{margin-top: -15px !important; margin-left: 5px !important;}
        table.bodymap button div#Right_ear{margin-top: -5px !important; margin-left: -18px !important;}
        table.bodymap button div#Left_ear{margin-top: -5px !important; margin-left: -5px !important;}
        table.bodymap button div#Head_back{margin-top: -15px !important; margin-left: 5px !important;}
        table.bodymap button div#Right_ear_back{margin-top: -5px !important; margin-left: -5px !important;}
        table.bodymap button div#Left_ear_back{margin-top: -5px !important; margin-left: -16px !important;}
        table.bodymap button div#Right_shoulder{margin-top: -15px !important;  }
        table.bodymap button div#Left_shoulder{margin-top: -15px !important;  }
        table.bodymap button div#Right_arm{margin-left: -15px !important;  }
        table.bodymap button div#Right_shoulder_back{margin-top: -15px !important;  }
        table.bodymap button div#Left_shoulder_back{margin-top: -15px !important;  }
        table.bodymap button div#Right_arm_back{margin-left: -1px !important;  }
        table.bodymap button div#Left_arm_back{margin-left: -10px !important;  }
        table.bodymap button div#Chest{margin-left: 1px !important;  margin-top: -3px;}
        table.bodymap button div#Right_inner_elbow{margin-left: -5px !important;  margin-top: -2px !important; }
        table.bodymap button div#Left_inner_elbow{ margin-top: 2px !important; }
        table.bodymap button div#Right_elbow{margin-left: 1px !important;  margin-top: -2px !important; }
        table.bodymap button div#Left_elbow{ margin-top: 2px !important; margin-left: -5px !important; }
        table.bodymap button div#Lower_back{ margin-left: 15px !important; }
        table.bodymap button div#Right_palm{margin-left: -1px !important;  }
        table.bodymap button div#Left_palm{margin-left: -5px !important;  }
        table.bodymap button div#Right_hand{margin-left: -5px !important;  }
        table.bodymap button div#Left_hand{margin-left: -5px !important;  }
        table.bodymap button div#Left_buttock{margin-left: -5px !important;  }
        table.bodymap button div#Right_knee{margin-left: -1px !important;  }
        table.bodymap button div#Sacrum{margin-left: -5px !important;  }
        table.bodymap button div#Left_foot{margin-left: -3px !important;  }
        table.bodymap button div#Right_foot{margin-left: -3px !important;  }
        table.bodymap button div#Left_foot_back{margin-left: -5px !important;  }
        table.bodymap button div#Right_foot_back{margin-left: -5px !important;  }


        /*        .bodymap_checkbox input{
                    visibility: visible;
                    left: 0 !important;
                    position: relative;
                }*/

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-1 pt-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{$form->FormName}}</strong></h3>
                <a class="btn btn-rounded btn-grey float-right" href="{{url('sprocess/view/'.$processId.'/'.$resident->_id)}}"><i class="fa fa-undo"></i> {{__('Return')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        @if($resident->_id != env('DUMMY_RESIDENT_ID'))
        <div class="row bg-white pl-2 pt-2 pb-2 mb-3">

            @include('resident.header', ['resident' => $resident])
        </div>
        @endif

        <div class="content m-0   pb-5">

            <div class="row bg-white pt-4  pl-4 pb-4">

                <div class="col">
                    <div class="bodymap_checkbox">
                        @foreach($bodymap as $k=>$v)
                            <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                            <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                        @endforeach
                    </div>

                    <form method="post" action="{{url('/assessment/store')}}" id="assessment-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="residentId" value="{{$resident->_id}}" />
                        <input type="hidden" name="formId" value="{{$form->_id}}" />
                        <input type="hidden" name="process" value="{{$processId}}" />
                        <input type="hidden" name="assessmentId" value="@if(!empty($data)){{$data->_id}}@endif" />


                        {{--<pre>{{print_r($data)}}</pre>--}}

                        @include('formeditor.question_list', ['questions' => $questions, 'data'=>array_get($data, 'data')])



                        <div class="row">
                            <button class="btn btn-rounded btn-danger">{{__('Save')}}</button>
                            <a href="{{url('sprocess/view/'.$processId.'/'.$resident->_id)}}" class="btn btn-rounded btn-grey" >{{__('Cancel')}}</a>
                        </div>


                    </form>
                </div>



            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 90
        });

        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        var hour = dt.getHours() + ":" + '00';

        $('#timepicker').pickatime(
            {
                default: hour,
                darktheme: true,
                twelvehour: true,
            });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();
        })

        $(document).ready(function() {

            @if(session('status'))
                  window.close();
            @endif

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');
            @foreach($bodymap as $k=>$v)



            $("table.bodymap button#{{$k}}").click(function () {


                $('table.bodymap td button#{{$k}}').tooltip();


                var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

                if(checkbox.length > 0){
                    $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                    $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                    $('table.bodymap button div#{{$k}}').hide();
                }else{
                    $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                    $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                    $('table.bodymap button div#{{$k}}').show();
                }

                var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                $('div#Location').text(data);
                $('input#Location').val(data);
                $('input#Bodymap').val(data_id);

                return false;
            })
            @endforeach
        })

        @php $data = array_get($data, 'data'); @endphp
        $(document).ready(function() {

            $('.bodymap  #Location').text('{{array_get($data, 'Location')}}');

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');

            @php $bmArr = explode(',',str_replace(' ','',array_get($data, 'Bodymap'))); @endphp

            @foreach($bodymap as $k=>$v)

            @if(in_array($k, $bmArr))

            $('table.bodymap button div#{{$k}}').show();

            var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

            if(checkbox.length > 0){
                $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                $('table.bodymap button div#{{$k}}').hide();
            }else{
                $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                $('table.bodymap button div#{{$k}}').show();
            }

            @endif

            @endforeach
        })


    </script>
@endsection
