<!-- Modal -->
<div class="modal fade" id="addSProcessModal" tabindex="-1" role="dialog" aria-labelledby="addSProcessModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" action="{{url('sprocess/store')}}" id="sprocess" >
            @csrf
            {{--{{csrf_field()}}--}}
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-2 mdb-color text-white ">
                    <h5 class="modal-title">{{__('Add Process')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col">

                            <div class="md-form">
                                <input type="hidden" name="sprocessId" id="sprocessId" value="" />
                                <input type="hidden" name="Archive" id="Archive" value="false" />
                                <input type="hidden" name="Redirect" id="Redirect"  />
                                <input required type="text" id="ProcessName" name="ProcessName" value="" autocomplete="off" class="form-control" placeholder=" ">
                                <label for="ProcessName">{{__('Process Name')}}</label>
                            </div>
                            <div class="md-form">
                                <textarea style="min-height: 140px;" id="Description" name="Description" class=" froala-editor form-control p-2" row="10" placeholder=" "></textarea>
                                <label for="ProcessName">{{__('Description')}}</label>
                            </div>

                            <div class="md-form">
                                <select name="ProcessCategory" id="ProcessCategory" class="mdb-select">
                                    <option value="Admin">Admin Process</option>
                                    <option value="Care">Care Process</option>
                                </select>
                                <label for="Process">{{__('Process Category')}}</label>
                            </div>

                        </div>

                    </div>



                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn btn-primary mr-3 ">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>