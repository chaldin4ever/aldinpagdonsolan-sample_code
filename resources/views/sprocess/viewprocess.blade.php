@extends('layouts.poc')

@section('style')

<style>
    ul#steplists li ul li   { margin-left: 30px  !important; padding-left: 10px  !important;; list-style: disc !important;}
    ul#steplists li ol li  { margin-left: 0px;  padding-left: 10px; list-style-type: decimal !important;}

    ul#steplists, ul#steplists li{
        list-style: disc !important;
    }

    .process-step-title {
        font-weight: bold;
        font-size: 1.25rem;
        line-height: 35px;
        text-decoration: underline;
    }

    .back-to-link {
        font-size:0.85rem;
        line-height:45px
    }
</style>
@endsection
@section('content')

    <div class="container-fluid mt-1">

        <div class="row  pl-4 pb-2 pt-4 " >
            <input type="hidden" id="selectedresident" name="selectedresident" />
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-cog"></i> {{$sprocess->ProcessName}} </strong>
                    {{--<small><a href="#" onclick="editprocess('{{$sprocess->_id}}', '{{$sprocess->ProcessName}}', '{{$sprocess->Category}}')"  data-toggle="modal" data-target="#addSProcessModal" class="btn btn-sm btn-primary badge p-1 pl-2 pr-2"><i class="fa fa-edit"></i> {{__('Edit Process')}}</a></small>--}}
                </h3>

                <a  href="{{url('sprocess/editstep/'.$sprocess->_id)}}"  class="float-right btn btn-primary"><i class="fa fa-edit" ></i> Edit Process</a>
                <a   href="{{url('sprocess')}}"  class="float-right back-to-link">Back to listing</a>
            </div>

        </div>


    </div>

    <div class="container-fluid mb-4">

        <div class="row bg-white">
            <div class="col p-3 {{--mdb-color text-white--}} m-3 " style="vertical-align: center !important;">
               {!! $sprocess->Description !!}
            </div>
        </div>

        <div class="row bg-light pl-2 pb-0 pt-0">
            <div class="dropdown float-right  mr-2">
                <button class="btn btn-ins dropdown-toggle w-100" id="selectresident"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select from most recent resident</button>

                <div class="dropdown-menu w-100 dropdown-ins">
                    @if(sizeof($residents)>0)

                        @foreach($residents as $k=>$r)
                            <a href="#" class="dropdown-item" id="{{$k}}" onclick="selectresident('{{$k}}', $( this ).html())">
                                @if(empty($r->Photo))
                                    <img class="rounded float-left mr-2 mb-1" style="margin-top: -3px" height=30px src="http://via.placeholder.com/50x50" />
                                @else
                                    <img class="rounded float-left mr-2 mb-1"  style="margin-top: -3px" height=30px  src="data:image/png;base64,{{$r->Photo}}"/>
                                @endif
                                <span>{{array_get($r->Resident, 'ResidentName')}}</span>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="row bg-white">
            <div class="col p-3">
                <ul class="list-group" id="steplists">
                    @if(!empty($steps))
                        @foreach($steps as $step)
                            <li class="list-group-item list-group-item-action flex-column align-items-start ">
                                <input type="hidden" name="DisplayOrder[]" value="{{$step->_id}}"/>
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1 process-step-title">{{$step->Title}}</h5>
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <p class="mb-1">{!! $step->Description !!}</p>
                                    </div>
                                    <div class="col-4">
                                        @if(!empty($step->Forms))
                                        <ul class="list-group" style="font-size: 15px;">
                                            <li class="list-group-item active mdb-color border-0 p-1 pl-3">Forms</li>
                                            @foreach($step->Forms as $form)
                                                @php
                                                    $fm = \App\Domains\AssessmentForm::find(array_get($form, 'FormId'));
                                                @endphp
                                                <li class="list-group-item p-1 pl-3">
                                                        <a href="#" onclick="openform('{{array_get($form, 'FormId')}}', '{{$fm->use_dummy_resident}}')" id="form{{array_get($form, 'FormId')}}">{{array_get($form, 'FormID')}} - {{array_get($form, 'FormName')}}</a>
                                                      <span class="cm-strikethrough invisible" id="created{{array_get($form, 'FormId')}}">{{array_get($form, 'FormName')}}</span>

                                                </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @else
                        <div class="alert alert-info">
                            <strong>No Steps found.</strong>
                        </div>
                    @endif

                </ul>
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script>

        $(document).ready(function(){

            @if(session('status'))
                    toastr.success('{{session('status')}}');
             @endif

            @if(isset($residentId))

                //var resObj = $('a#{{$residentId}}').html();

                //selectresident('{{$residentId}}', resObj)

            @endif
        })

        function selectresident(residentid, resObj){

            // console.log(resObj);
            $('.dropdown-menu a').removeClass(' active');

            $('input#selectedresident').val(residentid);
            $('a#'+residentid).addClass(' active');

            $('button#selectresident').html(resObj)

        }


        function openform(formid, use_dummy_resident){

            console.log('related='+use_dummy_resident);
            if(use_dummy_resident == "yes"){
                selectform("{{env('DUMMY_RESIDENT_ID')}}", formid);
                $('input#selectedresident').val("");
            } else {
                var selectedresident = $('input#selectedresident').val();
                console.log(">"+selectedresident+"<");
                if(selectedresident.length > 0){

                    selectform(selectedresident, formid);

                }else{

                    $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
                    alertify.alert('Please select a resident.', function(e){
                        if(e){
                            $('.modal-backdrop').remove();
                        }
                    });
                }
            }
        }

        function selectform(residentid, formid){

            var url = '{{url('/sprocess/selectform')}}/'+residentid+'/'+formid+'/{{$sprocess->_id}}';
            window.location.href = url;

            /*axios.get(url).then(function(response){

                $('#openFormModal #selectform').html(response.data);
            })*/
        }

    </script>
@endsection



