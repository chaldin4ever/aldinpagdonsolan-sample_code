
<html>
<head>
    <title>ECCA Email Notification</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#f2f2f2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (HTML EMAIL.psd) -->
<table id="Table_01" width="722" height="920" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td colspan="2" height="90px">
            <p align="right" style="text-decoration: underline; color:#163259; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif' "> <a href="#" style=" color:#163259 ">View in Browser</a></p>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="5px" bgcolor="#65839f"></td>
    </tr>
    <tr>
        <td height="135px" bgcolor="#ffffff" align="center">
            <img src="{{asset('images/email/ecca_logo.png')}}" alt="logo">
        </td>
        <td rowspan="4">
            <img src="{{asset('images/email/HTML-EMAIL_04.gif')}}" width="1" height="646" alt=""></td>
    </tr>
    <tr>
        <td height="210px" bgcolor="#FFFFFF">
            <p style="font-size: 36px; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'; padding-left: 50px;">Hello {{$provideruser->name}}</p>

            <p style="font-size: 14px; color: #3A4451; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'; padding-left: 50px; padding-right: 50px; line-height: 1.63em">
                {{$user->name}} ({{$user->email}}) is requesting to access {{$facility->NameLong}} Facility.
            </p>

        </td>
    </tr>
    <tr>
        <td bgcolor="ffffff" height="50px" align="center">
            <a href="{{url('messages')}}" style="
                    display: inline-block;
                    font-weight: 400;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: middle;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                    border: 1px solid transparent;
                    padding: 0.375rem 0.75rem;
                    font-size: 0.9rem;
                    line-height: 1.6;
                    border-radius: 0.25rem;
                    -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
                    background-color: #ff3547!important;
                    color: #fff!important;
                ">Click here to login.</a>
            {{--<a href="#"><img src="{{asset('images/email/button_active_now.png')}}" alt="button"></a>--}}
        </td>
    </tr>
    <tr>
        <td height="200" bgcolor="#FFFFFF"><p style="font-size: 14px; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'; padding-left: 50px; padding-right: 50px; line-height: 1.63em; color: #3A4451;">Thank you for using our application! </p>

            <p style="font-size: 14px; color: #3a4451; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'; padding-left: 50px; padding-right: 50px; line-height: 1.63em">Regards, <br />ECCA</p></td>
    </tr>
    <tr>
        <td colspan="2" height="80px" bgcolor="#f2f2f2" align="center">
            <a href="#"><img src="{{asset('images/email/Google.png')}}" alt="googleplus"></a> &nbsp;
            <a href="#"><img src="{{asset('images/email/Facebook.png')}}" alt="facebook"></a>&nbsp;&nbsp;
            <a href="#"><img src="{{asset('images/email/Twitter.png')}}" alt="twitter'"></a>&nbsp;&nbsp;

        </td>
    </tr>
    <tr>
        <td align="center"><p style=" color: #979faa; font-size: 14px; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'; padding-left: 50px; padding-right: 50px; line-height: 1.63em">
                <a style=" color: #979faa; text-decoration: none; " href="">Notification settings </a>  |
                <a style=" color: #979faa; text-decoration: none;" href="">Privacy Policy </a>  |
                <a style=" color: #979faa; text-decoration: none; " href="">Contact </a> </p>
            <p style="color: #979faa; font-size: 14px; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif'; padding-left: 50px; padding-right: 50px; line-height: 1.63em">Melbourne Australia © 2018 ECCA</p>
        </td>

    </tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>