@extends('layouts.app')

@section('style')
    <style>
        .dropzone {
            border-style:dotted;
            border-width: 3px;
            border-radius: 15px;
            background-color: #e0e0e0;
            color: #757575;
        }
    </style>
@endsection

@section('content')
    
<div class="d-flex flex-column justify-content-center">
    <div class="p-2" style="width:300px;">
                    <form method="post" action="{{url('/meltingpin/store')}}" 
                        enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                        {{ csrf_field() }}
                    </form>
    </div>
    <div class="p-2">
        <label class="label pull-left mr-2">Pin</label>
        <div class="control">
            <input class="input" type="number" id="pin"/>
            <input type="text" id="download_link" style="display:none; position: relative; left: -100000px;" 
                value=""/>
        </div>
    </div>
    <div class="p-2">
        <div id="show_download_link" class="show_download_link">
        </div>
    </div>
    <div class="p-2">
        <button type="button" class="btn btn-info btn-rounded btn-sm" onclick="reset()">Reset</button>
    </div>
</div>
              




@endsection


@section('script')
<script>

    function show_download_link(name){
        var pin = $("#pin").val();
        if(pin != ""){
            // var url = window.location.href +"/download/" + pin;
            var url = "{{url('')}}/meltingpin/download/" + pin;
            // console.log(url);
            var html = "Click on <i class='fa fa-copy'></i> icon to copy the link to download or just provides the pin (expires in 30 minutes)<br/>";
            if(name === "")
                html = html + "<a  is-warning href='" + url + "'>Download</a>";
            else
                html = html + "<a  href='" + url + "'>Download "+name + "</a>";
            
            html = html + "&nbsp;&nbsp;&nbsp;<a href='#' onclick='copyDownloadLink()'><i class='fa fa-copy'></i><a/>";
            $("#show_download_link").html(html);
            $('#download_link').val(url);
        }
    }

    $("#pin").on("keypress", function(e){
        if(e.which === 13){
            var pin = $("#pin").val();
            show_download_link(pin);
        }
    });

    Dropzone.autoDiscover = false;
    $(function() {
        // Now that the DOM is fully loaded, create the dropzone, and setup the
        // event listeners
        var myDropzone = new Dropzone("#my-dropzone");
        myDropzone.on("addedfile", function(file) {
            //console.log(file);
        });

        myDropzone.on('complete', function(file){
            myDropzone.removeFile(file);
        });

        myDropzone.on('success', function(file, response){
            console.log(file); 

            $("#pin").val(response);
            show_download_link(file.name);
            $("#show_download_link").show();
        });

        myDropzone.on('drop', function(file){
            reset();
        });

    })


    function copyDownloadLink(){
        var copyDiv = document.getElementById("download_link");
        copyDiv.style.display = 'block';
        copyDiv.focus();
        document.execCommand('SelectAll');
        document.execCommand("Copy", false, null);
        copyDiv.style.display = 'none';
        // var t = $("#download_link").val();
        toastr.info('Download link copied');
    }

    function reset(){
        $("#pin").val("");
        $("#show_download_link").hide();
    }
</script>
@endsection