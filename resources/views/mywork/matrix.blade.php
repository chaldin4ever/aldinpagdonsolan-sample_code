@extends('layouts.app')

@section('content')

<div class="d-flex">
    <h3 class="mr-auto">All Team Members</h3>
    <div>
        <a href="{{url('mywork')}}">My Tickets</a>
    </div>
</div>

<p  style="font-size:0.8rem">
    Click on name or count to view tickets assigned to the particular team member
</p>

<table class="table table-striped table-hover">
@foreach($members as $m)
    @php
        $email = array_get($m, 'email');
        $row = array_get($result, $email);
        $count = array_get($row, 'count');
        if($count <= 0)
            continue;
    @endphp
    <tr>
        <td><a href="{{url('mywork?email='.urlencode($email))}}"><i class="fa fa-search"></i> {{array_get($m, 'name')}}</a></td>
        <td><a href="{{url('mywork?email='.urlencode($email))}}">{{$count}}</a></td>
    </tr>
@endforeach
</table>

@endsection