@extends('layouts.app')

@section('content')

<div class="d-flex">
    <h3 class="mr-auto"><i class="fa fa-asterisk"></i> My Focus</h3>
    <div class="text-right">
        <a href="{{url('mywork')}}">My Tickets</a>
    </div>
</div>

@if(sizeof($tickets) == 0)
        <p>No tickets found</p>
    @endif
    
   
    <div id="sortable">
        @foreach($tickets as $i)
        @php
            // skip all parent tasks if it is set
            //if(isset($i->IsParent) && isset($i->SeeAlso) && $i->IsParent)
            //    continue;

            // make sure the user has the right permission to this project
            $project = array_get($projects, array_get($i->Project, 'id'));
            if(empty($project)) 
                continue;
        @endphp
        <div class="border rounded p-2 mb-2 dragit z-depth-1 ticket_row" id="{{$i->_id}}">
            <div class="row">
                <div class="col-1">
                    <a href="{{url('/ticket/'.$i->_id)}}" class="badge badge-pill teal  ticket-number" >{{$i->Ticket}}</a>
                </div>
                <div class="col-8">
                    <a href="{{url('/ticket/'.$i->_id)}}" class="ticket-title">
                        @if(empty($project->TitleFields))
                            {{$i->Title}}
                        @else
                            {{\App\Utils\Toolkit::GetCustomTitle($project, $i)}}
                        @endif
                    </a>
                    
                </div>
                
                <div class="col-2">
                    @if(!empty($i->SeeAlso))
                    <span style="font-weight:100;font-size:0.75rem;"><i class="fa fa-external-link"></i> 
                    <a href="{{url('/ticket/'.$i->Ticket)}}">{{array_get($i->SeeAlso, 'Code')}}</a></span>
                    @endif
                    
                </div>
                <div class="col-1">
                    <span class="pull-right" style="font-size:1rem;color:coral">
                        <a onclick="unflag_it('{{$i->_id}}')"><i class="fa fa-remove"></i></a>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    <span class="badge badge-pill primary-color"  style="font-weight:100">{{$i->State}}</span>
                </div>
                <div class="col-4">
                    
                    @if(!empty($i->Tags))
                            @foreach(explode(',', $i->Tags) as $tag)
                                <a href="#" class="badge badge-pill blue" 
                                style="font-weight:100;font-size:0.75rem;">{{$tag}}</a>
                            @endforeach
                    @endif
                </div>
                <div class="col-3">
                    <span style="color:#ccc;font-size:0.85rem">
                       <a href="{{url('project/'.$project->_id)}}">{{$project->Label}}</a>
                    </span>
                </div>
                <div class="col-2">
                    @if(!empty($i->AssignTo))
                        <div style="font-size:0.75rem">{{array_get($i->AssignTo, 'name')}}</div>
                    @endif
                </div>
                <div class="col-2">
                    <span style="font-size:0.70em">Created: {{\App\Utils\Toolkit::CarbonToDateString($i->created_at)}}</span>
                    
                </div>
            </div>
        </div>
        @endforeach
    </div>



@endsection


@section('script')
<script>
    $(function () {
        $( "#sortable" ).sortable({
            update(event, ui){
                var n = 10;
                $(this).children().each(function(){
                    var ticket_id = $(this).attr('id');
                    var url = "{{url('mywork/set_flag_priority')}}";
                    axios.post(url,{
                        ticket_id: ticket_id,
                        priority: n
                    }).then(function(response){
                        console.log(response.data);
                    });
                    n += 10;
                });

            }
        });

        $(".dragit").draggable(
            { 
                containment: "parent",
                connectToSortable: "#sortable", 
                cursor: "move"
            }
        );

    });


    function unflag_it(ticket_id){
            
            var url = "{{url('mywork/unflag_ticket')}}";
            axios.post(url,{
                ticket_id: ticket_id
            }).then(function(response){
                $("#"+ticket_id).hide();
                toastr.info('Ticket unflagged');
            });
        }

</script>
@endsection