

<!-- Modal -->
<div class="modal fade" id="edit_field_header" tabindex="-1" role="dialog" aria-labelledby="edit_list_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post"   onsubmit="return false;">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_new_list_label">Edit Field Name Header</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class=" md-form form-group">
                        <input type="hidden" id="key" name="key">
                        <input type="hidden" id="list_id" name="list_id">
                        <input type="hidden" id="field_id" name="field_id">
                        <input type="text" id="field_name" name="field_name" class="form-control white" placeholder=" "
                            onkeydown="if(this.keyCode === 13){updateFieldHeader()} "
                        >
                        <label for="list_name">Field Name Header</label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="updateFieldHeader()">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>