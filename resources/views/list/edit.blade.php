@extends('layouts.poc')

@section('style')


@endsection

@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-cog"></i>
                        @if(!empty($selected_list))
                            {{$selected_list->ListName}}
                        @endif
                    </strong></h3>
                <a class="float-right btn btn-blue-grey btn-rounded waves-effect" href="{{url('lists')}}">{{__('Return to Lists')}}</a>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white  p-4">
            <div class="col-xl-5">
                @include('list.select_form')
            </div>
            <div class="col-xl-7">
                @if(!empty($selected_list))
                    {{--<h4>{{$selected_list->ListName}}</h4>--}}
                    <div class="details">@include('list.list_details')</div>
                @endif

            </div>
        </div>
    </div>
    </div>


@include('list.edit_field_header')
@endsection


@section('script')
    <script>
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });

        function openForm(){
            $selected_form = $('#select_form').val();
            var url = "{{url('lists/edit')}}?id=" + ($selected_form);
            @if(!empty($selected_list))
                url = url + "&list_id={{$selected_list->_id}}" 
            @endif
            window.location = url;
        }

        $( function() {
            $( "#list_fields" ).sortable();
            $( "#list_fields" ).disableSelection();
        } );

        function editFieldHeader( listId, uid, key){


            var url = '{{url('lists/fieldtext/')}}/'+listId+'/'+key;

            axios.get(url).then(function(response){

                var data = response.data;

                $('#edit_field_header input#key').val(key);
                $('#edit_field_header input#field_name').val(data.text);
                $('#edit_field_header input#list_id').val(listId);
                $('#edit_field_header input#field_id').val(uid);

            })

        }

        function updateFieldHeader(){

            var url='{{url('lists/edit_field_header')}}';

            var key = $('#edit_field_header input#key').val();
            var uid = $('#edit_field_header input#field_id').val();
            var fieldtext = $('#edit_field_header input#field_name').val();
            var list_id = $('#edit_field_header input#list_id').val();

            axios.post(url,{
                key: key,
                field_name: fieldtext,
                list_id: list_id,
                field_id: uid
            }).then(function(response){

                var data = response.data;
                // alert(fieldtext);
                $('ul#list_fields li#'+uid+' p').text(fieldtext);
                toastr.success('Successfully saved.');
            })

        }

        function deleteFieldHeader(listid, key, fieldname, uid){



            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            alertify.confirm('Are you sure you want to delete this item ('+fieldname+')?', function(e) {
                if (e) {

                    var url='{{url('lists/delete_field')}}/'+listid+'/'+key;

                    axios.get(url).then(function(response){

                        $('ul#list_fields li#'+uid).fadeOut("slow",function(){
                            $('ul#list_fields li#'+uid).remove();
                        })

                        toastr.success('Successfully saved.');

                        $('.modal-backdrop').remove();
                    })


                }else{
                    $('.modal-backdrop').remove();
                }
            });

        }
    </script>

@endsection
