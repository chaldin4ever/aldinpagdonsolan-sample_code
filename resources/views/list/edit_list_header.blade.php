

<!-- Modal -->
<div class="modal fade" id="edit_list_header" tabindex="-1" role="dialog" aria-labelledby="edit_list_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{url('lists/edit_header')}}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_new_list_label">Edit List Header</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class=" md-form form-group">
                        <input type="hidden" id="list_id" name="list_id" class="form-control white">
                        <input type="text" id="list_name" name="list_name" class="form-control white" placeholder=" ">
                        <label for="list_name">List Name Header</label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>