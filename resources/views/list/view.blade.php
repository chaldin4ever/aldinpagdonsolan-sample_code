@extends('layouts.poc')

@section('style')


@endsection


@section('content')

@php
    $fields = $list->Fields;
    if(empty($fields)) $fields = [];
@endphp


<div class="container-fluid mt-1">
    <div class="row  pl-4 pb-2 pt-4 " >
        <div class="col pl-0">
            <h3 class="p-2 float-left"><strong><i class="fa fa-list-alt"></i> {{$list->ListName}}</strong></h3>
            <a class="  float-right btn btn-blue-grey btn-rounded waves-effect waves-light" href="{{url('lists')}}">Return to Lists</a>
        </div>


    </div>
</div>

<div class="container-fluid">

    <div class="row bg-white">
        <div class="col pt-3">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Room</th>
                    <th>Resident</th>
                    @foreach($fields as $field)
                        <th>
                            {{array_get($field, 'text')}}
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($rows as $row)
                    @php
                        $resident = $row->Resident;
                        $data = $row->data;
                    @endphp
                    <tr>
                        <td>{{array_get($resident, 'Room')}}</td>
                        <td>{{array_get($resident, 'ResidentName')}}</td>
                        @foreach($fields as $field)
                            <td>
                                {{array_get($data, array_get($field, 'uid'))}}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>


@endsection


@section('script')
@endsection
