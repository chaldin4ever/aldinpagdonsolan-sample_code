

<!-- Modal -->
<div class="modal fade" id="add_new_list" tabindex="-1" role="dialog" aria-labelledby="add_new_list_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post" action="{{url('lists/store_list')}}">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_new_list_label">New List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class=" md-form form-group">
                        <input type="text" id="list_name" name="list_name" class="form-control white">
                        <label for="list_name">List Name</label>
                    </div>
                    <div class="md-form form-group">
                        <select class="mdb-select" id="source" name="source">
                            <option value="Assessment">Completed assessment</option>
                        </select>
                        <label for="source">Source</label>
                    </div>
                    <div class="md-form form-group">
                        <select class="mdb-select" id="destination"  name="destination">
                            <option value="ResidentList">Resident List</option>
                        </select>
                        <label for="destination">Destination</label>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submit()">Save changes</button>
            </div>
        </div>
    </form>
  </div>
</div>