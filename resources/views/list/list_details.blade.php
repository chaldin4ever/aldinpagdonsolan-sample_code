@php
    $fields = $selected_list->Fields;
@endphp
@if(!empty($fields))
<ul class="list-group" id="list_fields">
    @foreach($fields as $k=>$field)
    <li class="list-group-item" id="{{array_get($field, 'uid')}}">
        <p id="txt" class="m-0 p-0 float-left">{{array_get($field, 'text')}}</p>
        <a href="#" onclick="deleteFieldHeader('{{$selected_list->_id}}','{{$k}}', '{{array_get($field, 'text')}}', '{{array_get($field, 'uid')}}')" class="pull-right ml-2"><i class="fa fa-times"></i></a>
        <a href="#" onclick="editFieldHeader('{{$selected_list->_id}}', '{{array_get($field, 'uid')}}', '{{$k}}')"
           data-toggle="modal" data-target="#edit_field_header" class="pull-right ml-2"><i class="fa fa-edit"></i></a>
        <span class="badge black pull-right">{{array_get($field, 'form_name')}}</span>
    </li>
    @endforeach
</ul>

<a class="btn btn-primary" href="{{url('lists/create_logics/'.$selected_list->_id)}}">Create Logics</a>

@endif