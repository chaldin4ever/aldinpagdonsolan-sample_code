@extends('layouts.poc')

@section('style')


@endsection


@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{__('Lists')}}</strong></h3>
                <button type="button" class=" float-right btn btn-blue-grey btn-rounded pull-right" data-toggle="modal" data-target="#add_new_list">
                   <i class="fa fa-plus"></i> {{__('Add new List')}}
                </button>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white pb-4">
            <div class="col pt-3">

                @if(sizeof($lists) == 0)
                    <h4>No list configured</h4>
                @else
                <ul class="list-group  ">
                    @foreach($lists as $list)
                        <li class="list-group-item"><a href="{{url('lists/view/'.$list->_id)}}"><i class="fa fa-list-alt"></i> {{$list->ListName}}</a>
                            <a href="{{url('lists/edit?list_id='.$list->_id)}}" class="ml-2 pull-right"><i class="fa fa-cog"></i></a>
                            <a href="{{url('lists/edit_header')}}" onclick="editListHeader('{{$list->_id}}', '{{$list->ListName}}')"
                               data-toggle="modal" data-target="#edit_list_header"
                               class="pull-right"><i class="fa fa-edit"></i></a>
                        </li>
                    @endforeach

                </ul>
                @endif
            </div>
        </div>
    </div>
    </div>




@include('list.new_list_modal')
@include('list.edit_list_header')

@endsection


@section('script')
    <script>
        $(document).ready(function() {
            $('.mdb-select').material_select();

            @if(session('status'))
                toastr.success('{{session('status')}}');
            @endif
        });

        function openForm(){
            $selected_form = $('#select_form').val();
            var url = "{{url('lists')}}?id=" + ($selected_form);
            window.location = url;
        }

        function editListHeader(id, name){

            $('#edit_list_header input#list_id').val(id);
            $('#edit_list_header input#list_name').val(name);
        }
    </script>

@endsection
