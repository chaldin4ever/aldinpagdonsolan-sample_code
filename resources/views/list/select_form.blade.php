
<div class="row">
    <div class="col-xl-12">
        <select class="mdb-select" id="select_form" onchange="openForm()" searchable="Search form..">
            <option value="" disabled selected>Select a form</option>
            @if(!empty($forms))
                @foreach($forms as $form)
                @php
                    $selected = '';
                    if(!empty($selected_form) && ($form->_id == $selected_form->_id)) $selected = 'selected';
                @endphp
                <option value="{{$form->_id}}" {{$selected}}>{{$form->FormName}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="row">
    <div class="col-xl-12">
        @if(!empty($selected_form))
        <form method="post" action="{{url('lists/store_fields')}}">
            {{ csrf_field() }}
            @php
                //$questions = $selected_form->template_json;
            @endphp
            @if(!empty($questions))
                <select class="mdb-select colorful-select dropdown-primary" 
                    name="fields[]"
                    style="height:300px" multiple searchable="Search here..">
                    <option value="" disabled selected>Select field(s)</option>
                        @foreach($questions as $q)
                        {{--<option value="{{array_get($q, 'code')}}">{{array_get(array_get($q, 'question'), 'text')}}</option>--}}
                            @if($q->Type != 'message')
                                <option value="{{$q->Code}}">{{$q->Question}}</option>
                            @endif
                        @endforeach
                </select>
             @else
                <div class="alert alert-info">No available list.</div>
            @endif
                <input type="hidden" name="mode" value="{{$mode}}"/>
                @if(!empty($selected_list))
                <input type="hidden" name="list_id" value="{{$selected_list->_id}}"/>
                @endif
                <input type="hidden" name="id" value="{{$selected_form->_id}}"/>
                <button class="btn-save btn btn-primary btn-sm" type="submit">Submit</button>
             </form>
        @endif
    </div>
</div>
