
<div class="container">
    <!-- Section: Pricing v.5 -->
    <section class="text-center my-5">

        <!-- Grid row -->
        <div class="row">
            @include("welcome.feature-process")
            @include("welcome.feature-form-editor")

        </div>
    </section>

    <footer class="footer">
        <div class="company-copyright">
            &copy; 2018 {{env("COMPANY_NAME")}}
        </div>
        <div class="build-number">
            Build {{env('BUILD_NUMBER')}}
        </div>
    </footer>
</div>