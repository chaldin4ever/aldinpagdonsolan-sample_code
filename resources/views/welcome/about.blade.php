@extends('layouts.front_alt')

@section('content')

<br/>

  <h2 class="h1-responsive font-weight-bold text-center my-5">About Us</h2>


<div class="row subpage-content mb-4">
    <div class="col-12">
        <p>
            OI DESK came from an idea 
        </p>    
        <div style="font-size:1.75em;font-family:Times" class="ml-4">
            <i>&quot;What if we can have a streamline helpdesk service for clients'
                <span style="color:#fff;font-size:1.5em">clients</span>?&quot;</i>
        </div>
    </div>
</div>

<br/>
<div class="d-flex flex-column subpage-content ">
    <div class="p-2">
        <p>
            Most online helpdesk software are based on the following assumptions:
        </p>    
    </div>
    <div class="p-2">
        <ol>
            <li>A company provides helpdesk service directly to a client.</li>
            <li>Helpdesk software is to help a company to achieve its servcie level agreement (SLA) with the client.</li>
        </ol>
    </div>
</div>

<div class="d-flex flex-column subpage-content ">
    <div class="p-2">
        <p>
            OI DESK addresses these assumptions by providing an online helpdesk software that is
            <ol>
                <li>Managed by configurable workflow. So each project can have their own workflow.</li>
                <li>Easy to integrate so end users can easily lodge a ticket within their existing applications. No additional software to learn or login.</li>
                <li>Working with the client as a team to resolve tickets instead of focusing on SLA.</li>
            </ol>
        </p>    
    </div>
</div>
@endsection