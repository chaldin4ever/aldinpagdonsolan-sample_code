<!-- Full Page Intro -->
<div class="view  jarallax" data-jarallax='{"speed": 0.2}' style="">
    <!-- Mask & flexbox options-->
    <div class="mask rgba-purple-slight d-flex justify-content-center align-items-center">
        <!-- Content -->
        <div class="container">
            <!--Grid row-->
            <div class="row wow fadeIn">
                <!--Grid column-->
                <div class="col-md-12 text-center">
                    <h1 class="display-4 font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInUp">#1 Cloud Software for <br/>Care &amp; Clinical Documentation</h1>
                    <h5 class="pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 wow fadeInUp" data-wow-delay="0.2s">
                        ECCA comes with features that you would expect in a care & clinical software plus a lot more.
                        <br/>Check it out now!
                    </h5>
                    <div class="wow fadeInUp" data-wow-delay="0.4s">
                        <a class="btn btn-purple btn-rounded" href="{{url('welcome/pricing')}}"><i class="fa fa-user left"></i> Try it free</a>
                        <a class="btn btn-outline-purple btn-rounded" href="{{url('welcome/features')}}"><i class="fa fa-book left"></i> Learn more</a>
                    </div>
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
        </div>
        <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
</div>
<!-- Full Page Intro -->