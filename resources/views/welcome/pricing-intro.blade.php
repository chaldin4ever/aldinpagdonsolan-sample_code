
<br/>

<div class="container">
    <!-- Section: Pricing v.5 -->
    <section class="text-center my-5">

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5">Our pricing plans</h2>
        <!-- Section description -->
        <p class="text-center w-responsive mx-auto mb-5">
            Our pricing is based on number of licensed beds. The minimum licences can
            be purchased is 10 for a low monthly fee of AUD $5 per licence</p>

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-lg-4 col-md-12 mb-lg-0 mb-4">

                <!-- Card -->
                <div class="pricing-card card">

                    <!-- Content -->
                    <div class="card-body">
                        <h5 class="font-weight-bold mt-3">Startup</h5>

                        <!-- Price -->
                        <div class="price pt-0">
                            <h2 class="number red-text mb-0">7</h2>
                        </div>

                        <ul class="striped mb-1">
                            <li>
                                <p><strong></strong> per licence</p>
                            </li>
                            <li>
                                <p><strong>1 - 10</strong> licences (beds)</p>
                            </li>
                            <li>
                                <p><strong>Unlimited</strong> staff</p>
                            </li>
                            <li>
                                <p><strong>Minimum licences</strong> 10</p>
                            </li>
                        </ul>
                        <a class="btn btn-danger btn-rounded mb-4"> Buy now</a>

                    </div>
                    <!-- Content -->

                </div>
                <!-- Card -->

            </div>
            <!-- Grid column -->

            <!--  Grid column  -->
            <div class="col-lg-4 col-md-6 mb-md-0 mb-4">

                <!-- Card -->
                <div class="card card-image" style="background-image: url(images/_coffee.jpg);">

                    <!-- Pricing card -->
                    <div class="text-white text-center pricing-card d-flex align-items-center rgba-indigo-strong py-3 px-3 rounded">

                        <!-- Content -->
                        <div class="card-body">
                            <h5 class="font-weight-bold mt-3">Business</h5>

                            <!-- Price -->
                            <div class="price pt-0">
                                <h2 class="number red-text mb-0">5</h2>
                            </div>

                            <ul class="striped mb-1">
                                <li>
                                    <p><strong></strong> per licence</p>
                                </li>
                                <li>
                                    <p><strong>11 - 200 </strong> licences (beds)</p>
                                </li>
                                <li>
                                    <p><strong>Unlimited</strong> staff</p>
                                </li>
                                <li>
                                    <p><strong>Minimum licences</strong> 20</p>
                                </li>
                            </ul>
                            <a class="btn btn-danger btn-rounded mb-4"> Buy now</a>

                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Pricing card -->

                </div>
                <!-- Card -->
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-lg-4 col-md-6">

                <!-- Card -->
                <div class="pricing-card card">

                    <!-- Content -->
                    <div class="card-body">
                        <h5 class="font-weight-bold mt-3">Enterprise</h5>

                        <!-- Price -->
                        <div class="price pt-0">
                            <h2 class="number red-text mb-0">4</h2>
                        </div>

                        <ul class="striped mb-1">
                            <li>
                                <p><strong></strong> per licence</p>
                            </li>
                            <li>
                                <p><strong>200+ </strong> licences (beds)</p>
                            </li>
                            <li>
                                <p><strong>Unlimited</strong> staff</p>
                            </li>
                            <li>
                                <p><strong>Minimum licences</strong> 100</p>
                            </li>
                        </ul>
                        <a class="btn btn-danger btn-rounded mb-4"> Call Us</a>

                    </div>
                    <!-- Content -->

                </div>
                <!-- Card -->

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </section>
    <!-- Section: Pricing v.5 -->
            
</div>