@push('styles')
<link href="{{ asset('css/features.css') }}" rel="stylesheet">
<link href="{{ asset('css/pricing.css') }}" rel="stylesheet">
@endpush

@extends('layouts.main')

@section('content')
<!--header section-->
<section id="header-section" class="features-header">
    @include('layouts.menu')
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 mt-5 mt-md-0">
                    <div class="header-right-content greyish-blue">
                        <h1 class="mb-2">Subscribe to ECCA</h1>
                        <p>All of our customers are able to enrol in our free trial prior to subscribing to a paid ECCA Plan. If the ECCA platform seamlessly fits into your business workflows you can choose to upgrade to a paid subscription.</p>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-md-block text-center">
                    <img src="/images/group-311.png" width="100%" height="100%"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end of header section-->

<!-- pricing content -->
<section id="features-content" class="p-0">
    <div class="container">
        <div class="row mt-5 pt-5 mb-5 pb-5">
            <div class="col-12 text-center">
                <h2 class="mb-4 p-title">How It Works</h2>
                <p>Businesses have the option to sign up as either a Small, Medium or Large business depending on the number beds in their facility. Once you select a plan that suits your needs, you’ll be granted immediate access to the ECCA Platform as well as being provided with any training and assistance that you might need when setting up our services.</p>
                <p>You will be billed automatically at the end of every month for your ECCA subscription until you cancel.</p>
            </div>
        </div>
        
        <div class="row mt-5 pt-5">
            <div class="col-12 text-center">
                <h2 class="mb-4 p-title">Pricing Tiers</h2>
                <p>All subscriptions to ECCA feature unlimited staff connections so all of your staff will have access to resident or patient data.</p>
            </div>
        </div>
        <div class="row mt-5 mb-5 pb-5">
            <div class="col-lg-3 p-lg-0">
                <div class="card rounded-0">
                    <div class="card-body text-center">
                        <h6 class="mt-3">&nbsp;</h6>
                        <p class="price-type">Trial</p>
                        <p><span class="currency">$</span> <span class="price-value">49</span>/mo</p>
                        <p class="card-text price-description">The ECCA trial features access to all of our services for up to one month.</p>
                        <a class="btn price-btn" href="">SIGN UP TODAY</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 p-lg-0 mt-5 mt-lg-0">
                <div class="card rounded-0">
                    <div class="card-body text-center">
                        <h6 class="mt-3">&nbsp;</h6>
                        <p class="price-type">Small</p>
                        <p><span class="currency">$</span> <span class="price-value">99</span>/mo</p>
                        <p class="card-text price-description">Recommended for smaller facilities or start-up businesses with up to 10 beds.</p>
                        <a class="btn price-btn" href="">SIGN UP TODAY</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 p-lg-0 mt-5 mt-lg-0">
                <div class="card rounded-0 recommended-card">
                    <div class="card-body text-center">
                        <h6 class="mt-3 mb-4"><span class="badge badge-recommended text-white">RECOMMENDED</span></h6>
                        <p class="price-type">Medium</p>
                        <p><span class="currency">$</span> <span class="price-value">219</span>/mo</p>
                        <p class="card-text price-description">Aimed toward larger facilities with at least 20 beds and up to 200 beds.</p>
                        <a class="btn btn-danger" href="">SIGN UP TODAY</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 p-lg-0 mt-5 mt-lg-0">
                <div class="card rounded-0">
                    <div class="card-body text-center">
                    <h6 class="mt-3">&nbsp;</h6>
                    <p class="price-type">Large</p>
                    <p><span class="currency">$</span> <span class="price-value">419</span>/mo</p>
                    <p class="card-text price-description">Designed to handle major customers the large tier is developed to suit over 200 beds.</p>
                    <a class="btn price-btn" href="">SIGN UP TODAY</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5 pt-5">
            <div class="col-12 text-center">
                <h2 class="mb-4 p-title">Let’s Get Started</h2>
                <p>Get a better insight into your day-to-day operations and bring your staff, residents and patients closer together with an ECCA monthly subscription.</p>
                <a class="btn btn-danger" href="">TRY IT NOW</a>
            </div>
        </div>
    </div>
</section>
<!-- end of pricing content -->
@endsection