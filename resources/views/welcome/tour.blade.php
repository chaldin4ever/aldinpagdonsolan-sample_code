@extends('layouts.front_alt')

@section('content')

<br/>

<!-- Section: Features v.2 -->
<section class="my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5">Features</h2>
  <!-- Section description -->
  <p class="lead white-text text-center w-responsive mx-auto mb-5">
      </p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-4 mb-md-0 mb-5">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-2 col-md-3 col-2">
          <i class="fa fa-bullhorn white-text fa-2x"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-10 col-md-9 col-10">
          <h4 class="font-weight-bold">Projects</h4>
          <p class="white-text">Project &amp; Team is the foundation of OI DESK.  
              Each ticket must belong to a project. 
              A project can have a default form or custom form that are managed by a workflow.
            </p>
          <a class="btn btn-dark-green btn-sm">Learn more</a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4 mb-md-0 mb-5">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-2 col-md-3 col-2">
          <i class="fa fa-cogs pink-text fa-2x"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-10 col-md-9 col-10">
          <h4 class="font-weight-bold">Team Chat</h4>
          <p class="white-text">Team members across multiple projects can communicate to each other via Chat. 
            Every plan allows for unlimited messages.</p>
          <a class="btn btn-indigo btn-sm">Learn more</a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-2 col-md-3 col-2">
          <i class="fa fa-dashboard purple-text fa-2x"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-10 col-md-9 col-10">
          <h4 class="font-weight-bold">Workflow</h4>
          <p class="white-text">Tickets are managed by workflow so user only needs to focus on tickets on hand. 
            Relevant users are only notified by email when tickets arrive requiring their attention.</p>
          <a class="btn btn-mdb-color btn-sm">Learn more</a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->




<br/>
<br/>
<br/>





  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-4 mb-md-0 mb-5">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-2 col-md-3 col-2">
          A<i class="fa fa-address-book fa-2x"></i>B
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-10 col-md-9 col-10">
          <h4 class="font-weight-bold">Forms</h4>
          <p class="white-text">A project can use a default form or customise form to suite prodjct's needs. 
            Custom forms use a simple marku language that end user with basic training will be able to create custom forms. </p>
          <a class="btn btn-dark-green btn-sm">Learn more</a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4 mb-md-0 mb-5">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-2 col-md-3 col-2">
          <i class="fa fa-cogs pink-text fa-2x"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-10 col-md-9 col-10">
          <h4 class="font-weight-bold">Calendar</h4>
          <p class="white-text">Tickets with due dates can be dislayed in a calendar format so it is easier to see 
            overall workload to the team. Calendar will be particular useful for time sensitive tasks, 
          such as software installation. </p>
          <a class="btn btn-indigo btn-sm">Learn more</a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-2 col-md-3 col-2">
          <i class="fa fa-dashboard purple-text fa-2x"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-10 col-md-9 col-10">
          <h4 class="font-weight-bold">Easy Integration</h4>
          <p class="white-text">Any web application can integrate with OI DESK using a simple URL link.  
            No coding is required.  A company can easily add OI DESK form to their existing inhouse application for 
          staff to lodge tickets.</p>
          <a class="btn btn-mdb-color btn-sm">Learn more</a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->




</section>
<!-- Section: Features v.2 -->
            

@endsection