<!-- Section: Features v.3 -->
<section class="my-5">

    <!-- Section heading -->
    <h2 class="h1-responsive font-weight-bold text-center my-5">Staff training included</h2>
    <!-- Section description -->
    <p class="lead grey-text text-center w-responsive mx-auto mb-5">
        ECCA is partnering with <a href="https://www.medehealth.com.au" target="_blank" style="color:#58AD2F">MedeHealth</a>
        to provide free online training courses for your staff.
    </p>

    <!-- Grid row -->
    <div class="row">

        <!-- Grid column -->
        <div class="col-lg-5 text-center text-lg-left">
            <img  class="img-fluid" src="https://www.medehealth.com.au/Content/static-pages/medehealth.com.au/wp-content/uploads/2016/06/Hero-Image-v3.png" alt="Sample image">
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-7">

            <!-- Grid row -->
            <div class="row mb-3">

                <!-- Grid column -->
                <div class="col-1">
                    <i class="fa fa-mail-forward fa-lg indigo-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-xl-10 col-md-11 col-10">
                    <h5 class="font-weight-bold mb-3">Competent Staff</h5>
                    <p class="grey-text">All staff will have access to online training to ensure they are competent in using ECCA</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mb-3">

                <!-- Grid column -->
                <div class="col-1">
                    <i class="fa fa-mail-forward fa-lg indigo-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-xl-10 col-md-11 col-10">
                    <h5 class="font-weight-bold mb-3">Access Everywhere</h5>
                    <p class="grey-text">Online training can be accessed via Wed or mobile phone.</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->



        </div>
        <!--Grid column-->

    </div>
    <!-- Grid row -->

</section>
<!-- Section: Features v.3 -->