<!-- Section: Features v.1 -->
<section class="text-center my-5">

    <!-- Section heading -->
    <h2 class="h1-responsive font-weight-bold my-5">You are in total control</h2>
    <!-- Section description -->
    <p class="lead grey-text w-responsive mx-auto mb-5">ECCA provides three core technologies that allow an organisation
        to create organisation-wide standarised processes and forms.</p>

    <!-- Grid row -->
    <div class="row">

        <!-- Grid column -->
        <div class="col-md-4">

            <i class="fa fa-pencil-square-o fa-3x red-text"></i>
            <h5 class="font-weight-bold my-4">Forms</h5>
            <p class="grey-text mb-md-0 mb-5">ECCA comes almost 100+ ready-to-use forms &amp; assessment tools.
                It also has a visual form editor allowing you to customise existing forms and build new ones.
            </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4">

            <i class="fa fa-code-fork fa-3x cyan-text"></i>
            <h5 class="font-weight-bold my-4">Logics</h5>
            <p class="grey-text mb-md-0 mb-5">Using Logics in ECCA, you can decide how to handle or process
                data collected from Forms.
            </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4">

            <i class="fa fa-cogs fa-3x orange-text"></i>
            <h5 class="font-weight-bold my-4">Processes</h5>
            <p class="grey-text mb-0">You can use 10+ standarised processes come with ECCA or create your own processes
                that are tailored for your organisation.
            </p>

        </div>
        <!-- Grid column -->

    </div>
    <!-- Grid row -->

</section>
<!-- Section: Features v.1 -->