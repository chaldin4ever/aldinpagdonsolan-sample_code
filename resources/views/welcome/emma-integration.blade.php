<!-- Section: Features v.4 -->
<section class="my-5">

    <!-- Section heading -->
    <h2 class="h1-responsive font-weight-bold text-center my-5">Medication Management Made Easy</h2>
    <!-- Section description -->
    <p class="lead grey-text text-center w-responsive mx-auto mb-5">
        ECCA is integrated with Compact's <a href="http://www.compactcare.com.au/" target="_blank" style="color:#FF002B">Electronic Medication Management Administration (EMMA)</a>.
        You can choose to implement medication management from day one or at a later day.
        The choice is yours.
    </p>

    <!-- Grid row -->
    <div class="row">

        <!-- Grid column -->
        <div class="col-md-4">

            <!-- Grid row -->
            <div class="row mb-3">

                <!-- Grid column -->
                <div class="col-2">
                    <i class="fa fa-2x fa-flag-checkered deep-purple-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-10">
                    <h5 class="font-weight-bold mb-3">Eliminate Missed
                        Signatures</h5>
                    <p class="grey-text">EMMA ensures staff must sign for every medication administered.
                        Missing signature issue is a thing of the past from day one.
                    </p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mb-3">

                <!-- Grid column -->
                <div class="col-2">
                    <i class="fa fa-2x fa-flask deep-purple-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-10">
                    <h5 class="font-weight-bold mb-3">Prompts, Alerts &amp;
                        Indicators</h5>
                    <p class="grey-text">EMMA is full of prompt and alerts to ensure staff remember to take INR, BGL, etc.</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mb-md-0 mb-3">

                <!-- Grid column -->
                <div class="col-2">
                    <i class="fa fa-2x fa-glass deep-purple-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-10">
                    <h5 class="font-weight-bold mb-3">Extensive up to the Minute Reporting</h5>
                    <p class="grey-text mb-md-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4 text-center">
            <img class="img-fluid" src="https://storage.googleapis.com/wzukusers/user-27211533/images/58c1ffed12051VT0yQSq/Home-Screen_d400.PNG" alt="Sample image">
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4">

            <!-- Grid row -->
            <div class="row mb-3">

                <!-- Grid column -->
                <div class="col-2">
                    <i class="fa fa-2x fa-heart deep-purple-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-10">
                    <h5 class="font-weight-bold mb-3">Remote Accessibility for all Stakeholders</h5>
                    <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mb-3">

                <!-- Grid column -->
                <div class="col-2">
                    <i class="fa fa-2x fa-flash deep-purple-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-10">
                    <h5 class="font-weight-bold mb-3">Real Time Medicine
                        Data</h5>
                    <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-2">
                    <i class="fa fa-2x fa-magic deep-purple-text"></i>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-10">
                    <h5 class="font-weight-bold mb-3">Compliments Compact Medication Charts</h5>
                    <p class="grey-text mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam minima assumenda deleniti hic.</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Grid column -->

    </div>
    <!-- Grid row -->

</section>
<!-- Section: Features v.4 -->