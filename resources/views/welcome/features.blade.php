@push('styles')
<link href="{{ asset('css/features.css') }}" rel="stylesheet">
@endpush

@extends('layouts.main')

@section('content')
<!--header section-->
<section id="header-section" class="features-header">
    @include('layouts.menu')
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 mt-5 mt-md-0">
                    <div class="header-right-content greyish-blue">
                        <h1 class="mb-2">The Future of Clinical Documentation</h1>
                        <p>ECCA has been designed with efficiency, clarity and intuitiveness in mind. That means it’s time to say hello to a faster, simpler and more effective clinical documentation processes.</p>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-md-block text-center">
                    <img src="/images/group-16.png"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end of header section-->

<!-- features content -->
<section id="features-content">
    <div class="container">
        <div class="row first-row">
            <div class="col-md-6">
                <img src="/images/group-14.png" class="mb-3"/>
                <h5 class="card-title">Medication Management</h5>
                <p class="card-text">The ECCA Platform allows for seamless recording and management of medication on a per-resident basis. This allows all staff to keep an updated record of all medications taken, as well as reconcile medications with new residents arriving.</p>
                <p class="card-text">Aged-care staff utilise ECCA to improve the accuracy and reliability of medication data and this data can be stored for use throughout the facility by multiple staff.</p>
            </div>
            <div class="col-md-6 mt-5 mt-md-0">
                <img src="/images/group-15.png"  class="mb-4"/>
                <h5 class="card-title">Automatic Data Collection</h5>
                <p class="card-text">No matter where you store your data, it’s always ready to go. The automatic data collection protocol in ECCA’s platform enables staff to save, store and retrieve their resident’s medical information anywhere they are - provided they have an internet connection.</p>
                <p class="card-text">ECCA makes it easier and more instinctive for staff to stay entirely updated with all medical data relating to their patients and residents.</p>
            </div>
            <div class="col-md-6 mt-5 mt-md-17">
                <img src="/images/group-13.png" width="43" class="mb-4"/>
                <h5 class="card-title">Secure Encryption</h5>
                <p class="card-text">We understand that patient and resident data is personal and sensitive information, so we’ve designed the ECCA platform to integrate state-of-the-art encryption to keep all data safe. That means that only users of the ECCA profiles are able to access sensitive data, no other third-parties are provided access and deep encryption methods prevent data theft.</p>
                <p class="card-text">When staff stores patient or resident information, it is immediately stored in a secure enclave awaiting upload to the cloud secure servers.</p>
            </div>
            <div class="col-md-6 mt-5 mt-md-17">
                <img src="/images/icon-doc.png" class="mb-3" width="90"/>
                <h5 class="card-title">Cloud Accessibility</h5>
                <p class="card-text">Thanks to all information being uploaded to the cloud instantly, there is no need to routinely save or retrieve data as a backup. All patient and resident data is available cross-platform, on a range of devices making it easier than ever to keep everything resident-related integrated into your staff’s daily workflows.</p>
                <p class="card-text">Cloud accessibility even allows multiple staff members to work alongside multiple different residents and still know exactly what medication needs to be provided or if any other processes need to be undertaken.</p>
            </div>
        </div>
    </div>
</section>
<!-- end of featues content -->
@endsection