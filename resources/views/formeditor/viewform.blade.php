@php
    $data = $assessment->data;
@endphp
{{--<pre>{{print_r($data)}}</pre>--}}
{{--<pre>{{print_r($form->template_json)}}</pre>--}}

<table class="table table-striped w-auto table-bordered" style="margin-bottom: 20px; width: auto !important;">
{{--<div class="col  p-3">--}}
    @php $num = 0; @endphp
    @foreach($questions as $fld)
        @php
            $code = $fld->Code;
            $question = $fld->Question;
            if(array_key_exists($code, $data))
                $ans = $data[$code];
            else
                $ans='';
            $fieldType = $fld->Type;
            $questionText = $fld->Question;
        @endphp
        <tr>
        {{--<div class="row p-0 " style=" margin-bottom: -1px; !important">--}}
            <td width="5%">{{$fld['DisplayOrder'] + 1}}</td>
            {{--<div style="width: 80px;" class="  @if($num % 2 == 0) bg-light @else bg-white @endif  p-3 pb-0  card border-right-1 mb-0" >{{$fld->DisplayOrder}}</div>--}}
            @if(is_array($questionText))
                @php
                    $questionText = implode('</br>', $questionText);
                @endphp
            @endif
            <td width="40%">{{$questionText}}</td>
            {{--<div class="col-4  p-3  pb-0 card border-right-1  mb-0  @if($num % 2 == 0) bg-light @else bg-white @endif " style="margin-left: -1px;  ">{!! $questionText !!}</div>--}}
            <td width="40">
            {{--<div class="col-7  p-3  pb-0 card border-right-1  mb-0  @if($num % 2 == 0) bg-light @else bg-white @endif " style="margin-left: -1px;  ">--}}
                @if($fieldType=='dropdown')
                    @foreach($fld->Fields as $f)
                        @php
                            $itemCode = $code.'-'.$f['code'];
                        @endphp
                        @if($itemCode==$ans)
                            {{$f['text']}}
                        @endif
                    @endforeach
                        @elseif($fieldType=='facility' || $fieldType=='area')
                            @foreach($fld->Fields as $f)
                                @php
                                    //$itemCode = $code.'-'.$f['code'];
                                    $itemCode = $f['code'];
                                @endphp
                                @if($itemCode==$ans)
                                    {{$f['text']}}
                                @endif
                            @endforeach
                @elseif($fieldType=='radio')
                    @foreach($fld->Fields as $f)
                        @php
                            $itemCode = $code.'-'.$f['code'];
                        @endphp

                        @if($itemCode==$ans)
                            {{$f['text']}}
                        @endif
                    @endforeach
                @elseif($fieldType=='checkbox')
                    @php
                        $ret = [];
                    @endphp
                    @foreach($fld->Fields as $f)
                        @php
                            $itemCode = $code.'-'.$f['code'];
                            $val = array_get($data, $itemCode);
                        @endphp
                        @if($val=="on")
                            @php
                                array_push($ret, $f['text']);
                            @endphp
                        @endif
                    @endforeach
                    {{implode(', ', $ret)}}
                @elseif($fieldType=='bodymap')

                    {{--{{array_get($data, 'Location')}}--}}

                    <div class=" ">@include('template.bodymap_new')</div>

                @elseif($fieldType=='upload')

                    <p>{{array_get($ans, 'filename')}}</p>

                    <object data="data:{{array_get($ans, 'filetype')}};base64,{{array_get($ans, 'document')}}" type="{{array_get($ans, 'filetype')}}" style="width: 100%; height: auto;" ></object>

                @else
                    {!! $ans !!}
                @endif
            {{--</div>--}}
            </td>
            </tr>
        {{--</div>--}}

        @php $num++; @endphp
    @endforeach
    </table>
{{--</div>--}}
<span class="completed_by" style="margin-bottom: 25px;">{{__('Created By')}} {{array_get($assessment->CreatedBy, 'FullName')}} on {{$assessment->updated_at}}</span>