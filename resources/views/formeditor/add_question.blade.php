<div id="Modal_add" class="modal fade"  tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" style="display: none;" >
    <div class="modal-dialog modal-fluid" role="document" >
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Add Question</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body" style="padding-top: 10px; padding-left: 25px;">
                <form id="formQuestion" method="post">

                    <!-- Classic tabs -->
                    <div class="classic-tabs mx-2">

                        <!-- Nav tabs -->
                        <ul class="nav tabs-blue-grey" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link waves-light active" data-toggle="tab" href="#panel1001" role="tab"><i class="fa fa-check-square-o"></i> Fields</a>
                            </li>
                            <li class="nav-item" id="Options">
                                <a class="nav-link waves-light" data-toggle="tab" href="#panel1002" role="tab"><i class="fa fa-list-ul"></i> Options</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-light" data-toggle="tab" href="#panel1003" role="tab"><i class="fa fa-file-text-o"></i> Prompt</a>
                            </li>
                        </ul>
                        <div class="tab-content border-right border-bottom border-left rounded-bottom">
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel1001" role="tabpanel">
                                <div class="field type col-6 pr-5">
                                    <input type="hidden" name="questionId" id="questionId" class="form-control"   />
                                    <input type="hidden" name="DisplayOrder" id="DisplayOrder" class="form-control" value="{{$defaultOrderValue}}" />
                                    <label>Type(*)</label>
                                    <p class="control">

                                        <select class="form-control browser-default" name="type" id="type" required  {{--style="display: block !important;"--}}>
                                            <option value="" selected>Select</option>
                                            @foreach($type as $k=>$t)
                                                <option value="{{$k}}">{{ucwords($t)}}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="type_hidden" id="type_hidden" />
                                    </p>
                                </div>

                                <div id="question_fields">
                                    <div class="row">
                                        <div class="col-6 ">
                                            <div class="field col">
                                                <label>Question(*)</label>
                                                <p class="control"><input  class="form-control" name="question" id="question" value=""  required="required" /></p>
                                            </div>
                                            <div class="field col selectpicklist" style="display: none;">
                                                <label>Choose Picklist</label>
                                                <p class="control"><input  class="form-control" name="selectpicklist" id="selectpicklist" value=""  autocomplete="off"/></p>
                                            </div>
                                            <div class="field col">
                                                <label>Required(*)</label>
                                                <p class="control">

                                                    <select style="display: block !important;" class="form-control" name="required" id="required" onchange="$('option[value='+this.value+']').attr('selected', 'selected'); " >
                                                        <option value="0" >No</option>
                                                        <option value="1" >Yes</option>
                                                    </select>
                                                </p>
                                            </div>

                                            <div class="field col">
                                                <label>Code(*)</label>
                                                <p class="control"><input class="form-control question_code" required  name="code" id="code" value=""/></p>
                                            </div>

                                        </div>

                                        <div class="col bg-light pt-4 pb-4 mr-4 goal" style="margin-top: -90px;">

                                            <div class="field col">
                                                <label>Care Domain</label>
                                                {{--<p class="control"><input class="form-control"  name="caredomain" id="caredomain" value=""  /></p>--}}
                                                <select name="caredomain[]" id="caredomain" class="mdb-select colorful-select dropdown-primary" multiple searchable="Search here..">
                                                    <option value="" disabled >Select..</option>
                                                    @foreach($caredomain as $cd)
                                                        <option value="{{$cd->_id}}" >{{$cd->CareDomain}}</option>
                                                    @endforeach
                                                </select>
                                                {{--<button type="button" class="btn-save btn btn-primary btn-sm">Close</button>--}}
                                            </div>
                                            <div class="field col">
                                                <label>Goal</label>
                                                <p class="control"><input class="form-control"  name="parent_goal" id="parent_goal" value=""
                                                                          {{--onkeyup="validateGoal(this)"--}} onfocus="$(this).select();"
                                                    /></p>
                                            </div>
                                            <div class="field col"   >
                                                <label>Response</label>
                                                <select class="form-control" name="parent_response" id="parent_response"  style="display: block !important;">
                                                    <option value="" selected>Select</option>
                                                    <option value="goal" selected>Goal</option>
                                                    <option value="intv">Intervention</option>
                                                    <option value="obs">Observation</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col bg-white pt-4 pb-4 mr-4 bodymap " style="margin-top: -110px; display:none;">
                                            @include('template.bodymap_new')
                                        </div>
                                 </div>
                                </div>
                                <div id="question_message" style="display: none;">
                                    <div class="field col-12">
                                        <label>Content</label>
                                        <p class="control" >
                                            <textarea  name="question" id="froala-editor" class="message froala-editor form-control"  ></textarea>
                                        </p>
                                    </div>
                                    {{--<div class="field col-5" id="message_point">
                                        <label style="width: 90%;">Point <a href="#" id="add_point" class="pull-right"><i class="fa fa-plus"></i> Add </a></label>
                                        <p class="control" id="point" >
                                            <input class="form-control"  type="text" name="point[]" />
                                        </p>

                                    </div>--}}
                                </div>
                            </div>
                            <!--/.Panel 1-->
                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel1002" role="tabpanel">
                                <div class="fields fieldsOptions" id="fields" style="padding-top: 10px; min-height: 60px; background: #f4f4f4;   ">
                                    <div class="row  pl-3 pb-3"><a href="#" class="add_fld float-left pt-2 pl-3" id="add_fld" onclick="" >
                                            <i class="fa fa-plus"></i> Add Options
                                        </a></div>

                                    <div class="fieldOptionsHeader mb-2"  style="padding: 15px 15px 10px 30px; background: rgb(244, 244, 244);">

                                        <div class="row">
                                            <div class="col-2 pl-3">
                                                <label>Code </label>
                                            </div>
                                            <div class="col-3 pl-3">
                                                <label>Text</label>

                                            </div>
                                            {{--<div class="col-3 pl-3">
                                                <label>Care Domain</label>

                                            </div>--}}
                                            <div class="col-3 pl-3">
                                                <label>Goal</label>

                                            </div>
                                            <div class="col-2 pl-3">
                                                {{--<label>Care Domain</label>--}}

                                            </div>
                                            <div class="col-1 pl-3">
                                                <label>Score</label>

                                            </div>
                                            <div class="col-1 pt-4 text-center">

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.Panel 2-->
                            <!--Panel 3-->
                            <div class="tab-pane fade" id="panel1003" role="tabpanel">
                                <textarea  name="Prompt" id="Prompt" class="  form-control" row="5" placeholder="Input Prompt Text"></textarea>
                            </div>
                            <!--/.Panel 3-->
                        </div>

                    </div>
                    <!-- Classic tabs -->

                    {{--<div class="tabs mb-4">
                        <ul class="nav md-pills  pills-secondary">
                            <li class="nav-item"  >
                                <a onclick="
                                var modalId = 'Modal_add ';
                                console.log($(this).parent().attr('id'));
    //                            $(this).attr('class','is-active');
                                $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','');
                                $('#'+modalId+' div.tabs ul li:first').attr('class','is-active');
                                $('#'+modalId+' #question_fields').show();
                                $('#'+modalId+' .fieldsOptions').hide();
                                $('#'+modalId+' div.type').show();
                            " a class="nav-link active" data-toggle="tab" href="#panel4" role="tab">Fields</a>
                            </li>
                            <li class="nav-item " id="displayFieldOption" style="display: none;" onclick="
                            var modalId ='Modal_add ';
                             console.log('option: '+modalId);
    //                            $(this).attr('class','is-active');
                                $('#'+modalId+' div.tabs ul li:first').attr('class','');
                                $('#'+modalId+' div.tabs ul li:not(:first)').attr('class','is-active');
                                $('#'+modalId+' #question_fields').hide();
                                $('#'+modalId+' .fieldsOptions').show();
                                $('#'+modalId+' div.type').hide();
                                // displayPicklist();
                            ">
                                <a class="nav-link" data-toggle="tab" href="#panel4" role="tab">Options</a>
                            </li>
                            <li class="nav-item " id="Prompt" style="display: block !important;">
                                <a class="nav-link" data-toggle="tab" href="#panel5" role="tab">Prompt</a>
                            </li>

                        </ul>
                        <hr />
                    </div>--}}

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-grey btn-rounded" id="save_add_qn">Add Question</button>
                <button type="button" id="close_add_modal" class="btn btn-danger btn-rounded" data-dismiss="modal">Close</button>

            </div>

            <div id="add_qn_template" style="visibility: hidden; height: 0px; overflow: hidden;">
                <div class="fieldOptions" id="fieldOptions" style="padding: 13px 15px 10px 30px !important; background: #f4f4f4;  ">

                    <div class="row border-bottom-1 pb-1">
                        <div class="col-2 p-1">
                            <label>Code </label>
                            <p class="control mb-1"> <input class=" form-control field_code" type="text" name="code[]" id="code" placeholder="Code"></p>
                        </div>
                        <div class="col-3 p-1">
                            <label>Text</label>
                            <p class="control mb-1">  <input class=" form-control" type="text" name="text[]" id="text" placeholder="Text Option"></p>

                        </div>
                        {{--<div class="col-2 p-1 ">
                            <label>Care Domain</label>
                            <p class="control mb-1"  id="caredomain">  <input class=" form-control" type="text" name="caredomain[]" id="caredomain" placeholder="Care Domain"></p>

                        </div>--}}
                        <div class="col-3 p-1 ">
                            <label>Goal</label>
                            <p class="control mb-1"  id="goal">  <input class=" form-control" type="text" name="goal[]" id="goal" placeholder="Goal"></p>
                        </div>
                        <div class="col-2 p-1 ">
                            {{--<label>Goal</label>--}}
                            <p class="control mb-1"  id="response">

                                <select class="form-control" name="response[]" id="response"  style="display: block !important;">
                                    <option value="" selected>Select</option>
                                    <option value="goal">Goal</option>
                                    <option value="intv">Intervention</option>
                                    <option value="obs">Observation</option>
                                </select>
                            </p>
                        </div>
                        <div class="col-1 p-1 pr-3">
                            <label>Score</label>
                            <p class="control mb-1">  <input class=" form-control" type="number" min="0" name="score[]" id="score" placeholder="Score"></p>

                        </div>
                        <div class="col-1 p-1 text-center">
                            <a href="#" class="qn_trash  " id="delete_fld" onclick="
                                    var id = $(this).parent().parent().parent().attr('id');
                                    var modalId = $(this).closest('.modal-dialog').parent().attr('id');
                                    // alert(id);
                                    confirmAction('#'+modalId+' div#fields div#'+id, id, '{{__('Are you sure you want to remove this field option?')}}');
                                    " style="float: none !important;">
                                <i class="fa fa-trash" style="font-size: 25px;     margin-top: 5px;"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>