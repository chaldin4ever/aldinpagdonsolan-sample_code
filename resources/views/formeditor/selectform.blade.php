@extends('layouts.poc')

@section('style')
    <style>

        ul.nav li.nav-item{
            list-style: none !important;
        }

        [type=radio]+label, [type=checkbox]+label{
            font-size: 13px !important;
        }

        .message {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .message li{
            list-style: square !important;
            margin-left: 20px;
        } 

        ul, li {
            list-style: decimal !important;
            margin-top: 0px;
        }

        li label:first-child {font-weight: 500;}

        li   li {
            list-style: decimal !important;
            margin-left: 30px;
            margin-top: 5px;

        }

        

        [type=radio]+label, [type=checkbox]+label{
            height: auto !important;
        }

        .white-skin input[type=checkbox].filled-in:checked+label:after {
            background-color: rgb(90, 118, 147);
            border-color: rgb(90, 118, 147);
        }
        .white-skin body{
        'Helvetica Light', Helvetica, Arial, sans-serif
        }

        .nav-link, .navbar {
            padding: 1.2rem .2rem;
            font-weight: normal;
        }

        .nav-link {
            font-size: 0.9rem;
            font-weight: 100;
            padding: 0.75rem 0.2rem;
        }


        .ui-helper-hidden-accessible {display: none;}

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-2">
        <div class="row  pl-4 mb-1   " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{$form->FormName}}</strong></h3>
                <a href="{{url('resident/view/'.$resident->_id)}}" class="float-right back-to-prev-page">Back to resident view</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white pl-2 pt-2 pb-2 mb-2">

            @include('resident.header', ['resident' => $resident])

        </div>

        <div class="content m-0   pb-5" id="content">

            <div class="row bg-white pt-4  pl-4 pb-4">

                <div class="col">
                    <div class="bodymap_checkbox">
                        @foreach($bodymap as $k=>$v)
                            <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                            <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                        @endforeach
                    </div>
                    <form method="post" action="{{url('/assessment/store')}}" id="assessment-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="residentId" value="{{$resident->_id}}" />
                        <input type="hidden" name="formId" value="{{$form->_id}}" />
                        <input type="hidden" name="assessmentId" value="@if(!empty($data)){{$data->_id}}@endif" />


                        {{--<pre>{{print_r($data)}}</pre>--}}

                        @include('formeditor.question_list', ['questions' => $questions, 'data'=>array_get($data, 'data')])

                        <div class="row">
                            <button class="btn btn-rounded btn-danger">{{__('Save')}}</button>
                            <a href="{{url('resident/view/'.$resident->_id.'?view=assessment')}}" class="btn btn-rounded btn-grey">{{__('Cancel')}}</a>
                        </div>


                    </form>
                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')

    @php
            $replace_resident_text = !empty($resident->PreferredName) ? $resident->PreferredName : $resident->FirstName;
            @endphp
    <script type="text/javascript" src="{{asset('js/findAndReplaceDOMText.js')}}"></script>
    <script>

        findAndReplaceDOMText(document.getElementById('content'), {
            find: 'the resident',
            replace: '{{$replace_resident_text}}'
            /*replace: function(portion, match) {
                return 'replaced_text';
            }*/
        });

    </script>
    <script type="text/javascript" src="{{asset('js/jquery.maphilight.js')}}"></script>
    <script>

        $.fn.maphilight.defaults = {
            fill: true,
            fillColor: '8b0000',
            fillOpacity: 0.2,
            stroke: true,
            strokeColor: '8b0000',
            strokeOpacity: 0.6,
            strokeWidth: .4,
            fade: true,
            alwaysOn: false,
            neverOn: false,
            groupBy: false,
            wrapClass: true,
            shadow: false,
            shadowX: 0,
            shadowY: 0,
            shadowRadius: 6,
            shadowColor: '000000',
            shadowOpacity: 0.8,
            shadowPosition: 'outside',
            shadowFrom: false
        }

        $(function() {
            $('.bodymap').maphilight();

        });

        function selectbodymap(map){

            var checkbox = $('.bodymap_checkbox input#'+map+':checked');
            var data = $('#'+map).data('maphilight') || {};


            if(checkbox.length > 0){
                $('.bodymap_checkbox input#'+map).removeAttr('checked');
                $('.bodymap_checkbox input#'+map+'_id').removeAttr('checked');

                data.alwaysOn = false;
                $('area#'+map).data('maphilight', data).trigger('alwaysOn.maphilight');
            }else{

                $('.bodymap_checkbox input#'+map+'').attr('checked', 'checked');
                $('.bodymap_checkbox input#'+map+'_id').attr('checked', 'checked');

                data.alwaysOn = true;
                $('area#'+map).data('maphilight', data).trigger('alwaysOn.maphilight');

            }

            var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                return this.value;
            }).get().join(", ");

            var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                return this.value;
            }).get().join(", ");

            $('div#Location').text(data);
            $('div.bodymap input#Location').val(data);
            $('div.bodymap input#Bodymap').val(data_id);
        }



        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 90
        });

        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        var hour = dt.getHours() + ":" + '00';

        $('#timepicker').pickatime(
            {
                default: hour,
                darktheme: true,
                twelvehour: true,
            });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();
        })




        $(document).ready(function() {

            setHtmlMessage();

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');

        })

        @php $data = array_get($data, 'data'); @endphp
        $(document).ready(function() {

            $('.bodymap  #Location').text('{{array_get($data, 'Location')}}');
            $('.bodymap  input#Location').val('{{array_get($data, 'Location')}}');
            $('.bodymap  input#Bodymap').val('{{array_get($data, 'Bodymap')}}');

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');

            @php $bmArr = explode(',',str_replace(' ','',array_get($data, 'Bodymap'))); @endphp

            @foreach($bodymap as $k=>$v)

            var data = $('#{{$k}}').data('maphilight') || {};
            @if(in_array($k, $bmArr))
                data.alwaysOn = true;
            $('area#{{$k}}').data('maphilight', data).trigger('alwaysOn.maphilight');
            {{--$('table.bodymap button div#{{$k}}').show();--}}

            @endif

            @if(in_array($k, $bmArr))

            $('table.bodymap button div#{{$k}}').show();

            var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

            if(checkbox.length > 0){
                $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                $('table.bodymap button div#{{$k}}').hide();
            }else{
                $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                $('table.bodymap button div#{{$k}}').show();
            }

            @endif

            @endforeach
        })

        function setHtmlMessage(){
            // $('textarea#froala-editor').froalaEditor('html.set', '');
            // $('textarea#froala-editor').froalaEditor('destroy');
            $('textarea#froala-editor').froalaEditor({
                toolbarButtons: ['fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough',
                    'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                    'undo', 'redo',
                    'html', 'clearFormatting'
                ],
            });

        }


    </script>

    @if(!empty($form->javascript))
        <script>
            {{$form->javascript}}
        </script>
    @endif
@endsection
