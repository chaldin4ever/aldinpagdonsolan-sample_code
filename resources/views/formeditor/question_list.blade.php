{{--<pre>{{print_r($questions)}}</pre>--}}

@php $num=0; @endphp

@if(!empty($questions))

    @if(sizeof($questions)>0)
        <ol id="questions" class="questions" >
            @foreach($questions as $k=>$cnt)

                @php
                    $required = '';
                    $requiredClass = '';
                    if(($cnt->Required ==1))
                    {
                        $required='required';
                        $requiredClass = 'is-required';
                    }
                    $key = $cnt->_id;
                @endphp

                @if($cnt->Type == 'text')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn">{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->Goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <p class="control">
                                    <input class="input form-control" type="text" id="{{$cnt->Code}}"  {{$required}}
                                    name="{{$cnt->Code}}"  value="{{array_get($data, $cnt->Code)}}" style="width:100%" />
                                </p>
                            </div>
                            @include('formeditor.prompt')
                        </div>


                    </li>
                @elseif($cnt->Type == 'upload')
                    <li id="qn{{$cnt->_id}}">
                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn">{{$cnt->Question}}</label>
                                {{--<a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->Goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>--}}
                                <p class="control">
                                    <input class="input form-control" type="file" id="{{$cnt->Code}}"  {{$required}}
                                    name="{{$cnt->Code}}"  style="width:100%" />
                                </p>
                                @php $ans = array_get($data, $cnt->Code); @endphp
                                @if(!empty($ans))
                                    <p>{{array_get($ans, 'filename')}}</p>
                                    <object data="data:{{array_get($ans, 'filetype')}};base64,{{array_get($ans, 'document')}}" type="{{array_get($ans, 'filetype')}}" style="width: 100%; height: auto;" ></object>
                                @endif
                            </div>
                            @include('formeditor.prompt')
                        </div>


                    </li>
                @elseif($cnt->Type == 'bodymap')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">        <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn" >{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->Goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <p class="control">
                                    @include('template.bodymap_new')
                                </p>
                            </div>
                            @include('formeditor.prompt')
                        </div>


                    </li>
                @elseif($cnt->Type == 'memo')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn" >{{$cnt->Question}}</label>
                                <p class="control">
                                    <textarea class="textarea" id="froala-editor" {{$required}} name="{{$cnt->Code}}"  >{{array_get($data, $cnt->Code)}}</textarea>
                                </p>
                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>


                @elseif($cnt->Type == 'number')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn">{{$cnt->Question}}</label>
                                <p class="control">
                                    <input class="form-control" type="number" {{$required}} name="{{$cnt->Code}}" step="any" value="{{array_get($data, $cnt->Code)}}"
                                           onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46'  style="width:150px"/>
                                </p>
                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>


                @elseif($cnt->Type == 'date')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label class="label_qn" id="label_{{$cnt->_id}}"   >{{$cnt->Question}}</label>
                                <p class="control">
                                    <input class="input datepicker form-control" id="datepicker" name="{{$cnt->Code}}" value="{{array_get($data, $cnt->Code)}}"  style="width:150px"/>
                                </p>
                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>
                @elseif($cnt->Type == 'time')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}" class="{{$requiredClass}} label_qn"  >{{$cnt->Question}}</label>
                                <p class="control">
                                    <input class="input form-control  " id="timepicker" {{$required}} name="{{$cnt->Code}}" value="{{array_get($data, $cnt->Code)}}"  style="width:150px"/>
                                </p>
                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>

                @elseif($cnt->Type == 'checkbox')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label index="{{$cnt->_id}}"  id="label_{{$cnt->_id}}">{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <div class="options">
                                    @if(!empty($cnt->Fields))
                                    @foreach($cnt->Fields as $fld)
                                        <div class="form-row form-check" id="checkbox_{{$fld['code']}}">
                                            <input type="checkbox"  id="{{$cnt->Code.'-'.$fld['code']}}" name="{{$cnt->Code.'-'.$fld['code']}}" class="regular-checkbox" @if(array_get($data, $cnt->Code.'-'.$fld['code'])=='on')checked @endif />
                                            <label class="form-check-label" id="{{$cnt->Code.'-'.$fld['code']}}" for="{{$cnt->Code.'-'.$fld['code']}}" index="{{$num}}" >
                                                {{$fld['text']}}
                                            </label>
                                        </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>

                @elseif($cnt->Type == 'radio')
                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">

                                <label  index="{{$cnt->_id}}" id="label_{{$cnt->_id}}" qname="{{$cnt->Code}}" >{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <div class="options">
                                    @if(!empty($cnt->Fields))
                                        @foreach($cnt->Fields as $fld)
                                            <div class="form-row " id="radio{{$fld['code']}}">
                                                <input type="radio"  id="{{$cnt->Code.'-'.$fld['code']}}" name="{{$cnt->Code}}" value="{{$cnt->Code.'-'.$fld['code']}}" class="regular-checkbox" @if(array_get($data, $cnt->Code) == $cnt->Code.'-'.$fld['code'])checked @endif />
                                                <label  id="{{$cnt->Code.'-'.$fld['code']}}" for="{{$cnt->Code.'-'.$fld['code']}}" index="{{$num}}" >
                                                    {{$fld['text']}}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>


                @elseif($cnt->Type == 'dropdown')

                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  >{{$cnt->Question}}
                                </label>
                                <p class="control">
                        <span class="select fordropdown">
                            <select name="{{$cnt->Code}}"  style="display: block !important;" class="form-control" >
                                @if(!empty($cnt->Fields))
                                    @foreach($cnt->Fields as $fld)
                                        <option name="{{$cnt->Code.'-'.$fld['code']}}"  value="{{$cnt->Code.'-'.$fld['code']}}" @if(array_get($data, $cnt->Code)==$fld['code'])selected @endif>{{$fld['text']}}</option>
                                     @endforeach
                                @endif
                            </select>
                        </span>
                                </p>

                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>
                @elseif($cnt->Type == 'facility' || $cnt->Type == 'area' || $cnt->Type == 'vacantrooms')

                    <li id="qn{{$cnt->_id}}">

                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  >{{$cnt->Question}}
                                </label>
                                <p class="control">
                        <span class="select fordropdown">
                            <select name="{{$cnt->Code}}"  style="display: block !important;" class="form-control" >

                                @if(!empty($cnt->Fields))
                                    <option value="" >{{__('Choose..')}}</option>
                                    @foreach($cnt->Fields as $fld)
                                        <option name="{{$fld['code']}}"  value="{{$fld['code']}}" @if(array_get($data, $cnt->Code)==$fld['code'])selected @endif>{{$fld['text']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </span>
                                </p>

                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>
                @elseif($cnt->Type == "message")
                    <div id="qn{{$cnt->_id}}" style="list-style: none;" value="" class="message row">

                            <div class="field col">
                                <div class="content">
                                    @if(!is_array($cnt->Question))
                                        {!!$cnt->Question!!}
                                    @endif
                                </div>
                                {{-- <label class="content label_qn" >
                                     {!!$cnt->Question!!}
                                 </label>--}}

                            </div>
                    </div>
                @else
                @endif
                @php $num++ @endphp
            @endforeach
        </ol>
    @else

        <div class="alert alert-warning align-middle"><strong><i class="fa fa-info-circle fa-2x mr-4"></i> {{__('No questions available.')}}</strong> </div>

    @endif


@endif

