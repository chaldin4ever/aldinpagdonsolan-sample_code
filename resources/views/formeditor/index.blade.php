@extends('layouts.poc')

@section('style')
    <style>


        [type=radio]+label, [type=checkbox]+label{
            height: auto !important;
        }

        .multiple-select-dropdown li [type=checkbox]+label {
            top: -7px; !important;
        }

        .content:not(:last-child) {
            margin-bottom: 0 !important;
        }

        /*#form_template_question ol#questions > li a:first-child {display: none !important;}*/

        #form_template_question ol#questions > li {
            margin-bottom: 5px !important;
            padding: 5px 15px 5px 0 !important;
        }

        #Modal_add #question_message ol li,
         div.content ol li
        {
             list-style: decimal   !important;
             list-style-type: decimal  !important;
             margin: 5px 20px 5px 20px  ;
             /*padding: 10px !important;*/
        }

         #Modal_add #question_message ul li, div.content ul li{
            list-style: disc;
            margin: 5px 20px 5px 20px  ;
        }

        #Modal_add #question_message ul {
            padding-left: 40px;
        }

        #form_template_question ol#questions > li pre{
            text-align: left;
            padding: 10px;
            white-space: pre-line;
            margin: 10px;
            background: #f5f5f5;
            overflow-x: auto;
        }

        #form_template_question ol#questions {padding-left: 50px;}
        #form_template_question ol#questions > li label {width: auto !important;}
        #form_template_question ol#questions > li .fa {vertical-align: middle;}
        #form_template_question ol#questions > li .columns {
            /*margin-top: -40px;*/
            padding-right: 15px;
        }

        #form_template_question ol#questions > li.message .columns {
            margin-top: 0 !important;
            padding-right: 15px;
        }

        #form_template_question ol#questions > li:before {font-weight: 700 !important;}
        #form_template_question ol#questions > li {margin-bottom: 10px; padding: 5px 10px 10px 0;}
        #form_template_question ol#questions > li:hover, ol#questions > li label.label_qn:hover {
            background: #f0f1f2;
            border: 1px dashed lightgrey;
            cursor: move; /* fallback if grab cursor is unsupported */
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        #form_template_question ol#questions > li:active, ol#questions > li label:active {
            background: #ccc;
            cursor: grabbing;
            cursor: -moz-grabbing;
            cursor: -webkit-grabbing;
        }

        #form_template_question .qn{float: left; margin-left: -50px; margin-top: 3px;}
        /*.qn_show_code {float: left; margin-left: -25px; margin-top: 3px;}*/
        .qn_trash {float: right; margin-left: -45px; margin-top: 3px;}

        /*#Modal_add .modal-body input,
        #Modal_add .modal-body select,
        #Modal_add .modal-body textarea {width: 95%; padding: 5px; margin-bottom: 5px; border: solid 1px #5c5c5c;}*/

        .precode {margin-left: 50%; top: -4%;}

        #form_template_question ol#questions > li {margin-bottom: 10px; border-bottom: 1px solid #f0f1f2;}

        i.fa-trash {font-size: 24px !important;}

        li.hide_remove{display: none;}

        /*#Modal_add  #fields .fieldOptions .row div label {display: none !important;}
        #Modal_add  #fields .fieldOptions {margin-top: -35px !important;}
        #Modal_add  #fields .fieldOptions a#delete_fld {display: none;}
        #Modal_add  #add_fld {visibility: hidden;}*/

        #fields .fieldOptions .row div label {display: none !important;}
        #fields .fieldOptions {margin-top: -35px !important;}
        /*#fields .fieldOptions a#delete_fld {display: none;}*/
        /*#add_fld {visibility: hidden;}*/


        .ui-helper-hidden-accessible {display: none !important;}

    </style>

@endsection

@section('content')

    <div class="container-fluid  mt-1">
        <div class="row   pb-0 pt-3 mb-2  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col-6 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{$form->FormName}}</strong>
                <a class="" href="{{url('/formeditor/preview/'.$form->_id)}}"><i class="fa fa-eye"></i></a>
                </h3>
            </div>
            <div class="col text-right">
                <div class="col">
                    <a class="" style="font-size:0.85rem" href="{{url('/form')}}">Back to Forms</a>
                    <a class="btn btn-grey btn-rounded" href="{{url('/form/edit/'.$form->_id)}}">{{__('Edit Header')}}</a>
                </div>
            </div>
        </div>

        <div class="  pb-5 bg-white pr-3 pt-1 pl-5">
            <div class=" bg-white  pb-5 pt-1 mb-4  ">
                {{--<pre>{{print_r($caredomain)}}</pre>--}}
                <a href="#" class="add_qn float-right btn btn-amber btn-sm btn-rounded" id="add_qn"
                   onclick="add_qn('preppend')"
                   data-toggle="modal" data-target="#Modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Add Question</a>

            </div>
            {{--<pre>{{json_encode($questions)}}</pre>--}}
            <div id="form_template_question" class="p-0 m-0">@include('formeditor.edit_question_list', ['questions' => $questions])</div>

            <div class="field is-grouped">
                <div class="bodymap_checkbox">
                    @foreach($bodymap as $k=>$v)
                        <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                        <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                    @endforeach
                </div>


                <h1 class="w-100  float-right append mt-1 mb-3 " style="margin-top: -30px;">
                    <a href="#" class="add_qn float-right btn btn-amber  btn-sm btn-rounded" id="add_qn"
                       onclick="add_qn('append')"
                       data-toggle="modal" data-target="#Modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Add Question</a>
                    <a class="btn btn-sm btn-grey btn-rounded is-link float-right" href="{{url('/form/listing')}}">Return to Forms</a>

                </h1>
            </div>
        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

@endsection

@section('script')

    <script type="text/javascript" src="{{asset('js/jquery.maphilight.js')}}"></script>
    <script>

        $('#Modal_add div.tabs ul li:last').show();

        $.fn.maphilight.defaults = {
            fill: true,
            fillColor: '8b0000',
            fillOpacity: 0.2,
            stroke: true,
            strokeColor: '8b0000',
            strokeOpacity: 0.6,
            strokeWidth: .4,
            fade: true,
            alwaysOn: false,
            neverOn: false,
            groupBy: false,
            wrapClass: true,
            shadow: false,
            shadowX: 0,
            shadowY: 0,
            shadowRadius: 6,
            shadowColor: '000000',
            shadowOpacity: 0.8,
            shadowPosition: 'outside',
            shadowFrom: false
        }

        $(function() {
            $('.bodymap').maphilight();

        });

        function selectbodymap(map){

            var checkbox = $('.bodymap_checkbox input#'+map+':checked');
            var data = $('#'+map).data('maphilight') || {};


            if(checkbox.length > 0){
                $('.bodymap_checkbox input#'+map).removeAttr('checked');
                $('.bodymap_checkbox input#'+map+'_id').removeAttr('checked');

                data.alwaysOn = false;
                $('area#'+map).data('maphilight', data).trigger('alwaysOn.maphilight');
            }else{
                $('.bodymap_checkbox input#'+map+'').attr('checked', 'checked');
                $('.bodymap_checkbox input#'+map+'_id').attr('checked', 'checked');

                data.alwaysOn = true;
                $('area#'+map).data('maphilight', data).trigger('alwaysOn.maphilight');

            }

            var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                return this.value;
            }).get().join(", ");

            var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                return this.value;
            }).get().join(", ");

            $('div#Location').text(data);
            $('input#Location').val(data);
            $('input#Bodymap').val(data_id);
        }


        var drake =  window.dragula();

        var modalForm = '#Modal_add ';



        $(document).ready(function() {

            $('.mdb-select').material_select();

            alertifySettings();
            resetDragula();

            $('#Modal_add button#save_add_qn').on('click', function(event) {


                var isvalidate = $("#formQuestion")[0].checkValidity();

                if(false === isvalidate){

                    toastr.warning('Please fill up the required (*) field(s).')

                }else{
                    apiStore();
                    // resetDragula();
                }


            });
        });


        function apiStore(){

            var type = $(modalForm +'input#type_hidden').val();

            var url = '{{url('formeditor/api/storequestion')}}';

            var fldcode_list = $(modalForm +' #fields div.fieldOptions input[name^=code]');

            var fldcode = [];
            $(fldcode_list).each(function() {
                var val = $(this).val();
                fldcode.push(val);
            });

            var fldtext_list = $(modalForm +' #fields div.fieldOptions input[name^=text]');

            var fldtext = [];
            $(fldtext_list).each(function() {
                var val = $(this).val();
                fldtext.push(val);
            });

            var fldgoal_list = $(modalForm +' #fields div.fieldOptions input[name^=goal]');

            var fldgoal = [];
            $(fldgoal_list).each(function() {
                var val = $(this).val();
                fldgoal.push(val);
            });

            var fldresponse_list = $(modalForm +' #fields div.fieldOptions select[name^=response]');

            var fldresponse = [];
            $(fldresponse_list).each(function() {
                var val = $(this).val();
                fldresponse.push(val);
            });

            var fldscore_list = $(modalForm +' #fields div.fieldOptions input[name^=score]');

            var fldscore = [];
            $(fldscore_list).each(function() {
                var val = $(this).val();
                fldscore.push(val);
            });


            if( type == 'message'){

                var question = $(modalForm +' textarea.message').val();
                // alert(question);

            }else{

                var question = $(modalForm +' input#question').val();

            }

            var caredomain_items = [];
            $(modalForm +' #question_fields select#caredomain option:selected').each(function(){ caredomain_items.push($(this).val()); })

            // console.log(caredomain_items);
            axios.post(url, {

                formId: '{{$form->_id}}',
                questionId:  $(modalForm +' input#questionId').val(),
                DisplayOrder: $(modalForm +' input#DisplayOrder').val(),
                type: $(modalForm +'select#type').val(),
                question: question,
                prompt: $(modalForm +' textarea#Prompt').val(),
                picklistId: $(modalForm +'input#selectpicklist').attr('selectpicklist_id'),
                required: $(modalForm +'select#required').val(),
                code: $(modalForm +'input#code').val(),
                caredomain:  caredomain_items,
                goal: $(modalForm +'input#parent_goal').val(),
                response: $(modalForm +'select#parent_response').val(),
                fld_code: fldcode,
                fld_text: fldtext,
                fld_goal: fldgoal,
                fld_response: fldresponse,
                fld_score: fldscore

            }).then(function(response){

                // console.log(response.data);
                apiDisplayQuestion();

                $(modalForm+' button#close_add_modal').trigger('click');

                toastr.success('Successfuly Saved.');

                // resetDragula();

            })

        }

        function reset(){
            $(modalForm +' .mdb-select').material_select('destroy');
            $(modalForm +' .mdb-select').val('0').change();
            $(modalForm +' .mdb-select').material_select();
        }
        //for editing question
        function apiGetQuestion(qId){

            reset();

            var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

            if(textlistcurrent.length > 0){
                $('#Modal_add #fields div.fieldOptions').remove();
            }

            $('#Modal_add .modal-title').text('Edit Question');
            $('#Modal_add button#save_add_qn').text('Save Question');

            $('#Modal_add select#type').addClass(' disabled');

            // $('#Modal_add .fieldsOptions').hide();

            $('#Modal_add input, #Modal_add select').val('');

            var url = '{{url('formeditor/api/getquestion')}}/'+qId;

            axios.get(url).then(function(response){

                var data =response.data;
                // console.log(data);
                $(modalForm +' input#questionId').val(qId);
                $(modalForm +' input#DisplayOrder').val(data.DisplayOrder);
                $(modalForm +' select#type').val(data.Type).trigger('change');
                $(modalForm +' input#selectpicklist').removeAttr('required');
                if(data.Type == 'message'){
                    $('#froala-editor').froalaEditor('html.set',data.Question);
                    $(modalForm +' #question_message textarea.message').val(data.Question);
                }else{
                    $(modalForm +'input#question').val(data.Question);
                }

                $(modalForm +'select#required').val(data.Required);
                $(modalForm +'input#code').val(data.Code);
                $(modalForm +'textarea#Prompt').val(data.Prompt);
                // console.log(data.CareDomain);
                // if ( data.CareDomain.length > 0) {
                if(!$.isEmptyObject(data.CareDomain)){
                    /*$(modalForm +'input#caredomain').attr('caredomain_id', data.CareDomain.caredomainId);
                    $(modalForm +'input#caredomain').val(data.CareDomain.CareDomain);*/

                    $.each(data.CareDomain, function(k, v){
                        console.log($.type(v));
                        var cd;
                        if($.type(v) === 'object'){
                            cd = v.CareDomain;
                        }else{
                            cd = v;
                        }
                        $(modalForm +' .multiple-select-dropdown li:contains('+cd+')').trigger('click');
                    })
                    $(modalForm +' select#caredomain').trigger('click');

                }

                $(modalForm +'input#parent_goal').val(data.Goal);
                $(modalForm +'select#parent_response').val(data.Response);

                if(data.Type == 'checkbox' || data.Type == 'radio' || data.Type == 'dropdown'){

                    $.each(data.FieldsCode, function(k, v){
                        $( "#Modal_add #add_fld" ).trigger('click');
                    });

                    var textlist = $('#Modal_add #fields div.fieldOptions input[name^=text]');
                    var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');
                    var goallist = $('#Modal_add #fields div.fieldOptions input[name^=goal]');
                    var resplist = $('#Modal_add #fields div.fieldOptions select[name^=response]');
                    var scorelist = $('#Modal_add #fields div.fieldOptions input[name^=score]');

                    $.each(textlist, function(k){$(this).val(data.FieldsText[k]);});
                    $.each(codelist, function(k){$(this).val(data.FieldsCode[k]);});
                    $.each(goallist, function(k){$(this).val(data.FieldsGoal[k]);});
                    $.each(resplist, function(k){$(this).val(data.FieldsResponse[k]);});
                    $.each(scorelist, function(k){$(this).val(data.FieldsScore[k]);});

                }

            })


        }

        function apiDisplayQuestion(){

            var url = '{{url('formeditor/api/displayquestion/'.$form->_id)}}';

            axios.get(url).then(function(response){

                var questions = response.data;

                $('#form_template_question').html(questions);

                resetDragula();
            })
        }


        function setDisplayOrder(){

            var url = '{{url('formeditor/api/gettotalquestion/'.$form->_id)}}';

            axios.get(url).then(function(response){

                $('#Modal_add input#DisplayOrder').val(response.data);

            })
        }

        function apiSaveDisplayOrder(){

            var question_list = $('#form_template_question li input[name^=qnId]');

            var qnlist = [];
            $(question_list).each(function() {
                var val = $(this).val();
                qnlist.push(val);
            });

            var url = '{{url('formeditor/api/savedisplayorder')}}';

            axios.post(url,{
                qnId: qnlist
            }).then(function(){

                toastr.success('Successfully saved.');
            })

        }



    </script>

    <script>


        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();

        })


        function getpicklist(){

            var picklists = $.parseJSON('{!! $picklists !!}');

            return picklists;
        }

        function getCareDomain(){

            var caredomain = $.parseJSON('{!! $caredomain !!}');

            return caredomain;

        }

        function getFacilities(){

            var facilities = $.parseJSON('{!! $userfacilities !!}');
            // console.log(facilities);
            return facilities;
        }

        function getFacilityArea(){

            var areas = $.parseJSON('{!! $areas !!}');
            // console.log(facilities);
            return areas;
        }

        function getVacantRooms(){

            var rooms = $.parseJSON('{!! $vacantrooms !!}');
            // console.log(facilities);
            return rooms;
        }

        $(document).ready(function() {

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');
            @foreach($bodymap as $k=>$v)

            $("table.bodymap button#{{$k}}").click(function () {
                $('table.bodymap td button#{{$k}}').tooltip();


                var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

                if(checkbox.length > 0){
                    $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                    $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                    $('table.bodymap button div#{{$k}}').hide();
                }else{
                    $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                    $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                    $('table.bodymap button div#{{$k}}').show();
                }

                var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                $('div#Location').text(data);
                $('input#Location').val(data);
                $('input#Bodymap').val(data_id);

                return false;
            })
            @endforeach
        })

        $(document).ready(function() {

            $('#Modal_add input#caredomain').mdb_autocomplete({
                data: getCareDomain()
            });

        })


        function generateCode() {

            var randomNum = (""+Math.random()).substring(2,7);
            var text = "";
            // var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            /*for (var i = 0; i < 2; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));*/

            var prefix = '{{strtoupper(substr($form->FormName, 0, 3))}}';
            return prefix+'-'+randomNum;
        }


        // $("ol#questions li:first a.qn_trash").hide();

        $("html").removeClass('full-height');
        // toastr.success('Please add a new question before removing the TEST question or you can just just update it. If you have done this before please ignore this message.').css('width', '450px');

        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 50,
            max: true
        });

        $('.timepicker').pickatime({
            // Light or Dark theme
            darktheme: true
        });



        /*var drak = dragula([document.querySelector('#questions')]);
        drak.on('drop', function(){
            apiSaveDisplayOrder();
        })*/

        function resetDragula(){

            var drake =  window.dragula();

            drake.destroy;
            drake = dragula([document.querySelector('#form_template_question ol#questions')]);
            // console.log(drake);

            drake.on('drop', function(){
                apiSaveDisplayOrder();
            })
        }


        function confirmAction ( elem, id, message) {
            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            alertify.confirm(message, function(e) {
                if (e) {
                    // a_element is the <a> tag that was clicked
                    $(elem+'#'+id).fadeOut("slow",function(){
                        $(elem+'#'+id).remove();

                        $('.modal-backdrop').remove();
//                        $('#is-remove-'+fldcode).val(1);
                    });

                }
            });
        }

        function alertifySettings(){
            alertify.defaults = {
                autoReset:true,
                basic:false,
                closable:true,
                closableByDimmer:true,
                frameless:false,
                maintainFocus:false, // <== global default not per instance, applies to all dialogs
                maximizable:true,
                modal:true,
                movable:true,
                moveBounded:false,
                overflow:true,
                padding: true,
                pinnable:true,
                pinned:true,
                preventBodyShift:false, // <== global default not per instance, applies to all dialogs
                resizable:true,
                startMaximized:false,
                transition:'pulse',

                // notifier defaults
                notifier:{
                    // auto-dismiss wait time (in seconds)
                    delay:5,
                    // default position
                    position:'bottom-right',
                    // adds a close button to notifier messages
                    closeButton: false
                },

                // language resources
                glossary:{
                    // dialogs default title
                    title:'AlertifyJS',
                    // ok button text
                    ok: 'OK',
                    // cancel button text
                    cancel: 'Cancel'
                },

                // theme settings
                theme:{
                    // class name attached to prompt dialog input textbox.
                    input:'ajs-input',
                    // class name attached to ok button
                    ok:'ajs-ok',
                    // class name attached to cancel button
                    cancel:'ajs-cancel'
                }
            };
        }

        function add_msg_point(modalId){
            $("#"+modalId+" #question_message p#point:first").clone().appendTo("#"+modalId+" #question_message div#message_point");
            $("#"+modalId+" #question_message div#message_point p:last input").val('');
        }

        $( " #Modal_add label #add_point" ).click(function() {

            $("#Modal_add #question_message p#point:first").clone().appendTo('#Modal_add #question_message div#message_point');
            $("#Modal_add #question_message div#message_point p:last input").val('');
        })

        //Adding field options
        $( "#add_fld" ).click(function() {

            var fld = $(modalForm +' #fields div.fieldOptions input[name^=code]');
            var config_code = $.parseJSON('{{json_encode(config('poc.question_field_code'))}}');
            var fld_code = config_code[fld.length];

            var d = new Date();
            var timestamp = d.getTime();

            // $("#Modal_add #add_qn_template .fieldOptions input.field_code").val(generateCode());
            $("#Modal_add #add_qn_template .fieldOptions input.field_code").val(fld_code);
            $("#Modal_add #add_qn_template .fieldOptions").attr('id', 'fieldOptions'+timestamp);
            $("#Modal_add #add_qn_template .fieldOptions").clone().appendTo('#Modal_add div#fields').hide().fadeIn(800);


            var caredomain = getCareDomain();
            $('#fields #fieldOptions'+timestamp+' #caredomain').mdb_autocomplete({
                data: caredomain
            });

        })


        $( '#Modal_add select#type' ).change(function(){

            val = $('#Modal_add select#type').val();

            if(val=='checkbox' || val=='radio' || val=='dropdown'){
                $('#Modal_add #question_fields').hide();
//                $('#Modal_add .fieldsOptions').show();
                $('#Modal_add .classic-tabs .nav li#Options').show();
                $('#Modal_add div.tabs ul li:not(:first)').show();

                    $('#Modal_add #question_fields .selectpicklist').show();
                    // $('#Modal_add #question_fields .selectpicklist input#selectpicklist').attr('required', true);

                    var picklists = getpicklist();

                    $('#Modal_add #question_fields .selectpicklist input#selectpicklist').mdb_autocomplete({
                        data: picklists
                    });


                $('#Modal_add .bodymap').hide();
                $('#Modal_add .goal').show();

            }else if(val=='facility'){

                displayFacility();

            }else if(val=='area'){

                displayFacilityAreas();

            }else if(val=='vacantrooms'){

                displayVacantRooms();

            }else if(val=='bodymap'){
                $('#Modal_add #question_fields').show();
                // $('#Modal_add .fieldsOptions').hide();
                $('#Modal_add .classic-tabs .nav li#Options').hide();
                $('#Modal_add div.tabs ul li:not(:first)').hide();
                $('#Modal_add .bodymap').show();
                $('#Modal_add .goal').hide();
            }else{

                $('#Modal_add .bodymap').hide();
                $('#Modal_add .goal').show();
                $('#Modal_add #question_fields').show();
                // $('#Modal_add .fieldsOptions').hide();
                $('#Modal_add .classic-tabs .nav li#Options').hide();
                $('#Modal_add div.tabs ul li:not(:first)').hide();
                $('#Modal_add #question_fields .selectpicklist').hide();
                $('#Modal_add #question_fields .selectpicklist input#selectpicklist').attr('required', false);

            }

            if(val=='message'){
                // $('#Modal_add #question_fields input#question').attr('required', false);
                // $('#froala-editor').froalaEditor('html.set','');
                setHtmlMessage();
                $('#Modal_add div#question_message .question').attr('required', true);
                $('#Modal_add div#question_message').show();
                $('#Modal_add div#question_fields').hide();
                $('#Modal_add input#question').attr('required', false);
            }else{
                // $('#Modal_add #question_fields input#question').attr('required', true);
                $('#Modal_add div#question_fields').show();
                $('#Modal_add div#question_message').hide();
                $('#Modal_add div#question_message .question').attr('required', false);
                $('#Modal_add input#question').attr('required', true);
            }

            $('#Modal_add input#type_hidden').val(val);

        })

        function setHtmlMessage(){
            $('#Modal_add div#question_message #froala-editor').froalaEditor('html.set', '');
            $('#Modal_add div#question_message #froala-editor').froalaEditor('destroy');
            $('#Modal_add div#question_message #froala-editor').froalaEditor({
                toolbarButtons: ['fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough',
                    'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                    'undo', 'redo',
                    'html', 'clearFormatting'
                ],
            });

        }

        function displayPicklist(){

            var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

            // console.log(textlistcurrent);

            // if(textlistcurrent.length > 0){
                $('#Modal_add #fields div.fieldOptions').remove();
            // }

            // var inputname =
            var picklistid = $('#Modal_add #question_fields .selectpicklist input#selectpicklist').attr('selectpicklist_id');

            var url = '{{url('picklist/getpicklist')}}/'+picklistid;
            axios.get(url).then(function(response){

                console.log('fixing..');
                $('#Modal_add #fields div.fieldOptions').remove();

                var lists  = response.data.Lists;
                var textlist = $('#Modal_add #fields div.fieldOptions input[name^=text]');

                $.each(lists, function(k, v){
                    $( "#Modal_add #add_fld" ).trigger('click');
                });

                var textlistnew = $('#Modal_add #fields div.fieldOptions input[name^=text]');
                var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');


                if(textlistnew.length > 0){

                    console.log(textlistnew);
                    $.each(textlistnew, function(k, v){

                        if(typeof(lists[k]) !== 'undefined'){
                            $(this).val(lists[k].text);
                            $(this).attr('picklistid', picklistid+'-'+k);
                        }

                    });
                }


                if(codelist.length > 0) {
                    $.each(codelist, function (k, v) {
                        $(this).val(lists[k].code);
                    });
                }


            })
            // $( "#add_fld" ).trigger('click');
        }

        function displayFacility(){

            var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

            if(textlistcurrent.length > 0){
                $('#Modal_add #fields div.fieldOptions').remove();
            }


            var lists  = getFacilities();
            console.log(lists);
            $.each(lists, function(k, v){
                $( "#Modal_add #add_fld" ).trigger('click');
            });

            var textlistnew = $('#Modal_add #fields div.fieldOptions input[name^=text]');
            var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');

            $.each(textlistnew, function(k, v){
                $(this).val(lists[k].FacilityName);
            });

            $.each(codelist, function(k, v){
                $(this).val(lists[k].FacilityId);
            });

            // $( "#add_fld" ).trigger('click');
        }

        function displayFacilityAreas(){

            var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

            if(textlistcurrent.length > 0){
                $('#Modal_add #fields div.fieldOptions').remove();
            }


            var lists  = getFacilityArea();
            console.log(lists);
            $.each(lists, function(k, v){
                $( "#Modal_add #add_fld" ).trigger('click');
            });

            var textlistnew = $('#Modal_add #fields div.fieldOptions input[name^=text]');
            var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');

            $.each(textlistnew, function(k, v){
                $(this).val(lists[k].AreaName);
            });

            $.each(codelist, function(k, v){
                $(this).val(lists[k].AreaId);
            });

            // $( "#add_fld" ).trigger('click');
        }

        function displayVacantRooms(){

            var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

            if(textlistcurrent.length > 0){
                $('#Modal_add #fields div.fieldOptions').remove();
            }


            var lists  = getVacantRooms();
            $.each(lists, function(k, v){
                $( "#Modal_add #add_fld" ).trigger('click');
            });

            var textlistnew = $('#Modal_add #fields div.fieldOptions input[name^=text]');
            var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');

            $.each(textlistnew, function(k, v){
                $(this).val(lists[k].RoomName);
            });

            $.each(codelist, function(k, v){
                $(this).val(lists[k]._id);
            });

            // $( "#add_fld" ).trigger('click');
        }


        function add_fld_option(modalId){
            var d = new Date();
            var timestamp = d.getTime();

            $('#'+modalId+" div.fieldOptions li").attr('id', 'field'+timestamp);
            $('#'+modalId+" div.fieldOptions li").clone().appendTo('#'+modalId+' ul.fields');

        }


        $( "#add_qn, #add_qn_append" ).click(function() {

            $('#Modal_add .bodymap').hide();
            $('#Modal_add .goal').show();

            var d = new Date();
            var timestamp = d.getTime();

            $('#Modal_add .modal-dialog').attr('id', 'modal-dialog'+timestamp);

            $('#Modal_add input, #Modal_add textarea').each(function(){
                $(this).val('');
            });

            $('#Modal_add input#question').attr('required', true);

            // $("#Modal_add select#type").val("");

            $("#Modal_add select#type").val($("#Modal_add select#type option:first").val());

            $('#Modal_add input#parent_goal').val('-');

            $("#Modal_add select#parent_response").val("").trigger('change');

            $("#Modal_add select#type").val("").trigger('change');

            // $('#Modal_add div#fields').hide();
            $('#Modal_add div#fields div.fieldOptions').remove();

            $("#Modal_add #question_message #message_point p").not(':first').remove();
            $('#Modal_add div.tabs ul li:not(:first)').hide();
            $('#Modal_add div.tabs ul li:not(:first)').attr('class', '');
            $('#Modal_add div.tabs ul li:first').attr('class', 'is-active');

            $('#Modal_add div.type').show();

            var uniquecode = generateCode();
            $('#Modal_add input[name=code]').val(uniquecode);

            setDisplayOrder();

        })


        function add_qn(addtype){
            $('#addQnType').val(addtype);
            $('#Modal_add .modal-title').text('Add Question');
            $('#Modal_add button#save_add_qn').text('Add Question');
            // $('#Modal_add button#save_add_qn').attr('onclick', 'save_add_qn(this, \'add\', \'\')');
            $('#Modal_add select#type').removeClass(' disabled');
            $('#Modal_add').show();

        }

        function apiDeleteQuestion(qId){
            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
            alertify.confirm('{{__('Are you sure you want to delete this question?')}}', function(e){
                if(e){

                    var url='{{url('formeditor/api/deletequestion')}}/'+qId;

                    axios.get(url).then(function(){
                        $( "li#qn"+qId ).remove();
                        $('.modal-backdrop').remove();
                        toastr.success('Successfully deleted.');
                    });


                }else{
                    $('.modal-backdrop').remove();
                }
            });
        }



    </script>

@endsection

@php $type = config('poc.form_editor_types'); ksort($type); @endphp

@include('formeditor.add_question', ['controls' => $questions, 'picklists'=> $picklists, 'type'=>$type])
