@extends('layouts.poc')

@section('style')
    <style>
        ul.nav li.nav-item{
            list-style: none !important;
        }

        [type=radio]+label, [type=checkbox]+label{
            font-size: 13px !important;
        }

        .message {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .message li{
            list-style: square !important;
            margin-left: 20px;
        } 

        ul, li {
            list-style: decimal !important;
            margin-top: 0px;
        }

        li label:first-child {font-weight: 500;}

        li   li {
            list-style: decimal !important;
            margin-left: 30px;
            margin-top: 5px;

        }

        .content:not(:last-child) {
            margin-bottom: 0 !important;
        }

        [type=radio]+label, [type=checkbox]+label{
            height: auto !important;
        }



    </style>
@endsection

@section('content')


    <div class="container-fluid  pt-3">
        <div class="row   pb-0 pt-3 mb-4  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col-3 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{__('Form Preview')}}</strong>
                </h3>
            </div>
            <div class="col text-right">
                <div class="col">
                    <a class="" style="font-size:0.85rem" href="{{url('/form/listing')}}">Back to Forms</i></a>
                    <!-- <a class="btn btn-grey btn-rounded " href="{{url('/form/compare/'.$form->_id)}}"><i class="fa fa-check-square-o"></i></a> -->
                    {{--                        <a class="btn btn-grey btn-rounded" href="{{url('/form/template/'.$form->_id)}}">{{__('Edit Template')}}</a>--}}
                    <a class="btn btn-grey btn-rounded" href="{{url('/form/edit/'.$form->_id)}}">{{__('Edit Header')}}</a>
                    <a class="btn btn-grey btn-rounded" href="{{url('/formeditor/'.$form->_id)}}">{{__('Form Editor')}}</a>

                    <a href=""></a>
                </div>
            </div>
        </div>
        <div class=" bg-white p-3 col">
            <h1 class="title">{{$form->FormName}}</h1>
            <div class="bodymap_checkbox">
                @foreach($bodymap as $k=>$v)
                    <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                    <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                @endforeach
            </div>

            @include('formeditor.question_list', ['questions' => $questions, 'data'=>[]])


            <div class="row">

                {{--                        <a class="btn btn-grey" href="{{url('/form/template/'.$form->_id)}}">Edit again</a>--}}


                <a class="btn btn-grey" href="{{url('/form/listing')}}">Return to Forms</a>

            </div>
        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('js/jquery.maphilight.js')}}"></script>
    <script>

        $.fn.maphilight.defaults = {
            fill: true,
            fillColor: 'ff0000',
            fillOpacity: 0.2,
            stroke: false,
            strokeColor: 'ff0000',
            strokeOpacity: 1,
            strokeWidth: 1,
            fade: true,
            alwaysOn: false,
            neverOn: false,
            groupBy: false,
            wrapClass: true,
            shadow: false,
            shadowX: 0,
            shadowY: 0,
            shadowRadius: 6,
            shadowColor: '000000',
            shadowOpacity: 0.8,
            shadowPosition: 'outside',
            shadowFrom: false
        }

        $(function() {
            $('.bodymap').maphilight();

        });

        function selectbodymap(map){

            var checkbox = $('.bodymap_checkbox input#'+map+':checked');
            var data = $('#'+map).data('maphilight') || {};


            if(checkbox.length > 0){
                $('.bodymap_checkbox input#'+map).removeAttr('checked');
                $('.bodymap_checkbox input#'+map+'_id').removeAttr('checked');

                data.alwaysOn = false;
                $('area#'+map).data('maphilight', data).trigger('alwaysOn.maphilight');
            }else{
                $('.bodymap_checkbox input#'+map+'').attr('checked', 'checked');
                $('.bodymap_checkbox input#'+map+'_id').attr('checked', 'checked');

                data.alwaysOn = true;
                $('area#'+map).data('maphilight', data).trigger('alwaysOn.maphilight');

            }

            var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                return this.value;
            }).get().join(", ");

            var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                return this.value;
            }).get().join(", ");

            $('div#Location').text(data);
            $('input#Location').val(data);
            $('input#Bodymap').val(data_id);
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();
        })

        $(document).ready(function() {

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');
            @foreach($bodymap as $k=>$v)

            $("table.bodymap button#{{$k}}").click(function () {
                $('table.bodymap td button#{{$k}}').tooltip();


                var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

                if(checkbox.length > 0){
                    $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                    $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                    $('table.bodymap button div#{{$k}}').hide();
                }else{
                    $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                    $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                    $('table.bodymap button div#{{$k}}').show();
                }

                var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                $('div#Location').text(data);
                $('input#Location').val(data);
                $('input#Bodymap').val(data_id);

                return false;
            })
            @endforeach
        })


    </script>
    <script>
        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 90,
            // max: true
        });

        $('#timepicker').pickatime(
            {
                // Light or Dark theme
                darktheme: true,
                twelvehour: true,
            });


    </script>

    @if(!empty($form->javascript))
        <script>
            {{$form->javascript}}
        </script>
    @endif
@endsection
