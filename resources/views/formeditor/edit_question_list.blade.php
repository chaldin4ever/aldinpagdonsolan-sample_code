{{--<pre>{{print_r($questions)}}</pre>--}}

    @php $num=0; @endphp

    @if(!empty($questions))

        @if(sizeof($questions)>0)
            <ol id="questions" class="questions" >
            @foreach($questions as $k=>$cnt)

                @php
                    $required = '';
                    $requiredClass = '';
                    if(($cnt->Required ==1))
                    {
                        $required='required';
                        $requiredClass = 'is-required';
                    }
                    $key = $cnt->_id;
                @endphp

                @if($cnt->Type == 'text')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$cnt->_id}}--}}" class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn">{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->Goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <p class="control">
                                    <input class="input form-control" type="text" id="{{$cnt->Code}}"  {{$required}}
                                    name="{{$cnt->Code}}"  value="{{array_get($data, $cnt->Code)}}" style="width:100%" />
                                </p>
                            </div>
                            @include('formeditor.prompt')

                        </div>
                    </li>
                @elseif($cnt->Type == 'upload')
                        <li id="qn{{$cnt->_id}}">
                            <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                            <a href="#" data-toggle="modal" data-target="#Modal_add{{--{{$cnt->_id}}--}}" class="qn"
                               onclick="apiGetQuestion('{{$key}}')"
                               data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                            <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                            <div class="columns row">
                                <div class="field col-8">
                                    <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn">{{$cnt->Question}}</label>
                                    <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->Goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                    <p class="control">
                                        <input class="input form-control" type="file" id="{{$cnt->Code}}"  {{$required}}
                                        name="{{$cnt->Code}}"  value="{{array_get($data, $cnt->Code)}}" style="width:100%" />
                                    </p>
                                </div>
                                @include('formeditor.prompt')

                            </div>
                        </li>
                @elseif($cnt->Type == 'bodymap')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add " class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn" >{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->Goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <p class="control">
                                    @include('template.bodymap_new')
                                    {{--<img src="{{asset('images/bodymap_v2.png')}}" style="width: 50%;"/>--}}
                                </p>
                            </div>
                            @include('formeditor.prompt')

                        </div>


                    </li>
                @elseif($cnt->Type == 'memo')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add " class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row">
                            <div class="field  col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn" >{{$cnt->Question}}</label>
                                <p class="control">
                                    <textarea class="textarea" id="froala-editor" {{$required}} name="{{$cnt->Code}}"  >{{array_get($data, $cnt->Code)}}</textarea>
                                </p>
                            </div>

                            @include('formeditor.prompt')

                        </div>
                    </li>


                @elseif($cnt->Type == 'number')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add " class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row">
                            <div class="field col-8">
                                <label id="label_{{$cnt->_id}}"  class="{{$requiredClass}} label_qn">{{$cnt->Question}}</label>
                                <p class="control">
                                    <input class="input" type="number" {{$required}} name="{{$cnt->Code}}" step="any" value="{{array_get($data, $cnt->Code)}}"
                                           onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46'  style="width:150px"/>
                                </p>
                            </div>

                            @include('formeditor.prompt')
                        </div>
                    </li>


                @elseif($cnt->Type == 'date')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add " class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit  fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row">
                            <div class="field  col-8">
                                <label class="label_qn" id="label_{{$cnt->_id}}"   >{{$cnt->Question}}</label>
                                <p class="control">
                                    <input class="input datepicker form-control" id="datepicker" name="{{$cnt->Code}}" value="{{array_get($data, $cnt->Code)}}"  style="width:150px"/>
                                </p>
                            </div>

                            @include('formeditor.prompt')
                        </div>
                    </li>
                @elseif($cnt->Type == 'time')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add " class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"  ></i></a>

                        <div class="columns row">
                            <div class="field  col-8">
                                <label id="label_{{$cnt->_id}}" class="{{$requiredClass}} label_qn"  >{{$cnt->Question}}</label>
                                <p class="control">
                                    <input class="input form-control  " id="timepicker" {{$required}} name="{{$cnt->Code}}" value="{{array_get($data, $cnt->Code)}}"  style="width:150px"/>
                                </p>
                            </div>

                            @include('formeditor.prompt')

                        </div>
                    </li>

                @elseif($cnt->Type == 'checkbox')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add" class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row">
                            <div class="field  col-8">

                                <label index="{{$cnt->_id}}"  id="label_{{$cnt->_id}}">{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <div class="options">
                                    @if(!empty($cnt->Fields))
                                        @foreach($cnt->Fields as $fld)
                                            <div class="form-row form-check" id="checkbox_{{$fld['code']}}">
                                                <input type="checkbox"  id="{{$fld['code']}}" name="{{$fld['code']}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                                <label class="form-check-label" id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}" >
                                                    {{$fld['text']}}
                                                </label>
                                            </div>
                                        @endforeach
                                      @endif
                                </div>
                            </div>
                            @include('formeditor.prompt')
                        </div>

                    </li>


                @elseif($cnt->Type == 'radio')
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add" class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>

                        <div class="columns row">
                            <div class="field  col-8">

                                <label  index="{{$cnt->_id}}" id="label_{{$cnt->_id}}" qname="{{$cnt->Code}}" >{{$cnt->Question}}</label>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$cnt->goal}}" style="@if($cnt->Goal!='' && $cnt->Goal != '-') display: inline; @else display: none; @endif"><i class="fa fa-asterisk"></i></a>
                                <div class="options">
                                    @if(!empty($cnt->Fields))
                                        @foreach($cnt->Fields as $fld)
                                            <div class="form-row " id="radio{{$fld['code']}}">
                                                <input type="hidden" name="is-remove" class="is-remove" id="is-remove-{{$fld['code']}}" />
                                                <input type="radio"  fld_name="{{$fld['code']}}" fld_code="{{$fld['code']}}" fld_label="{{$fld['text']}}"  id="{{$fld['code']}}" name="{{$cnt->Code}}" class="regular-checkbox" @if(array_get($data, $fld['code'])=='on')checked @endif />
                                                <label  id="{{$fld['code']}}" for="{{$fld['code']}}" index="{{$num}}" >
                                                    {{$fld['text']}}
                                                </label>
                                            </div>
                                        @endforeach
                                     @endif

                                </div>
                            </div>

                            @include('formeditor.prompt')

                        </div>
                    </li>


                @elseif($cnt->Type == 'dropdown' )
                    <li id="qn{{$cnt->_id}}">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add" class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static"  data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                        <div class="columns row" >
                            <div class="field column  col-8">
                                <label id="label_{{$cnt->_id}}"  >{{$cnt->Question}}
                                </label>
                                <p class="control">
                        <span class="select fordropdown">
                            <select name="{{$cnt->Code}}"  style="display: block !important;" class="form-control" >
                                    @if(!empty($cnt->Fields))
                                        @foreach($cnt->Fields as $fld)
                                            <option name="{{$fld['code']}}"  value="{{$fld['code']}}" @if(array_get($data, $cnt->Code)==$fld['code'])selected @endif>{{$fld['text']}}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </span>
                                </p>

                            </div>
                            @include('formeditor.prompt')
                        </div>
                    </li>

                    @elseif($cnt->Type == 'facility' || $cnt->Type == 'area' || $cnt->Type == 'vacantrooms')

                        <li id="qn{{$cnt->_id}}">
                            <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                            <a href="#" data-toggle="modal" data-target="#Modal_add" class="qn"
                               onclick="apiGetQuestion('{{$key}}')"
                               data-backdrop="static"  data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                            <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>
                            <div class="columns row" >
                                <div class="field column  col-8">
                                    <label id="label_{{$cnt->_id}}"  >{{$cnt->Question}}
                                    </label>
                                    <p class="control">
                        <span class="select fordropdown">
                            <select name="{{$cnt->Code}}"  style="display: block !important;" class="form-control" >

                                @if(!empty($cnt->Fields))
                                    <option value="" >{{__('Choose..')}}</option>
                                    @foreach($cnt->Fields as $fld)
                                    <option name="{{$fld['code']}}"  value="{{$fld['code']}}" @if(array_get($data, $cnt->Code)==$fld['code'])selected @endif>{{$fld['text']}}</option>
                                     @endforeach
                                 @endif
                            </select>
                        </span>
                                    </p>

                                </div>

                                @include('formeditor.prompt')
                            </div>
                        </li>
                @elseif($cnt->Type == "message")
                    <li id="qn{{$cnt->_id}}" style="list-style: none;" value="0" class="message">
                        <input type="hidden" id="qnId"  name="qnId[]" value="{{$cnt->_id}}"/>
                        <a href="#" data-toggle="modal" data-target="#Modal_add" class="qn"
                           onclick="apiGetQuestion('{{$key}}')"
                           data-backdrop="static" and data-keyboard="false"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="#" class="qn_trash" onclick="apiDeleteQuestion('{{$cnt->_id}}')" id="qn_trash_{{$cnt->_id}}"><i class="fa fa-trash"></i></a>

                        <div class="columns">
                            <div class="field col-9">
                                <div class="content">
                                    {!!$cnt->Question!!}
                                </div>
                               {{-- <label class="content label_qn" >
                                    {!!$cnt->Question!!}
                                </label>--}}

                            </div>
                        </div>
                    </li>
                @else
                @endif
                @php $num++ @endphp
            @endforeach
            </ol>
        @else

            <div class="alert alert-warning align-middle"><strong><i class="fa fa-info-circle fa-2x mr-4"></i> {{__('No questions available.')}}</strong> </div>

        @endif


    @endif

