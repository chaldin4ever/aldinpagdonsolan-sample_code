@extends('layouts.poc')

@section('style')
    <style>

        [type=radio]+label, [type=checkbox]+label{
            height: auto !important;
        }

        .white-skin input[type=checkbox].filled-in:checked+label:after {
            background-color: rgb(90, 118, 147);
            border-color: rgb(90, 118, 147);
        }
        .white-skin body{
        'Helvetica Light', Helvetica, Arial, sans-serif
        }

        .nav-link, .navbar {
            padding: 1.2rem .2rem;
            font-weight: normal;
        }



        table.table {
            border: 1px solid #d9d9d9 !important;
            background-color: #fbfbfb;
        }

        .card {border-radius: 0 !important;}

        .bg-light {background: #f4f4f4 !important;}
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-1 pt-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{$form->FormName}}</strong></h3>
                <a href="{{url('resident/view/'.$resident->_id.'?view='.Request::get('view'))}}" class="float-right back-to-prev-page">Back to resident view</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white pl-2 pt-2 pb-2 mb-3">

            @include('resident.header', ['resident' => $resident])

        </div>

        <div class="content m-0   pb-5">

            <div class="row bg-white pt-4  pl-4 pb-4">

                <div class="col">
                    <div class="bodymap_checkbox">
                        @foreach($bodymap as $k=>$v)
                            <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                            <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                        @endforeach
                    </div>
                    <form method="post" action="{{url('/assessment/store')}}" id="assessment-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="residentId" value="{{$resident->_id}}" />
                        <input type="hidden" name="formId" value="{{$form->_id}}" />

                        {{--{{array_get($assessment->data, 'Bodymap')}}
                        <pre>{{print_r($bodymap)}}</pre>--}}
                        @include('formeditor.viewform', ['assessment' => $assessment])

                        <div class="row mt-4">
                            {{--<button class="btn btn-rounded btn-danger">{{__('Save')}}</button>--}}
                            <a href="{{url('resident/view/'.$resident->_id.'?view='.Request::get('view'))}}" class="btn btn-rounded btn-grey">{{__('Go Back')}}</a>
                        </div>


                    </form>
                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('js/jquery.maphilight.js')}}"></script>
    <script>

        $.fn.maphilight.defaults = {
            fill: true,
            fillColor: '8b0000',
            fillOpacity: 0.2,
            stroke: true,
            strokeColor: '8b0000',
            strokeOpacity: 0.6,
            strokeWidth: .4,
            fade: true,
            alwaysOn: false,
            neverOn: false,
            groupBy: false,
            wrapClass: true,
            shadow: false,
            shadowX: 0,
            shadowY: 0,
            shadowRadius: 6,
            shadowColor: '000000',
            shadowOpacity: 0.8,
            shadowPosition: 'outside',
            shadowFrom: false
        }

        $(function() {
            $('.bodymap').maphilight();

        });

        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 90
        });

        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        var hour = dt.getHours() + ":" + '00';

        $('#timepicker').pickatime(
            {
                default: hour,
                darktheme: true,
                twelvehour: true,
            });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();
        })

        $(document).ready(function() {

            $('.bodymap  #Location').text('{{array_get($assessment->data, 'Location')}}');

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');

            @php $bmArr = explode(',',str_replace(' ','',array_get($assessment->data, 'Bodymap'))); @endphp


            @foreach($bodymap as $k=>$v)
            var data = $('#{{$k}}').data('maphilight') || {};
            @if(in_array($k, $bmArr))
                data.alwaysOn = true;
                $('area#{{$k}}').data('maphilight', data).trigger('alwaysOn.maphilight');
                {{--$('table.bodymap button div#{{$k}}').show();--}}

            @endif

            $("table.bodymap button#{{$k}}").click(function () {
                return false;
            })

            @endforeach
        })

    </script>

    @if(!empty($form->javascript))
        <script>
            {{$form->javascript}}
        </script>
    @endif
@endsection
