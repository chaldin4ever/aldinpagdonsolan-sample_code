@extends('layouts.poc')

@section('style')
    <link rel="stylesheet" href="{{url(asset('codemirror/lib/codemirror.css'))}}">
    <link rel="stylesheet" href="{{url(asset('codemirror/addon/lint/lint.css'))}}">
    <style type="text/css">
        .CodeMirror {border: 1px solid #78909c; font-size:13px; width: 96%;}
    </style>
@endsection

@section('content')


    <div class="container-fluid  pt-3">
        <div class="row   pb-0 pt-3 mb-4  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-file-text-o"></i> {{__('Javascript')}}</strong>
                </h3>
                <a class="btn btn-grey float-right" href="{{url('/form/listing')}}">Return to Forms</a>
            </div>

        </div>
        <div class=" bg-white p-3 col">
            <form method="post" name="javascript" action="{{url('formeditor/javascript/save/'.$form->_id)}}">
                @csrf
               <div class="row pt-4 pl-4 pb-0"> <textarea id="javascript" name="javascript" rows="10" class="   p-3 mr-4 w-100">{{$form->javascript}}</textarea></div>


                <div class="row p-4 pt-0">
                <button type="button" class="btn btn-blue-grey btn-rounded float-left" onclick="validateJS();">{{__('Save')}}</button>
                </div>

            </form>
        </div>


    </div>


@endsection

@section('script')

    <script src="{{url(asset('codemirror/lib/codemirror.js'))}}"></script>
    <script src="{{url(asset('codemirror/mode/javascript/javascript.js'))}}"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jshint/2.9.5/jshint.min.js"></script>
    <script src="//rawgithub.com/zaach/jsonlint/79b553fb65c192add9066da64043458981b3972b/lib/jsonlint.js"></script>
    <script src="https://unpkg.com/csslint@1.0.5/dist/csslint.js"></script>
    <script src="{{url(asset('codemirror/addon/lint/lint.js'))}}"></script>
    <script src="{{url(asset('codemirror/addon/lint/javascript-lint.js'))}}"></script>
    <script src="{{url(asset('codemirror/addon/lint/json-lint.js'))}}"></script>
    <script src="{{url(asset('codemirror/addon/lint/css-lint.js'))}}"></script>


{{--    <script src="{{url(asset('codemirror/addon/selection/active-line.js'))}}"></script>
    <script src="{{url(asset('codemirror/addon/edit/matchbrackets.js'))}}"></script>--}}

    <script>

        var editor = CodeMirror.fromTextArea(document.getElementById("javascript"), {
            lineNumbers: true,
            mode: "javascript",
            gutters: ["CodeMirror-lint-markers"],
            lint: true
        });

        $(document).ready(function(){
            $('.fr-toolbar button#html-1').trigger('click');

            @if(session('status'))
                toastr.success('{{session('status')}}');
            @endif

        })

        $(function() {
            $('.jscript').froalaEditor({
                codeMirror: false,
                pluginsEnabled: ['codeView']
            })
        })

        function validateJS(){

            var error = $('.CodeMirror-lint-marker-error').length;
            var warning = $('.CodeMirror-lint-marker-warning').length;

            var checkerror = error + warning;
            // console.log(checkerror);

            if(error > 0){
                toastr.error("Fix error in your script.");
                return false;
            }else if(warning > 0){
                toastr.warning("Fix warning in your script.");
                return false;
            }else{
                $('form[name=javascript]').submit();
                // console.log($('textarea#javascript').val());
                /*if($('textarea#javascript').val().length > 0){
                    $('form[name=javascript]').submit();
                }else{
                    toastr.warning("Please fill up the textarea.");
                }*/

            }

        }
    </script>
@endsection
