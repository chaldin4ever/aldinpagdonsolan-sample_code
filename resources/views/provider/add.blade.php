<!-- Modal -->
<div class="modal fade" id="addProviderModal" tabindex="-1" role="dialog" aria-labelledby="addProviderModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" action="{{url('provider/store')}}" id="provider" >
            @csrf
{{--            {{csrf_field()}}--}}
            <input type="hidden" name="_id" id="_id" />
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-2 bg-dark text-white">
                    <h5 class="modal-title"  >{{__('Add Provider')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col">
                            {{--<input type="hidden" name="doctorId" value="{{$doctor->_id}}" />--}}
                            <div class="md-form">
                                <input required type="text" id="ProviderName" name="ProviderName" value="" autocomplete="off" class="form-control" placeholder=" ">
                                <label for="ProviderName">{{__('Provider Name')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="ContactName" name="ContactName" value="" class="form-control" autocomplete="off" placeholder=" ">
                                <label for="ContactName">{{__('Contact Name')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="ContactEmail" name="ContactEmail" value="" class="form-control" autocomplete="off" placeholder=" ">
                                <label for="ContactEmail">{{__('Contact Email')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="ContactPhone" name="ContactPhone" value="" class="form-control" autocomplete="off" placeholder=" ">
                                <label for="ContactPhone">{{__('Contact Phone')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="State" name="State" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="suburb">{{__('State')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="Suburb" name="Suburb" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="suburb">{{__('Suburb')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="PostCode" name="PostCode" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="PostCode">{{__('Post Code')}}</label>
                            </div>
                        </div>

                        <div class="col">
                            <div class="md-form" id="facility">
                                {{--<input type="text" id="Facility" name="Facility" value="" class="form-control" autocomplete="off"  placeholder=" ">--}}
                                {{--<textarea id="Facility" name="Facility" multiple class="mdb-select"/>--}}
                                <select multiple class=" mdb-select " name="Facility[]" id="Facility">
                                    <option disabled>Select</option>
                                    @foreach($facility as $f)
                                            <option value="{{$f->_id}}">{{$f->NameLong}}</option>
                                        @endforeach
                                </select>
                                <label for="Facility">{{__('Facility')}}</label>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2 pl-0 ">
                    <div class="row w-100">
                        <div class="col-2">
                            <button type="button" id="archive" class="btn btn-danger mr-3 float-left" onclick="archiveProvider()">
                                <i class="fa fa-trash"></i> {{ __('Archive') }}
                            </button>
                        </div>
                        <div class="col">
                        <button type="button" id="cancel"  class=" btn btn-grey float-right" data-dismiss="modal" >
                            <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                        <button type="submit" id="save" class="float-right btn btn-primary mr-3 ">
                            {{ __('Save') }}
                        </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>