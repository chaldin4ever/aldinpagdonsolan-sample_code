@extends('layouts.poc')

@section('style')
<style>

    tr.archive , tr.archive div{
        text-decoration: line-through !important;
    }

    #addProviderModal div#facility .dropdown-content   label {
        top: .25rem !important;
    }

</style>
@endsection

@section('content')

<div class="container-fluid mt-1">
    <div class="row  pl-4 pb-2 pt-4 " >
        <div class="col pl-0">
            @php
                $key = 'current.provider.'.Auth::user()->_id;
                //echo $key;
                //echo Redis::get($key);
            @endphp

            <h3 class="p-2 float-left"><strong><i class=" fa fa-building "></i> {{__('Provider')}}</strong></h3>

            <a data-toggle="modal" data-target="#addProviderModal" class="float-right btn btn-blue-grey pl-3 pr-3"
               onclick="    clearform();
                            $('#addProviderModal .modal-title').text('Add Provider');
                            $('#addProviderModal button#archive').hide();
                    "
               style=" color: #fff;">
                <i class="fa fa-plus" ></i> {{__('New')}}
            </a>
        </div>
    </div>
</div>

<div class="container-fluid">

    <div class="row bg-white">
        <div class="col pt-3">

            @if(!empty($provider))
                @if(sizeof($provider) > 0)
                <table class="table table-striped">
                <thead>
                <tr>
                    <th>{{__('Provider Name')}}</th>
                    <th>{{__('Contact Name')}}</th>
                    <th>{{__('Contact Email')}}</th>
                    <th>{{__('Contact Phone')}}</th>
                    <th>{{__('State')}}</th>
                    <th>{{__('Suburb')}}</th>
                    <th>{{__('Postcode')}}</th>
                    <th>{{__('Facility List')}}</th>
                    <th>{{__('Action')}}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($provider as $list)
                    <tr
                            @if(isset($list->Archive))
                            @if($list->Archive == true)
                            class=" archive";
                            @endif
                            @endif
                    >
                        <td>{{$list->ProviderName}}</td>
                        <td>{{$list->ContactName}}</td>
                        <td>{{$list->ContactEmail}}</td>
                        <td>{{$list->ContactPhone}}</td>
                        <td>{{$list->State}}</td>
                        <td>{{$list->Suburb}}</td>
                        <td>{{$list->PostCode}}</td>
                        <td>
                            {{implode(', ', $list->Facilities)}}
                        </td>
                        <td><a href="#" id="{{$list->_id}}"  data-toggle="modal" data-target="#addProviderModal"
                               class=" text-center ml-3"
                               onclick="getProvider('{{$list}}')"><i class="fa fa-edit fa-1x " style="color: #003056; font-size: 21px;"></i></a>

                            <a href="{{url('provider/config/'.$list->_id)}}"
                               class=" text-center ml-3"><i class="fa fa-cog fa-1x " style="color: #003056; font-size: 21px;"></i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
                    @else
                        <div class="alert alert-info"><strong>{{__('No available record(s).')}}</strong></div>

                    @endif
                @endif
        </div>
    </div>

    @include('provider.add')
</div>


@endsection

@section('script')
<script>

    $(document).ready(function(){

        $('.mdb-select').material_select();

        @if(session('status'))
            toastr.success('{{session('status')}}')
        @endif
    })

    function clearform(){

        $('#addProviderModal input#_id').val('');
        $('#addProviderModal input#ProviderName').val('');
        $('#addProviderModal input#ContactName').val('');
        $('#addProviderModal input#ContactEmail').val('');
        $('#addProviderModal input#ContactPhone').val('');
        $('#addProviderModal input#State').val('');
        $('#addProviderModal input#Suburb').val('');
        $('#addProviderModal input#PostCode').val('');

        reset();

    }

    function reset(){

        $('#addProviderModal .mdb-select').material_select('destroy');
        $('#addProviderModal .mdb-select').material_select();
        $('.select-dropdown li.active').trigger('click');
        // $('.select-dropdown').val('Select');

    }

    function getProvider(data){

        $('#addProviderModal button#archive').show();

        var rData = jQuery.parseJSON(data);

        // console.log($('#addProviderModal select[name^=Facility]').val());

        reset();
        $('.modal-title').text('Update Provider');

        $.each(rData, function(key, val){

            $('#'+key).val(val);

            if(key === 'Facility') {

                $.each(val, function(k,v){
                    $('#addProviderModal .select-dropdown li:contains(' + v.FacilityName + ')').trigger('click');
                })

            }

        })
    }


    function archiveProvider(){
        $( "#addProviderModal " ).append( "<div class='modal-backdrop fade show'></div>" );

        var id = $('#addProviderModal input#_id').val();
        var url = '{{url('provider/archive')}}/'+id;

        alertify.confirm('Are you sure?', function(e){

            if(e){
                window.location.href = url;
            }

            $('#addProviderModal .modal-backdrop').fadeOut("slow");
        })
    }


</script>
@endsection

