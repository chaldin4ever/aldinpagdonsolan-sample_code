@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-cog  "></i> {{$provider->ProviderName}} {{__('Configuration')}}</strong></h3>
                <a href="{{url('provider')}}" class="float-right pt-2" style="font-size: 0.85rem;">Back to provider listing</a>
            </div>

{{--            @php $key = 'current.provider.' . Auth::user()->_id; echo Redis::del($key); @endphp--}}
        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white p-4">
            <div class="col pt-3">

                <div class="form-check">
                    <input type="checkbox" name="configPublicForm" value="AllowPublicForms" class="form-check-input" onchange="setconfig(this)" @if($provider->AllowPublicForms == 'true') checked @endif id="config1">
                    <label class="form-check-label" for="config1">Allow public forms to be used</label>
                </div>

                <div class="form-check">
                    <input type="checkbox" name="configPublicProcess" value="AllowPublicProcesses" onchange="setconfig(this)"  @if($provider->AllowPublicProcesses == 'true') checked @endif class="form-check-input" id="config2">
                    <label class="form-check-label" for="config2">Allow public processes to be used</label>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script>

        function setconfig(config){

            if(config.checked){
                var cnf = true;
            }else{
                var cnf = false;
            }
            console.log(cnf);
            var url = '{{url('provider/setconfig')}}';

            axios.post(url, {
                ProviderId: '{{$provider->_id}}',
                ConfigName: config.value,
                ConfigValue: cnf
            }).then(function(response){

                toastr.success('Successfully configured.');

            })
        }
    </script>
@endsection

