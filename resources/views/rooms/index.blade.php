@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><i class="fa fa-th"></i> {{$facility->NameLong}} <strong>{{__('Rooms')}}</strong></h3>
                <a class="float-right btn mdb-color" href="{{url('rooms/add/'.$facilityId)}}"><i class="fa fa-plus"></i> New</a>
                <a class="float-right  btn mdb-color" href="{{url('facility')}}">Return to Facility</a>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">
                @if(!empty($rooms))
                    @if(sizeof($rooms) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 150px;">{{__('Display Order')}}</th>
                            <th>{{__('Room Name')}}</th>
                            <th>{{__('Facility')}}</th>
                            <th>{{__('Area')}}</th>
                            <th>{{__('Vacant')}}</th>
                            <th>{{__('Current Resident')}}</th>
                            <th style="width: 70px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($rooms as $room)
                        <tr>
                            <td>{{$room->DisplayOrder}}</td>
                            <td>{{$room->RoomName}}</td>
                            <td>{{$room->FacilityName}}</td>
                            <td>{{$room->AreaName}}</td>
                            <td>{{$room->VacantName}}</td>
                            <td>{{$room->ResidentName}}</td>
                            <td>{{--<a href="{{url('rooms/edit/'.$room->_id)}}"><i class="fa fa-edit" style="font-size: 20px;"></i></a>--}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @else
                    <div class="alert alert-info">
                       No available rooms.
                    </div>
                    @endif
                    @endif
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script>

            @if(session('status'))
                toastr.success('{{session('status')}}');
             @endif
    </script>
@endsection

