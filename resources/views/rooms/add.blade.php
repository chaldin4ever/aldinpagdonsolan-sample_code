@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><i class="fa fa-th"></i> {{$selected_facility->NameLong}} <strong>{{__('New Room')}}</strong></h3>
                <a class="float-right  btn mdb-color" href="{{url('rooms/'.$facilityId)}}">Return to Rooms</a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <form  method="post" action="{{url('rooms/store')}}">
            @csrf
            <input type="hidden" name="roomId" value="@if(!empty($room)){{$room->_id}}@endif" />
        <div class="row bg-white p-4">

            <div class="col-5 pt-3 pl-lg-4">


                    <div class="md-form">

                        <select required class="mdb-select" name="Facility" id="Facility" onchange="getArea(this.value)" >
                            @foreach($facility as $fa)
                                <option value="{{$fa->_id}}" @if($fa->_id == $facilityId) selected @endif>{{$fa->NameLong}}</option>
                            @endforeach
                        </select>
                        <label for="Facility">{{__('Facility')}}</label>
                    </div>

                    <div class="md-form area"  style="margin-top:0rem">
                        <select name="Area" required id="Area" class="mdb-select  "  style="display: block !important; margin-top: -20px; width: 0 ; height: 0;" placeholder=" " >
                            <option value="" selected disabled>--Select Area--</option>

                        </select>
                        <label for="Area">{{__('Areas')}}</label>
                    </div>

                    <div class="md-form">
                        <input required type="text" id="RoomName" name="RoomName" value="@if(!empty($room)){{$room->RoomName}}@endif"  autocomplete="off" class="form-control" placeholder=" ">
                        <label for="RoomName">{{__('Room Name')}}</label>
                    </div>

                    <div class="md-form">
                        <input required type="text" id="DisplayOrder" name="DisplayOrder" value="@if(!empty($room)){{$room->DisplayOrder}}@else {{$order}} @endif"  autocomplete="off" class="form-control" placeholder=" ">
                        <label for="DisplayOrder">{{__('Display Order')}}</label>
                    </div>


                    <div class="md-form">
                        <button class="btn mdb-color" type="submit">Save</button> <a href="{{url('rooms')}}" class="btn btn-danger">Cancel</a>
                    </div>

            </div>
                <div class="col-5 pt-3 pl-lg-4">
                    <div class=" ">
                        <label for="Vacant" class="  mr-3">{{__('Vacant')}}</label><br />
                        <div class="switch  ">
                            <label>
                                No
                                <input  type="checkbox" name="Vacant" checked>
                                <span class="lever"></span> Yes
                            </label>
                        </div>
                    </div>

                    <div class="md-form">
                        <select  class="mdb-select" name="CurrentResident" searchable="Search here..">
                            <option selected disabled>Select resident</option>
                            @foreach($residents as $resident)
                                <option value="{{$resident->_id}}">{{$resident->FullName}}</option>
                            @endforeach
                        </select>
                        <label for="Current Resident">{{__('Current Resident')}}</label>
                    </div>
                </div>

        </div>
        </form>
    </div>


@endsection

@section('script')
    <script>

        $(document).ready(function(){

            $('.mdb-select').material_select();

            var facilityId = '{{$facilityId}}';
            getArea(facilityId);

            @if(!empty($room))
                $(' div.area .select-dropdown li:contains({{$room->AreaName}})').trigger('click');
            @endif

            // $('select#Area').css('display', 'block !important')

        })

        function getArea(facilityId){

            var areaid = '{{Request::get('Area')}}' ? '{{Request::get('Area')}}' : 'null';
            var url='{{url('rooms/getarea')}}/'+facilityId+'/'+areaid;

            axios.get(url).then(function(response){


                var data = response.data;
                $(' .area').html(data);
                $('.mdb-select-area').material_select();
            });
        }


    </script>
@endsection

