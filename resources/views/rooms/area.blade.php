<select name="Area" id="Area"  required class=" mdb-select-area"  placeholder=" "  style="display: block !important; margin-top: -25px; width: 0 ; height: 0;" >
    <option value="" selected disabled>Select Area</option>
    @foreach($areas as $area)
        <option value="{{array_get($area, 'AreaId')}}" @if($areaId == array_get($area, 'AreaId')) selected @endif>{{array_get($area, 'AreaName')}}</option>
    @endforeach
</select>
<label for="Area">{{__('Areas')}}</label>
