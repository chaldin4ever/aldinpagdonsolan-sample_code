@extends('layouts.front')

@section('content')

<div class="container">
   <div class="content">

       @include('welcome.in-total-control')

       <hr/>

       @include('welcome.staff-education')

       <hr/>

       @include('welcome.emma-integration')



        <footer class="footer">
            <div class="company-copyright">
                &copy; 2018 {{env("COMPANY_NAME")}}
            </div>
            <div class="build-number">
                Build {{env('BUILD_NUMBER')}}
            </div>
        </footer>

    </div>
</div>




            

        

@endsection