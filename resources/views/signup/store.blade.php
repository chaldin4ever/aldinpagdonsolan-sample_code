@extends('layouts.mail')

@section('content')


<br/>
<br/>
<br/>
<div class="container">
   <div class="content">
       <div class="row">
            <div class="col-8 Step-up-and-get-resu" style="color:#bbc1c1">
                Stay cool <br/>&amp; stay focused
            </div>
            <div class="col-4">
                
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <div class="col">
                        <div class="sign-me-up" style="color:#000000">Thank you for signing up. 
                            <br/>
                            You will receive an email shortly to confirm your email address.
                            <br/>


                            <a href="{{'/'}}" style="color:blue">Return to home</a>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <div class="col-6">
                <img src="{{asset('images/group-9.png')}}" width="420px"/>
            </div>
        </div>

        <div class="green-wall-panel">
        </div>

        <footer class="footer">
            <div class="company-copyright">
                &copy; 2018 {{env("COMPANY_NAME")}}
            </div>
            <div class="build-number">
                Build {{env('BUILD_NUMBER')}}
            </div>
        <footer>
        



    </div>
</div>

@endsection