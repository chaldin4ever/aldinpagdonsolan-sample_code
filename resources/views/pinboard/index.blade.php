@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection

@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-thumb-tack"></i> {{__('Pinboard')}}</strong></h3>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">
                <table class="table table-striped table-sm">
                    <thead class="black text-white">
                    <tr>
                        <th>Resident Name</th>
                        <th>Room</th>
                        <th>Note</th>
                        <th>Created By</th>
                        <th></th>
                    </tr>
                    </thead>
                    @foreach($items as $item)
                        @php
                            $resident = $item->Resident;
                            $urlResident = url("resident/view")."/".array_get($resident, "ResidentId");
                            if(empty($item->target_form))
                                $url = "";
                            else
                                $url = url('assessmentform/selectform')."/".array_get($resident, "ResidentId")."/".$item->target_form;
                        @endphp
                        <tr>
                            <td><a href="{{$urlResident}}">{{array_get($resident, 'ResidentName')}}</a></td>
                            <td>{{array_get($resident, 'Room')}}</td>
                            @if(empty($url))
                                <td>{!! $item->note !!}</td>
                            @else
                                <td>{!! $item->note !!} <a href="{{$url}}"><i class="fa fa-external-link"></i></a></td>
                            @endif
                            <td>
                                {{array_get($item->CreatedBy, 'FullName')}} on {{$item->created_at}}
                            </td>
                            <td>
                                <a href="{{url('pinboard/view/'.$item->_id)}}"><i class="fa fa-eye"></i> View</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script>

    </script>
@endsection
