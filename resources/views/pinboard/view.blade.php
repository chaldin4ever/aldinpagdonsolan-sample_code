@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-thumb-tack"></i> {{__('View Pinboard Item')}}</strong></h3>
{{--                <a href="{{url('pinboard')}}" class="btn mdb-color float-right">Back</a>--}}
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col-5 justify-content-center pt-3">
                @php
                        $resident = $item->Resident;
                        $urlResident = url("resident/view")."/".array_get($resident, "ResidentId");
                        if(empty($item->target_form))
                            $url = "";
                        else
                            $url = url('assessmentform/selectform')."/".array_get($resident, "ResidentId")."/".$item->target_form;
                @endphp

                <table class="  table table-striped ">
                    <tr>
                        <th class="font-weight-bold">{{__('Resident Name')}}</th>
                        <td><a href="{{$urlResident}}">{{array_get($resident, 'ResidentName')}}</a></td>
                    </tr>
                    <tr>
                        <th  class="font-weight-bold">{{__('Room')}}</th>
                        <td>{{array_get($resident, 'Room')}}</td>
                    </tr>
                    <tr>
                        <th  class="font-weight-bold">{{__('Note')}}</th>

                        @if(empty($url))
                            <td>{!! $item->note !!}</td>
                        @else
                            <td>{!! $item->note !!} <a href="{{$url}}"><i class="fa fa-external-link"></i></a></td>
                        @endif
                    </tr>
                    <tr>
                        <th  class="font-weight-bold">{{__('Created By')}}</th>
                        <td>{{array_get($item->CreatedBy, 'FullName')}} on {{$item->created_at}}</td>
                    </tr>

                    @if(!empty($item->ClosedBy))
                    <tr>
                        <th  class="font-weight-bold">{{__('Closed By')}}</th>
                        <td>{{array_get($item->ClosedBy, 'FullName')}} on {{$item->updated_at}}</td>
                    </tr>
                    @else
                    <tr>
                        <th></th>
                        <td><button class="btn mdb-color" onclick="close_item()">Close this item</button></td>
                    </tr>
                        @endif
                </table>

            </div>
        </div>
    </div>


@endsection

@section('script')
    <script>

        $(document).ready(function(){
            @if(session('status'))
                toastr.success('{{session('status')}}');
            @endif
        })

        function close_item(){
            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            alertify.confirm('Are you sure?', function (e){


                if(e){
                    var url = '{{url('pinboard/close/')}}/{{$item->_id}}';
                    window.location.href = url;
                }

                $('.modal-backdrop').fadeOut("slow");
            })
        }

    </script>
@endsection

