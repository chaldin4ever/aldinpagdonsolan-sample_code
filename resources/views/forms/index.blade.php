@extends('layouts.poc')
@section('style')

    <style>

    </style>
@endsection

@section('content')

    <div class="container-fluid bg-white pt-5">
        <div class="row   pb-0 pt-3 mb-4  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{__('Forms')}}</strong></h3>
            </div>

            <div class="col-2 pl-0">
                <div class="fixed-action-btn" style="top: 90px; right: 75px;">
                    <a data-toggle="modal" id="addform" data-target="#addFormModal" class="btn-floating btn-lg "
                       style=" color: #fff;background: #92ca46 !important;" onclick="
                       $('#addFormModal .modal-title').text('Add Form');
                       $('#addFormModal #formId').val('');
                        $('#addFormModal form')[0].reset();
                        ">
                        <i class="fa fa-plus"  data-toggle="tooltip" data-placement="left" title="{{__('Add Form')}}"></i>
                    </a>

                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="content   pb-5">

            <div class="row bg-white">
                <div class=" col m-0 p-0">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th style="width: 150px;">{{__('Form Code')}}</th>
                                <th class="w-50">{{__('Form Name')}}</th>
                                <th class="w-auto">{{__('Category')}}</th>
                                <th style="width: 350px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                    @foreach($forms as $form)
                        <tr>
                            <td>{{$form->FormCode}}</td>
                            <td><a  data-toggle="modal" id="addform" data-target="#addFormModal"
                                    onclick="
                                            $('#addFormModal #formId').val('{{$form->_id}}');
                                            $('#addFormModal .modal-title').text('Update Form');
                                            $('#addFormModal #formname').val('{{$form->FormName}}');
                                            $('#addFormModal #formcode').val('{{$form->FormCode}}');
                                            var category = '{{$form->Category}}';
                                            $('#addFormModal .category .select-dropdown li:contains('+category+')').trigger('click');

                                            var lang = '{{$form->Language}}';
                                            $('#addFormModal .language .select-dropdown li:contains('+lang+')').trigger('click');

                                            var role = '{{$form->Role}}';
                                            $('#addFormModal .role .select-dropdown li:contains('+role+')').trigger('click');

                                            "
                                ><i class="fa fa-edit"></i> {{$form->FormName}}</a></td>
                            <td>{{$form->Category}}</td>
                            <td></td>
                        </tr>
                    @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('forms.add-modal')
@endsection

@section('script')
    <script>
        $('#addform').click(function () {
            $('#addFormModal').modal({show : true});
        });

        $(document).ready(function() {
            $('.mdb-select').material_select();
        });

        @if(session('status'))
            toastr.success('{{session('status')}}').css("width","auto");
        @endif
    </script>

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="http://formbuilder.online/assets/js/form-builder.min.js"></script>

    <script>
        jQuery(function($) {
            $(document.getElementById('fb-editor')).formBuilder();

        });

        $(document).ready(function(){
            $('#fb-editor div.stage-wrap ul.frmb').attr('style', 'min-height: 300px;');
        })
    </script>
@endsection

