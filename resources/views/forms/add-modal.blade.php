<!-- Modal -->
<div class="modal fade" id="addFormModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fluid" role="document">
        <form class="form-horizontal" id="action-form" action="{{url('form/add')}}" method="post">
            {{--@csrf--}}
            {{csrf_field()}}
            <input required type="hidden" id="formId" name="formId" class="form-control">
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-0">
                    <h5 class="modal-title"  >{{__('Add Form')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">

                    <div class="row">
                        <div class="col-3 border-right-1 mr-3">
                            <div class="md-form ">
                                <input required type="text" id="formname" name="FormName" value="" class="form-control" placeholder=" ">
                                <label for="formname">{{__('Form Name')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="formcode" name="FormCode" value="" class="form-control " placeholder=" ">
                                <label for="formcode">{{__('Form Code')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="parentform" name="ParentForm" value="" class="form-control" placeholder=" ">
                                <label for="parentform">{{__('Parent Form')}}</label>
                            </div>

                            <div class="md-form  category">

                                <select class="mdb-select" name="Category" required>
                                    <option value="" disabled selected>Choose </option>
                                    <option value="Form">Form</option>
                                    <option value="Assessment">Assessment</option>
                                    <option value="Charting">Charting</option>
                                </select>
                                <label>{{__('Category')}}</label>
                            </div>

                            <div class="md-form language">

                                <select class="mdb-select" name="Language">
                                    <option value="" disabled  >Select </option>
                                    <option value="en">{{__('English')}}</option>

                                </select>
                                <label>{{__('Language')}}</label>
                            </div>
                            <div class="md-form role">

                                <select class="mdb-select" name="Role">
                                    <option value="" disabled  >Select Role</option>

                                </select>
                                <label>{{__('Role')}}</label>
                            </div>

                        </div>

                        <div class="col">

                            <div id="fb-editor"></div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn btn-primary mr-3 ">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>