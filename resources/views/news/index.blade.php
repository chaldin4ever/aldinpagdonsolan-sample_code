@extends('layouts.poc')

@section('style')
    <style>

        .archive{ text-decoration: line-through;}
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{__('News')}}</strong></h3>
                <button type="button" onclick="reset()" data-toggle="modal" data-target="#addNewsModal" class="float-right btn btn-mdb-color">Add News</button>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">

                @if(!empty($news))
                    @if(sizeof($news) > 0)
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 320px;">News Title</th>
                                    {{--<th style="width: 380px;">Description</th>--}}
                                    <th>Expiry Date</th>
                                    <th>Created By</th>
                                    <th>Announcement</th>
                                    <th>Status</th>
                                    <td style="width: 100px"></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $n)
                                    @php
                                            $format = Auth::user()->date_format;
                                            $expdate = new Carbon\Carbon(\App\Utils\Toolkit::UTCDateTimeToYYMMDD($n->ExpiryDate));
                                    @endphp
                                    <tr class="@if($n->Archive == true) archive @endif">
                                        <td>{{$n->NewsTitle}}</td>
                                        {{--<td>{{strip_tags($n->Description) }}</td>--}}
                                        <td>@if(!empty($format))
                                                {{$expdate->format($format)}}
                                            @else
                                                {{$n->ExpiryDateFormatted}}
                                            @endif</td>
                                        <td>{{array_get($n->CreatedBy, 'FullName')}} <br />on
                                            @if(!empty($format)) {{$n->created_at->format($format)}} @else {{$n->created_at}} @endif</td>
                                        <td>@if($n->Announcement)<i class="fa fa-check-circle " style="color: green; font-size: 18px;"></i>@else<i class="fa fa-times-circle text-danger"></i>@endif</td>
                                        <td>{{$n->Status}}</td>
                                        <td class="text-center">
{{--                                            @if($n->Archive == false)--}}
                                            <a href="#" data-toggle="modal" data-target="#addNewsModal" onclick="getNews('{{$n->_id}}')"><i class="fa fa-edit" style="font-size: 21px"></i></a>
                                            {{--<a href="#" onclick="archive('{{$n->_id}}')"><i class="fa fa-trash"></i></a>--}}
                                            {{--@endif--}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info"><p>No News records.</p></div>
                    @endif

                @endif

            </div>
        </div>


        @include('news.add')
    </div>


@endsection

@section('script')
    <script>

        $(document).ready(function(){

            $('.datepicker').pickadate({
                // Escape any “rule” characters with an exclamation mark (!).
                format: 'mm/dd/yyyy',
                formatSubmit: 'yyyy-mm-dd',
            });

            console.log('{{$expiryDate}}');
            $('input#ExpiryDate').val('{{$expiryDate}}');

        })

        function getNews(id){

            destroyEditor();
            //

            $('#addJobsModal h5.modal-title').text('Update Job');

            var url = '{{url('intranet/news/getdata/')}}/'+id;

            axios.get(url).then(function(response){

                var data = response.data;

                console.log(data);

                $('#newsId').val(data._id);
                $('#NewsTitle').val(data.NewsTitle);
                $('#Content').val(data.Content);
                $('#ExpiryDate').val(data.ExpiryDateFormatted);
                $('#ExpiryDate_submit').val(data.ExpiryDate_submit);

                if(data.Announcement === true){
                    $('#Announcement').prop('checked', true);
                }

                $('#Draft, #Publish, #Archive').prop('checked', false);
                $('#'+data.Status).prop('checked', true);
                setEditor();
            });


        }

        function reset(){

            destroyEditor();
            $('#addJobsModal h5.modal-title').text('Add Job');
            $('input#jobId').val('');
            $('input#JobTitle').val('');
            $('textarea#Content').val('');
            $('#Draft').prop('checked', true);
            $('#Announcement').prop('checked', false);

            setEditor();
        }


        function setEditor(){

            $(' textarea#Content').froalaEditor({
                toolbarButtons: ['fontSize', 'bold', 'italic', 'underline', 'strikeThrough',
                    'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                    'undo', 'redo',
                    'html', 'clearFormatting'
                ],
            });

        }

        function destroyEditor(){
            $('textarea#Content').froalaEditor('destroy');
        }

        function archive(id){

            $( "body" ).append("<div class='modal-backdrop fade show'></div>");

            alertify.confirm('Are you sure?', function(e){

                if(e){
                    var url = '{{url('intranet/jobs/archive')}}/'+id;

                    window.location.href = url;
                }

            })

            $('.modal-backdrop').remove();
        }

    </script>
@endsection

