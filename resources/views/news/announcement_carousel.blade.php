@php
    $facility = \App\Utils\Toolkit::getFacility();
    if(!empty($facility)){
        $lists = \App\Models\News::where('Announcement', true)
                        ->where('Facility.FacilityId', $facility->_id)
                        ->where('Status', 'Publish')->get();
    }else{
        $lists = [];
    }

@endphp
@if(!empty($lists))
<div id="carouselExampleControls" class="carousel slide pt-3 text-left pr-1" data-ride="carousel">
    <div class="carousel-inner">

            @php $num=1; @endphp
            @foreach($lists as $list)
                @php
                    $content_str = strip_tags($list->Content);
                    $content_intro = substr($content_str, 0, 85);

                        @endphp
                <div class="carousel-item @if($num===1)active @endif ">
                    <p class="mb-1 text-center"><strong class="font-italic">{{$list->NewsTitle}}</strong> - {{ str_limit($content_intro, $limit = 20, $end = '...')}} </p>
                </div>
                @php $num++; @endphp
            @endforeach

    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
@endif