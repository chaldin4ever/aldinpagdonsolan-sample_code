<!-- Modal -->
<div class="modal fade" id="addNewsModal" tabindex="-1" role="dialog" aria-labelledby="addJobsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" action="{{url('intranet/news/store')}}" id="news" >
            @csrf

            {{--{{csrf_field()}}--}}
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-2 mdb-color text-white ">
                    <h5 class="modal-title">{{__('Add News')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3" style="min-height: 500px;">
                    <div class="row">
                        <div class="col">

                            <div class="md-form">
                                <input type="hidden" name="newsId" id="newsId" value="" />
                                <input required type="text" id="NewsTitle" name="NewsTitle" value="" autocomplete="off" class="form-control" placeholder=" ">
                                <label for="JobTitle">{{__('News Title')}}</label>
                            </div>
                            <div class="md-form">
                                <textarea style="min-height: 340px;" id="Content" name="Content" class=" froala-editor form-control p-2" row="10" placeholder=" "></textarea>
                                <label for="Content">{{__('Content')}}</label>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="md-form">
                                        <input type="text"  id="ExpiryDate" name="ExpiryDate" required class="form-control datepicker" value="{{$expiryDate}}" placeholder=" " />
                                        <label for="ExpiryDate">{{__('Expiry Date')}}</label>
                                    </div>

                                    <div class="switch mt-4">
                                        Announcement &nbsp;&nbsp;&nbsp;
                                        <label>
                                            No
                                            <input type="checkbox" name="Announcement" id="Announcement">
                                            <span class="lever"></span> Yes
                                        </label>
                                    </div>
                                    <div class="md-form mt-0 ">Status</div>
                                    <div class="md-form mt-0 mb-0">
                                                                               <!-- Material inline 1 -->
                                        <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="Draft"  value="Draft" name="Status" checked>
                                            <label class="form-check-label" for="Draft">Draft</label>
                                        </div>

                                        <!-- Material inline 2 -->
                                        <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="Publish"  value="Publish" name="Status">
                                            <label class="form-check-label" for="Publish">Publish</label>
                                        </div>

                                        <!-- Material inline 3 -->
                                        <div class="form-check form-check-inline">
                                            <input type="radio" class="form-check-input" id="Archive"  value="Archive" name="Status">
                                            <label class="form-check-label" for="Archive">Archive</label>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <!-- Material checked -->


                        </div>

                    </div>



                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn btn-primary mr-3 ">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>