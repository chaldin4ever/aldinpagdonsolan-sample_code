@extends('layouts.poc')

@section('style')

@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-picture-o"></i> {{__('Photos')}}</strong></h3>
                <button type="button"  data-toggle="modal" data-target="#uploadModal" class="float-right btn btn-mdb-color">Add Photo(s)</button>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col-md-12">

                <div id="mdb-lightbox-ui"></div>

                <div class="mdb-lightbox {{--no-margin--}}">

                    @if(!empty($photos))
                        @foreach($photos as $photo)
                            <figure class="col-2 float-left">
                                <a href="{{url($photo->AWSFile)}}" data-size="1600x1067">
                                    <img alt="picture" src="{{url($photo->AWSFile)}}"
                                         class="img-fluid">
                                </a>
                                <p class="{{--text-uppercase font-weight-bold--}} blue-grey-text mt-1">{{$photo->Title}}</p>

                            </figure>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>

        @include('photos.upload')
    </div>


@endsection

@section('script')
    <script>

        // MDB Lightbox Init
        $(function () {
            $("#mdb-lightbox-ui").load("{{url('mdb-addons/mdb-lightbox-ui')}}");
        });

        @if(session('status'))
            toastr.success('{{session('status')}}');
        @endif

    </script>
@endsection

