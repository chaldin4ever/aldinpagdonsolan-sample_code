@extends('layouts.poc')

@section('style')
    <style>

/*
        #app main {
            margin-top: -43%;
        }
*/
    [type=radio]+label, [type=checkbox]+label{
        height: auto !important;
    }

.content:not(:last-child) {
            margin-bottom: 0 !important;
        }

        /*#form_template_question ol#questions > li a:first-child {display: none !important;}*/

        #form_template_question ol#questions > li {
            margin-bottom: 5px !important;
            padding: 5px 15px 5px 0 !important;
        }

        #form_template_question ol#questions > li pre{
            text-align: left;
            padding: 10px;
            white-space: pre-line;
            margin: 10px;
            background: #f5f5f5;
            overflow-x: auto;
        }

        #form_template_question ol#questions {padding-left: 50px;}
        #form_template_question ol#questions > li label {width: auto !important;}
        #form_template_question ol#questions > li .fa {vertical-align: middle;}
        #form_template_question ol#questions > li .columns {
            /*margin-top: -40px;*/
            padding-right: 15px;
        }

        #form_template_question ol#questions > li.message .columns {
            margin-top: 0 !important;
            padding-right: 15px;
        }

        #form_template_question ol#questions > li:before {font-weight: 700 !important;}
        #form_template_question ol#questions > li {margin-bottom: 10px; padding: 5px 10px 10px 0;}
        #form_template_question ol#questions > li:hover, ol#questions > li label.label_qn:hover {
            background: #f0f1f2;
            border: 1px dashed lightgrey;
            cursor: move; /* fallback if grab cursor is unsupported */
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        #form_template_question ol#questions > li:active, ol#questions > li label:active {
            background: #ccc;
            cursor: grabbing;
            cursor: -moz-grabbing;
            cursor: -webkit-grabbing;
        }

        #form_template_question .qn{float: left; margin-left: -50px; margin-top: 3px;}
        /*.qn_show_code {float: left; margin-left: -25px; margin-top: 3px;}*/
        .qn_trash {float: right; margin-left: -45px; margin-top: 3px;}

        /*#Modal_add .modal-body input,
        #Modal_add .modal-body select,
        #Modal_add .modal-body textarea {width: 95%; padding: 5px; margin-bottom: 5px; border: solid 1px #5c5c5c;}*/

        .precode {margin-left: 50%; top: -4%;}

        #form_template_question ol#questions > li {margin-bottom: 10px; border-bottom: 1px solid #f0f1f2;}

        i.fa-trash {font-size: 24px !important;}

        li.hide_remove{display: none;}

        /*#Modal_add  #fields .fieldOptions .row div label {display: none !important;}
        #Modal_add  #fields .fieldOptions {margin-top: -35px !important;}
        #Modal_add  #fields .fieldOptions a#delete_fld {display: none;}
        #Modal_add  #add_fld {visibility: hidden;}*/

        #fields .fieldOptions .row div label {display: none !important;}
            #fields .fieldOptions {margin-top: -35px !important;}
            /*#fields .fieldOptions a#delete_fld {display: none;}*/
             /*#add_fld {visibility: hidden;}*/

            table.bodymap td  {    font-size: 0rem !important;}
            table.bodymap button{ background: none !important; padding: 0 !important; margin:0!important; border: none !Important; cursor: pointer !important;}

            table.bodymap .circle_selected{
                position: absolute !important;
                background: darkred !important;
                opacity: .2 !important;
                display: none;

            }

            table.bodymap button div#Head{margin-top: -15px !important; margin-left: 5px !important;}
            table.bodymap button div#Right_ear{margin-top: -5px !important; margin-left: -18px !important;}
            table.bodymap button div#Left_ear{margin-top: -5px !important; margin-left: -5px !important;}
            table.bodymap button div#Head_back{margin-top: -15px !important; margin-left: 5px !important;}
            table.bodymap button div#Right_ear_back{margin-top: -5px !important; margin-left: -5px !important;}
            table.bodymap button div#Left_ear_back{margin-top: -5px !important; margin-left: -16px !important;}
            table.bodymap button div#Right_shoulder{margin-top: -15px !important;  }
            table.bodymap button div#Left_shoulder{margin-top: -15px !important;  }
            table.bodymap button div#Right_arm{margin-left: -15px !important;  }
            table.bodymap button div#Right_shoulder_back{margin-top: -15px !important;  }
            table.bodymap button div#Left_shoulder_back{margin-top: -15px !important;  }
            table.bodymap button div#Right_arm_back{margin-left: -1px !important;  }
            table.bodymap button div#Left_arm_back{margin-left: -10px !important;  }
            table.bodymap button div#Chest{margin-left: 1px !important;  margin-top: -3px;}
            table.bodymap button div#Right_inner_elbow{margin-left: -5px !important;  margin-top: -2px !important; }
            table.bodymap button div#Left_inner_elbow{ margin-top: 2px !important; }
            table.bodymap button div#Right_elbow{margin-left: 1px !important;  margin-top: -2px !important; }
            table.bodymap button div#Left_elbow{ margin-top: 2px !important; margin-left: -5px !important; }
            table.bodymap button div#Lower_back{ margin-left: 15px !important; }
            table.bodymap button div#Right_palm{margin-left: -1px !important;  }
            table.bodymap button div#Left_palm{margin-left: -5px !important;  }
            table.bodymap button div#Right_hand{margin-left: -5px !important;  }
            table.bodymap button div#Left_hand{margin-left: -5px !important;  }
            table.bodymap button div#Left_buttock{margin-left: -5px !important;  }
            table.bodymap button div#Right_knee{margin-left: -1px !important;  }
            table.bodymap button div#Sacrum{margin-left: -5px !important;  }
            table.bodymap button div#Left_foot{margin-left: -3px !important;  }
            table.bodymap button div#Right_foot{margin-left: -3px !important;  }
            table.bodymap button div#Left_foot_back{margin-left: -5px !important;  }
            table.bodymap button div#Right_foot_back{margin-left: -5px !important;  }
/*        .popover-header{background: #000 !important; color: #fff !important;
            }

        .popover-body{display: none !important;}*/

    </style>

@endsection

@section('content')

    <div class="container-fluid  mt-1">
        <div class="row   pb-0 pt-3 mb-2  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col-6 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{$form->FormName}}</strong>
                </h3>
            </div>
            <div class="col text-right">
                <div class="col">
                    <a class="btn btn-grey btn-rounded" href="{{url('/form')}}">Return to Forms</a>
                    {{--<a class="btn btn-grey btn-rounded" href="{{url('/form/template/'.$form->_id)}}">{{__('Edit Template')}}</a>--}}
                    <a class="btn btn-grey btn-rounded" href="{{url('/form/edit/'.$form->_id)}}">{{__('Edit Header')}}</a>
                    <a class="btn btn-grey btn-rounded" href="{{url('/formeditor/preview/'.$form->_id)}}">{{__('Preview')}}</a>
                </div>
            </div>
        </div>

        <div class="  pb-5 bg-white pr-3 pt-1 pl-5">
            <div class=" bg-white  pb-5 pt-1 mb-4  ">
                {{--<pre>{{print_r($caredomain)}}</pre>--}}
                <a href="#" class="add_qn float-right btn btn-amber btn-sm btn-rounded" id="add_qn"
                   onclick="add_qn('preppend')"
                   data-toggle="modal" data-target="#Modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Add Question</a>

            </div>
            {{--<pre>{{json_encode($controls)}}</pre>--}}
            <div id="form_template_question">@include('template.form_controls_edit', ['controls' => $controls])</div>

            <div class="field is-grouped">
                <div class="bodymap_checkbox">
                    @foreach($bodymap as $k=>$v)
                        <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                        <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                    @endforeach
                </div>
                <form id="htmlform" method="post" action="{{url('form/validateTemplate')}}" style="width: 100%;" >
                    {{csrf_field()}}
                    <input type="hidden" id="formId" name="formId" value="{{$form->_id}}" />
                    {{--<input type="submit" value="Submit" />--}}
                    <textarea name="template_content" id="template_content" style="visibility: hidden; height: 1px;" rows="1"></textarea>
                    {{--<input type="submit" value="submit" />--}}

                            <h1 class="w-100  float-right append mt-1 mb-3 " style="margin-top: -30px;">
                                <a href="#" class="add_qn float-right btn btn-amber  btn-sm btn-rounded" id="add_qn"
                                   onclick="add_qn('append')"
                                   data-toggle="modal" data-target="#Modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Add Question</a>
                                <a class="btn btn-sm btn-grey btn-rounded is-link float-right" href="{{url('/form/listing')}}">Return to Forms</a>

                            </h1>

                    <input type="hidden" id="addQnType" />
                    <input type="hidden" id="uniqueCode" />

                    <div class="field" id="success">

                    </div>
                </form>
            </div>
        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

@endsection

@section('script')

    <script>


/*        $(document).ready(function(){
            $('body').append("<div class=\"modal-backdrop fade show\"></div>");
        })*/

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();

        })

        /*$(function() {
            ShowLoader();
            $("#loading-overlay").fadeOut(5000, function() {
                // $("body").fadeIn(1000);
            });
        });*/


        $(document).ready(function() {

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');
            @foreach($bodymap as $k=>$v)

            $("table.bodymap button#{{$k}}").click(function () {
                $('table.bodymap td button#{{$k}}').tooltip();


                var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

                if(checkbox.length > 0){
                    $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                    $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                    $('table.bodymap button div#{{$k}}').hide();
                }else{
                    $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                    $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                    $('table.bodymap button div#{{$k}}').show();
                }

                var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                $('div#Location').text(data);
                $('input#Location').val(data);
                $('input#Bodymap').val(data_id);

                return false;
            })
            @endforeach
        })

        $(document).ready(function() {

            $('#Modal_add input#caredomain').mdb_autocomplete({
                data: getCareDomain()
            });

            // console.log(getCareDomain());

        })


        function generateCode() {

            var randomNum = (""+Math.random()).substring(2,7);
            var text = "";
            // var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            /*for (var i = 0; i < 2; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));*/

            var prefix = '{{strtoupper(substr($form->FormName, 0, 3))}}';
            return prefix+'-'+randomNum;
        }


        // $("ol#questions li:first a.qn_trash").hide();

        $("html").removeClass('full-height');
        // toastr.success('Please add a new question before removing the TEST question or you can just just update it. If you have done this before please ignore this message.').css('width', '450px');

        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 50,
            max: true
        });

        $('.timepicker').pickatime({
            // Light or Dark theme
            darktheme: true
        });




        var drak = dragula([document.querySelector('#questions')]);
        drak.on('drop', function(){

                parse_template();
                auto_save();
            })

/*        $( "button#save_template_form" ).click(function(e) {
            e.preventDefault();
            parse_template();
            $( "form#htmlform" ).submit();
        })*/

        $(document).ready(function(){
//            parse_template();
//            $('[data-toggle="tooltip"]').tooltip();
        })


        function confirmAction ( elem, id, message) {
            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
            alertify.confirm(message, function(e) {
                if (e) {
                    // a_element is the <a> tag that was clicked
                    $(elem+'#'+id).fadeOut("slow",function(){
                        $(elem+'#'+id).remove();
                            parse_template();
                            auto_save();
                        $('.modal-backdrop').remove();
//                        $('#is-remove-'+fldcode).val(1);
                    });

                }
            });
        }


        function add_msg_point(modalId){
            $("#"+modalId+" #question_message p#point:first").clone().appendTo("#"+modalId+" #question_message div#message_point");
            $("#"+modalId+" #question_message div#message_point p:last input").val('');
        }

        $( " #Modal_add label #add_point" ).click(function() {

            $("#Modal_add #question_message p#point:first").clone().appendTo('#Modal_add #question_message div#message_point');
            $("#Modal_add #question_message div#message_point p:last input").val('');
        })

        //Adding field options
        $( "#add_fld" ).click(function() {

            var d = new Date();
            var timestamp = d.getTime();

            $("#Modal_add #add_qn_template .fieldOptions input.field_code").val(generateCode());
            $("#Modal_add #add_qn_template .fieldOptions").attr('id', 'fieldOptions'+timestamp);
            $("#Modal_add #add_qn_template .fieldOptions").clone().appendTo('#Modal_add div#fields').hide().fadeIn(800);

            /*var picklists = getpicklist();
            $('#fields #fieldOptions'+timestamp+' #text').mdb_autocomplete({
                data: picklists
            });*/

            var caredomain = getCareDomain();
            $('#fields #fieldOptions'+timestamp+' #caredomain').mdb_autocomplete({
                data: caredomain
            });


        })


        $( '#Modal_add select#type' ).change(function(){

            val = $('#Modal_add select#type').val();

            if(val=='checkbox' || val=='radio' || val=='dropdown'){
                $('#Modal_add #question_fields').hide();
//                $('#Modal_add .fieldsOptions').show();
                $('#Modal_add div.tabs ul li:not(:first)').show();
                $('#Modal_add #question_fields .selectpicklist').show();
                $('#Modal_add #question_fields .selectpicklist input#selectpicklist').attr('required', true);

                var picklists = getpicklist();

                $('#Modal_add #question_fields .selectpicklist input#selectpicklist').mdb_autocomplete({
                    data: picklists
                });

                $('#Modal_add .bodymap').hide();
                $('#Modal_add .goal').show();

            }else if(val=='bodymap'){
                $('#Modal_add #question_fields').show();
                $('#Modal_add .fieldsOptions').hide();
                $('#Modal_add div.tabs ul li:not(:first)').hide();
                $('#Modal_add .bodymap').show();
                $('#Modal_add .goal').hide();
            }else{

                $('#Modal_add .bodymap').hide();
                $('#Modal_add .goal').show();
                $('#Modal_add #question_fields').show();
                $('#Modal_add .fieldsOptions').hide();
                $('#Modal_add div.tabs ul li:not(:first)').hide();
                $('#Modal_add #question_fields .selectpicklist').hide();
                $('#Modal_add #question_fields selectpicklist input#selectpicklist').attr('required', false);

            }

            if(val=='message'){
                // $('#Modal_add #question_fields input#question').attr('required', false);
                $('#Modal_add div#question_message .question').attr('required', true);
                $('#Modal_add div#question_message').show();
                $('#Modal_add div#question_fields').hide();
            }else{
                // $('#Modal_add #question_fields input#question').attr('required', true);
                $('#Modal_add div#question_fields').show();
                $('#Modal_add div#question_message').hide();
                $('#Modal_add div#question_message .question').attr('required', false);
            }

        })

        function displayPicklist(){

                var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

                if(textlistcurrent.length > 0){
                    $('#Modal_add #fields div.fieldOptions').remove();
                }

                // var inputname =
                var picklistid = $('#Modal_add #question_fields .selectpicklist input#selectpicklist').attr('selectpicklist_id');

                var url = '{{url('picklist/getpicklist')}}/'+picklistid;
                axios.get(url).then(function(response){

                    var lists  = response.data.Lists;
                    // console.log(lists);
                    var textlist = $('#Modal_add #fields div.fieldOptions input[name^=text]');

                        $.each(lists, function(k, v){
                            $( "#Modal_add #add_fld" ).trigger('click');
                        });

                        var textlistnew = $('#Modal_add #fields div.fieldOptions input[name^=text]');
                        var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');

                        $.each(textlistnew, function(k, v){
                            $(this).val(lists[k].text);
                            $(this).attr('picklistid', picklistid+'-'+k);
                        });

                        $.each(codelist, function(k, v){
                            $(this).val(lists[k].code);
                        });


                })
                // $( "#add_fld" ).trigger('click');
        }

        function getpicklist(){

            var picklists = $.parseJSON('{!! $picklists !!}');

            return picklists;
        }

        function getCareDomain(){

            var caredomain = $.parseJSON('{!! $caredomain !!}');

            return caredomain;

        }

        $( "#add_qn, #add_qn_append" ).click(function() {

            $('#Modal_add .bodymap').hide();
            $('#Modal_add .goal').show();

            var d = new Date();
            var timestamp = d.getTime();

            $('#Modal_add .modal-dialog').attr('id', 'modal-dialog'+timestamp);

            $('#Modal_add input, #Modal_add textarea').each(function(){
                $(this).val('');
            });

            $('#Modal_add input#parent_goal').val('-');

            $("#Modal_add select#parent_response").val("").trigger('change');

            $("#Modal_add select#type").val("default").trigger('change');

            $('#Modal_add div#fields').hide();
            $('#Modal_add div#fields div.fieldOptions').remove();

            $("#Modal_add #question_message #message_point p").not(':first').remove();
            $('#Modal_add div.tabs ul li:not(:first)').hide();
            $('#Modal_add div.tabs ul li:not(:first)').attr('class', '');
            $('#Modal_add div.tabs ul li:first').attr('class', 'is-active');

            $('#Modal_add div.type').show();

            var uniquecode = generateCode();
            $('#Modal_add input[name=code]').val(uniquecode);


            $('#Modal_add input#intervention').val('intv');
            $('#Modal_add input#observation').val('obs');

        })

        function add_fld_option(modalId){
            var d = new Date();
            var timestamp = d.getTime();

            $('#'+modalId+" div.fieldOptions li").attr('id', 'field'+timestamp);
            $('#'+modalId+" div.fieldOptions li").clone().appendTo('#'+modalId+' ul.fields');

        }

                @php      $html = ""; @endphp
                @foreach($controls as $cnt)




                @php
                    $qn = substr($cnt->qn, -1) == '*' ? str_replace('*', '', $cnt->qn)  : $cnt->qn;


                    $html = $html ."document.querySelector('#_dragable_".$qn."'),";

                @endphp

                        {{--@if($cnt->code == 'PLEASE_UPDATE_THIS_QUESTION_OR_REMOVE_THIS_AFTER_ADDING_A_NEW_QUESTION')
                            --}}{{--alert('{{array_get($tmp, 'code')}}');--}}{{--
                             $("ol#questions li#qn{{$qn}}").css('visibility', 'hidden');
                             $("ol#questions li#qn{{$qn}}").css('height', '0');
                             $("ol#questions li#qn{{$qn}}").css('padding', '0px !important');
                             $("ol#questions li#qn{{$qn}}").css('margin', '0');

                        @endif--}}

        var drak = dragula([{!! $html !!}]);

        $( "#edit_{{$qn}}" ).click(function() {

            goal = $('li#qn{{$qn}} #label_{{$qn}}').attr('goal');
            obs = $('li#qn{{$qn}} #label_{{$qn}}').attr('obs');
            intv = $('li#qn{{$qn}} #label_{{$qn}}').attr('intv');

            $('#Modal_{{$qn}} input#goal').val(goal);
            $('#Modal_{{$qn}} input#observation').val(obs);
            $('#Modal_{{$qn}} input#intervention').val(intv);

            $('#Modal_add input#caredomain').mdb_autocomplete({
                data: getCareDomain()
            });

        });

/*        $( "#Modal_{{$qn}} #add_fld_{{$qn}}" ).click(function() {
            var d = new Date();
            var timestamp = d.getTime();

            $("#Modal_{{$qn}} div.fieldOptions li").attr('id', 'field'+timestamp);
            $("#Modal_{{$qn}} div.fieldOptions li").clone().appendTo('#Modal_{{$qn}} ul.fields');
        })*/


        $( "#qn_trash_{{$qn}}" ).click(function() {
            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
            alertify.confirm('{{__('Are you sure you want to delete this question?')}}', function(e){
                if(e){
                    $( "#qn{{$qn}}" ).remove();
                    parse_template();
                    auto_save();
                    $('.modal-backdrop').remove();
                }
            });

        });

        function showOption_{{$qn}}(){

            // alert($(this).closest('li').attr('id'));
            var ModalDiv = '#Modal_{{$qn}}';
            var val = $('#Modal_{{$qn}} select#type').val();

            if(val=='checkbox' || val=='radio' || val=='dropdown'){
                $(ModalDiv+' #question_fields').hide();
                $(ModalDiv+' div.tabs ul li:not(:first)').show();
                $(ModalDiv+' div.tabs ul li:not(:first)').attr('class', '');
                $(ModalDiv+' div.tabs ul li:first').attr('class', 'is-active');
                $(ModalDiv+' div.tabs ul li:first').show();
            }else{
                $(ModalDiv+' #question_fields').show();
                $(ModalDiv+' .fieldsOptions').hide();
                $(ModalDiv+' div.tabs ul li:not(:first)').hide();
            }

            if(val=='message'){
                $(ModalDiv+' div#question_message').show();
                $(ModalDiv+' div#question_fields').hide();
            }else{
                $(ModalDiv+' div#question_fields').show();
                $(ModalDiv+' div#question_message').hide();
            }
        }

        var type_{{$qn}} = $('#Modal_{{$qn}} #type').val();

        function getData_{{$qn}}(){

            var qcode        = $('#Modal_{{$qn}} #code').val();
            var req          = $('#Modal_{{$qn}} #required').val();
            var question     = $('#Modal_{{$qn}} #question').val();
            var goal         = $('#Modal_{{$qn}} #goal').val();
            var observation  = $('#Modal_{{$qn}} #observation').val();
            var intervention = $('#Modal_{{$qn}} #intervention').val();

            var qnData = {
                type: type_{{$qn}},
                code: qcode,
                req: req,
                question: question,
                goal: goal,
                obs: observation,
                intv: intervention
            };

            return qnData;
        }


        $( "#save_"+type_{{$qn}}+"_{{$qn}}" ).click(function() {

            var data = getData_{{$qn}}();

            // console.log(data);

            $('li#qn{{$qn}} label').html(data.question);
            $('input#{{$cnt->code}}').attr('name', data.code);

            if(data.goal.length > 0){
                $('li#qn{{$qn}} #label_{{$qn}}').attr('goal', data.goal);
            }

            if(data.obs.length > 0){
                $('li#qn{{$qn}} #label_{{$qn}}').attr('obs', data.obs);
            }

            if(data.intv.length > 0){
                $('li#qn{{$qn}} #label_{{$qn}}').attr('intv', data.intv);
            }

            @if(isset($cnt->fields))

                    @foreach($cnt->fields as $fld)

                    @php $fldcode = array_get($fld, 'code');

                         $scoreArr[] = array_get($fld, 'score');
                    @endphp

                fldcode = $('#Modal_{{$qn}} input#code_{{$fldcode}}').val();
            fldtext = $('#Modal_{{$qn}} input#text_{{$fldcode}}').val();
            {{--fldgoal = $('#Modal_{{$qn}} input#goal_{{$fldcode}}').val();--}}

            $('li#qn{{$qn}} #'+type_{{$qn}}+'_{{$fldcode}} input#{{$fldcode}}').attr('name', fldcode);
            $('li#qn{{$qn}} #'+type_{{$qn}}+'_{{$fldcode}} input#{{$fldcode}}').val(fldcode);
            $('li#qn{{$qn}} #'+type_{{$qn}}+'_{{$fldcode}} label#{{$fldcode}}').html(fldtext);


            if($('#is-remove-{{$fldcode}}').val() == 1){

                $('li#qn{{$qn}} #'+type_{{$qn}}+'_{{$fldcode}}').remove();
            }

            @endforeach

            @endif

        });

        @endforeach


        function auto_save(){

            toastr.warning('Saving...').addClass(' saving-proccess');

            var template_content = $('textarea#template_content').val();
            // console.log(template_content);
            var url = "{{url('form/validateTemplate')}}";
            axios.post(url, {
                _token: $('input[name=_token]').val(),
                formId: $('input[name=formId]').val(),
                template_content: template_content
            }).then(function(response){
                   // console.log(response.data);
                $('.saving-proccess').remove();
                toastr.info("{{__('Successfully Saved.')}}");

                /*$("ol#questions li:first a.qn_trash").hide();
                $("ol#questions li:not(:first) a.qn_trash").show();*/
            });

        }

/*        $(document).ready(function(){

            var qnlist =  $.parseJSON('{!! json_encode($controls) !!}');

            console.log(qnlist);
        })*/

           /* $("#Modal_add input[name=response]").on('change', function () {
                var s = $(this).val();
                alert(s);
            });*/

        function add_qn(addtype){
            $('#addQnType').val(addtype);
            $('#Modal_add .modal-title').text('Add Question');
            $('#Modal_add button#save_add_qn').text('Add Question');
            $('#Modal_add button#save_add_qn').attr('onclick', 'save_add_qn(this, \'add\', \'\')');
            $('#Modal_add select#type').removeClass(' disabled');
            $('#Modal_add').show();

        }

        function getQuestionData(qn){

            // var Modal_ID = $('button#save_add_qn').parent().parent().parent().parent().attr('id');
            $('#Modal_add .selectpicklist .mdb-autocomplete-wrap').remove();

            var picklists = getpicklist();

            $('#Modal_add .selectpicklist input#selectpicklist').mdb_autocomplete({
                data: picklists
            });

            $('#Modal_add .modal-title').text('Edit Question');
            $('#Modal_add button#save_add_qn').text('Save Question');

            $('#Modal_add select#type').addClass(' disabled');

            $('#Modal_add button#save_add_qn').attr('onclick', "save_add_qn(this, 'edit')");

            $('#Modal_add .fieldsOptions').hide();

            $('#Modal_add input, #Modal_add select').val('');

            // var ModalDiv = '#Modal_add '

            var d = new Date();
            var timestamp = d.getTime();

            $('#Modal_add .modal-dialog').attr('id', 'modal-dialog'+timestamp);

            @php

                $controls_string = str_replace("'","\\'", json_encode($controls));

                    @endphp

            var qnlist = $.parseJSON('{!! $controls_string !!}');

            var data = qnlist[qn];
            // console.log(qnlist);

            var cdArr = getCareDomain();

            var qn_no = data.qn;
            $('li#qn'+qn_no).attr('id', 'qn'+qn_no+'-'+timestamp);

            $('#Modal_add button#save_add_qn').attr('onclick', "save_add_qn(this, 'edit','qn"+qn_no+'-'+timestamp+"')");

            // $('#Modal_add button#save_add_qn').attr('onclick', "save_add_qn(this, 'add','qn"+qn_no+'-'+timestamp+"')");

            $('#Modal_add #type').val(data.type);
            $('#Modal_add #type').trigger('change');

            $('#Modal_add #question').val(data.question);
            $('#Modal_add #required').val(data.required);
            $('#Modal_add div.field input#caredomain').val(cdArr[data.caredomain]);
            $('#Modal_add div.field input#caredomain').attr('caredomain_id', data.caredomain);
            $('#Modal_add #code').val(data.code);

            var care_plan = data.care_plan;
            // console.log(care_plan);

                if(care_plan != null){
                    // $('#Modal_add #parent_goal').val(care_plan[0].domain);
                    $('#Modal_add #parent_goal').val(care_plan[0].goal);
                    $('#Modal_add #parent_response').val(care_plan[0].map_to);
                   /* if(typeof care_plan[1] != 'undefined'){
                        $('#Modal_add #parent_response').val(care_plan[1].map_to);
                    }*/
                    /*else{
                        $('#Modal_add #parent_response').val(care_plan[0].map_to);
                    }*/

                }

            // $('#Modal_add #intervention').val(data.intervention);
            $('#Modal_add textarea.message').val(data.question);


            var lists = data.fields;
            // console.log(lists);
            // alert(lists);

            if(lists !== 'undefined'){
                // if(lists.length > 0){

                    var textlistcurrent = $('#Modal_add #fields div.fieldOptions input[name^=text]');

                    if(textlistcurrent.length > 0){
                        $('#Modal_add #fields div.fieldOptions').remove();
                    }

                    $.each(lists, function(k, v){
                        $( "#Modal_add #add_fld" ).trigger('click');
                    });

                    var textlistnew = $('#Modal_add #fields div.fieldOptions input[name^=text]');
                    var codelist = $('#Modal_add #fields div.fieldOptions input[name^=code]');
                    var caredomain = $('#Modal_add #fields div.fieldOptions input[name^=caredomain]');
                    var score = $('#Modal_add #fields div.fieldOptions input[name^=score]');
                    var goal = $('#Modal_add #fields div.fieldOptions input[name^=goal]');
                    var response = $('#Modal_add #fields div.fieldOptions select[name^=response]');

                    var pid = [];
                    $.each(textlistnew, function(k, v){
                        var txt = lists[k].text;
                        var data = txt.split('***');

                        $(this).val(txt);

                        var picklistid = lists[k].picklist;
                        $(this).attr('picklistid', picklistid+'-'+k);

                    });

                    $.each(caredomain, function(k, v){

                        var cd = lists[k].caredomain;

                        if(cd !== 'undefined'){

                            $(this).attr('caredomain_id', cd);
                            var value = cdArr[cd];
                            $(this).val(value);
                        }
                    })

                    $.each(goal, function(k, v){

                        var val = lists[k].goal;
                        $(this).val(val);
                    })

                // console.log(response);
                    $.each(response, function(k, v){

                        var val = lists[k].response;
                        $(this).val(val);

                    })

                @if(!empty($scoreArr))
                    var scoreArr = $.parseJSON('{!! json_encode($scoreArr) !!}');
                @else
                    var scoreArr = [];
                @endif

                    $.each(score, function(k, v){

                        var scr = scoreArr[k];
                        $(this).val(scr);
                    })



                if(typeof lists != 'undefined'){
                    var plist =  lists[0].picklist ;
                    var p = plist.split('-');
                    $('#Modal_add input#selectpicklist').attr('selectpicklist_id', p[0]);

                    $.each(codelist, function(k, v){
                        var fld_code = lists[k].code;
                        $(this).val(fld_code);
                    });
                }
                // }
            }
        }

/*
function ShowLoader() {
    var overlay = jQuery('<div id="loading-overlay" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.6); z-index: 10000;"><div style="text-align: center; width: 100%; position: absolute; top: 40%; margin-top: -50px;"> <div class="preloader-wrapper big active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div> </div> </div>');
    overlay.appendTo(document.body);
}
function HideLoader() {
    $('#loading-overlay').remove();
}*/

    </script>
    @include('template.editform.js.parse_htmlform')
    @include('template.editform.js.save_add_qn', ['template_json' => $form->template_json, 'formid'=>$form->_id])
    @include('template.editform.js.save_edit_qn')
@endsection

@include('template.form_controls_modal', ['controls' => $controls, 'picklists'=> $picklists])
