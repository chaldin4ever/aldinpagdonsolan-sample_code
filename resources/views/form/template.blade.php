@extends('layouts.poc')

@section('content')

<div class="container-fluid  pt-3">
    <div class="row   pb-0 pt-3 mb-4 " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
        <div class="col pl-0">
            <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i>{{-- {{__('Forms')}}--}}</strong>
                {{$form->FormID}} - {{$form->FormName}}  </h3>
        </div>
        <div class="col text-right">
            <a class="btn   btn-grey btn-rounded" href="{{url('/form/listing')}}">{{__('Forms')}}</a>
            <a class="btn   btn-grey btn-rounded" href="{{url('/form/edit/'.$form->_id)}}">{{__('Edit Form Header')}}</a>
        </div>
    </div>


        <div class="row  bg-white">
            <div class="col-8">
                <form id="template" method="post" action="{{url('/form/validateTemplate')}}">

                    {{ csrf_field() }}
                    
                    <div class="">
                        <h2 class="title"></h2>
                        <h4 class="p-3">{{__('Template')}}
                            <button class="btn btn-grey btn-rounded float-right">{{__('Save')}}</button></h4>
                        <div class="md-form pb-5">

                                <textarea id="template_content"  class="mdb-textarea form-control p-3" style="min-height:710px;" name="template_content">
                                    {{--@if($content==''){{$form->template}}@else{{$content}}@endif--}}
                                </textarea>
                            <button class=" float-right btn btn-grey btn-rounded">{{__('Save')}}</button>
                        </div>
                    </div>

                    <input type="hidden" name="formId" value="{{$form->_id}}"/>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                </form>
            </div>

            <div class="col-4 bg-light  ">
                <div class="p-3">
                    <table class="table is-striped">
                        <tr>
                            <td>#</td>
                            <td>Question numbering<br/>
                            Postfix * at the end to indicate a mandatory field<br/>
                            e.g. #1*</td>
                        </tr>
                        <tr>
                            <td>$</td>
                            <td>Code for the question; must start with a letter</td>
                        </tr>
                        <tr>
                            <td>?</td>
                            <td>Text for the question</td>
                        </tr>
                        {{-- <tr>
                            <td>~</td>
                            <td>Additional information for user</td>
                        </tr> --}}
                        <tr>
                            <td>=</td>
                            <td>Begin and end of a block of message<br/>
<pre>
=
This is a message
* this is bullet point 1
* this is bullet point 2
> this is order point 1
> this is order point 2
=
</pre>
                            </td>
                        </tr>
                        {{-- <tr>
                            <td>{}</td>
                            <td>Additional attributes to present the question</td>
                        </tr> --}}
                        <tr>
                            <td>@</td>
                            <td>Type of question; text, memo, number, date, time, checkbox, radio, dropdown</td>
                        </tr>
                        <tr>
                            <td>(000)</td>
                            <td>Code for the checkbox, radio, dropdown</td>
                        </tr>
                        {{-- <tr>
                            <td>+</td>
                            <td>Create a task for a follow up assessment / form</td>
                        </tr> --}}
                        <tr>
                            <td>^^</td>
                            <td>Prefix to start a question</td>
                        </tr>
                        <tr>
                            <td>&gt;</td>
                            <td>Link to care plan<br/>
                                e.g <strong>obs=Mobility &amp; Transfer</strong> <br/>
                                obs =&gt; Observation <br/>
                                intv =&gt; Intervention <br/>
                                goal =&gt; Goal <br/>
                                &quot;Mobility &amp; Transfer&quot; =&gt; domain
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
<div id="sidenav-overlay" style="background: #030303 !important;"></div>

    @if(!empty($templateString))
    <div class="container container-top">
<pre>
{{$templateString}}
</pre>
    </div>
    @endif
@endsection

@section('script')
<script>

    $(document).ready(function(){

        $('textarea#template_content').val('#1\n$PLEASE_UPDATE_THIS_QUESTION_OR_REMOVE_THIS_AFTER_ADDING_A_NEW_QUESTION\n?TEST\n@text\n^^');
        $('form#template').submit();
    })


</script>

@endsection
