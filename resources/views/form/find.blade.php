@extends('layouts.poc')
@section('style')
<style>
    .form-check {padding-left: 0;}

    .strikethrough-text{
        text-decoration: line-through;
    }

    ul li {
        list-style:none !important;
        margin: 0 0 !important;
    }
</style>
    @endsection

@section('content')

    <div class="container-fluid ">
        <div class="row   pb-0 pt-3 mb-4 border-bottom  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col-8 pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-file-text-o"></i> {{__('Forms')}}</strong></h3>
            </div>
            <div class="col-4">
                <i class="fa fa-circle" style="color: green;font-size:1.85rem"></i>Publish &nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: red;font-size:1.85rem"></i>Draft &nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: black;font-size:1.85rem"></i>Archive
                <i class="fa fa-rss-square ml-2" style="color: #1C3356;font-size:1.2rem"></i> Private
            </div>
        </div>
        <div class="row " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>


        <div class="col">
            <div class="col is-paddingless">
                <form method="post" action="{{url('form/find')}}">
                    <div class="row ">
                        <div class="col-2">
                            <div class="form-horizontal">
                                <div class="form-group is-marginless">
                                    <select class="mdb-select" name="key">
                                        <option value="all" @if($key=='all') selected @endif>{{__('All')}}</option>                                        
                                        <option value="assessment" @if($key=='assessment') selected @endif>{{__('Assessment')}}</option>
                                        <option value="charting" @if($key=='charting') selected @endif>{{__('Charting')}}</option>
                                        {{--<option value="evaluation" @if($key=='evaluation') selected @endif>{{__('Evaluation')}}</option>--}}
                                        <option value="form" @if($key=='form') selected @endif>{{__('Form')}}</option>                                        
                                    </select>
                                    <label class="col-xs-2 control-label is-marginless">{{__('Category')}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-horizontal">
                                <div class="form-group is-marginless">
                                    <select class="mdb-select" name="state">
                                        <option value="all" @if($state=='all') selected @endif>Draft &amp; Published</option>
                                        <option value="published" @if($state=='published') selected @endif>{{__('Published')}}</option>
                                        <option value="draft" @if($state=='draft') selected @endif>{{__('Draft')}}</option>
                                        <option value="archived" @if($state=='archived') selected @endif>{{__('Archived')}}</option>
                                    </select>
                                    <label class="col-xs-2 control-label is-marginless">{{__('Status')}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 pt-0 pr-0">
                            <div class="form-inline">
                                <div class="form-group is-marginless">
                                    <label class="col-xs-2 control-label is-marginless mr-2">{{__('Search')}}</label>
                                    <div class="col-xs-8 is-paddingless pb-2">
                                            <input type="text" name="name" class="form-control" autocomplete="off"
                                                    placeholder="Search by form name or ID" value="{{$name}}" style="width:350px">
                                    </div>
                                    <span style="margin:10px;font-size:1em">{{$rows->total()}} forms found!</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-2 pl-4 pr-0">
                            <input class="btn btn-grey btn-rounded btn-sm" type="submit" value="{{__('Go')}}"/>
                            <a class="btn-floating btn-grey m-0 pull-right"  href="{{url('form/add')}}" style=" color: #fff;">
                                <i class="fa fa-plus"  data-toggle="tooltip" data-placement="bottom" title="{{__('Add Form')}}"></i>
                            </a>

                        </div>

                    </div>

                    {{csrf_field()}}
                </form>
            </div>

        </div>
        </div>

       @php
            $providers = [];
       @endphp

        <div class="row bg-white p-3">
            <div class="col content">
                @if(sizeof($rows)==0)
                    <h2>{{__('No forms found.')}}</h2>
                @else
                    <div id=" ">
                        <table class="table table-hover table-bordered table-striped" width="100%">
                            <thead class="blue-grey text-white font-weight-bold">
                            <tr>
                                <th width="3%">&nbsp;</th>
                                <th width="5%">{{__('Code')}}</th>
                                <th width="25%">{{__('Form Name')}}</th>
                                <th width="15%">{{__('Category')}}</th>
                                <th width="20%">Reference</th>
                                <th width="10%">Provider</th>
                                <th width="12%">{{__('Updated At')}}</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            @foreach($rows as $t)
                                @php
                                    $provider_id = $t->Provider;
                                    $provider = array_get($providers, $provider_id);
                                    if(empty($provider)){
                                        $provider = \App\Models\Provider::find($provider_id);
                                        if(!empty($provider)){
                                            $providers[$provider_id] = $provider;
                                        }
                                    }
                                @endphp
                                <tr>
                                    <td >
                                        @if($t->Status == 'published')
                                            <i class="fa fa-circle" style="color: green;font-size:1.85rem"></i>
                                        @elseif($t->Status == 'draft')
                                            <i class="fa fa-circle fa-2x" style="color: red;font-size:1.85rem"></i>
                                        @else
                                            <i class="fa fa-circle fa-2x" style="color: black;font-size:1.85rem"></i>
                                        @endif

                                    </td>
                                    <td>{{$t->FormID}}</td>

                                    <td>{{$t->FormName}}
                                        @if(!empty($t->Provider))
                                            <i class="fa fa-rss-square" style="font-size:1.2rem;color:#1C3356"></i>
                                        @endif
                                    </td>

                                    <td>
                                        @if($t->AssessmentCategory==1) {{__('Assessment')}}
                                        @elseif($t->ChartingCategory==1) {{__('Charting')}}
                                        @elseif($t->FormCategory==1) {{__('Form')}}
                                        @elseif($t->EvaluationCategory==1) {{__('Evaluation')}}
                                        @endif
                                    </td>
                                    <td>{{$t->reference}}</td>
                                    <td>
                                        @if(empty($provider) || !is_object($provider))
                                            Public
                                        @else
                                        {{$provider->ProviderName}}
                                        @endif
                                    </td>
                                    <td>{{App\Utils\Toolkit::DateTimeToString(array_get($t, 'updated_at'))}}</td>
                                    <td class="p-1">
                                        <select class="mdb-select m-0 colorful-select dropdown-dark "  onchange="getAction(this.value, '{{$t->_id}}')">
                                            <option value="">{{__('Select Action')}}</option>
                                            <option value="copy">{{__('Copy Form')}}</option>
                                            <option value="edit">{{__('Edit Form')}}</option>
                                            <option value="template">{{__('Form Editor')}}</option>
                                            <option value="javascript">{{__('Javascript')}}</option>
                                            <option value="preview">{{__('Preview')}}</option>
                                            <option value="version">{{__('Versions')}}</option>
                                            {{--<option value="ccs">{{__('CCS')}}</option>
                                            <option value="print">{{__('Print')}}</option>--}}
                                        </select>
                                    </td>


                                </tr>
                            @endforeach
                        </table>
                        <?php echo $rows->render(); ?>
                    </div>
                @endif
            </div>
        </div>

    </div>

        <div class="modal fade" id="versionsFormModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">

                    <div class="modal-content">
                        <div class="modal-header border-bottom-0 pb-0">
                            <h5 class="modal-title"  >{{__('Versions')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">

                            <div class="row">
                                <h5 id="formName"></h5>
                                <div class=" col" id="versions">

                                </div>
                            </div>

                        </div>
                        <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                            <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                                <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>

                        </div>
                    </div>

            </div>
        </div>



@endsection


@section('script')

            <script type="text/javascript">


                $(document).ready(function(){
                    @if(session('status'))

                        toastr.success('{{session('status')}}').css('width', 'auto');

                    @endif

                })

                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })


                $(document).ready(function() {

                    $('.fixed-action-btn').unbind('click');

                    $('.mdb-select').material_select();

                });

                function getAction(action, id){

                    if(action == 'template'){
                        //form editor page
                        {{--var url = '{{url('form/template/')}}/'+id;--}}
                        {{--var url = '{{url('form/editform/')}}/'+id;--}}
                        var url = '{{url('formeditor/')}}/'+id;
                        window.location.href = url;

                    }else if(action == 'javascript'){

                        var url = '{{url('formeditor/javascript/')}}/'+id;
                        window.location.href = url;

                    }else if(action == 'preview'){

                        //var url = '{{url('form/preview/')}}/'+id;
                        var url = '{{url('formeditor/preview/')}}/'+id;
                        window.location.href = url;

                    }else if(action == 'ccs'){

                        var url = '{{url('ccs/listq/')}}/'+id;

                        window.location.href = url;

                    }else if(action == 'version'){

                        openVersionsModal(id);
                        $('#versionsFormModal').modal('show');

                    }else if(action == 'print'){

                        url = '{{url('form/print/')}}/'+id;

                        window.location.href = url;

                    }else if(action == 'edit'){

                        url = '{{url('form/edit/')}}/'+id;

                        window.location.href = url;

                    }else if(action == 'copy'){

                        $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
                        alertify.confirm('Are you sure?', function(e) {
                            if (e) {
                                url = '{{url('form/copy/')}}/'+id;
                                window.location.href = url;
                                $('.modal-backdrop').remove();
                            }else{
                                $('.modal-backdrop').remove();
                            }
                        });

                    }
                }


            </script>
    <script>


        function openVersionsModal(formId){
            // $('.modal').addClass('is-active');

            $url = "{{url('form/versions/')}}/"+formId;

            axios.get($url)
                .then(function(response){
                    var data = response.data;
                    $('#formName').html(data.notes);
                    $('#versions').html(data);
                });
       }
    </script>
@endsection
