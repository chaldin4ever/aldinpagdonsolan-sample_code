<div id="mycare-table-container-3">
    <table class="table">
        <thead>
        <tr>
            <th width="2%">Version</th>
            <th width="5%">Code</th>
            <th width="35%">Form name</th>
            <th width="35%">Archived Date</th>

        </tr>
        </thead>
        <tbody>
        @foreach($formArchive as $form)

            <tr>
                <td>
                    {{$form->version}}
                </td>
                <td>{{$form->FormID}}</td>
                <td> {{$form->FormName}}</td>
                <td>
                    {{\App\Utils\Toolkit::CarbonToDateString($form->updated_at)}}
                </td>
                <td>
                    <a class="btn btn-sm btn-primary" href="{{url('form/previewarchive/'.$form->_id)}}">Preview</a>
                </td>


            </tr>

        @endforeach

        </tbody></table>
</div>