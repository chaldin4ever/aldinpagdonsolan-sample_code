@extends('layouts.app')
@section('content')

    <div class="container content container-top">
        <div class="columns">
            <div class="column is-8">
                <h1 class="content-title">{{trans_choice('mycare.available_form',2)}}</h1>
            </div>
            <div class="column is-4">
                <div class="p-0 clearfix">
                    <a href="{{url('/form/add')}}" class="button is-primary btn btn-styles btn-primary2 right">{{__('mycare.add_form')}}</a>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-8">
                <form method="get" action="{{url('form/listing')}}">

                    <div class="">

                        <ul class="list-unstyled list-inline avail-form-filter left">
                            {{--<li>

                                <input class="form-control" name="formName" type="text" placeholder="{{__('mycare.enter_a_form_name')}}" value="{{array_get($params, 'form-name')}}">

                            </li>--}}
                            <li>
                                <select name="category" class="form-control">
                                    <option value="all" @if(array_get($params, 'category')=='all') selected @endif>
                                        {{__('mycare.all')}}
                                    </option>
                                    <option value="assessment" @if(array_get($params, 'category')=='assessment') selected @endif>
                                        {{trans_choice('mycare.assessment',2)}}
                                    </option>
                                    <option value="charting" @if(array_get($params, 'category')=='charting') selected @endif>
                                        {{__('mycare.charting')}}
                                    </option>
                                    <option value="form" @if(array_get($params, 'category')=='form') selected @endif>
                                        {{trans_choice('mycare.form',2)}}
                                    </option>

                                </select>
                            </li>
                            <li>
                                <select name="isActive" class="form-control">
                                    <option value="1" @if(array_get($params, 'state')==1) selected @endif>
                                        {{__('mycare.active')}}
                                    </option>
                                    <option value="-1" @if(array_get($params, 'state')==-1) selected @endif>
                                        {{__('mycare.all')}}
                                    </option>
                                    <option value="0" @if(array_get($params, 'state')==0) selected @endif>
                                        {{__('mycare.inactive')}}
                                    </option>
                                </select>
                            </li>
                            <li>
                                <select name="language" class="form-control">
                                    <option value="all" @if(array_get($params, 'language')=='all') selected @endif>
                                        {{__('mycare.all')}}
                                    </option>
                                    <option value="en" @if(array_get($params, 'language')=='en') selected @endif>
                                        {{__('mycare.english')}}
                                    </option>
                                    <option value="zh" @if(array_get($params, 'language')=='zh') selected @endif>
                                        {{__('mycare.chinese')}}
                                    </option>
                                </select>
                            </li>
                            <li>
                                <input type="submit" value="{{__('mycare.search')}}" class="button is-primary">
                            </li>
                        </ul>

                    </div>
                </form>

            </div>
        </div>

        <div class="columns">

            <div class="column is-12">
                @php

                if(is_array($params)){
                    $parameters = '?';
                    foreach($params as $k=>$v){

                        $parameters .= "$k=$v&";
                    }
                }


                @endphp

                <Goodtable title="" url="{{url('form/listing/result'.$parameters)}} "></Goodtable>
            </div>
        </div>
    </div>
@endsection
