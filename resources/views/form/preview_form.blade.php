@extends('layouts.poc')

@section('style')
    <style>
        .content:not(:last-child) {
             margin-bottom: 0 !important;
        }

        [type=radio]+label, [type=checkbox]+label{
            height: auto !important;
        }


        table.bodymap td  {    font-size: 0rem !important;}
        table.bodymap button{ background: none !important; padding: 0 !important; margin:0!important; border: none !Important; cursor: pointer !important;}

        table.bodymap .circle_selected{
            position: absolute !important;
            background: darkred !important;
            opacity: .2 !important;
            display: none;

        }

        table.bodymap button div#Head{margin-top: -15px !important; margin-left: 5px !important;}
        table.bodymap button div#Right_ear{margin-top: -5px !important; margin-left: -18px !important;}
        table.bodymap button div#Left_ear{margin-top: -5px !important; margin-left: -5px !important;}
        table.bodymap button div#Head_back{margin-top: -15px !important; margin-left: 5px !important;}
        table.bodymap button div#Right_ear_back{margin-top: -5px !important; margin-left: -5px !important;}
        table.bodymap button div#Left_ear_back{margin-top: -5px !important; margin-left: -16px !important;}
        table.bodymap button div#Right_shoulder{margin-top: -15px !important;  }
        table.bodymap button div#Left_shoulder{margin-top: -15px !important;  }
        table.bodymap button div#Right_arm{margin-left: -15px !important;  }
        table.bodymap button div#Right_shoulder_back{margin-top: -15px !important;  }
        table.bodymap button div#Left_shoulder_back{margin-top: -15px !important;  }
        table.bodymap button div#Right_arm_back{margin-left: -1px !important;  }
        table.bodymap button div#Left_arm_back{margin-left: -10px !important;  }
        table.bodymap button div#Chest{margin-left: 1px !important;  margin-top: -3px;}
        table.bodymap button div#Right_inner_elbow{margin-left: -5px !important;  margin-top: -2px !important; }
        table.bodymap button div#Left_inner_elbow{ margin-top: 2px !important; }
        table.bodymap button div#Right_elbow{margin-left: 1px !important;  margin-top: -2px !important; }
        table.bodymap button div#Left_elbow{ margin-top: 2px !important; margin-left: -5px !important; }
        table.bodymap button div#Lower_back{ margin-left: 15px !important; }
        table.bodymap button div#Right_palm{margin-left: -1px !important;  }
        table.bodymap button div#Left_palm{margin-left: -5px !important;  }
        table.bodymap button div#Right_hand{margin-left: -5px !important;  }
        table.bodymap button div#Left_hand{margin-left: -5px !important;  }
        table.bodymap button div#Left_buttock{margin-left: -5px !important;  }
        table.bodymap button div#Right_knee{margin-left: -1px !important;  }
        table.bodymap button div#Sacrum{margin-left: -5px !important;  }
        table.bodymap button div#Left_foot{margin-left: -3px !important;  }
        table.bodymap button div#Right_foot{margin-left: -3px !important;  }
        table.bodymap button div#Left_foot_back{margin-left: -5px !important;  }
        table.bodymap button div#Right_foot_back{margin-left: -5px !important;  }

    </style>
@endsection

@section('content')


    <div class="container-fluid  pt-3">
        <div class="row   pb-0 pt-3 mb-4  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col-3 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{__('Form Preview')}}</strong>
                     </h3>
            </div>
            <div class="col text-right">
                <div class="col">
                    <a class="btn btn-grey btn-rounded " href="{{url('/form/listing')}}"><i class="fa fa-undo"></i></a>
                    <a class="btn btn-grey btn-rounded " href="{{url('/form/compare/'.$form->_id)}}"><i class="fa fa-check-square-o"></i></a>
{{--                        <a class="btn btn-grey btn-rounded" href="{{url('/form/template/'.$form->_id)}}">{{__('Edit Template')}}</a>--}}
                        <a class="btn btn-grey btn-rounded" href="{{url('/form/edit/'.$form->_id)}}">{{__('Edit Header')}}</a>
                        <a class="btn btn-grey btn-rounded" href="{{url('/form/editform/'.$form->_id)}}">{{__('Form Editor')}}</a>

                    <a href=""></a>
                </div>
            </div>
        </div>
            <div class=" bg-white p-3 col">
                <h1 class="title">{{$form->FormName}}</h1>
                <div class="bodymap_checkbox">
                    @foreach($bodymap as $k=>$v)
                        <input type="checkbox" class="bodymap_list" name="bodymap_list[]" value="{{$v}}" id="{{$k}}" />
                        <input type="checkbox" class="bodymap_list_id" name="bodymap_list_id[]" value="{{$k}}" id="{{$k}}_id" />
                    @endforeach
                </div>

                @include('template.form_controls', ['controls' => $controls])


                <div class="row">

{{--                        <a class="btn btn-grey" href="{{url('/form/template/'.$form->_id)}}">Edit again</a>--}}


                        <a class="btn btn-grey" href="{{url('/form/listing')}}">Return to Forms</a>

                </div>
            </div>


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

    </div>

@endsection

@section('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            // $('[data-toggle="popover"]').popover();
        })

        $(document).ready(function() {

            $('table.bodymap button div.btn-floating').hide();
            // $('table.bodymap td a#Head').tooltip('show');
            @foreach($bodymap as $k=>$v)

            $("table.bodymap button#{{$k}}").click(function () {
                $('table.bodymap td button#{{$k}}').tooltip();


                var checkbox = $('.bodymap_checkbox input#{{$k}}:checked');

                if(checkbox.length > 0){
                    $('.bodymap_checkbox input#{{$k}}').removeAttr('checked');
                    $('.bodymap_checkbox input#{{$k}}_id').removeAttr('checked');
                    $('table.bodymap button div#{{$k}}').hide();
                }else{
                    $('.bodymap_checkbox input#{{$k}}').attr('checked', 'checked');
                    $('.bodymap_checkbox input#{{$k}}_id').attr('checked', 'checked');
                    $('table.bodymap button div#{{$k}}').show();
                }

                var data = $('.bodymap_checkbox input.bodymap_list:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                var data_id = $('.bodymap_checkbox input[name^="bodymap_list_id"]:checked').map(function(){
                    return this.value;
                }).get().join(", ");

                $('div#Location').text(data);
                $('input#Location').val(data);
                $('input#Bodymap').val(data_id);

                return false;
            })
            @endforeach
        })


    </script>
    <script>
        $('.datepicker').pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'yyyy-mm-dd',
            selectYears: 90,
            // max: true
        });

        $('#timepicker').pickatime(
            {
            // Light or Dark theme
            darktheme: true,
            twelvehour: true,
        });


    </script>
@endsection
