@php
    $n = 1;
@endphp

@extends('layouts.app_hide_sidemenu')

@section('content')

    <table class="table">
        @foreach($results as $r)
            <tr>
                <td>{{$n++}}</td>
                <td>{{array_get($r, 'FormID')}}</td>
                <td>{{array_get($r, 'FormName')}}</td>
                <td>{{array_get($r, 'Type')}}</td>
                <td>{{array_get($r, 'Code')}}</td>
                <td>{{array_get($r, 'Text')}}</td>
            </tr>
        @endforeach
    </table>

@endsection