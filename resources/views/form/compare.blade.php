@extends('layouts.app_hide_sidemenu')

@section('content')
    <div class="container container-top content">
        <div class="columns">
            <a href="{{url('/form/preview/').'/'.$form1->_id}}" class="button is-outlined is-primary">
                <i class="fa fa-undo"></i></a>
        </div>

        <div class="columns">

            <div class="column is-6">
                @include('form.compare_form', [ 'form' => $form1])
            </div>

            <div class="column is-6">
                @include('form.compare_form', [ 'form' => $form2])
            </div>
        </div>
    </div>

    <div class="modal" id="modal-edit">
        <div class="modal-background"></div>
        <div class="modal-card content" style="width:90%">
            <header class="modal-card-head">
                <p class="modal-card-title">{{__('mycare.edit')}}</p>
                <button class="delete" aria-label="close" onclick="cancel()"></button>
            </header>
            <section class="modal-card-body">
                <div class="columns">
                    <div class="column is-6">
                        <div class="columns">
                            <div class="column is-4">
                                <label>#</label>
                                <input type="text" id="qn" name="qn" value=""/>
                            </div>
                            <div class="column is-4">
                                <label>Code</label>
                                <input type="text" id="qcode" name="qcode" value="" readonly/>
                            </div>
                            <div class="column is-4">
                                <label>Type</label>
                                <input type="text" id="qtype" name="qtype" value="" readonly/>
                            </div>
                        </div>

                        <div class="columns">
                            <div class="column is-12">
                                <label>Question</label>
                                <input type="text" id="qtext" name="qtext" value=""/>
                            </div>
                        </div>

                        <div class="columns">
                            <div class="column is-12">
                                <label>Text for care plan</label>
                                <input type="text" id="qgoal" name="qgoal" value=""/>
                            </div>
                        </div>
                    </div>

                    <div class="column is-6">
                        <div id="modal-fields" class=" hidden">
                            <div class="columns">
                                <div class="column is-12">
                                    <label>Items</label>
                                    <textarea id="qitems" name="qitems"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="form_id" value=""/>
            </section>
            <footer class="modal-card-foot">
                <button class="button is-success" onclick="save()">{{__('mycare.save')}}</button>
                <button class="button" onclick="cancel()">{{__('mycare.cancel')}}</button>

            </footer>

        </div>
    </div>


@endsection

@section('style')
    <style>
        textarea{
            min-height: 450px;
        }

        .each-question {
            border-bottom: solid thin #1f648b;
            margin-bottom: 15px;
        }

        .qtn-text {
            display: inline-block;
        }

        .qtn-goal {
            display: inline-block;
            margin-left: 20px;
            background-color: #b1dcfb;
        }

        .item-text {
           display: inline-block;
        }

        .item-goal {
            display: inline-block;
            margin-left: 30px;
            background-color: #b1dcfb;
        }

        .indent-me {
            margin-left: 40px;
        }

        .indent-me .question-item {

        }

        .modal-card-body label{
            display: block;
        }

        .modal-card-body input{
            width: 100%;
        }

        input:read-only {
            background-color: silver;
        }

        .line-through{
            background-color: palegoldenrod;
            color: silver;
        }

    </style>
@endsection

@section('script')
    <script>
        function editQuestion(form_id, qtn_code)
        {
            $('#form_id').val("");
            $('#qn').val("");
            $('#qcode').val("");
            $('#qtype').val("");
            $('#qtext').val("");
            $('#qgoal').val("");
            $('#qitems').text("");

            var url = "{{url('/question/get/')}}/"+form_id+"/"+qtn_code;
            axios.get(url)
                .then(function(response){

                    var data = response.data;

                    $('#form_id').val(form_id);
                    $('#qn').val(data.qn);
                    $('#qcode').val(data.qcode);
                    $('#qtype').val(data.qtype);
                    $('#qtext').val(data.qtext);
                    $('#qgoal').val(data.qgoal);
                    $('#qitems').text(data.qitems);

                    $("#qitems").height = ($("#qitems").scrollHeight+10) + 'px';

                    $('#modal-edit').addClass('is-active');

                    $('#modal-fields').removeClass('hidden');
                });

        }



        function cancel(){
            $('#modal-edit').removeClass('is-active');
            $('#form_id').val("");
            $('#qn').val("");
            $('#qcode').val("");
            $('#qtype').val("");
            $('#qtext').val("");
            $('#qgoal').val("");
            $('#qitems').text("");
        }

        function save(){

            var form_id = $('#form_id').val();
            var qn = $('#qn').val();
            var qcode = $('#qcode').val();
            var qtype= $('#qtype').val();
            var qtext = $('#qtext').val();
            var qgoal = $('#qgoal').val();
            var qitems = $('#qitems').val();

            console.log(qitems);

            var url = "{{url('/question/store/')}}";
            axios.post(url,{
                form_id: form_id,
                qn: qn,
                qcode : qcode,
                qtype : qtype,
                qtext : qtext,
                qgoal : qgoal,
                qitems : qitems
            }).then(function(response){

                console.log(response.data);

                $('#modal-edit').removeClass('is-active');
                $("#"+form_id+"_"+qcode).addClass("line-through");
            })


        }
    </script>
@endsection