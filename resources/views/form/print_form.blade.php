<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
    body {
        font-family: 'Roboto', sans-serif;
        font-size: 15px;
        line-height: 1.6;
        color: #000;
    }

    .field:not(:last-child) {
        margin-bottom: 20px;
    }

</style>
    <div class="container container-top">

        <div class="box">
            <h1 class="title">{{$form->FormName}}</h1>

            @include('template.form_controls', ['controls' => $controls])


        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



    </div>

