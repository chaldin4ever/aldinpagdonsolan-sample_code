@extends('layouts.show_errors')


@section('style')
    <style>
        .header {
            background-color: #0ca56a;
        }

        .source-field {

        }

        .target-field {
            color: #002A80;
        }

        .error-text{
            color:red;
        }

        .field-code {
            font-size:0.7em;
            font-weight:bold;
        }
    </style>
@endsection

@section('content')

    <div class="container container-top content">

        <div class="columns">
            <a class="button is-primary is-outlined"
               href="{{url('/form/find')}}">{{trans_choice('mycare.form',2)}}</a>
        </div>
        <div class="columns">
            <table class="table">
                <tr>
                    <th>&nbsp;</th>
                    <th>Form ID</th>
                    <th>FormName</th>
                    <th>QuestionText</th>
                    <th>QuestionCode</th>
                    <th>FieldType</th>
                    <th>Domain</th>
                    <th>MapTo</th>
                    <th>Goal</th>
                    <th>Error</th>
                </tr>
                @php
                    $n = 1;
                @endphp
                @foreach($results as $r)
                    @php
                        $errText = array_get($r, 'Error');
                        if($error == 1 && empty($errText)) continue;
                    $headerClass = array_get($r, "IsHeader") ? "header": "";
                    @endphp
                    <tr>
                        <td class="field-code">{{$n++}}</td>
                        <td>{{array_get($r, 'FormID')}}</td>
                        <td><a href="{{url('form/find?name='.array_get($r, 'FormID'))}}" target="_blank">{{array_get($r, 'FormName')}}</a></td>
                        <td>{!! array_get($r, 'QuestionText') !!}</td>
                        <td>{!! array_get($r, 'QuestionCode') !!}</td>
                        <td>{{array_get($r, 'FieldType')}}</td>
                        <td>{{array_get($r, 'Domain')}}</td>
                        <td>{{array_get($r, 'map_to')}}</td>
                        <td>{{array_get($r, 'Goal')}}</td>
                        <td class="error-text">{!! $errText !!}</td>
                    </tr>
                @endforeach
            </table>

        </div>

    </div>

@endsection