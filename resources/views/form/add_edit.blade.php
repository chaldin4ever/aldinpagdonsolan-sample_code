@extends('layouts.poc')
@section('style')
    <style>
        [type=checkbox]+label:before {
            margin-top: -6px !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid  pt-5">
        <div class="row   pb-0 pt-3 mb-4  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{__('Add/Edit Forms')}}</strong></h3>
            </div>
            <div class="col text-right">
                @isset($form->_id)
{{--                    <a class="btn btn-grey btn-rounded  " href="{{url('/form/template/'.$form->_id)}}">{{__('Edit Template')}}</a>--}}
                    <a class="btn btn-grey btn-rounded" href="{{url('/formeditor/preview/'.$form->_id)}}">{{__('Preview')}}</a>
                @endisset
            </div>
        </div>

    <div class="row bg-white p-3">

        <div class="col pl-0">
{{--{{print_r($form)}}--}}
            <form role="form" action="{{url('/form/store')}}" method="post">

                {{csrf_field()}}

            <div class="row p-4">
                    <div class="box has-shadow col-6 pl-5 pr-5">

                        <div class="md-form">
                            <input  type="text" class="form-control " name="form_code" value="{{$form->FormID}}" autocomplete="off"/>
                            <label class="label has-text-left">{{__('Form Code')}}</label>
                        </div>
                        <div class="md-form">
                                <input type="text" class="form-control " name="formName" value="{{$form->FormName}}" autocomplete="off"/>
                                <label class="label has-text-left">{{__('Form Name')}}</label>
                        </div>
                        <div class="md-form">
                            <input  type="text" class="form-control " name="form_reference" value="{{$form->reference}}" placeholder="Source of this form"/>
                            <label class="label has-text-left">{{__('Reference')}}</label>
                        </div>

                        <div class="row">
                            <div class="col-6 md-form">

                                <p>{{__('Category')}}</p>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="category" value="assessment"  @if($form->AssessmentCategory==1)  checked @endif id="assessment">
                                    <label class="form-check-label" for="assessment">{{__('Assessment')}}</label>
                                </div>
                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="category" value="charting" @if($form->ChartingCategory==1) checked @endif id="charting">
                                    <label class="form-check-label" for="charting">{{__('Charting')}}</label>
                                </div>
                                {{--<div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="category" value="evaluation" @if(isset($form->EvaluationCategory)) @if($form->EvaluationCategory==1) checked @endif @endif id="evaluation">
                                    <label class="form-check-label" for="evaluation">{{__('Evaluation')}}</label>
                                </div>--}}
                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="category" value="form" @if($form->FormCategory==1) checked @endif id="form">
                                    <label class="form-check-label" for="form">{{__('Form')}}</label>
                                </div>

                            </div>
                            <div class="col-6 md-form">

                                <p>{{__('Status')}}</p>
                                {{--<div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="status" value="active"   @if($form->IsActive) checked @endif id="stateactive">
                                    <label class="form-check-label" for="stateactive">{{__('Active')}}</label>
                                </div>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="status" value="inactive" @if(!$form->IsActive) checked @endif id="stateinactive">
                                    <label class="form-check-label" for="stateinactive">{{__('Inactive')}}</label>
                                </div>--}}

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="status" value="draft"   @if($form->Status == 'draft') checked @endif checked id="statedraft">
                                    <label class="form-check-label" for="statedraft">{{__('Draft')}}</label>
                                </div>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="status" value="published" @if($form->Status == 'published') checked @endif id="statepublished">
                                    <label class="form-check-label" for="statepublished">{{__('Publish')}}</label>
                                </div>


                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="status" value="archived" @if($form->Status == 'archive') checked @endif id="statearchive">
                                    <label class="form-check-label" for="statearchive">{{__('Archive')}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 md-form">

                                <p>{{__('Carry Data Over')}}</p>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="CarryDataOver" value="1" checked @if(isset($form->CarryDataOver)) @if($form->CarryDataOver == 1) checked @endif @endif id="yes">
                                    <label class="form-check-label" for="yes">{{__('Yes')}}</label>
                                </div>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="CarryDataOver" value="0" @if(isset($form->CarryDataOver)) @if($form->CarryDataOver == 0) checked @endif @endif id="no">
                                    <label class="form-check-label" for="no">{{__('No')}}</label>
                                </div>
                            </div>

                            <div class="col-6 md-form">
                                <p>{{__('Use Dummy Resident')}}</p>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="use_dummy_resident" value="yes" checked @if(isset($form->use_dummy_resident)) @if($form->use_dummy_resident == "yes") checked @endif @endif id="use_dummy_resident_yes">
                                    <label class="form-check-label" for="use_dummy_resident_yes">{{__('Yes')}}</label>
                                </div>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="use_dummy_resident" value="no" @if(!isset($form->use_dummy_resident) || ($form->use_dummy_resident == "no")) checked @endif id="use_dummy_resident_no">
                                    <label class="form-check-label" for="use_dummy_resident_no">{{__('No')}}</label>
                                </div>
                            </div>

                            <div class="col-6 md-form">

                                <p>{{__('Language')}}</p>

                                <div class="form-check radio-pink">
                                    <input type="radio" class="form-check-input" name="language" value="en" @if($form->language=='en') checked @endif id="en">
                                    <label class="form-check-label" for="en">{{__('English')}}</label>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="col pl-5 pt-3" >
                        <div class="md-form">
                            <select name="roles[]" class="mdb-select" multiple searchable="Search here.." style="display: block !important; width: 1px; height: 1px;">
                                <option class="rounded-circle" value="" selected disabled>Select Roles</option>
                                @foreach($roles as $role)
                                    <option class="rounded-circle" value="{{$role->_id}}" @if(isset($selectedRoles)) @if(in_array($role->_id, $selectedRoles)) selected @endif @endif >{{$role->roleName}}</option>
                                @endforeach
                            </select>
                            <label class="form-check-label" for="en">{{__('Assigned Roles')}}</label>

                        </div>

                        <div class=" md-form mb-4 pb-4">

                            <p class="">{{__('Need For')}}</p>

                            <div class="  radio-pink form-check">
                                <input type="radio" class="form-check-input" name="NeedFor" value="Resident" onchange="if(!this.checked){$('#assigned_staff').hide();}else{$('#assigned_staff').hide()}" @if($form->NeedFor=='Resident') checked @endif id="Resident">
                                <label class="form-check-label" for="Resident">{{__('Resident')}}</label>
                            </div>
                            <div class="form-check radio-pink">
                                <input type="radio" class="form-check-input" name="NeedFor" onchange="if(this.checked){$('#assigned_staff').show();}else{$('#assigned_staff').hide()}" value="Staff" @if($form->NeedFor=='Staff')  checked @endif  id="Staff">
                                <label class="form-check-label" for="Staff">{{__('Staff')}}</label>
                            </div>
                            <div class="form-check radio-pink">
                                <input type="radio" class="form-check-input" name="NeedFor" value="None" onchange="if(!this.checked){$('#assigned_staff').hide();}else{$('#assigned_staff').hide()}"  @if($form->NeedFor=='None') checked @endif id="None" @if(empty($form->NeedFor))checked @endif >
                                <label class="form-check-label" for="None">{{__('None')}}</label>
                            </div>

                        </div>

                        <div class="md-form mt=4" id="assigned_staff" @if($form->NeedFor!='Staff')style="display: none;"@endif>
                            {{--{{print_r($selected_staff)}}--}}
                            <select name="Staff[]" class="mdb-select" multiple searchable="Search staff.." style="display: block !important; width: 1px; height: 1px;">
                                <option class="rounded-circle" value="" selected disabled>Select Staff</option>
                                @foreach($staff_list as $staff)
                                    <option class="rounded-circle" value="{{$staff->_id}}" @if(!empty($selected_staff)) @if(in_array($staff->_id, $selected_staff)) selected @endif @endif >{{$staff->name}}</option>
                                @endforeach
                            </select>
                            <label class="form-check-label" for="en">{{__('Selected Staff')}}</label>

                        </div>

                        <!--
                        <div class="md-form">
                            <select name="facilities[]" class="mdb-select" multiple searchable="Search here.." style="display: block !important; width: 1px; height: 1px;">
                                <option class="rounded-circle" value="" selected disabled>Select Facility</option>
                                @foreach($facilities as $facility)
                                    <option class="rounded-circle" value="{{$facility->_id}}" @if(isset($selectedFacilities)) @if(in_array($facility->_id, $selectedFacilities)) selected @endif @endif >{{$facility->NameLong}}</option>
                                @endforeach
                            </select>
                            <label class="form-check-label" for="en">{{__('Assigned Facilities')}}</label>
                        </div>
                        -->

                        <div class="row text-right p-3 mt-4">

                            <button class="btn btn-primary btn-rounded" >{{__('Save')}}</button>

                            <a class="btn btn-grey btn-rounded" href="{{url('/form')}}">{{__('Cancel')}}</a>

                        </div>

                    </div>


            </div>




                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <input type="hidden" name="formId" value="{{$form->_id}}"/>

            </form>
        </div>
        </div>
    </div>
@endsection




@section('script')
    <script>

        var parents = jQuery.parseJSON('{!! $parents !!}');

        $('#parentForm').mdb_autocomplete({
            data: parents
        });


        // Initiaize typeahead 
        // $('#typeahead_input input').attr("class", "input");
        @if (isset($form->ParentFormID) and $form->ParentFormID != 0)
            var parentFormScreenName = "{{$form->ParentFormID . ' - '.$form['ParentFormName']}}";
            var parentFormID = "{{$form->ParentFormID}}";
            $('#parentForm').val(parentFormScreenName);
        // $('#parentFormID').val(parentFormID);
        @endif
      

        function onSelectForm(item){
            // You might want to store item value in some hidden <input> (In case the screen_name of <typeahead> is different from <input> value required.)
            
            console.log(item.screen_name);
            //console.log(item.FormID);
            $('#parentFormID').val(item.FormID);   
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.mdb-select').material_select();

        });

        function setevalformlink(configid, formid, evalid){

            if($('#'+configid).is(":checked")){
                var url = '{{url('form/setevaluationformlink')}}/'+configid+'/'+formid+'/'+evalid;
                axios.get(url).then(function(response){
                    toastr.success('Successfully Saved.').css('width', 'auto');
                })
            }else{
                var url = '{{url('form/deleteevaluationformlink')}}/'+configid+'/'+formid+'/'+evalid;
                axios.get(url).then(function(response){
                    toastr.success('Successfully Saved.').css('width', 'auto');
                })
            }

        }

    </script>
@endsection

