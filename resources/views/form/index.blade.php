@extends('layouts.poc')
@section('style')
    <style>
        .form-check {padding-left: 0;}
    </style>
@endsection

@section('content')

    <div class="container-fluid ">
        <div class="row   pb-0 pt-3 mb-4 border-bottom  " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>
            <div class="col pl-0">
                <h3 class="p-2"><strong><i class="fa fa-file-text-o"></i> {{__('Forms')}}</strong></h3>
            </div>
        </div>
        <div class="row " {{--style="box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.13);"--}}>


            <div class="col">
                <div class="col is-paddingless">
                    <form method="post" action="{{url('form/find')}}">
                        <div class="row ">
                            <div class="col-2">
                                <div class="form-horizontal">
                                    <div class="form-group is-marginless">
                                        <select class="mdb-select" name="key">
                                            <option value="all" selected >{{__('All')}}</option>
                                            <option value="assessment" >{{__('Assessment')}}</option>
                                            <option value="charting" >{{__('Charting')}}</option>
                                            <option value="evaluation" >{{__('Evaluation')}}</option>
                                            <option value="form" >{{__('Form')}}</option>
                                        </select>
                                        <label class="col-xs-2 control-label is-marginless">{{__('Category')}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-horizontal">
                                    <div class="form-group is-marginless">
                                        <select class="mdb-select" name="state">
                                            <option value="all" selected >{{__('All')}}</option>
                                            <option value="assessment" ) >{{__('Active')}}</option>
                                            <option value="inactive" >{{__('Inactive')}}</option>
                                        </select>
                                        <label class="col-xs-2 control-label is-marginless">{{__('Status')}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-horizontal">
                                    <div class="form-group is-marginless">
                                        <div class="form-group is-marginless">
                                            <select class="mdb-select" name="lang">
                                                <option value="all" selected >{{__('All')}}</option>
                                                <option value="en" >{{__('English')}}</option>
                                            </select>
                                            <label class="col-xs-2 control-label is-marginless">{{__('Language')}}</label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-3 pt-0 pr-0">
                                <div class="form-inline">
                                    <div class="form-group is-marginless">
                                        <label class="col-xs-2 control-label is-marginless mr-2">{{__('Search')}}</label>
                                        <div class="col-xs-8 is-paddingless pb-2">
                                            <input type="text" name="name" class="form-control" autocomplete="off" placeholder="{{__('Form Name')}} {{__('Or')}} {{__('ID')}}" value="">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-3 ">
                                <input class="btn btn-grey btn-rounded btn-sm" type="submit" value="{{__('Go')}}"/>
                                <a class="btn-floating btn-grey m-0 pull-right"  href="{{url('form/add')}}" style=" color: #fff;">
                                    <i class="fa fa-plus"  data-toggle="tooltip" data-placement="bottom" title="{{__('Add Form')}}"></i>
                                </a>

                            </div>

                        </div>

                        {{csrf_field()}}
                    </form>
                </div>

            </div>
        </div>

        {{-- <div class="row">
                 {{ $rows->appends(request()->input())->links() }}
         </div>--}}

        <div class="row bg-white p-3">
            <div class="col content">
                <ul class="nav nav-tabs nav-justified p-0 m-0" style="background-color: #949494; z-index: 1 !important;">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#assessment" role="tab">{{__('Assessment')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#chart" role="tab">{{__('Chart')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#evaluation" role="tab">{{__('Evaluation')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#forms" role="tab">{{__('Forms')}}</a>
                    </li>

                </ul>

                <div class="tab-content card">
                    <!--Panel 1-->
                    <div class="tab-pane fade in show active" id="assessment" role="tabpanel">
                        @include('form.tabs.assessment', ['rows'=> $assessmentTab])
                    </div>

                    <div class="tab-pane  " id="chart" role="tabpanel">
                        @include('form.tabs.chart', ['rows'=> $chartTab])
                    </div>

                    <div class="tab-pane  " id="evaluation" role="tabpanel">
                        @include('form.tabs.evaluation', ['rows'=> $evaluationTab])
                    </div>

                    <div class="tab-pane  " id="forms" role="tabpanel">
                        @include('form.tabs.forms', ['rows'=> $formsTab])
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="versionsFormModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">

            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-0">
                    <h5 class="modal-title"  >{{__('Versions')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">

                    <div class="row">
                        <h5 id="formName"></h5>
                        <div class=" col" id="versions">

                        </div>
                    </div>

                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>

                </div>
            </div>

        </div>
    </div>


@endsection

@section('style')
    <style>
        .strikethrough-text{
            text-decoration: line-through;
        }
    </style>
@endsection

@section('script')

    <script type="text/javascript">


        $(document).ready(function(){
            @if(session('status'))

            toastr.success('{{session('status')}}').css('width', 'auto');

            @endif

        })

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })


        $(document).ready(function() {

            $('.fixed-action-btn').unbind('click');

            $('.mdb-select').material_select();

        });

        function getAction(action, id){

            if(action == 'template'){

                        {{--var url = '{{url('form/template/')}}/'+id;--}}
                var url = '{{url('form/editform/')}}/'+id;

                window.location.href = url;

            }else if(action == 'preview'){

                var url = '{{url('form/preview/')}}/'+id;

                window.location.href = url;

            }else if(action == 'ccs'){

                var url = '{{url('ccs/listq/')}}/'+id;

                window.location.href = url;

            }else if(action == 'version'){

                openVersionsModal(id);
                $('#versionsFormModal').modal('show');

            }else if(action == 'print'){

                url = '{{url('form/print/')}}/'+id;

                window.location.href = url;

            }else if(action == 'edit'){

                url = '{{url('form/edit/')}}/'+id;

                window.location.href = url;

            }else if(action == 'copy'){

                $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
                alertify.confirm('Are you sure?', function(e) {
                    if (e) {
                        url = '{{url('form/copy/')}}/'+id;
                        window.location.href = url;
                        $('.modal-backdrop').remove();
                    }else{
                        $('.modal-backdrop').remove();
                    }
                });

            }
        }


    </script>
    <script>


        function openVersionsModal(formId){
            // $('.modal').addClass('is-active');

            $url = "{{url('form/versions/')}}/"+formId;

            axios.get($url)
                .then(function(response){
                    var data = response.data;
                    $('#formName').html(data.notes);
                    $('#versions').html(data);
                });
        }
    </script>
@endsection
