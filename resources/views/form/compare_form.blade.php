

@php
    $controls = array_get($form, 'template_json');
@endphp

@if(empty($controls))
    <h2>Form missing</h2>
@else
    <h2>{{$form->FormName}}</h2>
    @foreach($controls as $c)
        @php
            $q = array_get($c, 'question');
            $qcode = array_get($c, 'code');
            $qtype = array_get($c, 'field_type');
            $text = array_get($q, 'text');
            $qtnGoal = array_get($q, 'goal');

            if(is_array($text)) $text = implode("<br/>", $text);

        @endphp
        <div class="each-question" id="{{$form->_id}}_{{$qcode}}">
            @if(!empty($qcode))
            <a href="#" onclick="editQuestion('{{$form->_id}}', '{{$qcode}}')"><i class="fa fa-edit"></i></a>
            @endif
            <label>{{array_get($c, 'qn')}}</label>
            <div class="qtn-text">{!! $text !!}</div>
            <div class="qtn-goal">
                {{$qtnGoal}}
            </div>
            @if(in_array($qtype, ["radio", "checkbox", "dropdown"]) )
                @php
                    $fields = array_get($c, 'fields');
                @endphp
                <div class="indent-me">
                    @foreach($fields as $fld)
                        @php
                            $itemCode = array_get($fld, 'code');
                            $itemGoal = array_get($fld, 'goal');
                        @endphp
                        <div class="question-item">
                            @if($qtype=='radio')
                                <label><input type="radio"></label>
                            @elseif($qtype=='checkbox')
                                <label><input type="checkbox"></label>
                            @else
                                <label><i class="fa fa-circle"></i></label>
                            @endif
                            <div class="item-text">{{array_get($fld, 'text')}}</div>

                                <div class="item-goal">
                                    {{$itemGoal}}
                                </div>
                        </div>

                    @endforeach
                </div>
            @endif
        </div>
    @endforeach

@endif