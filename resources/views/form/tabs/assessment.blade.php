<div class="row bg-white p-3">
    <div class="column content">
        @if(sizeof($rows)==0)
            <h2>{{__('No assessment(s) found.')}}</h2>
        @else
            <div id=" ">
                <table class="table table-hover table-bordered table-striped">
                    <thead class="blue-grey text-white font-weight-bold">
                    <tr>
                        <th width="2%">&nbsp;</th>
                        <th width="5%">{{__('Code')}}</th>
                        <th width="40%">{{__('Form Name')}}</th>
                        <th width="25%">{{__('Category')}}</th>
                        <th width="13%">{{__('Updated At')}}</th>
                        <th class="w-auto"></th>
                    </tr>
                    </thead>
                    @foreach($rows as $t)
                        @php

                                @endphp
                        <tr>
                            <td >
                                @if($t->IsActive)
                                    <i class="fa fa-star" style="color: green"></i>
                                @else
                                    <i class="fa fa-moon-o" style="color: red"></i>
                                @endif
                            </td>
                            <td>{{$t->FormID}}</td>

                            <td>{{$t->FormName}}</td>

                            <td>
                                @if($t->AssessmentCategory==1) {{__('Assessment')}}
                                @elseif($t->ChartingCategory==1) {{__('Charting')}}
                                @elseif($t->FormCategory==1) {{__('Form')}}
                                @elseif($t->EvaluationCategory==1) {{__('Evaluation')}}
                                @endif
                            </td>
                            <td>{{App\Utils\Toolkit::DateTimeToString(array_get($t, 'updated_at'))}}</td>
                            <td class="p-1">
                                <select class="mdb-select m-0 colorful-select dropdown-dark "  onchange="getAction(this.value, '{{$t->_id}}')">
                                    <option value="">{{__('Select Action')}}</option>
                                    <option value="copy">{{__('Copy Form')}}</option>
                                    <option value="edit">{{__('Edit Form')}}</option>
                                    <option value="template">{{__('Form Editor')}}</option>
                                    <option value="preview">{{__('Preview')}}</option>
                                    <option value="version">{{__('Versions')}}</option>
                                    {{--<option value="ccs">{{__('CCS')}}</option>
                                    <option value="print">{{__('Print')}}</option>--}}
                                </select>
                            </td>

                        </tr>
                    @endforeach
                </table>
                <?php echo $rows->render(); ?>
            </div>
        @endif
    </div>
</div>