@extends('layouts.show_errors')


@section('style')
    <style>
        .header {
            background-color: #0ca56a;
        }

        .source-field {

        }

        .target-field {
            color: #002A80;
        }

        .error-text{
            color:red;
        }

        .field-code {
            font-size:0.7em;
            font-weight:bold;
        }
    </style>
@endsection

@section('content')

    <div class="container container-top content">

        <div class="columns">
            <a class="button is-primary is-outlined"
               href="{{url('/businessrules')}}">{{__('mycare.business_rules')}}</a>
            <a class="button is-primary is-outlined"
               href="{{url('/businessrules/listing?error=1')}}">Show Errors Only</a>
            <a class="button is-primary is-outlined"
               href="{{url('/businessrules/listing?error=0')}}">Show All</a>
        </div>
        <div class="columns">
            <table class="table">
                <tr>
                    <th>&nbsp;</th>
                    <th>Code</th>
                    <th>Process</th>
                    <th>Title</th>
                    <th>Lang</th>
                    <th>Form</th>
                    <th>FormName</th>
                    <th>If</th>
                    <th width="30%">Then</th>
                    <th>Fields</th>
                    <th>Error</th>
                </tr>
                @php
                    $n = 1;
                @endphp
                @foreach($results as $r)
                    @php
                        if(!empty($lang)){
                            if(array_get($r, 'Language') != $lang)
                                continue;
                        }
                        $errText = array_get($r, 'Error');
                        if($error == 1 && empty($errText)) continue;
                        $headerClass = array_get($r, "IsHeader") ? "header": "";
                    @endphp
                    <tr>
                        <td class="field-code">{{$n++}}</td>
                        <td  class="{{$headerClass}}">{{array_get($r, 'Code')}}</td>
                        <td>{{array_get($r, 'Process')}}</td>
                        <td>{{array_get($r, 'Title')}}</td>
                        <td>{{array_get($r, 'Language')}}</td>
                        <td>{{array_get($r, 'Form')}}</td>
                        <td>
                            @if(empty(array_get($r, 'Form')))
                                {{array_get($r, 'FormName')}}
                            @else
                                <a href="{{url('form/find?name='.array_get($r, 'Form'))}}" target="_blank">{{array_get($r, 'FormName')}}</a>
                            @endif
                        </td>
                        <td>{!! array_get($r, 'If') !!}</td>
                        <td>{!! array_get($r, 'Then') !!}</td>
                        <td>{!! array_get($r, 'Fields') !!}</td>
                        <td class="error-text">{!! $errText !!}</td>
                    </tr>
                @endforeach
            </table>

        </div>

    </div>

@endsection