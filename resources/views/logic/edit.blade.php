@extends('layouts.poc')

@section('style')
    <style>
        .heading {
            font-weight: bold;
            font-size: 1rem;
        }

    </style>
@endsection

@section('content')

<div class="container">

    <div class="row">
           <h2>Business Logic</h2>
    </div>

    <form role="form" id="form" method="post" action="{{url('logic/store')}}">
        {{csrf_field()}}
        <input type="hidden" name="ruleId"  id="ruleId" value="{{$rule->_id}}"/>

    <div class="row">
        <label class="heading" for="title">Title</label>
        <input type="text" class="form-control" type="text" id="title" name="title" value="{{$rule->title}}" style="background-color:#fff"/>
    </div>

    <div class="row mt-2">
        <div class="col-md-2">
            <label class="heading"  for="jsoneditor">Logic</label>
        </div>
        <div class="col-md-10">
            <a class="btn btn-primary btn-rounded btn-sm" onclick="saveForm()">{{__('save')}}</a>
            &nbsp;
            <a class="btn btn-danger btn-rounded btn-sm" href="{{url('/logic')}}">{{__('cancel')}}</a>
        </div>
    </div>

    <div class="d-flex">
        <div id="jsoneditor" style="width: 700px; height: 700px;"></div>
        <div class="ml-2">
            <select class="mdb-select" id="json_mode" name="json_mode" onchange="change_json_mode()">
                <option value="" disabled selected> Change view</option>
                <option value="tree">Tree</option>
                <option value="code">Code</option>
            </select>

            <table class="table table-striped" >
                <tr>
                    <td>form_code</td>
                    <td>0 - all forms; 1057 - form of 1057</td>
                </tr>
                <tr>
                    <td>inputs (an array)</td>
                    <td>
                        <table class="table">
                            <tr>
                                <td>field</td>
                                <td>field code, e.g AC-1234; <br/>FormState - state of a form; 1 = Completed form; 0 - Incomplete form</td>
                            </tr>
                            <tr>
                                <td>use_raw_data
                                </td>
                                <td>
                                    use whatever selected by the user if true, otherwise, use value
                                    </td>
                            </tr>
                            <tr>
                                <td>operation</td>
                                <td>in = in the op_values array
                                    "" = always true
                                </td>
                            </tr>
                            <tr>
                                <td>op_values</td>
                                <td>array of values used by operation</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>output</td>
                    <td>
                        <table class="table">
                            <tr>
                                <td>action</td>
                                <td>create = create a new document.
                                    upsert = create a new doc if not found, otherwise update it.
                                    archive = archive a document.
                                </td>
                            </tr>
                            <tr>
                                <td>keys</td>
                                <td>an array of key to be used for upsert. 
                                    fixed key are resident = resident-id; assessment = assessement-id
                                </td>
                            </tr>
                            <tr>
                                <td>target_class</td>
                                <td>Name of the collection name. e.g. ResidentMovement, ResidentBGL, ResidentWound</td>
                            </tr>
                            <tr>
                                <td>target_form</td>
                                <td>form cod of the target form. used for opening new form from Pinboard item</td>
                            </tr>
                            <tr>
                                <td>copy_fields</td>
                                <td>
                                    an array of field to copy data
                                    from_field = from which
                                    to_field = field name of new doc
                                    use_raw_data = use whatever selected by the user if true, otherwise, use value
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>pinboard</td>
                    <td>
                        <table class="table">
                            <tr>
                                <td>action</td>
                                <td>create = create a pinboard item
                                    archive = archive a pinboard item.
                                </td>
                            </tr>
                            <tr>
                                <td>note</td>
                                <td>note to show in the pinboard item.  fixed variables are
                                    $resident_name = name of the resident
                                    $form_name = name of the form
                                    any field name in copy_fields
                                </td>
                            </tr>
                            <tr>
                                <td>due_days</td>
                                <td>when the item is due.  0 = no due date which is set to 1989-12-31</td>
                            </tr>
                            <tr>
                                <td>item_type</td>
                                <td>indicate type of the pinboard item.  resident-movement = related to resident movement.  
                                    incomplete-form = incomplete form</td>
                            </tr>
                            <tr>
                                <td>user_roles</td>
                                <td>
                                    an array of user roles. not in use but can be used to decicde which user role can do this item
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

            <a class="btn btn-primary btn-rounded" onclick="saveForm()">{{__('save')}}</a>
            &nbsp;
            <a class="btn btn-danger btn-rounded" href="{{url('/logic')}}">{{__('cancel')}}</a>

    </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        

    </form>

  </div>  



@endsection


@section('script')

<script>

        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
        // create the editor
        // https://github.com/josdejong/jsoneditor
        var container = document.getElementById("jsoneditor");
        var options = { mode: 'code', 
            onError: function(e){
                console.log(e);
            },
        };
        var editor = new JSONEditor(container, options);

        // set json
        var json = {
            "key": "",
            "form": 123,
            "field": "",
            "use_user_input" : true,
            "op_action": "in",
            "op_values": [],
            "target_field": "",
            "data_type": "string"
        };

        @if(!empty($rule))
        json = {!!$rule->json!!};
        @endif
        editor.set(json);

        function saveForm(){
            var json = JSON.stringify(editor.get());

            var rule_id = $("#ruleId").val();
            var title = $("#title").val();
            var _token = $("input[name='_token']").val();
            
            if(title == ""){
                toastr.warning('Please enter a title');
                $("#title").focus();
            }
            else {
                var url="{{url('logic/store')}}";
                axios.post(url, {
                    rule_id: rule_id,
                    title: title,
                    data: (json),
                    _token: _token
                }).then(function(response){
                    // console.log(response);
                    window.location = "{{url('logic')}}";
                });
            }
        }

        function change_json_mode(){
            var mode = $("#json_mode").val();
            editor.setMode(mode);
        }


</script>
@endsection