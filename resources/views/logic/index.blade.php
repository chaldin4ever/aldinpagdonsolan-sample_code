@extends('layouts.poc')

@section('content')


    <div class="row mt-4">
        <div class="col-xl-6">
            <h5>Business logic</h5>
        </div>
        <div class="col-xl-6">
            <a class="btn-floating pull-right teal" href="{{url('logic/edit/new')}}"><i class="fa fa-plus"></i></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($rules))
                @if(sizeof($rules) > 0)
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="85%">Title</th>
                            <th width="5%">&nbsp;</th>
                            <th width="5%">&nbsp;</th>
                        </tr>
                        </thead>
                        @php
                            $n = 1;
                        @endphp
                        <tbody>
                        @foreach($rules as $rule)
                            <tr>
                                <td>{{$n++}}</td>
                                <td><a href="{{url('/logic/edit/'.$rule->_id)}}">{{$rule->title}}</a></td>
                                <td><a href="{{url('/logic/copy/'.$rule->_id)}}"><i class="fa fa-copy"></i></a></td>
                                <td><a href="{{url('/logic/archive/'.$rule->_id)}}"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                  @else
                    <div class="alert alert-info p-2 mt-4">No business logic(s) records.</div>
                  @endif
            @endif
        </div>
    </div>

@endsection