@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-info-circle"></i>{{__(' Message')}}</strong></h3>
                <a href="#" class="btn mdb-color float-right" onclick="window.close()">Close Window</a>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">
                <div class="alert alert-success"><strong>
                        {{$info}}
                    </strong></div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script>


    </script>
@endsection

