@extends('layouts.poc')

@section('style')

    <style>
        ul.messages a {color: #000;}

        #openMessageModal .speech-bubble, #open_inbox_message .speech-bubble {
            background-color: #f8f8f8;
            border: 1px solid #c8c8c8;
            border-radius: 5px;
            width: 95%;
            text-align: left;
            padding: 20px;
        }

        #openMessageModal .speech-bubble .arrow,  #open_inbox_message .speech-bubble .arrow {
            border-style: solid;
            position: absolute;
        }

        #openMessageModal .bottom {
            border-color: #c8c8c8 transparent transparent transparent;
            border-width: 8px 8px 0px 8px;
            bottom: -8px;
        }

        #openMessageModal .bottom:after {
            border-color: #f8f8f8 transparent transparent transparent;
            border-style: solid;
            border-width: 7px 7px 0px 7px;
            bottom: 1px;
            /*content: "";*/
            position: absolute;
            left: -7px;
        }

        #inbox_messages [type=checkbox][class*=filled-in]:not(:checked)+label:after {
            background-color: transparent;
            border: solid 1px rgba(0, 0, 0, 0.15);
        }

        #inbox_messages [type=checkbox]+label:before{
            /*background-color: transparent;*/
            border: solid 1px rgba(0, 0, 0, 0.15);
        }

        #inbox_messages  input[type=checkbox]:checked+label:before {
            border-right: 2px solid  #347c67;
            border-bottom: 2px solid #347c67;
        }

        #inbox_messages [type=checkbox]:checked+label:before {
            /* background-color: transparent; */
            border: transparent;
        }

        table.inbox .read {
            background-color: #f7f6f5;
            box-shadow: inset 0 -1px 0 0 rgba(0, 0, 0, 0.05);
        }

        table.inbox tr.archive {
            text-decoration: line-through;
        }

        table.inbox tr.archive td a#replymessage,
        table.inbox tr.archive td a#archivemessage{
            display: none;
        }

        table.inbox .checked_inbox{
            background-color: rgba(52, 124, 103, 0.05);
            box-shadow: inset 0 -1px 0 0 rgba(0, 0, 0, 0.07);
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid ">
        <div class="row  pl-4 pb-0 pt-3 mb-0  " >
            <div class="col pl-0">
                <h3 class="p-2"><strong style="color: #4a4a4a; font-size: 24px; font-weight: 500;"><i class="fa fa-envelope"></i> {{__('Message')}}</strong></h3>
            </div>

        </div>

        <div class="content ml-0 mr-0 pl-0 pb-0">

            <div class="row pt-0 p-0" >
                <div class="col-2  bg-white mr-0 p-0" style="min-height: 600px; overflow: auto; box-shadow: 1px 0 2px 0 rgba(0, 0, 0, 0.05), inset -1px 0 0 0 rgba(0, 0, 0, 0.11);">
                    <ul class="list-group messages pr-1">
                        <li class=" list-group-item text-center border-0">
                            <a type="button" class=" " onclick="selectFacility()"  data-toggle="modal" data-target="#createMessageModal">
                               <img src="{{asset('images/compose.jpg')}}" />
                            </a>
                            <div style="width: 150px; display: block; margin: 0 auto;
                                      height: 20px;
                                      opacity: 0.54;
                                      box-shadow: inset 0 -1px 0 0 rgba(0, 0, 0, 0.12);" >

                            </div>
                        </li>
                        <li class="list-group-item  border-0 border-top-1 pt-2 pb-1">
                            <div class="pl-3">
                            <span style="letter-spacing: 1px;
                              color: rgba(27, 27, 27, 0.3); font-size: 12px;
                              font-weight: bold;">{{__('Messages')}}</span>
                            </div>
                        </li>

                        <li class="list-group-item  border-0">
                            <div class="pl-3">
                            <a  onclick="refreshInbox();" id="inbox_button" >
                                <i class="fa fa-envelope mr-2"></i> {{__('Inbox')}} <div class=" text-center float-right" style=" width: 30px;
                                          height: 20px;
                                          border-radius: 100px;
                                          background: rgb(52, 124, 103, .07);
                                       "><span style=" text-align: center;
                                          color: #347c67;">{{$totalinbox}}</span></div>
                            </a>
                            </div>
                        </li>
                        <li class="list-group-item   border-0">
                            <div class="pl-3">
                            <a  onclick="sentInbox();"  >
                                <i class="fa fa-paper-plane mr-2"></i> {{__('Sent')}} <div class="text-center float-right" style=" width: 30px;
                                height: 20px;
                                          border-radius: 100px;
                                          background: rgb(52, 124, 103, .07);
                                          height: 20px;

                                       "><span style=" text-align: center;
                                          color: #347c67;">{{$totalsent}}</span></div>
                            </a>
                            </div>
                        </li>
                    </ul>

                </div>

                <div class="col  bg-white p-0" style="min-height: 400px; overflow: auto;  box-shadow: inset 0 -1px 0 0 rgba(0, 0, 0, 0.07);">
                    <div class=" row p-0 m-0 bg-white text-dark" style="background-color: #f4f2f2 !important; box-shadow: inset 0 -1px 0 0 rgba(0, 0, 0, 0.09);">
                        <h3 class="m-3 header">{{__('Inbox')}}</h3>
                    </div>
                    <div class=" row p-0 m-0" >
                        <div class="col p-0" id="inbox_messages" style="box-shadow: rgba(0, 0, 0, 0.05) 1px 0px 2px 0px, rgba(0, 0, 0, 0.11) -1px 0px 0px 0px inset;" >
                            @include('messages.inbox', compact('inboxMessages'))
                        </div>
                        <div class="col p-3" id="open_inbox_message" style="

                        display: none; max-height: 450px; overflow: auto;">
                            <button data-toggle="modal" data-target="#openMessageModal"  style="background: #347c67 !important" class="btn btn-sm btn-dark-green float-right"><i class="fa fa-mail-reply"></i> {{__('Reply')}}</button>
                            <h3></h3>
                            <div id="facilityresidents" class="">
                                <div id="facility" class="row p-2 ml-2"></div>
                                <div id="residents" class="row p-2 ml-2"></div>
                            </div>
                            <div id="documents"  class="ml-2 p-2" style="display: none;">
                                <ul class="list-group">
                                    <li class="list-group-item mdb-color text-white">Document(s)</li>

                                </ul>
                            </div>
                            <div id="usermessage" class="ml-2 p-2"></div>


                            <div id="replies" >

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('messages.add')
    @include('messages.open-modal')
@endsection

@section('script')


    <script>

        $(document).ready(function(){

            $('.mdb-select').material_select();

        })


        var users = [
            "{!! implode('","',$userdata) !!}"
        ];

        $('#user').mdb_autocomplete({
            data: users
        });

        @if(session('status'))
            toastr.success('{{session('status')}}').css("width","auto");
        @endif

        function selectFacility(){
            @php $facility = \App\Utils\Toolkit::getFacility(); @endphp
            $('#createMessageModal .select-dropdown li:contains({{$facility->NameLong}})').trigger('click');
            $('#createMessageModal select#Facility').attr('readonly');
        }

        function getMessage(inboxid){

            // console.log(inboxid);
            $('#open_inbox_message div#documents ul').html('');
            $('#open_inbox_message div#facilityresidents div#residents').html('');
            var url = '{{url('messages/getmessage')}}/'+inboxid;

            axios.get(url).then(function(response){

                var data = response.data;
                $('#openMessageModal #InboxId').val(data._id);
                $('#openMessageModal .modal-title').text(data.Subject);
                $('#openMessageModal #Subject').val(data.Subject);
                $('#openMessageModal span#message').html(data.Message);
                $('#openMessageModal small#from').text('-- '+data.FromUser.FullName);
                $('#openMessageModal input#Message').val('');

                $('#open_inbox_message h3').text(data.Subject);

                if(!$.isEmptyObject(data.Facility)) {
                    // console.log(data.Facility.FacilityName);
                    if(typeof(data.Facility.FacilityName) !== 'undefined'){
                        $('#open_inbox_message div#facilityresidents div#facility').html('<strong>'+data.Facility.FacilityName+'</strong>');
                    }
                }

                if(!$.isEmptyObject(data.Residents)) {
                    $.each(data.Residents, function(k, res){
{{--                        var link = "{{url('/messages/file')}}/"+data._id+"/"+k;--}}
                        var residentlist = "<span class='badge badge-lg badge-default p-2 mr-2 mt-2'>" +
                            res.ResidentName+
                        "</span>";
                        $('#open_inbox_message div#facilityresidents div#residents').append(residentlist);
                    })
                }

                $('#open_inbox_message div#usermessage').html(data.Message);

                // if(data.Documents.length > 0){
                if(!$.isEmptyObject(data.Documents)){
                    $.each(data.Documents, function(k, doc){
                        var link = "{{url('/messages/file')}}/"+data._id+"/"+k;
                        var list = "<li class='list-group-item'>" +
                            "<a href='"+link+"' target='_blank'>" +doc.FileName+"</a>"
                        "</li>";
                        $('#open_inbox_message div#documents ul').append(list);
                    })

                    $('#open_inbox_message div#documents').show();
                }


            })


            getReplies(inboxid);

        }

        function getReplies(inboxid){
            var url = '{{url('messages/replies/')}}/'+inboxid;

            axios.get(url).then(
                function(response){
                    var data = response.data;

                    //populate moda
                    $('#openMessageModal #replies').html(data);
                    /*$('#openMessageModal #messagelists').animate({
                        scrollTop: $('#messagelists')[0].scrollHeight}, 2000);*/

                    //populate right pane
                    $('#inbox_messages').addClass(' col-5');
                    $('#open_inbox_message').show();

                    $('#open_inbox_message #replies').html(data);
                    $('#open_inbox_message  ').animate({
                        scrollTop: $('#open_inbox_message ')[0].scrollHeight}, 2000);
                }
            )
        }

        function storeReplyMessage(){

            var inboxid = $('#openMessageModal #InboxId').val();
            var subject = $('#openMessageModal #Subject').val();
            var message = $('#openMessageModal input#Message').val();

            var url = '{{url('messages/store_reply')}}';
            axios.post(url, {
                InboxId: inboxid,
                Subject: subject,
                Message: message
            }).then(function(response){
                toastr.success('{{__('Successfully sent.')}}').css("width","auto");
                refreshInbox();
            })
        }

        function refreshInbox(){

            $('h3.header').text('Inbox');
            $('#inbox_messages .progress').show();
            $('#inbox_messages').removeClass(' col-5');
            $('#open_inbox_message').hide();

            var url = '{{url('messages/getinbox/')}}';

            axios.get(url).then(
                function(response){
                    var data = response.data;

                    $('#inbox_messages').html(data);
                    $('#inbox_messages .progress').hide();

                }
            )
        }

        function sentInbox(){

            $('h3.header').text('Sent Item(s)');
            $('#inbox_messages .progress').show();
            $('#inbox_messages').removeClass(' col-5');
            $('#open_inbox_message').hide();

            var url = '{{url('messages/sentinbox/')}}';

            axios.get(url).then(
                function(response){
                    var data = response.data;

                    $('#inbox_messages').html(data);
                    $('#inbox_messages .progress').hide();

                }
            )
        }

        function getFacilityResident(facilityId){

            axios.get('{{url('messages/getfacilityresident')}}/'+facilityId+'').then(function(response){

                $('.showresidents').html(response.data);
                $('input[name="Residents"]').removeClass('disabled');
                $('button#hidresidents').show();
            })

        }

        function archiveInbox(inboxId){

            $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );

            alertify.confirm('Are you sure?', function(e){

                if(e){

                    var url = '{{url('messages/archive')}}/'+inboxId;
                    axios.get(url).then(function(response){

                        $('table.inbox tr#'+inboxId).addClass('archive');
                        toastr.success('Successfully Archived.');
                        $('.modal-backdrop').fadeOut("slow");
                    });


                }else{
                    $('.modal-backdrop').fadeOut("slow");
                }

            })
        }



    </script>

    <script>

        $(document).ready(function(){
            $('a#inbox_button').trigger('click');
        })

        $('input[name="Residents"]').on('keyup', function() {

            var input, filter, tr, td, i;

            input  = $(this);
            filter = input.val().toUpperCase();
            tr     = $("div.showresidents table tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0]; // <-- change number if you want other column to search
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        })
    </script>
@endsection