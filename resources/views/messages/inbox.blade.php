<div class="progress primary-color-dark m-0" style="display: none; height: 3px !important;">
    <div class="indeterminate"></div>
</div>

@if(!empty($inboxMessages))

    @if(sizeof($inboxMessages) > 0)
        <table class="table   inbox">

            @foreach($inboxMessages as $inbox)
                <tr class="border-bottom-1 @if($inbox->Archive == 1) archive @endif @if($inbox->Read == 1) read @endif" id="{{$inbox->_id}}" >
                   {{-- <td class="w-auto border-bottom-1 pr-1 pl-1" style="width: 30px !important; ">
                        <div class="form-check ">
                            <input type="checkbox" class="checkbox-teal-filled form-check-input" id="checkbox_{{$inbox->_id}}"
                                   onchange="
                                   if(this.checked){
                                       $(this).closest('tr').addClass(' checked_inbox');
                                       $(this).closest('tr').removeClass(' read');
                                   }else{
                                       $(this).closest('tr').removeClass(' checked_inbox');
                                       $(this).closest('tr').addClass(' read');
                                   }">
                            <label class="form-check-label mt-2" for="checkbox_{{$inbox->_id}}"></label>
                        </div>
                    </td>--}}
                    <td class="w-auto border-bottom-1" style="width: auto !important; line-height: 2em;">
                        <a {{--data-toggle="modal" data-target="#openMessageModal" --}}
                           onclick="
                                $(this).closest('tr').addClass(' read');
                                getMessage('{{$inbox->_id}}')">
                            <span style="color: #355b8c; ">{{array_get($inbox->FromUser, 'FullName')}} @if($inbox->TotalReply > 1) ({{$inbox->TotalReply}}) @endif</span>
                            <br /><span style="color: #355b8c; ">{{$inbox->FromUserEmail}}</span>

                            <br /><strong style="color: #355b8c; font-weight: bold;">{{$inbox->Subject}}</strong> - {{substr(strip_tags($inbox->Message), 0, 50)}}...
                        </a>
                    </td>

                    <td style="min-width: 90px;">
                        {{$inbox->created_at/*->format('M d')*/}}
                    </td>
                    <td style="min-width: 90px;">
                        <a href="#" id="replymessage"  data-toggle="modal" data-target="#openMessageModal"   onclick="getMessage('{{$inbox->_id}}')"  class="mr-3"><i class="fa fa-mail-reply" style="font-size: 21px;"></i></a>
                        <a href="#" id="archivemessage" onclick="archiveInbox('{{$inbox->_id}}')"><i class="fa fa-trash" style="font-size: 21px;"></i></a>
                    </td>
                </tr>
            @endforeach

        </table>
     @else
        <div class="alert alert-info">
            {{__('No mail(s) found.')}}
        </div>
     @endif
@endif