
@php $num = 0; $tmp = ''; @endphp
@foreach($messages as $msg)
    @php

        $name = array_get($msg->FromUser, 'FullName');

        if($tmp != $name){

            $addclass= '  m-0 mt-4 ';

            /*if($num % 2 == 0){
                $addclass= 'bg-white m-4';
            }else{
                $addclass= '  m-0 mt-4 ';
            }*/

            //echo 'not the same <br />';

        }else{
            $addclass= 'bg-white m-4';
            //echo 'the same <br />';
            /* if($num % 2 != 0){
                $addclass= 'bg-white m-4';
            }else{
                $addclass= '  m-0 mt-4 ';
            }*/
        }
        $tmp = $name;
    @endphp
<div class="speech-bubble  {{--{{$addclass}}--}} @if($num % 2 == 0 ) m-0 mt-4  @else bg-white m-4 @endif">
    {{--<div class="arrow bottom right"></div>--}}
    <div id="message">{!! $msg->Message !!}</div>
    {{--<div id="documents">{{print_r($msg->Documents)}}</div>--}}
    <br /><small style="font-size: 12px; float: right;" class="{{--badge  badge-primary--}}">{{array_get($msg->FromUser, 'FullName')}}    {{$msg->updated_at}}</small>
</div>
    @php $num++; @endphp
@endforeach
{{--<button data-toggle="modal" data-target="#openMessageModal" id="#openMessageModal"  style="background: #347c67 !important" class="btn btn-sm btn-dark-green float-right"><i class="fa fa-reply"></i> {{__('Reply')}}</button>--}}