<!-- Modal -->
<div class="modal fade" id="createMessageModal" tabindex="-1" role="dialog" aria-labelledby="createMessageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fluid" role="document">
        <form method="post" action="{{url('messages/store')}}" enctype="multipart/form-data" >
            @csrf
            {{--{{csrf_field()}}--}}
            <input type="hidden" name="UserId" id="userid"  />
            <div class="modal-content">
                <div class="modal-header border-bottom-1 pb-2 bg-light ">
                    <h5 class="modal-title "  >{{__('New Message')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3 pt-1">
                    <div class="row">
                            <div class="col">

                                <div class="md-form">
                                    <input required type="text" id="subject" name="Subject" value="" class="form-control" autocomplete="off">
                                    <label for="subject">{{__('Subject')}}</label>
                                </div>
                                {{--<div class="md-form">
                                    <input id="#inputtags" type="text" value="Amsterdam,Washington" data-role="tagsinput" />
                                </div>--}}

                                <div class="md-form ">
                                    <input required type="text" id="user" name="User" value="" class="form-control" autocomplete="off">
                                    <label for="user">{{__('User')}}</label>
                                </div>

                                <div class="md-form searchresidents" >
                                    {{--<label class="ml-2" for="user">{{__('Residents')}}</label>--}}
                                    <div class="row">
                                        <div class="col selectedresidents">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col ">
                                            <input  type="text" id="resident"
                                                   onfocus="$('.showresidents').show(); $('button#hideresidents').show()"
                                                   name="Residents" value="" class="disabled form-control float-left" placeholder="Search for a resident (optional)" autocomplete="off">
                                            <button class="btn btn-sm mdb-color float-right" onclick="$(this).hide();$('.showresidents').hide();" style="margin-top: -50px; display: none;" id="hideresidents" type="button">Hide</button>
                                        </div>
                                    </div>
                                    <div class=" showresidents w-100 float-left" style="position:absolute; overflow:auto; height: 250px; z-index: 999; display: none;"></div>
                                </div>

                                {{--<div class=" text-dark text-center  light-version mt-3 mb-3  " style="border-radius: 0px !important; display: block; z-index: 100; ">


                                    <div class="file-field">
                                        <div class="btn btn-outline-dark btn-rounded waves-effect btn-sm float-left">
                                            <span>Document<i class="fa fa-cloud-upload ml-3" aria-hidden="true"></i></span>
                                            <input type="file" multiple name="files[]" id="files">
                                        </div>
                                        <div class="file-path-wrapper md-form w-75 mt-0">
                                            <input onclick="$('input#files').trigger('click')" class="file-path validate text-dark" id="file_placeholder" type="text"  placeholder="Upload one or more files">
                                        </div>
                                    </div>

                                </div>--}}

                                <div class="md-form" style="display: block; z-index: 100; ">

                                    <textarea required  type="text" id="message" name="Message" autocomplete="off"   class="form-control p-3 froala-editor" placeholder="Body" style="display: block;"></textarea>
                                    {{--<label for="message">{{__('Body')}}</label>--}}

                                </div>

                                <div class="md-form ">
                                    {{--<input required type="text" id="user" name="User" value="" class="form-control" autocomplete="off">--}}
                                    @php $current_facility = \App\Utils\Toolkit::getFacility(); @endphp

                                    <select class="mdb-select" name="Facility" onchange="getFacilityResident(this.value)" searchable="Search here..">
                                        {{--<option disabled selected>Select</option>--}}
{{--                                        <option value="{{$current_facility->_id}}" >{{$current_facility->NameLong}}</option>--}}
                                        @foreach($facility as $fa)
                                            @if($current_facility->_id ===  $fa->_id)
                                                <option value="{{$fa->_id}}"  selected  >{{$fa->NameLong}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <label for="user">{{__('From Facility')}}</label>
                                </div>

                            </div>

                    </div>

                </div>
                <div class="modal-footer border-top-1 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="send" class="btn btn-primary mr-3 ">
                        {{ __('Send') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>