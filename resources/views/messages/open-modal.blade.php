<!-- Modal -->
<div class="modal fade" id="openMessageModal" tabindex="-1" role="dialog" aria-labelledby="openMessageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        {{--<form method="post" action="{{url('messages/store')}}" name="msg">--}}
            {{--@csrf--}}
            {{csrf_field()}}
            <input type="hidden" name="InboxId" id="InboxId"  />
            <input type="hidden" name="Subject" id="Subject"  />
            <div class="modal-content">
                <div class="modal-header border-bottom-1 pb-2 bg-light ">
                    <h5 class="modal-title "  ></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col ">
                            <div  >
                                {{--<div class="arrow bottom right"></div>--}}
                                <p><span id="message"></span></p>
                                {{--<br /><small id="from"></small>--}}
                            </div>
                            <div class="md-form mb-5 p-2" id="messagelists" style="max-height: 380px; overflow: auto;">

                                <div id="replies">

                                </div>

                            </div>

                            <div class="md-form ">
                                {{--<i class="fa fa-pencil prefix" style="font-size: 14px;"></i>--}}
                                <input required  type="text" id="Message" name="Message" autocomplete="off"  autofocus  class="form-control bg-light p-2"   />
                                {{--<textarea class="form-control " name="Message" id="Message"></textarea>--}}
                                <label for="message">{{__('Reply')}}</label>

                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer border-top-1 pr-4 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" onclick="refreshInbox();">
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button data-dismiss="modal" type="button" id="reply" onclick="storeReplyMessage()" class="btn btn-primary mr-3 ">
                        {{ __('Reply') }}
                    </button>
                </div>
            </div>
        {{--</form>--}}
    </div>
</div>