@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2"><strong><i class="fa fa-info-circle"></i>{{__(' Message')}}</strong></h3>
            </div>


        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">
                <div class="alert alert-warning">
                    <strong>Please contact your service provider. You do not have a System Admin access.</strong>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script>


    </script>
@endsection

