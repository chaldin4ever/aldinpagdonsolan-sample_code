@extends('layouts.poc')

@section('style')
    <style>
        ul#facilities > li, ul#user_facilities > li {
            cursor: move;
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        ul#facilities > li:hover, ul#user_facilities > li:hover{
            background: #f4f4f4;
        }

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col-xl-10 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-home"></i> {{__('Assign Facility')}} to {{$user->name}}</strong></h3>
            </div>
            <div class="col-xl-2 pl-0">
                <a href="{{url('user')}}" class="pull-right">{{__('Back to users')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            {{--<form method="post" action="{{url('user/assign/facility')}}">--}}
                <div class="col pt-3">
                    <div class="row">
                        <div class="col-5 m-3">

                            @if(!empty($facility))
                                <li class="list-group-item active bg-dark">{{__('Select Facility')}}</li>
                                <ul class="list-group" id="facilities"  style="min-height: 200px; border: 1px solid #f4f4f4">

                                    @foreach($facility as $f)
                                        @if(!in_array(array_get($f, 'FacilityId'), $userfacilities))
                                        <li class="list-group-item" id="{{array_get($f, 'FacilityId')}}">
                                            <input type="hidden" name="facilityId[]" value="{{array_get($f, 'FacilityId')}}">
                                            <i class="fa fa-mouse-pointer"></i> {{array_get($f, 'FacilityName')}}
                                        </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                            Drag &amp; drop a facility from left to right to assign a facility
                        </div>
                        <div class="col m-3">
                            <li class="list-group-item active bg-danger">{{__("Selected facilities")}}</li>
                            <ul class="list-group" id="user_facilities" style="min-height: 200px; border: 1px solid #f4f4f4">
                                @if(isset($user->Facility))
                                    @if(!empty($user->Facility))
                                        @foreach($user->Facility as $uf)
                                                <li class="list-group-item" id="{{array_get($uf, 'FacilityId')}}">
                                                    <input type="hidden" name="facilityId[]" value="{{array_get($uf, 'FacilityId')}}">
                                                    <i class="fa fa-mouse-pointer"></i> {{array_get($uf, 'FacilityName')}}
                                                </li>
                                        @endforeach
                                    @endif
                                @endif
                            </ul>
                            Drag &amp; drop a facility from right to left to un-assign a facility

                        </div>

                    </div>
                    <div class="row p-3">
                        <div class="col">
                            <button class="btn btn-danger pl-4 pr-4 float-right" onclick="assignFacility('{{$user->_id}}')">{{__('Save')}}</button>
                        </div>
                    </div>
                </div>
            {{--</form>--}}
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script>

        $(document).ready(function() {
            var drake = window.dragula();

            drake = dragula([document.querySelector('#facilities'), document.querySelector('#user_facilities')]);

        })


        function assignFacility(userid){

            var facilityIDs = $( 'ul#user_facilities input[name^=facilityId]');

            var facility = [];
            $(facilityIDs).each(function() {
                var val = $(this).val();
                facility.push(val);
            });

            var url = '{{url('user/assign/facility')}}';
            axios.post(url, {
                userid: userid,
                facilityIDs : facility
            }).then(function(response){
                toastr.success('Successfully Saved');
            })
            console.log(facility);
        }


    </script>
@endsection

