@extends('layouts.poc')

@section('content')

<div class="container">
<h4 class="mt-4">Profile - {{Auth::user()->name}}</h4>

<div class="row">
    <div class="col-12">

        <form method="post" action="{{url('user/store')}}">
        {{ csrf_field() }}
            <div class="md-form ">
                <input type="text" id="email" class="form-control " name="email" value="{{Auth::user()->email}}" readonly>
                <label for="email">User Email (Read only)</label>
            </div>

            <div class="md-form ">
                <input type="text" id="name" class="form-control " name="name" value="{{Auth::user()->name}}">
                <label for="name">User Name</label>
            </div>
            
                    @php
                        $items = [];
                    @endphp
                    @foreach(timezone_abbreviations_list() as $abbr => $timezone)
                        @foreach($timezone as $val)
                            @if(isset($val['timezone_id']))
                                @php
                                    $tz = $val['timezone_id'];
                                    if(in_array($tz, $items))
                                        continue;
                                    $items[] = $tz;
                                @endphp
                            @endif
                        @endforeach
                    @endforeach
                    @php
                        asort($items);
                    @endphp

            <div class="md-form">
                <select class="mdb-select" name="timezone">
                    <option value="" disabled selected>Choose your timezone</option>
                        @foreach($items as $tz)
                            @php
                                $selected = "";
                                if(Auth::user()->timezone == $tz)
                                    $selected = "selected";
                            @endphp
                            <option value="{{$tz}}" {{$selected}}>{{$tz}}</option>
                        @endforeach
                </select>
                <label>Timezone</label>
            </div>

            @php
                $date_format = Auth::user()->date_format;
            @endphp
            <!-- Form inline with radios -->
            <div class="md-form">
                <span style="font-size:0.8rem">Date Format (e.g. 30 December 2001)</span>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="date_format" id="date_format_1" value="Y-m-d" {{empty($date_format) || $date_format == "Y-m-d" ? "checked": ""}}>
                    <label class="form-check-label" for="date_format_1">
                        2001-12-30
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="date_format" id="date_format_2" value="d/m/Y" {{$date_format == "d/m/Y" ? "checked": ""}}>
                    <label class="form-check-label" for="date_format_2">
                        30/12/2001
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="date_format" id="date_format_3" value="m/d/Y" {{$date_format == "m/d/Y" ? "checked": ""}}>
                    <label class="form-check-label" for="date_format_3">
                        12/30/2001
                    </label>
                </div>
            </div>
            <!-- Form inline with radios -->

            <div class="md-form">
                    <input type="submit" value="Save" class="btn btn-primary btn-rounded"/>
            </div>
        </form>      
       
    </div>
</div>

</div>


@endsection


@section('script')

<script>
    $(function () {
        $('.mdb-select').material_select();
    });

</script>

@endsection