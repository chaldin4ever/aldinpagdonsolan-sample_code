@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-user"></i> {{__('Users')}}</strong></h3>
                {{--<a data-toggle="modal" data-target="#addUserModal" class="float-right btn btn-blue-grey pl-3 pr-3"--}}
                   {{--style=" color: #fff;">{{__('Add New User')}}</a>--}}
                <a href="{{url('user/admin')}}"  class="float-right btn btn-blue-grey pl-3 pr-3"
                   style=" color: #fff;">{{__('Assign Admin')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            <div class="col pt-3">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>{{__('Username / Email')}}</th>
                        <th>{{__('Last Name')}}</th>
                        <th>{{__('First Name')}}</th>
                        <th>{{__('User Type')}}</th>
                        <th style="width: 150px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(!empty($users))
                           @foreach($users as $user)
                                <tr>
                                    <td>
                                        {{$user->email}}<br />
                                        <small>Role(s): {{$user->UserRoles}}</small><br />
                                        <small>Default: {{$user->DefaultUserRole}}</small>

                                    </td>
                                    <td>{{$user->LastName}}</td>
                                    <td>{{$user->FirstName}}</td>
                                    <td> {{$user->UserType}}</td>
                                    <td>
                                        <select name="action" class=" mdb-select" onchange="redirect(this.value, '{{$user->_id}}')">
                                            <option value="">{{__('Select Action')}}</option>
                                            {{--<option value="edit">{{__('Edit')}}</option>
                                            <option value="role">{{__('Role')}}</option>--}}
                                            <option value="facility">{{__('Facility')}}</option>
                                            <option value="role">{{__('Role')}}</option>
                                          {{--  <option value="reset">{{__('Reset Password')}}</option>
                                            <option value="disable">{{__('Disable')}}</option>--}}
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        @include('user.adduser');
    </div>



@endsection

@section('script')
    <script>

       function validate(){
           var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

           var email = $('#addUserModal input#email').val();
           if(!re.test(email)){
               toastr.warning('Email format invalid');
               return false;
           }else{
               $('form').submit();
           }
       }

        $(document).ready(function() {

            @if(session('warningstatus'))
                    toastr.warning('{{session('warningstatus')}}');
            @endif

            @if(session('status'))
            toastr.success('{{session('status')}}');
            @endif

            $('.mdb-select').material_select();

        });

        function redirect(val, userid){

            if(val === 'facility'){
                url = '{{url('user/facility/')}}/'+userid;
                window.location.href = url;
            }

            if(val === 'role'){
                url = '{{url('user/role/')}}/'+userid;
                window.location.href = url;
            }

        }

    </script>
@endsection

