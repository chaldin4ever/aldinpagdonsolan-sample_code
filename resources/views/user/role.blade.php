@extends('layouts.poc')

@section('style')
    <style>
        ul#roles > li, ul#user_roles > li {
            cursor: move;
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        ul#roles > li:hover, ul#user_roles > li:hover{
            background: #f4f4f4;
        }

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col-xl-10 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-user"></i> {{__('Assign Roles')}} to user {{$user->name}}</strong></h3>
            </div>
            <div class="col-xl-2 pl-0">
                <a href="{{url('user')}}" class="pull-right">{{__('Back to users')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            {{--<form method="post" action="{{url('user/assign/facility')}}">--}}
                <div class="col pt-3">
                    <div class="row">
                        <div class="col m-3">

                            @if(!empty($role))
                                <li class="list-group-item active bg-dark">{{__('Select Roles')}}</li>
                                <ul class="list-group" id="roles"  style="min-height: 200px; border: 1px solid #f4f4f4">
                                    @if(sizeof($role) > 0)
                                        @foreach($role as $f)
                                            @if(!in_array($f->_id, $userroles))
                                            <li class="list-group-item" id="{{$f->_id}}">
                                                <div class="float-left">
                                                <input type="hidden" name="roleId[]" value="{{$f->_id}}">
                                                <i class="fa fa-mouse-pointer"></i> {{$f->roleName}}
                                                </div>
                                                <div class="float-right" id="setdefault" style="display: none;">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" id="default_{{$f->_id}}" value="{{$f->_id}}" name="defaultrole[]" >
                                                        <label class="custom-control-label" for="default_{{ $f->_id}}">Default</label>
                                                    </div>
                                                </div>
                                            </li>
                                            @endif
                                        @endforeach
                                     @else
                                        <li class="list-group-item"  >
                                            Your facility doesn't have a provider or list of roles.
                                        </li>

                                      @endif
                                </ul>
                            @endif
                            Drag &amp; drop a role from left to right to assign a role
                        </div>
                        <div class="col m-3">
                            <li class="list-group-item active bg-danger">{{__("Selected roles")}}</li>
                            <ul class="list-group" id="user_roles" style="min-height: 200px; border: 1px solid #f4f4f4">
                                @if(isset($user->Roles))
                                    @if(!empty($user->Roles))
                                        @foreach($user->Roles as $role)
                                                <li class="list-group-item" id="{{array_get($role, 'RoleId')}}">
                                                    <div class="float-left">
                                                        <input type="hidden" name="roleId[]" value="{{array_get($role, 'RoleId')}}">
                                                    <i class="fa fa-mouse-pointer"></i> {{array_get($role, 'RoleName')}}
                                                    </div>
                                                    <div class="float-right" id="setdefault">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input"
                                                                   @if(array_get($user->DefaultRole, 'RoleId') == array_get($role, 'RoleId')) checked @endif
                                                                   value="{{array_get($role, 'RoleId')}}"  id="default{{array_get($role, 'RoleId')}}"
                                                                   name="defaultrole[]" >
                                                            <label class="custom-control-label" for="default{{array_get($role, 'RoleId')}}">Default</label>
                                                        </div>
                                                    </div>
                                                </li>

                                        @endforeach
                                    @endif
                                @endif
                            </ul>

                            Drag &amp; drop a role from right to left to un-assign a role

                        </div>


                    </div>
                    <div class="row p-3">
                        <!-- Default checked -->

                        <div class="col">
                            <button class="btn btn-danger pl-4 pr-4 float-right" onclick="assignRole('{{$user->_id}}')">{{__('Save')}}</button>
                        </div>
                    </div>
                </div>
            {{--</form>--}}
        </div>
    </div>

@endsection

@section('script')
    <script>

        $(document).ready(function() {

            var drake = window.dragula();

            /*drake = dragula([document.querySelector('#roles'), document.querySelector('#user_roles')], {
                copy: function (el, source) {
                    return source === document.getElementById('#roles')
                },
                accepts: function (el, target) {
                    return target !== document.getElementById('#roles')
                }
            });*/

                /*/!*.on('drop', function (el) {
                    var id = el.id;
                    $('ul#user_roles li#'+id+' div#setdefault').show();

                 }).on('out', function (el) {
                    var id = el.id;

                    $('ul#roles li#'+id+' div#setdefault').hide();
                });
 *!/*/
                drake = dragula([document.querySelector('#roles'), document.querySelector('#user_roles')]).on('drop', function (el) {

                    var id = el.id;
                    var parent = $('#user_roles li#'+id+'').closest('ul');

                    if(parent.length === 0){
                        $('#roles li#'+id+' div#setdefault').hide();
                    }else{
                        $('#'+parent[0].id+' li#'+id+' div#setdefault').show();
                    }

                });

        })


        function assignRole(userid, id){

            var defaultrole = $( 'ul#user_roles input[name^=defaultrole]:checked');
            var roleIDs = $( 'ul#user_roles input[name^=roleId]');

            var roles = [];
            $(roleIDs).each(function() {
                var val = $(this).val();
                roles.push(val);
            });

            if(defaultrole.length === 0){
                $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
                alertify.alert('Please select a Default Role.', function(e){
                    if(e){
                        $('.modal-backdrop').fadeOut("slow");
                    }
                });
            }else{
                var url = '{{url('user/assign/role')}}';
                axios.post(url, {
                    userid: userid,
                    roleIDs : roles,
                    defaultRole: defaultrole[0].value
                }).then(function(response){
                    toastr.success('Successfully Saved');
                })
            }

           // console.log(roles);
        }


    </script>
@endsection

