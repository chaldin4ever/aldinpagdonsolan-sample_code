@extends('layouts.poc')

@section('style')
    <style>
        ul#users > li, ul#user_admin > li {
            cursor: move;
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        ul#users > li:hover, ul#user_admin > li:hover{
            background: #f4f4f4;
        }

        .email {
            color: #6c6c6c;
            font-size: 0.85rem;
            font-weight: 100;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col-xl-10 pl-0">
                <h3 class="p-2"><strong><i class="fa fa-user"></i> {{__('Assign Users')}} as a Facility/Provider Admin</strong></h3>
            </div>
            <div class="col-xl-2 pl-0">
                <a href="{{url('user')}}" class="pull-right">{{__('Back to users')}}</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-white">
            {{--<form method="post" action="{{url('user/assign/facility')}}">--}}
            <div class="col pt-3">
                <div class="row">
                    <div class="col-5 m-3">

                        @if(!empty($users))
                            <li class="list-group-item active bg-dark">{{__('Users')}}</li>
                            <ul class="list-group" id="users"  style="min-height: 200px; border: 1px solid #f4f4f4">
                                @if(sizeof($users) > 0)
                                    @foreach($users as $user)

                                            <li class="list-group-item" id="{{$user->_id}}">
                                                <div class="float-left">
                                                    <input type="hidden" name="userId[]" value="{{$user->_id}}">
                                                    <i class="fa fa-mouse-pointer"></i> {{$user->name}} <span class="email">({{$user->email}})</span>
                                                </div>
                                                <div class="float-right" id="setdefault" style="display: none;" >
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" class="custom-control-input" id="facility_{{$user->_id}}" value="{{config('poc.facility_admin')}}" name="usertype_{{$user->_id}}" >
                                                        <label class="custom-control-label" for="facility_{{$user->_id}}">Facility</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" class="custom-control-input" id="provider_{{$user->_id}}" value="{{config('poc.provider_admin')}}" name="usertype_{{$user->_id}}" >
                                                        <label class="custom-control-label" for="provider_{{$user->_id}}">Provider</label>
                                                    </div>

                                                </div>
                                            </li>

                                    @endforeach
                                @else
                                    <li class="list-group-item"  >
                                        Your facility doesn't have a provider or list of roles.
                                    </li>

                                @endif
                            </ul>
                        @endif
                        Drag &amp; drop a user from left to right to assign as a facility/provider admin.
                    </div>
                    <div class="col m-3">
                        <li class="list-group-item active bg-danger">{{__("Selected users")}}</li>
                        <ul class="list-group" id="user_admin" style="min-height: 200px; border: 1px solid #f4f4f4">
                                @if(!empty($useradmins))
                                    @foreach($useradmins as $useradmin)
                                        <li class="list-group-item" id="{{$useradmin->_id}}">
                                            <div class="float-left">
                                                <input type="hidden" name="userId[]" value="{{$useradmin->_id}}">
                                                <i class="fa fa-mouse-pointer"></i> {{$useradmin->name}} <span class="email">({{$useradmin->email}})</span>
                                            </div>
                                            <div class="float-right" id="setdefault">
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="custom-control-input" id="facility_{{$useradmin->_id}}"
                                                           @if($useradmin->UserType == config('poc.facility_admin')) checked @endif
                                                           value="{{config('poc.facility_admin')}}" name="usertype_{{$useradmin->_id}}" >
                                                    <label class="custom-control-label" for="facility_{{$useradmin->_id}}">Facility</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="custom-control-input" id="provider_{{$useradmin->_id}}"
                                                           @if($useradmin->UserType == config('poc.provider_admin')) checked @endif
                                                           value="{{config('poc.provider_admin')}}" name="usertype_{{$useradmin->_id}}" >
                                                    <label class="custom-control-label" for="provider_{{$useradmin->_id}}">Provider</label>
                                                </div>

                                            </div>
                                        </li>

                                    @endforeach
                                @endif

                        </ul>

                        Drag &amp; drop a user from right to left to un-assign as a facility/provider admin.<br />
                        <button class="btn btn-danger pl-4 pr-4 float-right" onclick="assignAdmin()">{{__('Save')}}</button>

                    </div>


                </div>
                <div class="row p-3">
                    <!-- Default checked -->

                    <div class="col">

                    </div>
                </div>
            </div>
            {{--</form>--}}
        </div>
    </div>

@endsection

@section('script')
    <script>

        $(document).ready(function() {

            var drake = window.dragula();


            drake = dragula([document.querySelector('#users'), document.querySelector('#user_admin')]).on('drop', function (el) {

                var id = el.id;
                var parent = $('#user_admin li#'+id+'').closest('ul');

                if(parent.length === 0){
                    $('#users li#'+id+' div#setdefault').hide();
                }else{
                    $('#'+parent[0].id+' li#'+id+' div#setdefault').show();
                }

            });

        })


        function assignAdmin(){

            var defaultadmin = $( 'ul#user_admin input[type^=radio]:checked');
            var userIDs = $( 'ul#user_admin input[name^=userId]');

            console.log(defaultadmin);
            console.log(userIDs);
            var users = [];
            $(userIDs).each(function() {
                var val = $(this).val();
                users.push(val);
            });

            var admin = [];
            $(defaultadmin).each(function() {
                var val = $(this).val();
                admin.push(val);
            });

            if(defaultadmin.length !== userIDs.length){
                $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
                alertify.alert('Please select a Provider/Facility for each selected user.', function(e){
                    if(e){
                        $('.modal-backdrop').fadeOut("slow");
                    }
                });
            }else{

                var url = '{{url('user/assign/admin')}}';
                axios.post(url, {
                    users : users,
                    admin: admin
                }).then(function(response){
                    console.log(response.data);
                    toastr.success('Successfully Saved');
                })
            }

            // console.log(roles);
        }


    </script>
@endsection

