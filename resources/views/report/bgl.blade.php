@extends('layouts.poc')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/datatables.min.css')}}"/>

    <style>
        .dataTables_wrapper .dataTables_filter {
            float: left !important;
            text-align: right;
        }

        #search {
            height: auto !important;
        }
    </style>


@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-bar-chart"></i> {{__('BGL')}}</strong></h3>
            </div>


        </div>
    </div>

    <div class="container-fluid pb-4">


        <div class="row bg-white" style="min-height: 600px;">
            <div class="col-5 pt-3">
                @if(!empty($residents))

                    @if(sizeof($residents)>0)
                        <table class="table table-striped table-bordered  " id="bglTable">
                            <thead>
                            <th class="text-center">Room</th><th>Resident Name</th>
                            </thead>
                            @foreach($residents as $r)
                                <tr>
                                    @php

                                        $id = $r->_id;
                                        $room = !empty($r->CurrentRoom) ? array_get($r->CurrentRoom, 'RoomName') : $r->Room;

                                    @endphp

                                    <td class="p-2 text-center">  {{$room}}</td>
                                    <td  class="p-2 text"> <a href="#" onclick="getResidentWeightChart('{{$id}}')">{{$r->FullName}} </a> </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <div class="alert alert-info">
                            <p>No Resident.</p>
                        </div>
                    @endif
                @endif


            </div>
            <div class="col-6 mt-4 pt-4" id="bglchart"  >
                <iframe id="chart" width="100%" height="100%" frameBorder="0"  src=""></iframe>
            </div>
        </div>

        <div class="row bg-white pb-4">


        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('js/datatables.min.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('js/dataTables.material.min.js')}}"></script>--}}

    <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
    {{-- {!! $incidentchart->script() !!}--}}


    <script>


        $(document).ready(function(){

            $('#bglTable').DataTable({
                scrollY:        '50vh',
                "scrollCollapse": true,
                "paging":         false,
                "dom": '<"toolbar">frtip'
            });

            var html = " <div class=\"md-form\" >\n" +
                "                                <input placeholder=\" \"   type=\"text\" id=\"custom_search\" class=\"form-control \" name=\"search\">\n" +
                "                                <label for=\"custom_search\">{{__('Search (Room, Resident Name)')}}</label>\n" +
                "                            </div>";
            $("div.toolbar").html(html);
            $('input#custom_search').on('keyup', function(){
                $('#bglTable_filter input').trigger('keyup').val(this.value);
            });
            $('#bglTable_filter').css('height', '0px');

        });

        function getResidentWeightChart(id){


            var url = '{{url('report/bgl/')}}/'+id;
            var $iframe = $('iframe#chart');
            $iframe.attr('src',url);

        }
    </script>
@endsection

