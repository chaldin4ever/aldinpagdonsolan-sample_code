@extends('layouts.poc')

@section('style')
    <style>

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-bar-chart"></i> {{__('Infections')}}</strong></h3>
                {{--<a   href="{{url('indicator')}}"  class="float-right back-to-link">Back to Indicator</a>--}}
            </div>


        </div>
    </div>

    <div class="container-fluid pb-4">
        <div class="row bg-white  pt-4 pb-2 border-bottom">
            <div class="col">

                @if(!empty($message))
                    <div class="row bg-white pt-0 pl-2 mb-3"><div class="alert alert-info w-100"><strong>{{$message}}</strong></div></div>
                @endif
                <form method="get" action="">
                    @csrf

                    <div class="row bg-white pt-0 pl-2 mb-3">

                        <div class="col-md-3">

                            <div class="md-form">
                                <!--The "from" Date Picker -->
                                <i class="fa fa-calendar prefix"></i>
                                <input placeholder="Start Date" value="{{$fromDate}}" type="text" id="startingDate" class="form-control datepicker_start">
                                <label for="startingDate">{{__('Start Date')}}</label>
                            </div>

                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-md-3 mr-0">

                            <div class="md-form">
                                <!--The "to" Date Picker -->
                                <i class="fa fa-calendar prefix"></i>
                                <input placeholder="End Date" value="{{$toDate}}" type="text" id="endingDate" class="form-control datepicker_end">
                                <label for="endingDate">{{__('End Date')}}</label>
                            </div>

                        </div>

                        <div class="col-md-4 pt-4 pl-0">

                            <button class="btn float-left btn-blue-grey btn-rounded btn-sm  ml-0">{{__('Search')}}</button>

                        </div>


                    </div>



                </form>
            </div>
        </div>

        <div class="row bg-white">

            <div class="col-6 pt-3">

                <div>{!! $infectionchart->container() !!}</div>

            </div>


            <div class="col-6 pt-3">
                <table class="table  float-left w-100 table-striped text-sm-center">
                    <thead>
                    <tr>
                        <th class="p-2 pl-4">Month-Year</th>
                        <th class="p-2">Total Number of Infections</th>
                    </tr>
                    </thead>
                    @foreach($infectiondata as $fd)
                        <tr>
                            <td  class="p-1 pl-4">{{array_get($fd, 'monthyear')}}</td>
                            <td  class="p-1">{{array_get($fd, 'count')}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>


        </div>


        <div class="row bg-white pb-4">

            <div class="col-6 pt-3">

                <div>{!! $infectionbytypechart->container() !!}</div>

            </div>


            <div class="col-6 pt-3">
                <table class="table float-left  w-100 table-striped ">
                    <thead>
                    <tr>
                        <th class="p-2 pl-4 w-75"  >Infection By Type</th>
                        <th class="p-2 w-25" style="width: 120px;">Total Numbers</th>
                    </tr>
                    </thead>
                    @foreach($infectionbytypedata as $key=>$factorArr)
                        <tr>
                            <td  class="p-1 pl-4">{{array_get($factorArr, 'Title')}}</td>
                            <td  class="p-1 text-sm-center">{{array_get($factorArr, 'Total')}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>


        </div>

    </div>
    </div>

@endsection

@section('script')
    <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
     {!! $infectionchart->script() !!}
     {!! $infectionbytypechart->script() !!}

    <script>
        $(document).ready(function(){

            $('.mdb-select').material_select();

            $('.datepicker_start').pickadate({
                // Escape any “rule” characters with an exclamation mark (!).
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                hiddenPrefix: 'From',
                hiddenSuffix: 'Date'
            })

            $('.datepicker_end').pickadate({
                // Escape any “rule” characters with an exclamation mark (!).
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                hiddenPrefix: 'To',
                hiddenSuffix: 'Date'
            })


            // Get the elements
            var from_input = $('#startingDate').pickadate(),
                from_picker = from_input.pickadate('picker')
            var to_input = $('#endingDate').pickadate(),
                to_picker = to_input.pickadate('picker')

// Check if there’s a “from” or “to” date to start with and if so, set their appropriate properties.
            if ( from_picker.get('value') ) {
                to_picker.set('min', from_picker.get('select'))
            }
            if ( to_picker.get('value') ) {
                from_picker.set('max', to_picker.get('select'))
            }

// Apply event listeners in case of setting new “from” / “to” limits to have them update on the other end. If ‘clear’ button is pressed, reset the value.
            from_picker.on('set', function(event) {
                if ( event.select ) {
                    to_picker.set('min', from_picker.get('select'))
                }
                else if ( 'clear' in event ) {
                    to_picker.set('min', false)
                }
            })
            to_picker.on('set', function(event) {
                if ( event.select ) {
                    from_picker.set('max', to_picker.get('select'))
                }
                else if ( 'clear' in event ) {
                    from_picker.set('max', false)
                }
            })
        });
    </script>
@endsection

