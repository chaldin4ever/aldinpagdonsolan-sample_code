<form method="post" id="filter" action="@if(empty($page)){{url('resident/')}}@else {{url('resident/indicator/'.$page)}}@endif">
    @csrf
    <input type="hidden" name="page" id="page" value="@if(isset($page)){{$page}}@endif"/>
    <div class="row bg-white pt-4 pl-2">
        <div class="col-3">
            <div class="md-form " style="margin-top:0rem">
                <select name="Facility" id="Facilities" class="mdb-select-facility  " onchange="setFacility(this.value)" placeholder=" ">
                    <option value="" selected disabled>--All Facilities--</option>
                    @if(!empty($facility_select))
                        @foreach($facility_select as $fa)
                            @php $facilityID = array_get($fa, 'FacilityId'); @endphp
                            <option value="{{$facilityID}}"
                                    @if(Request::has('Facility'))
                                    @if(Request::get('Facility') == $facilityID)
                                    selected
                                    @endif
                                    @else
                                    @if(!empty($facilityId)) @if($facilityId == $facilityID)
                                    selected
                                    @endif
                                    @endif
                                    @endif
                                    {{--@endif--}}
                            >{{array_get($fa, 'FacilityName')}}</option>
                        @endforeach
                    @endif
                </select>
                <label for="Facilities">{{__('Facility')}}</label>
            </div>
        </div>
        <div class="col-3">
            <div class="md-form area"  style="margin-top:0rem">
                <select name="Area" id="Area" class="mdb-select  "  placeholder=" " >
                    <option value="" selected disabled>--All Areas--</option>

                </select>
                <label for="Area">{{__('Areas')}}</label>
            </div>
        </div>
        <div class="col-4">
            <div class="md-form pt-2"  style="margin-top:0rem">
                <input type="text" name="ResidentName" autocomplete="off" placeholder="Enter Resident Name or Room No. to filter..." class="form-control" @if(Request::get('ResidentName')) value="{{Request::get('ResidentName')}}" @endif />
                <label for="ResidentName">{{__('Resident Name, Preferred Name or Room Number')}}</label>
            </div>
        </div>
    </div>
    @php
        $statusArr = [
        'permanent'=>'Permanent',
        'respite' => 'Respite',
        'transitional'=> 'Transitional',
        'discharge' => 'Discharged',
        'archive' => 'Archived'
        ];
    @endphp
    <div class="row bg-white pl-2">
        <div class="form-inline">
            @foreach($statusArr as $k=>$st)
                <div class="form-check">
                    <input type="checkbox" name="StatusFilter[]" class="filled-in form-check-input" value="{{$k}}" id="{{$st}}"  @if(!empty($selectedStatus)) @if(in_array($k, $selectedStatus))  checked="checked" @endif @endif >
                    <label class="form-check-label" for="{{$st}}" style="font-size:0.85rem">{{$st}}</label>
                </div>
            @endforeach



            <div class="form-check ml-3">
                <button class="btn btn-blue-grey btn-rounded btn-sm">{{__('Search')}}</button>
            </div>

        </div>
    </div>
</form>