@extends('layouts.poc')

@section('style')
    <style>
        #search {
            height: 40px;
            margin-top: 5px;
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{__('Chart')}}</strong></h3>
                <a href="{{url('resident/view/'.$resident->_id)}}" class="float-right" style="font-size:0.85rem">Back to resident view</a>
            </div>


        </div>
    </div>

    <div class="container-fluid">
        <div class="row bg-white pl-2 pt-2 pb-2 mb-3">

            @include('resident.header', ['resident' => $resident])

        </div>

        <div class="row bg-white">
            <div class="col pt-3">

                <ul class="nav nav-tabs nav-justified p-0 m-0" id="residentTab" style="background-color: #949494; z-index: 1 !important;">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#bgl" role="tab" id="bgl_link" >{{__('BGL')}}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#bp" role="tab" id="bp_link" >{{__('BP')}}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#pulse" role="tab" id="pulse_link" >{{__('Pulse')}}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#weight" role="tab" id="weight_link" >{{__('Weight')}}</a>
                    </li>

                </ul>

                <div class="tab-content card">
                    <!--Panel 1-->
                    <div class="tab-pane fade active show" id="bgl" role="tabpanel">
                        {{--@include('assessment.tabs.progressnotes', compact('progressnotes'))--}}
                        <div class="row">
                            <div class="col-8">
                                <div>{!! $bglchart->container() !!}</div>
                            </div>
                            <div class="col-4">
                                <table class="table table-striped table-bordered bgl">
                                    <thead>
                                    <tr>

                                        <th>{{__('Date')}}{{__('Time')}}</th>

                                        <th class="w-20">{{__('BGL')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($bgl as $b)
                                            <tr>

                                                <td>{{$b->BGLDateTime}}</td>

                                                <td>{{array_get($b->data, 'BGL')}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade show" id="bp" role="tabpanel">
                        {{--@include('assessment.tabs.progressnotes', compact('progressnotes'))--}}
                        <p>BP Content</p>
                    </div>

                    <div class="tab-pane fade show" id="pulse" role="tabpanel">
                        {{--@include('assessment.tabs.progressnotes', compact('progressnotes'))--}}
                        <p>Pulse Content</p>
                    </div>

                    <div class="tab-pane fade show " id="weight" role="tabpanel">
                        {{--@include('assessment.tabs.progressnotes', compact('progressnotes'))--}}
                        <p>WEight Content</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>
     {!! $bglchart->script() !!}
@endsection

