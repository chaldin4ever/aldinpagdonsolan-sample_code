@extends('layouts.poc')

@section('style')
<style>
    .white-skin input[type=checkbox].filled-in:checked+label:after {
        background-color: rgb(90, 118, 147);
        border-color: rgb(90, 118, 147);
    }
    .white-skin body{
        'Helvetica Light', Helvetica, Arial, sans-serif
    }

    td .select-wrapper input.select-dropdown {padding-top: 5px; padding-left: 25px !important; font-size: 14px !important;}

    tr.archive , tr.archive div{
        text-decoration: line-through !important;
    }

    ul li {
        list-style:none !important;
        margin: 0 0 !important;
    }
</style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pt-4  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-users"></i> {{__('Residents')}}</strong></h3>

                {{--<a data-toggle="modal" data-target="#addPatientModal" class="float-right btn btn-blue-grey pl-3 pr-3 invisible"
                   onclick="$('#addPatientModal form#resident .modal-body input, #addPatientModal form#resident .modal-body textarea').val('');
                            $('#addPatientModal .modal-title').text('Add Resident');
                            $('#addPatientModal button#archive').hide();
                           $('#addPatientModal input#_id').val('');
                            $('#addPatientModal input#Facility').val('{{$currentFacility->_id}}');
                           $('#addPatientModal input#Provider').val('{{array_get($currentFacility->Provider, 'ProviderId')}}');

                    "
                   style=" color: #fff;">
                    <i class="fa fa-plus" ></i> {{__('New')}}
                </a>--}}
            </div>
            {{--<div class="col-4 pl-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item "><a href="{{url('landingpage')}}" ><i class="fa fa-arrow-circle-left "></i> {{__('Go Back')}}</a></li>
                    <li class="pl-5 pr-4">

                    </li>
                </ol>
            </div>--}}

        {{--    <div class="fixed-action-btn" style="top:60px; right: 105px; height: 140px;">
                <a data-toggle="modal" data-target="#addPatientModal" class="btn-floating btn-grey"
                   onclick="$('#addPatientModal form#resident input, #addPatientModal form#resident textarea').val(''); $('#addPatientModal .modal-title').text('Add Resident');"
                   style=" color: #fff;">
                    <i class="fa fa-plus"  data-toggle="tooltip" data-placement="bottom" title="{{__('Add New Resident')}}"></i>
                </a>

            </div>--}}

        </div>
    </div>

    <div class="container-fluid">
        <div class="content ml-3 mr-3 pl-2 pb-5">

            @include('resident.filter')

                <div class="row bg-white">
                        <div class="col-12">
                            {{--<pre>{{print_r($residents)}}</pre>--}}
                            <?php /*echo $residents->render(); */?>
                        @if(!empty($residents))
                            @if(sizeof($residents) > 0)
                                <table class="mt-3 table table-hover table-bordered table-striped">
                                        <thead class="blue-grey text-white font-weight-bold" style="background: #9bb1c7 !important;">
                                            {{--<th style="width: 180px;">{{__('Facility')}}</th>--}}
                                            <tr>

                                            <th>{{__('Full Name')}}</th>
                                                <th>{{__('Prefer Name')}}</th>
                                                <th >{{__('Room')}}</th>
                                            <th >{{__('Status')}}</th>
                                            <th  style="width: 50px;">{{__('Actions')}}</th>

                                            </tr>
                                </thead>
                                <tbody>
                                @foreach($residents as $r)
                                    @php

                                        $photo = \App\Models\ResourceFile::GetPhoto(array_get($r, 'ResourceFileId'));
                                        $residentPhoto = array_get($r, 'Photo');
                                    @endphp
                                    @php $id = array_get($r, '_id');
                                            $resident = \App\Models\Resident::find($id);
                                            $residentfile = \App\Models\ResourceFile::where('Resident.ResidentId', (string)$id)->first();  @endphp
                                    <tr
                                            @if(array_get($r, 'Archive') == true)
                                                class=" archive";
                                            @endif
                                        >

                                        <td>
                                            <a href="{{url('resident/view/'.array_get($r, '_id'))}}" class=" ">
                                                @if(empty($residentfile) && empty($photo))

                                                    <img  id="photo_{{array_get($r, '_id')}}"   class="rounded-circle float-left mr-2" src="http://via.placeholder.com/50x50" />
                                                @else
                                                    @if(!empty($residentfile))

                                                        <img id="photo_{{array_get($r, '_id')}}" src="{{$residentfile->AWSFile}}" class="rounded-circle float-left mr-2" height=50px  />
                                                    @else
                                                        <img id="photo_{{array_get($r, '_id')}}" class="rounded-circle float-left mr-2" height=50px  src="data:image/png;base64,{{$photo}}"/>
                                                    @endif
                                                @endif

                                                    @php
                                                        $obj = new \App\Http\Controllers\ResidentController();
                                                        $totalPinboard = $obj->getReminder(array_get($r, '_id'));
                                                    @endphp
                                                    @if($totalPinboard > 0)
                                                        <div class="counter rounded-circle position-absolute  float-right" style="margin-left:37px;  margin-top: -10px;">{{$totalPinboard}}</div>
                                                    @endif

                                            </a>

                                            <div class="float-left pt-2">{{array_get($r, 'FullName')}} </div></td>
                                        <td>{{array_get($r, 'PreferredName')}}</td>
                                        <td>
                                            {{array_get($resident->Object, 'Room')}}
{{--                                            {{array_get(array_get($r, 'CurrentRoom'), 'RoomName')}}--}}
                                            {{--{{$residentPhoto}}--}}
                                        </td>
                                        <td>
                                            @php
                                                if(is_int(array_get($r, 'Status'))){
                                                    $statusArr = [
                                                        1=>'Permanent',
                                                        2 => 'Respite',
                                                        3=> 'Transitional',
                                                        4 => 'Discharged',
                                                        5 => 'Archived'
                                                        ];

                                                   $status = array_get($statusArr, array_get($r, 'Status')) ;

                                                }else{
                                                    $status = ucfirst(array_get($r, 'Status'));
                                                }


                                            @endphp
                                            {{$status}}</td>

                                            <td>
                                                @php $data = json_encode($r); @endphp
                                                <a href="{{url('resident/edit/'.array_get($r, '_id'))}}" id="{{array_get($r, '_id')}}"  {{--data-toggle="modal" data-target="#addPatientModal"--}}
                                                   class=" text-center ml-3
                                                           @if(array_get($r, 'Archive') == true)
                                                                disabled
                                                           @endif
                                                    "
                                                  {{-- onclick="getResident('{{$data}}')"--}}><i class="fa fa-edit fa-1x " style="color: #003056; font-size: 21px;"></i></a>
                                            </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                </table>
                             @else
                                <div class="alert alert-info mt-4"><strong>{{__('No resident(s) found.')}}</strong></div>
                             @endif
                         @else
                          <div class="alert alert-info"><strong>{{__('No resident(s) found.')}}</strong></div>
                         @endif
                        {{--{{$residents->links()}}--}}
                        <?php echo $residents->appends(\Illuminate\Support\Facades\Input::except(['page','_token']))->render(); ?>
                </div>
            </div>
        </div>
    </div>

    @include('resident.add')
@endsection

@section('script')
    <script>

        $(document).ready(function(){

            var facilityId = $('select#Facilities').val();
            getArea(facilityId);
        })

        var facility = jQuery.parseJSON('{!! $facility !!}');
        var facilityArray = $.map(facility, function(el) { return el.FacilityName; } );

        $('#Facility').mdb_autocomplete({
            data: facilityArray
        });

        function GetFacilityId(facilityname)
        {
            $.each(facility, function(n, item) {
                if (item.FacilityName === facilityname) {
                    $('#FacilityId').val(item.id);
                    // alert(item.id);
                }
            });
        }

        function GetFacilityName(facilityId)
        {
            var facilityName;
            $.each(facility, function(n, item) {
                if (item.id === facilityId) {
                    facilityName = item.FacilityName;
                }
            });

            return facilityName;
        }

        var doctor = jQuery.parseJSON('{!! $doctors !!}');
        var doctorArray = $.map(doctor, function(el) { return el.DoctorName; } );

        $('#Doctor').mdb_autocomplete({
            data: doctorArray
        });

        function GetDoctorId(name)
        {
            $.each(doctor, function(n, item) {
                if (item.DoctorName === name) {
                    $('#DoctorId').val(item.id);
                    // alert(item.id);
                }
            });
        }

        var pharma = jQuery.parseJSON('{!! $pharmacy !!}');
        var pharmaArray = $.map(pharma, function(el) { return el.PharmacyName; } );

        $('#Pharmacy').mdb_autocomplete({
            data: pharmaArray
        });

        function GetPharmacyId(name)
        {
            $.each(pharma, function(n, item) {
                if (item.PharmacyName === name) {
                    $('#PharmacyId').val(item.id);
                    // alert(item.id);
                }
            });
        }

        var client = jQuery.parseJSON('{!! $clients !!}');
        var clientArray = $.map(client, function(el) { return el.ClientName; } );

/*        console.log(client);
        console.log(clientArray);*/
        $('#Client').mdb_autocomplete({
            data: clientArray
        });

        @php
                $statuslist = json_encode([
                1=>'Permanent',
                2=> 'Respite',
                3=> 'Transitional',
                4=> 'Discharged',
                5=> 'Archived'
                ]);
                @endphp
        var statusList = $.parseJSON('{!! $statuslist !!}');

        function GetClientId(name)
        {
            $.each(client, function(n, item) {
                if (item.ClientName === name) {
                    $('#ClientId').val(item.id);
                    // alert(item.id);
                }
            });
        }

        function getResident(data){

            $('#addPatientModal button#archive').show();

            var rData = jQuery.parseJSON(data);

            // console.log(rData);
            $('.modal-title').text('Update Resident');
            $.each(rData, function(key, val){

                $('#'+key).val(val);

                if(key === '_id'){
                    $('#'+key).val(val.$oid);
                }

                if(key === 'Gender') {
                    $('#addPatientModal .select-dropdown li:contains(' + val + ')').trigger('click');
                }

                if(key === 'Status') {

                    var status = val.charAt(0).toUpperCase();
                    $(' #addPatientModal .Status .select-dropdown li:contains(' + status + ')').trigger('click');
                }

                if(key === 'DOB') {
                    $('#addPatientModal input[name="DOB_submit"]').val(val);
                }

                if(key === 'Client'){
                    $('#addPatientModal #'+key).val(val.clientName);
                    $('#addPatientModal #'+key+'Id').val(val.clientId);
                }

                if(key === 'Facility'){
                    var facility = GetFacilityName(val.facilityId);
                    // console.log(facility);
                    $('#addPatientModal #'+key).val(facility);
                    $('#addPatientModal #'+key+'Id').val(val.facilityId);
                }

                if(key === 'Doctor'){
                    $('#addPatientModal #'+key).val(val.FirstName+' '+val.LastName);
                    $('#addPatientModal #'+key+'Id').val(val.DoctorId);
                }

                if(key === 'Pharmacy'){
                    $('#addPatientModal #'+key).val(val.PharmacyName);
                    $('#addPatientModal #'+key+'Id').val(val.PharmacyId);
                }
            })
        }

        $(document).ready(function() {

            $('.mdb-select-gender').material_select();
            $('.mdb-select').material_select();
            $('.mdb-select-facility').material_select();

            $('.datepicker').pickadate({
                format: 'dd/mm/yyyy',
                formatSubmit: 'yyyy-mm-dd',
                selectYears: 50,
                max: true
            });

            @if (session('status'))
             toastr.success('{{ session('status') }}');
            @endif

            getArea($('#Facilities').val());
            // alert($('#Facilities').val());

        });

        $(document).ready(function () {

            $('.fixed-action-btn').unbind('click');

            $('ul.pagination').addClass(' pg-red');
            $('ul.pagination').addClass(' pagination-lg');
            $('ul.pagination').addClass(' justify-content-center');
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        function getArea(facilityId){

            var areaid = '{{Request::get('Area')}}' ? '{{Request::get('Area')}}' : 'null';
            var url='{{url('facility/getarea')}}/'+facilityId+'/'+areaid;

            axios.get(url).then(function(response){


                var data = response.data;
                $(' .area').html(data);
                $('.mdb-select-area').material_select();
            });
        }

        function setFacility(facilityId){

            var url='{{url('facility/selected')}}/'+facilityId;

            window.location.href = url;

        }

        function submitform(){
            $('form#filter').submit();
        }

        function archive_resident(){
            $( "#addPatientModal " ).append( "<div class='modal-backdrop fade show'></div>" );

            var id = $('#addPatientModal input#_id').val();
            var url = '{{url('resident/archive')}}/'+id;

            alertify.confirm('Are you sure?', function(e){

                if(e){
                    window.location.href = url;
                }

                $('#addPatientModal .modal-backdrop').fadeOut("slow");
            })
        }

       {{-- @foreach($residents as $r)

                @php $id = array_get($r, '_id');
                    $residentfile = \App\Models\ResourceFile::where('Resident.ResidentId',(string)$id)->first();  @endphp
--}}{{--                    console.log('{{print_r($residentfile)}}');--}}{{--
                @if(!empty($residentfile))
                        /*var photo = JSON.parse('{!! $residentfile->File !!}');
                        $('#photo_{{$id}}').attr('src', photo.data);*/
                        var url = '{{$residentfile->AWSFile}}';
                        $('#photo_{{$id}}').attr('src', url);
                @endif
        @endforeach--}}

    </script>
    @endsection

