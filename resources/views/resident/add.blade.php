<!-- Modal -->
<div class="modal fade" id="addPatientModal" tabindex="-1" role="dialog" aria-labelledby="addPatientModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" action="{{url('resident/store')}}" id="resident" >
            @csrf
{{--            {{csrf_field()}}--}}
            <input type="hidden" name="_id" id="_id" />
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-2 bg-dark text-white">
                    <h5 class="modal-title"  >{{__('Add Resident')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="col-4">
                            {{--<input type="hidden" name="doctorId" value="{{$doctor->_id}}" />--}}
                            <div class="md-form">
                                <input required type="text" id="FirstName" name="FirstName" value="" autocomplete="off" class="form-control" placeholder=" ">
                                <label for="firstname">{{__('First Name')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="LastName" name="LastName" value="" class="form-control" autocomplete="off" placeholder=" ">
                                <label for="lastname">{{__('Last Name')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="PreferredName" name="PreferredName" value="" class="form-control" autocomplete="off" placeholder=" ">
                                <label for="PreferredName">{{__('Preferred Name')}}</label>
                            </div>


                            <div class="md-form">
                                <input required type="text" id="DOB" name="DOB" value="" class="form-control datepicker" autocomplete="off"  placeholder=" ">
                                <label for="dob">{{__('Date of Birth')}}</label>
                            </div>

                            <div class="md-form">
                                <select name="Gender" id="Gender" class="mdb-select-gender  "  placeholder=" ">
                                    <option value="">Select</option>
                                    <option value="M"  >{{__('Male')}}</option>
                                    <option value="F"  >{{__('Female')}}</option>
                                </select>
                                <label for="gender">{{__('Gender')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="Address" name="Address" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="address">{{__('Address')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="Suburb" name="Suburb" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="suburb">{{__('Suburb')}}</label>
                            </div>

                        </div>

                        <div class="col-4">
                            <div class="md-form Status">
                                <select name="ResidentStatus" id="Status" class="mdb-select  "  placeholder=" ">
                                    <option value="" selected disabled>Select</option>
                                    <option value="permanent"  >{{__('Permanent')}}</option>
                                    <option value="respite"  >{{__('Respite')}}</option>
                                    <option value="transitional"  >{{__('Transitional')}}</option>
                                    <option value="discharge"  >{{__('Discharged')}}</option>
                                    {{--<option value="archive"  >{{__('Archived')}}</option>--}}
                                </select>
                                <label for="Status">{{__('Status')}}</label>
                            </div>

                            <div class="md-form">
                                <input type="text" id="Postcode" name="PostCode" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="postcode">{{__('Post Code')}}</label>
                            </div>

                            <div class="md-form">
                                {{--<input readonly type="text" id="Room" name="Room" value="" class="form-control bg-light" autocomplete="off"  placeholder=" ">--}}
                                <select name="Room" class="mdb-select" searchable="Search ..">
                                    <option disabled selected>Select</option>
                                    @foreach($rooms as $room)
                                        <option value="{{$room->_id}}">{{$room->RoomName}}</option>
                                     @endforeach
                                </select>
                                <label for="Room">{{__('Room')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="URN" name="URN" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="URN">{{__('URN')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="MedicareNumber" name="MedicareNumber" value="" class="form-control" autocomplete="off"  placeholder=" ">
                                <label for="PreferredName">{{__('Medicare Number')}}</label>
                            </div>


                            <div class="md-form">
                                <input required type="text" id="PensionNumber" name="PensionNumber" value="" class="form-control  " autocomplete="off"  placeholder=" ">
                                <label for="PensionNumber">{{__('Pension Number')}}</label>
                            </div>

                            <div class="md-form">
                                <input required type="text" id="DVANumber" name="DVANumber" value="" class="form-control  " autocomplete="off"  placeholder=" ">
                                <label for="DVANumber">{{__('DVA Number')}}</label>
                            </div>
                        </div>

                        <div class="col-4">

                            <div class="md-form">

                                <input required type="text" id="ProviderName" readonly name="ProviderName" value="{{array_get($currentFacility->Provider, 'ProviderName')}}" class="form-control  bg-light"
                                       autocomplete="off"  placeholder="{{array_get($currentFacility->Provider, 'ProviderName')}}">
                                <label for="Client">{{__('Provider')}}</label>
                                <input required type="hidden" id="Provider" name="Provider" class="form-control  "
                                       autocomplete="off"  value="{{array_get($currentFacility->Provider, 'ProviderId')}}">{{--
                                <input required type="text" id="Client" name="Client" value="" class="form-control  "
                                       onblur="GetClientId(this.value)"
                                       autocomplete="off"  placeholder=" ">
                                <label for="Client">{{__('Client')}}</label>
                                <input  type="hidden" id="ClientId" name="ClientId" value="" class="form-control  ">--}}
                            </div>

                            <div class="md-form">
                                <input required type="text" id="FacilityName" readonly name="FacilityName" value="{{$currentFacility->NameLong}}" class="form-control  bg-light"
                                       autocomplete="off"  placeholder="{{$currentFacility->NameLong}}">
                                <label for="FacilityName">{{__('Facility')}}</label>
                                <input required type="hidden" id="Facility" name="Facility" class="form-control  "
                                       autocomplete="off"  value="{{$currentFacility->_id}}">
                                {{--<input required type="text" id="Facility" name="Facility" value="" class="form-control  "
                                       onblur="GetFacilityId(this.value)"
                                       autocomplete="off"  placeholder=" ">
                                <label for="Facility">{{__('Facility')}}</label>
                                <input  type="hidden" id="FacilityId" name="FacilityId" value="" class="form-control  ">--}}
                            </div>

                            <div class="md-form">
                                <input  type="text" id="Doctor" name="Doctor" value="" class="form-control  "
                                       onblur=" GetDoctorId(this.value); "
                                       autocomplete="off"  placeholder=" ">
                                <label for="Doctor">{{__('Doctor')}}</label>
                                <input  type="hidden" id="DoctorId" name="DoctorId" value="" class="form-control  ">
                            </div>

                            <div class="md-form">
                                <input  type="text" id="Pharmacy" name="Pharmacy" value="" class="form-control  "
                                       onblur="GetPharmacyId(this.value)"
                                       autocomplete="off"  placeholder=" ">
                                <label for="Pharmacy">{{__('Pharmacy')}}</label>
                                <input  type="hidden" id="PharmacyId" name="PharmacyId" value="" class="form-control  ">
                            </div>

                            <div class="md-form">
                                <textarea  class="md-textarea form-control" name="Notes" id="Notes" row="1"  placeholder=" "></textarea>
                                <label for="Notes">{{__('Notes')}}</label>
                            </div>
                        </div>

                    </div>



                </div>
                <div class="modal-footer border-top-0 pr-4 pt-2 pb-2 pl-0 ">
                    <div class="row w-100">
                        <div class="col-2">
                            <button type="button" id="archive" class="btn btn-danger mr-3 float-left" onclick="archive_resident()">
                                <i class="fa fa-trash"></i> {{ __('Archive') }}
                            </button>
                        </div>
                        <div class="col">
                        <button type="button" id="cancel"  class=" btn btn-grey float-right" data-dismiss="modal" >
                            <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                        <button type="submit" id="save" class="float-right btn btn-primary mr-3 ">
                            {{ __('Save') }}
                        </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>