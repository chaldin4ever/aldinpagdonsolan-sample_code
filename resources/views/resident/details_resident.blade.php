<table class="table table-bordered table-striped">
    <tr>
        <th style="width: 20%;">Title</th>
        <td>{{$resident->Title}}</td>
        <th>Preferred Name</th>
        <td>{{$resident->PreferredName}}</td>
        <th>Date Of Birth</th>
        <td>{{$resident->DOB}}</td>
    </tr>

    <tr>
        <th>First Name</th>
        <td>{{$resident->FirstName}}</td>
        <th>Middle Name</th>
        <td>{{$resident->MiddleName}}</td>
        <th>Last Name</th>
        <td>{{$resident->LastName}}</td>
    </tr>

    <tr>
        <th>Gender</th>
        <td>{{$resident->Gender}}</td>
        <th>I describe myself as</th>
        <td></td>
        <th>Country of Birth</th>
        <td>{{$resident->COB}}</td>
    </tr>
    <tr>
        <th>Primary Language</th>
        <td>{{$resident->PrimaryLanguage}}</td>
        <th>Secondary Language</th>
        <td>{{$resident->SecondaryLanguage}}</td>
        <th>Religion</th>
        <td>{{$resident->Religion}}</td>
    </tr>

    <tr>
        <th colspan="3">Aboriginal or Torres Strait Islander status</th>

        <td colspan="3">{{$resident->Aboriginal}}</td>

    </tr>


    <tr>
        <th>Address</th>
        <td>{{$resident->Address}}</td>
        <th>Suburb</th>
        <td>{{$resident->Suburb}}</td>
        <th>Post Code</th>
        <td>{{$resident->Postcode}}</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{{ucwords($resident->Status)}}</td>
        <th>Room</th>
        <td>{{$resident->Room}}</td>
        <th>URN</th>
        <td>{{$resident->URN}}</td>
    </tr>

    <tr>
        <th>Medicare Number</th>
        <td>{{$resident->MedicareNumber}}</td>
        <th>Pension Number</th>
        <td>{{$resident->PensionNumber}}</td>
        <th>DVA Number</th>
        <td>{{$resident->DVANumber }}</td>
    </tr>
    <tr>
        <th>Client</th>
        <td>{{array_get($resident->Client, 'clientName')}}</td>
        <th>Facility</th>
        <td>{{array_get($resident->Facility, 'facilityName')}}</td>
        <th></th>
        <td></td>
    </tr>
</table>