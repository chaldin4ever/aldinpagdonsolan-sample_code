<div class="col-2">
   {{-- @if(empty($resident->Photo))
        <img  src="http://via.placeholder.com/100x100" />
    @else
        --}}{{--<img  height="100px" src="data:image/png;base64,{{$resident->Photo}}"/>--}}{{--
        <img  id="resident_photo"  height="100px" />
    @endif   --}}

    @if(empty($residentfile))
        <img  src="http://via.placeholder.com/150x150" class="img-thumbnail" />
    @else
        @if($residentfile)
            <img  id=""  height="150px" class="img-thumbnail" src="{{$residentfile->AWSFile}}"/>
        @else
            @if($resident->Photo)
                <img  height="150px" class="img-thumbnail" src="data:image/png;base64,{{$resident->Photo}}"/>
            @else
                <img  src="http://via.placeholder.com/150x150" class="img-thumbnail" />
            @endif
        @endif
    @endif
</div>
<div class="col-10 pl-0">
    <div class="row">
        <div class="col-xl-8">
            <h2>{{$resident->FullName}}</h2>
            <div class="col">
                <div class="row">
                    <div class="col-3 pl-0">
                        {{__('Room: ')}} {{array_get($resident->CurrentRoom, 'RoomName')}}
                    </div>
                    <div class="col-3 pl-0">
                        {{__('UR: ')}}{{$resident->URN}}
                    </div>

                </div>
                <div class="row mt-2">
                    <div class="col-6 pl-0">
                        {{__('Allergies: ')}}
                    </div>

                    <div class="col-xl-6">
                        @if($resident->Status == 'discharge')
                            <div class="form-check">
                                <input type="checkbox" class="filled-in form-check-input" id="checkbox101" disabled checked="checked">
                                <label class="form-check-label" for="checkbox101">{{__('Discharged')}}</label>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2">
            <a href="{{url('resident/details/'.$resident->_id)}}" class="btn btn-outline-primary btn-sm btn-block">Resident Details</a>
            <a href="" class="btn btn-outline-primary btn-sm btn-block">Clinical Profiles</a>
        </div>
        <div class="col-xl-2">

            <a href="" class="btn btn-outline-primary btn-sm btn-block">Movements</a>
            <a href="{{url('resident/charts/'.$resident->_id)}}" class="btn btn-outline-primary btn-sm btn-block">Charts</a>
        </div>
    </div>


    
</div>

</div>