<select name="Area" id="Area" class=" mdb-select-area"  placeholder=" " onchange="submitform()" >
    <option value="" selected disabled>Select Area</option>
    @foreach($areas as $area)
        <option value="{{array_get($area, 'AreaId')}}" @if($areaId == array_get($area, 'AreaId')) selected @endif>{{array_get($area, 'AreaName')}}</option>
    @endforeach
</select>
<label for="Area">{{__('Areas')}}</label>
