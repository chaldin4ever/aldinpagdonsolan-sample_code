<!-- Modal -->
<div class="modal fade " id="uploadModal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <form method="post" action="{{url('resident/upload')}}" id="upload" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header border-bottom-0 pb-0">
                    <h5 class="modal-title">{{__('Upload')}}</h5>
                    <button type="button" class="close" onclick="cancelUpload()" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-0 p-2 m-0">
                    <div class="row">
                        <div class="col p-2">

                            <!--Card-->
                            <div class="card mdb-color text-center  light-version py-4 px-3 m-0 " style="border-radius: 0px !important;">
                                <input type="hidden" name="folderId" id="folderId" value="" />
                                <input type="hidden" name="residentId" id="residentId" value="{{$residentId}}" />

                                    <div class="file-field">
                                        <div class="btn btn-outline-white btn-rounded waves-effect btn-sm float-left">
                                            <span>Choose files<i class="fa fa-cloud-upload ml-3" aria-hidden="true"></i></span>
                                            <input type="file" multiple name="files[]" id="files">
                                        </div>
                                        <div class="file-path-wrapper md-form w-100">
                                            <input class="file-path validate text-white" id="file_placeholder" type="text" placeholder="Upload one or more files">
                                        </div>
                                    </div>

                            </div>

                        </div>
                    </div>


                </div>
                <div class="modal-footer border-top-0 pr-0 pt-2 pb-2">
                    <button type="button" id="cancel"  class=" btn btn-grey" data-dismiss="modal" onclick="cancelUpload()" >
                        <span aria-hidden="true" >&times;</span> &nbsp;{{__('Cancel')}}</button>
                    <button type="submit" id="save" class="btn mdb-color mr-2 ">
                        {{ __('Upload') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>