@extends('layouts.poc_imagepreview')
@section('content')
<img  id="resident_photo"  />
    @endsection

@section('script')
    <script>
        var photo = JSON.parse('{!! $residentfile->File !!}');

        $('img#resident_photo').attr('src', photo.data);
    </script>
 @endsection