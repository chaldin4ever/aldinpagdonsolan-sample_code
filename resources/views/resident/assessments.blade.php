@extends('layouts.poc')

@section('style')
    <style>
        #search {
            height: 40px;
            margin-top: 5px;
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{__('Assessment')}}</strong></h3>
                <a href="{{url('resident/view/'.$resident->_id)}}" class="float-right"><i class="fa fa-chevron-circle-left fa-2x"></i></a>
            </div>


        </div>
    </div>

    <div class="container-fluid">
        <div class="row bg-white pl-2 pt-2 pb-2 mb-3">

            @include('resident.header', ['resident' => $resident])

        </div>

        <div class="row bg-white">
            <div class="col pt-3">
                <form method="get" action="">
                    @csrf

                    <div class="row bg-white pt-0 pl-2 mb-3">

                        <div class="col-md-2">

                            <div class="md-form">
                                <!--The "from" Date Picker -->
                                <i class="fa fa-calendar prefix"></i>
                                <input placeholder="Start Date" value="{{$fromDate}}" type="text" id="startingDate" class="form-control datepicker_start">
                                <label for="startingDate">{{__('Start Date')}}</label>
                            </div>

                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-md-2 ">

                            <div class="md-form">
                                <!--The "to" Date Picker -->
                                <i class="fa fa-calendar prefix"></i>
                                <input placeholder="End Date" value="{{$toDate}}" type="text" id="endingDate" class="form-control datepicker_end">
                                <label for="endingDate">{{__('End Date')}}</label>
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="md-form mt-3">
                                <!--The "from" Date Picker -->
                                <i class="fa fa-user prefix"></i>
                               <select class="mdb-select" name="role">
                                   <option value=""   selected>       Choose..</option>
                                   @foreach($roles as $role)
                                       <option value="{{$role->_id}}" @if($role->_id == $selectedrole) selected @endif>       {{$role->roleName}}</option>
                                   @endforeach
                               </select>
                                <label for="patient">{{__('Roles')}}</label>
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="md-form ">
                                <!--The "from" Date Picker -->
                                <i class="fa fa-users prefix"></i>
                                <input placeholder="{{__('User')}}"  value="{{$user}}"  name="user" type="text" id="user" class="form-control ">
                                <label for="patient">{{__('User')}}</label>
                            </div>

                        </div>

                        <div class="col-md-3">

                            <div class="md-form">
                                <!--The "from" Date Picker -->
                                <i class="fa fa-list prefix"></i>
                                <input placeholder="{{__('Title')}}"  value="{{$title}}"  name="title" type="text" id="title" class="form-control ">
                                <label for="title">{{__('Title')}}</label>
                            </div>

                        </div>
                        <div class="col-md-1 ">

                            <button class="float-left btn btn-blue-grey btn-rounded btn-sm   mt-4" style="margin-left: -20px; ">{{__('Search')}}</button>
                        </div>


                    </div>




                </form>
                {{--<div class="col-4 pl-0">
                    <input autocomplete="off" type="text" class="form-control" id="search" name="search" placeholder="Search Assessment"/>
                </div>--}}
                @if(sizeof($assessment) > 0)
                    <table class="table table-striped table-bordered assessment">
                        <thead>
                        <tr>
                            <th class="w-50">{{__('Assessment')}}</th>
                            <th>{{__('Created By')}}</th>
                            <th>{{__('Updated At')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($assessment as $assm)
                            <tr  @if($assm->Archive == 1)style="text-decoration: line-through; color: silver;"@endif>
                                <td>{{array_get($assm->Form, 'FormName')}}</td>
                                <td>{{array_get($assm->CreatedBy, 'FullName')}}</td>
                                <td>{{ $assm->updated_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                @else
                    <table class="table table-striped table-bordered mt-1">
                        <body>
                        <tr>
                            <td>{{__('No available assessment.')}}</td>
                        </tr>
                        </body>
                    </table>
                @endif
            </div>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script>

        $(document).ready(function(){

            $('.mdb-select').material_select();

            $('.datepicker_start').pickadate({
                // Escape any “rule” characters with an exclamation mark (!).
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                hiddenPrefix: 'From',
                hiddenSuffix: 'Date'
            })

            $('.datepicker_end').pickadate({
                // Escape any “rule” characters with an exclamation mark (!).
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                hiddenPrefix: 'To',
                hiddenSuffix: 'Date'
            })


            // Get the elements
            var from_input = $('#startingDate').pickadate(),
                from_picker = from_input.pickadate('picker')
            var to_input = $('#endingDate').pickadate(),
                to_picker = to_input.pickadate('picker')

// Check if there’s a “from” or “to” date to start with and if so, set their appropriate properties.
            if ( from_picker.get('value') ) {
                to_picker.set('min', from_picker.get('select'))
            }
            if ( to_picker.get('value') ) {
                from_picker.set('max', to_picker.get('select'))
            }

// Apply event listeners in case of setting new “from” / “to” limits to have them update on the other end. If ‘clear’ button is pressed, reset the value.
            from_picker.on('set', function(event) {
                if ( event.select ) {
                    to_picker.set('min', from_picker.get('select'))
                }
                else if ( 'clear' in event ) {
                    to_picker.set('min', false)
                }
            })
            to_picker.on('set', function(event) {
                if ( event.select ) {
                    from_picker.set('max', to_picker.get('select'))
                }
                else if ( 'clear' in event ) {
                    from_picker.set('max', false)
                }
            })
        });

        $('input[name="search"]').on('keyup', function() {

            var input, filter, tr, td, i;

            input  = $(this);
            filter = input.val().toUpperCase();
            tr     = $("table.assessment tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0]; // <-- change number if you want other column to search
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        })
    </script>
@endsection

