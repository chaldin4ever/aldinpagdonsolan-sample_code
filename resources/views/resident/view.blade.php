@extends('layouts.poc')

@section('style')
    <style>
        .card.light-version .file-field input[type=text] {
            border-bottom: 1px solid #fff; }
        .card.light-version .file-field input[type=text]::-webkit-input-placeholder {
            color: #fff;
            font-weight: 300; }
        .card.light-version .file-field input[type=text]::-moz-placeholder {
            color: #fff;
            font-weight: 300; }
        .card.light-version .file-field input[type=text]:-ms-input-placeholder {
            color: #fff;
            font-weight: 300; }
        .card.light-version .file-field input[type=text]::placeholder {
            color: #fff;
            font-weight: 300; }
        .card.light-version .file-field input[type=text]:focus:not([readonly]) {
            -webkit-box-shadow: 0 1px 0 0 #fff;
            box-shadow: 0 1px 0 0 #fff; }
        .card.light-version .file-field input[type=text].valid {
            border-bottom: 1px solid #00c851;
            -webkit-box-shadow: 0 1px 0 0 #00c851;
            box-shadow: 0 1px 0 0 #00c851; }
        .card.light-version .file-field input[type=text]:focus.valid {
            border-bottom: 1px solid #00c851;
            -webkit-box-shadow: 0 1px 0 0 #00c851;
            box-shadow: 0 1px 0 0 #00c851; }

        .white-skin input[type=checkbox].filled-in:checked+label:after {
            background-color: rgb(90, 118, 147);
            border-color: rgb(90, 118, 147);
        }


        .nav-link, .navbar {
            padding: 1.2rem .2rem;
            font-weight: normal;
        }

        .nav-link {
            font-size: 0.9rem;
            font-weight: 100;
            padding: 0.75rem 0.2rem;
        }

        .pinboard {
            height:100%;
        }

        .pinboard .title {
            font-size:1.1rem;
            font-weight: bold;
            text-align: center;
            border-bottom: 1px solid #ccc;
            height: 40px;
            line-height: 40px;
        }

        .large.tooltip-inner {
            max-width: 500px;
            min-width: auto;
        }

        .h5, h5 {
            font-size: 1rem !important;
        }

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-users"></i> Resident View</strong></h3>

                <a href="{{url('resident')}}" class="float-right pt-2" style="font-size:0.85rem">Back to resident listing</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="">

            <div class="row bg-white pl-2 pt-2 pb-2">

                @include('resident.header', ['resident' => $resident])

            </div>

            <div class="row bg-white p-2 pt-2 mt-2 mb-5">
                <!-- Nav tabs -->
                <div class="col-10">
                    <ul class="nav nav-tabs nav-justified p-0 m-0" id="residentTab" style="background-color: #949494; z-index: 1 !important;">
                        <li class="nav-item" onclick="
                                var url = '{{url('resident/view/'.$resident->_id)}}';

                                window.location.href = url;
                            ">
                            @php  if($active_tab == 'pnote_link') $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" data-toggle="tab" href="#progressnotes" role="tab" id="pnote_link" onclick="setTab('pnote_link')">{{__('Progress Notes')}}</a>
                        </li>
                        <li class="nav-item">
                            @php  if($active_tab == 'assessment_link') $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" id="assessment_link" data-toggle="tab" href="#assessment" role="tab"  onclick="setTab('assessment_link')">{{__('Assessments')}}</a>
                        </li>
                      {{--  <li class="nav-item">
                            @php  if($active_tab == 'evaluation_link')  $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" data-toggle="tab" id="evaluation_link"  href="#evaluation" role="tab" onclick="setTab('evaluation_link')">{{__('Evaluations')}}</a>
                        </li>--}}
                        <li class="nav-item">
                            @php  if($active_tab == 'charting_link') $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" data-toggle="tab" id="charting_link"  href="#charting" role="tab" onclick="setTab('charting_link')">{{__('Charting')}}</a>
                        </li>
                        <li class="nav-item">
                            @php  if($active_tab == 'forms_link') $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" data-toggle="tab" id="forms_link"  href="#forms" role="tab" onclick="setTab('forms_link')">{{__('Forms')}}</a>
                        </li>
                        <li class="nav-item">
                            @php  if($active_tab == 'careplan_link') $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" data-toggle="tab" id="careplan_link" href="#careplan" role="tab" onclick="setTab('careplan_link')">{{__('Care Plan')}}</a>
                        </li>
                        <li class="nav-item">
                            @php  if($active_tab == 'documents_link') $active = 'active'; else  $active = ''; @endphp
                            <a class="nav-link {{$active}}" data-toggle="tab" href="#documents" id="documents_link" role="tab" onclick="setTab('documents_link')">{{__('Documents')}}</a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content card">
                        <!--Panel 1-->
                        @php  if($active_tab == 'pnote_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade {{$active}}" id="progressnotes" role="tabpanel">
                           @include('assessment.tabs.progressnotes', compact('progressnotes', 'endofshift'))
                        </div>
                        <!--/.Panel 1-->
                        <!--Panel 2-->
                        @php  if($active_tab == 'assessment_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade  {{$active}}" id="assessment" role="tabpanel">
                            <div class="col float-right">
                                <div class="row ">
                                    <div class="float-right col pr-2 ">
                                        <a href="{{url('resident/assessment/'.$resident->_id)}}" class="float-right btn btn-outline-blue-grey btn-sm p-2"><i class="fa fa-list"></i>  {{__('Assessments')}}</a>
                                    </div>
                                </div>
                            </div>
                            @include('assessment.tabs.assessment', ['last10assessment'=>$last10assessment, 'residentId'=>$resident->_id])

                        </div>
                        <!--/.Panel 2-->
                        <!--Panel 3-->
                       {{-- @php  if($active_tab == 'evaluation_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade {{$active}}" id="evaluation" role="tabpanel">
                                <div class="col float-right">
                                    <div class="row ">
                                        <div class="float-right col pr-2 ">
                                            <a href="{{url('resident/evaluation/'.$resident->_id)}}" class="float-right btn btn-outline-blue-grey btn-sm p-2"><i class="fa fa-list"></i>  {{__('Evaluation')}}</a>
                                        </div>
                                    </div>
                                </div>

                            @include('assessment.tabs.evaluation', ['last10evaluation'=>$last10evaluation, 'residentId' => $resident->_id])
                        </div>--}}

                        @php  if($active_tab == 'charting_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade {{$active}}" id="charting" role="tabpanel">
                                <div class="col float-right">
                                    <div class="row ">
                                        <div class="float-right col pr-2 ">
                                            <a href="{{url('resident/charting/'.$resident->_id)}}" class="float-right btn btn-outline-blue-grey btn-sm p-2"><i class="fa fa-list"></i>  {{__('Charting')}}</a>
                                        </div>
                                    </div>
                                </div>
                            @include('assessment.tabs.charting', ['last10charting'=>$last10charting, 'residentId' => $resident->_id])

                        </div>

                        @php  if($active_tab == 'forms_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade {{$active}}" id="forms" role="tabpanel">
                            <div class="col float-right">
                                <div class="row ">
                                    <div class="float-right col pr-2 ">
                                        <a href="{{url('resident/forms/'.$resident->_id)}}" class="float-right btn btn-outline-blue-grey btn-sm p-2"><i class="fa fa-list"></i>  {{__('Form')}}</a>
                                    </div>
                                </div>
                            </div>
                            @include('assessment.tabs.forms', ['last10forms'=>$last10forms, 'residentId' => $resident->_id])
                        </div>

                        @php  if($active_tab == 'careplan_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade {{$active}}" id="careplan" role="tabpanel">
                            @if(empty($careplan))
                                <h5>No care plan found for this resident</h5>
                            @else
                                @include('assessment.tabs.careplan', [ 'residentId' => $resident->_id])
                            @endif

                        </div>

                        @php  if($active_tab == 'documents_link')  $active = 'show active'; else  $active = ''; @endphp
                        <div class="tab-pane fade {{$active}}" id="documents" role="tabpanel">
                            @include('assessment.tabs.documents', ['folders'=>$folders, 'residentId' => $resident->_id])

                        </div>
                        <!--/.Panel 3-->
                    </div>
                </div>
                <div class="col-2 p-0">
                    @include('resident.pinboard')
                </div>
            </div>
        </div>
    </div>
    @include('resident.upload',[ 'residentId' => $resident->_id])
@endsection

@section('script')
<script>

    $(function () {
        $('[data-toggle="tooltip"]').tooltip({
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
        });
    })

    $(document).ready(function(){

        $('#progressnotes .addComment').hide();
        $(' .commentlist li.list-group-item:not(:first)').hide();
        $(' .commentlist li.list-group-item-dark').show();
        $('#resident-header-menu').dropdown();

        @if(Request::has('view'))
            var tab = '#{{Request::get('view')}}';
            $("#residentTab a[href='"+tab+"']").tab('show');

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                console.log('hi');
            console.log(e.target); // newly activated tab
            //e.relatedTarget // previous active tab
            })
        @endif

        getAssessmentForms('assessment');
        $('#assessment #assessmentResult').hide();

        getAssessmentForms('charting');
        $('#charting #chartingResult').hide();

        getAssessmentForms('evaluation');
        $('#evaluation #evaluationResult').hide();

        getAssessmentForms('forms');
        $('#forms #formsResult').hide();

        
    })

    $('body').click(function(){
        // $('#assessment #assessmentResult').hide();
    })

    @if(session('status'))
        toastr.success('{{__('Successfully Saved.')}}').css('width', 'auto');
    @endif

    @if(session('warning'))
    toastr.warning('{{session('warning')}}').css('width', 'auto');
    @endif

    @if(session('danger'))
        toastr.error('{{session('danger')}}').css('width', 'auto');
    @endif

    function getAssessmentForms(category){

        var url = '{{url('assessment/assessmentResult')}}';
        var str = $('div#'+category+' input#SearchAssessment').val();
        var residentId = '{{$resident->_id}}';
        // console.log(category);
        // console.log(str);
        axios.post(url, {
            str: str,
            residentid: residentId,
            category: category
        }).then(function(response){
            var data = response.data;
            // console.log(data);
            $('div#'+category+' #'+category+'Result').html(data);
            // $('#assessment #assessmentResult').show();

        })
    }



   /* function createProgressNote(){

        // $('#progress_note_form').show();
        $('#progress_note_form').show();

    }*/

    function addProgressNote(){

        // var title = $('#pnote_title').val();
        var notes = $('div#progressnotes #pnote').val();
        var condition = $('div#progressnotes #condition').val();
        var actionTaken = $('div#progressnotes #actionTaken').val();
        var response = $('div#progressnotes #response').val();
        var endofshift = $('div#progressnotes input[name="EndOfShift"]:checked').val();
        var eos = (endofshift === 'true') ? endofshift : 'false';
        var fromDate = '{{$fromDate}}';
        var toDate = '{{$toDate}}';

        if(notes.length > 0){
            var url = '{{url('progressnote/store')}}';

            axios.post(url, {
                residentId: '{{$resident->_id}}',
                notes: notes,
                condition: condition,
                actionTaken: actionTaken,
                response: response,
                endofshift: eos
            }).then(function(response){
                var data = response.data;
                $('.tab-content #progressnotes').html(data);
                $(' .commentlist li.list-group-item:not(:first)').hide();
                $('.tab-content #progressnotes textarea.froala-editor-sm').froalaEditor({
                    toolbarButtons: ['fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough',
                        'align', 'formatOL', 'formatUL', 'indent', 'outdent',
                        'undo', 'redo',
                        'html', 'clearFormatting'
                    ],
                });

                toastr.success('Progress Note was successfully saved.').css('width', 'auto');
                // $('.tab-content #progressnotes textarea.froala-editor').trigger('click');
            })
        }else{
            toastr.warning('Please fill up notes.').css('width', 'auto');

        }

    }

    function addProgressNoteComment(id){

        var comment =  $('div#progressnotes .addComment textarea#comment_'+id).val();

        if(comment.length > 0){
            var url = '{{url('progressnote/comment/store')}}';
            axios.post(url,{

                progressNoteId: id,
                comment: comment

            }).then(function(response){

                getProgressNoteComment(id);
                toastr.success('Successfully saved.');

                $('div#progressnotes .addComment textarea#comment_'+id).val('');
                $('#progressnotes tr#'+id+' .addComment').hide();
                $('#progressnotes td.progress-note div#'+id+' li.list-group-item:not(:first)').hide();
            })
        }else{
            toastr.warning('Please enter your comments.');
        }

    }

    function getProgressNoteComment(id){

        var url = '{{url('progressnote/comment/show')}}/'+id;
        //
        axios.get(url).then(function(response){
            // alert('comment_'+id);
            $('div#comment_'+id).html(response.data);

            // $('div#comment_'+id+' li.list-group-item:not(:first)').hide();
            $('div#comment_'+id+' li button#comment_hide').trigger('click');
            // $(' .commentlist li.list-group-item-dark').show();

        })
    }

    function archiveProgressNote(pnId){

        $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
        var message = 'Please enter your reason.';
        alertify.prompt(message, function(e, v) {
            if (e) {
                if(v.length > 0){

                    url = '{{url('progressnote/archive')}}';

                    axios.post(url, {
                        pnId: pnId,
                        reason: v
                    }).then(function(response){

                        var data = response.data;
                        console.log(data);
                        $('table.progressnote tr#'+pnId+' div.archive small').html('Archived by <span class=\"user-name\">'+data.user+'</span> on '+data.date+'<br />('+data.reason+')');

                    });

                    $('table.progressnote tr#'+pnId).css('text-decoration-line', ' line-through');
                    $('.modal-backdrop').fadeOut("slow");
                    toastr.success('Successfully saved.');
                }else{
                    alertify.alert("You did not input any text. Please enter your reason.", function(pnId){
                        $('.modal-backdrop').remove();
                        archiveProgressNote(pnId);
                    });
                }

            }else{
                $('.modal-backdrop').fadeOut("slow");
            }
        });

    }


    function archiveAssessment(assessmentId, category){

        $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
        var message = 'Please enter your reason.';
        alertify.prompt(message, function(e, v) {
            if (e) {
                if(v.length > 0){

                    url = '{{url('assessment/archive')}}';

                    axios.post(url, {
                        assessmentId: assessmentId,
                        reason: v
                    }).then(function(response){

                        var data = response.data;
                        console.log('table.'+category+' tr#'+assessmentId);
                        $('table.'+category+' tr#'+assessmentId+' div.archive').html('<small>Archived by <span class=\"user-name\">'+data.user+'</span> on '+data.date+'<br />('+data.reason+')</small>');

                    });

                    $('table.'+category+' tr#'+assessmentId+' td span.formname').css('text-decoration-line', ' line-through');
                    $('table.'+category+' tr#'+assessmentId+' td span.formname').css('color', ' silver');
                    $('.modal-backdrop').fadeOut("slow");
                    toastr.success('Successfully saved.');
                }else{
                    alertify.alert("You did not input any text. Please enter your reason.", function(assessmentId){
                        $('.modal-backdrop').remove();
                        archiveAssessment(assessmentId);
                    });
                }

            }else{
                $('.modal-backdrop').fadeOut("slow");
            }
        });

    }


    $(document).ready(function(){

        $('.mdb-select').material_select();

        $('.datepicker_start').pickadate({
            // Escape any “rule” characters with an exclamation mark (!).
            format: 'mm/dd/yyyy',
            formatSubmit: 'mm/dd/yyyy',
            hiddenPrefix: 'From',
            hiddenSuffix: 'Date'
        })

        $('.datepicker_end').pickadate({
            // Escape any “rule” characters with an exclamation mark (!).
            format: 'mm/dd/yyyy',
            formatSubmit: 'mm/dd/yyyy',
            hiddenPrefix: 'To',
            hiddenSuffix: 'Date'
        })


        // Get the elements
        var from_input = $('#startingDate').pickadate(),
            from_picker = from_input.pickadate('picker')
        var to_input = $('#endingDate').pickadate(),
            to_picker = to_input.pickadate('picker')

// Check if there’s a “from” or “to” date to start with and if so, set their appropriate properties.
        if ( from_picker.get('value') ) {
            to_picker.set('min', from_picker.get('select'))
        }
        if ( to_picker.get('value') ) {
            from_picker.set('max', to_picker.get('select'))
        }

// Apply event listeners in case of setting new “from” / “to” limits to have them update on the other end. If ‘clear’ button is pressed, reset the value.
        from_picker.on('set', function(event) {
            if ( event.select ) {
                to_picker.set('min', from_picker.get('select'))
            }
            else if ( 'clear' in event ) {
                to_picker.set('min', false)
            }
        })
        to_picker.on('set', function(event) {
            if ( event.select ) {
                from_picker.set('max', to_picker.get('select'))
            }
            else if ( 'clear' in event ) {
                from_picker.set('max', false)
            }
        })
    });

    function addUpload(id){

        $('#uploadModal input#files').val('');
        $('#uploadModal input#file_placeholder').val('');

        $('#uploadModal input#folderId').val(id);
        $('#uploadModal').modal('show');
        $( "body" ).append( "<div class='modal-backdrop fade show'></div>" );
    }

    function cancelUpload(){

        $('#uploadModal').modal('hide');
        $( ".modal-backdrop").remove();
    }

    function setTab(tab){
        var url = "{{url('resident/settab')}}";
        axios.post(url, {
            tab: tab
        })
    }

            @if(!empty($residentfile))
                var photo = JSON.parse('{!! $residentfile->File !!}');

                $('img#resident_photo').attr('src', photo.data);
                // $('div.dropzone').attr('data-image', photo.data);
                @endif

</script>
@endsection

