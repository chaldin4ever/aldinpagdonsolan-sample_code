
<form method="post" name="form_{{$num}}" id="contacts" action="{{url('resident/contact/store/'.$residentId)}}" >
    @csrf
    <input type="hidden" id="_id"  name="_id" value="@if(!empty($contact->_id)){{$contact->_id}}@endif" />
    <input type="hidden" id="Resident"  name="ResidentId" value="{{$residentId}}" />
    <input type="hidden" id="DisplayOrder"  name="DisplayOrder" value="{{$num}}" />

    <div class="row border-bottom pl-4 pr-4 pb-4 pt-0">
        {{--<pre>{{print_r($contact)}}</pre>--}}
        <h4 class="w-100 bg-secondary p-2 text-white rounded">Contact {{$num}}</h4>
        <div class="col-4 ">

            <div class="md-form">
                <input required type="text" id="Title" name="Title" value="@if(!empty($contact->Title)){{$contact->Title}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Title">{{__('Title')}}</label>
            </div>

            <div class="md-form">
                <input required type="text" id="FirstName" name="FirstName" value="@if(!empty($contact->FirstName)){{$contact->FirstName}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="FirstName">{{__('First Name')}}</label>
            </div>

            <div class="md-form">
                <input required type="text" id="LastName" name="LastName" value="@if(!empty($contact->LastName)){{$contact->LastName}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="LastName">{{__('Last Name')}}</label>
            </div>
            <div class="md-form">
                <input required type="text" id="Relationshp" name="Relationshp" value="@if(!empty($contact->Relationshp)){{$contact->Relationshp}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Relationshp">{{__('Relationshp')}}</label>
            </div>

        </div>
        <div class="col-4 ">

            <div class="md-form">
                <input required type="text" id="Address" name="Address" value="@if(!empty($contact->Address)){{$contact->Address}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Address">{{__('Address')}}</label>
            </div>

            <div class="md-form">
                <input required type="text" id="Suburb" name="Suburb" value="@if(!empty($contact->Suburb)){{$contact->Suburb}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Suburb">{{__('Suburb')}}</label>
            </div>

            <div class="md-form">
                <input required type="text" id="City" name="City" value="@if(!empty($contact->City)){{$contact->City}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="City">{{__('City')}}</label>
            </div>

            <div class="md-form">
                <input required type="text" id="PostCode" name="PostCode" value="@if(!empty($contact->PostCode)){{$contact->PostCode}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="PostCode">{{__('Post Code')}}</label>
            </div>

        </div>
        <div class="col-4 ">

            <div class="md-form">
                <input required type="text" id="Mobile" name="Mobile" value="@if(!empty($contact->Mobile)){{$contact->Mobile}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Mobile">{{__('Mobile')}}</label>
            </div>

            <div class="md-form">
                <input required type="text" id="Phone" name="Phone" value="@if(!empty($contact->Phone)){{$contact->Phone}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Phone">{{__('Phone')}}</label>
            </div>

            <div class="md-form">
                <input required type="email" id="Email" name="Email" value="@if(!empty($contact->Email)){{$contact->Email}}@endif" autocomplete="off" class="form-control" placeholder=" ">
                <label for="Email">{{__('Email')}}</label>
            </div>
            <div class="md-form pb-0">
                <button class="float-right mb-0 btn btn-green w-50" id="save" type="submit">Save</button>
            </div>

        </div>

    </div>
</form>