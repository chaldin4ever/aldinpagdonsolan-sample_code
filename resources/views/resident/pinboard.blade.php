<div class="pinboard" style="padding: 4px 4px">

    <div class="list-group">
        <li class="list-group-item active pt-2 pb-2 text-center">Pinboard</li>
        @foreach($pinboard_items as $item)
        @php
            if(empty($item->target_form))
                $url = "#";
            else
                $url = url('assessmentform/selectform')."/".array_get($item->Resident,'ResidentId')."/".$item->target_form;
        @endphp
        <li class="list-group-item"><a class="" style="padding: 0.5rem 0.5rem; line-height: .5em;" href="{{$url}}">{!!$item->note!!}
            <br /><span class="font-weight-normal small-font " style="color: #ccc;">Room {{array_get($item->Resident,'Room')}}</span><br />
        @if(!empty($item->action_date) && ($item->action_date->toDateTime()->format('Y') > 1900))
        <span class="font-weight-normal small-font " style="color: #ccc;">by {{array_get($item->CreatedBy, 'FullName')}} {{\App\Utils\Toolkit::UTCDateTimeToDateString($item->action_date)}}</span>
        @endif
        <span class="pinboard-timestamp">({{\App\Utils\Toolkit::CarbonToDateFullString($item->created_at)}})</span>
        </a>
           <br /> <small><a class="text-right" href="{{url('pinboard/view/'.$item->_id)}}"><i class="fa fa-eye"></i> View</a></small>
        </li>
    @endforeach
    </div>
</div>