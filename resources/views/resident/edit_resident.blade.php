<form method="post" enctype="multipart/form-data" action="{{url('resident/store/true')}}">
    @csrf
    <input type="hidden" name="_id" id="_id" value="{{$resident->_id}}" />
<div class="row">

    <div class="col-4">
        {{--<input type="hidden" name="doctorId" value="{{$doctor->_id}}" />--}}
        <div class="md-form">

            <input required type="text" id="FirstName" name="FirstName" value="{{$resident->FirstName}}" autocomplete="off" class="form-control" placeholder=" ">
            <label for="firstname">{{__('First Name')}}</label>
        </div>

        <div class="md-form">
            <input required type="text" id="LastName" name="LastName" value="{{$resident->LastName}}" class="form-control" autocomplete="off" placeholder=" ">
            <label for="lastname">{{__('Last Name')}}</label>
        </div>

        <div class="md-form">
            <input required type="text" id="PreferredName" name="PreferredName" value="{{$resident->PreferredName}}" class="form-control" autocomplete="off" placeholder=" ">
            <label for="PreferredName">{{__('Preferred Name')}}</label>
        </div>


        <div class="md-form">
            <input required type="text" id="DOB" name="DOB" value="{{$resident->DOB}}" class="form-control datepicker" autocomplete="off"  placeholder=" ">
            <label for="dob">{{__('Date of Birth')}}</label>
        </div>

        <div class="md-form">
            <select name="Gender" id="Gender" class="mdb-select  "  placeholder=" ">
                <option value="">Select</option>
                <option value="M" @if($resident->Gender == 'M') selected @endif >{{__('Male')}}</option>
                <option value="F"  @if($resident->Gender == 'F') selected @endif  >{{__('Female')}}</option>
            </select>
            <label for="gender">{{__('Gender')}}</label>
        </div>

        <div class="md-form">
            <input type="text" id="Address" name="Address" value="{{$resident->Address}}" class="form-control" autocomplete="off"  placeholder=" ">
            <label for="address">{{__('Address')}}</label>
        </div>

        <div class="md-form">
            <input type="text" id="Suburb" name="Suburb" value="{{$resident->Suburb}}" class="form-control" autocomplete="off"  placeholder=" ">
            <label for="suburb">{{__('Suburb')}}</label>
        </div>

    </div>

    <div class="col-4">
        <div class="md-form Status">
            <select name="ResidentStatus" id="Status" class="mdb-select  "  placeholder=" ">
                <option value="" selected disabled>Select</option>
                <option value="permanent" @if($resident->Status == 'permanent') selected @endif >{{__('Permanent')}}</option>
                <option value="respite"  @if($resident->Status == 'respite') selected @endif >{{__('Respite')}}</option>
                <option value="transitional"  @if($resident->Status == 'transitional') selected @endif >{{__('Transitional')}}</option>
                <option value="discharge"  @if($resident->Status == 'discharge') selected @endif >{{__('Discharged')}}</option>
                {{--<option value="archive"  >{{__('Archived')}}</option>--}}
            </select>
            <label for="Status">{{__('Status')}}</label>
        </div>

        <div class="md-form">
            <input type="text" id="Postcode" name="PostCode" value="{{$resident->Postcode}}" class="form-control" autocomplete="off"  placeholder=" ">
            <label for="postcode">{{__('Post Code')}}</label>
        </div>

        <div class="md-form">
            {{--<input readonly type="text" id="Room" name="Room" value="" class="form-control bg-light" autocomplete="off"  placeholder=" ">--}}
            <select name="Room" class="mdb-select" searchable="Search ..">
                <option disabled selected>Select</option>
                @if(!empty($resident->CurrentRoom))
                    <option value="{{array_get($resident->CurrentRoom, 'RoomId')}}" selected>{{array_get($resident->CurrentRoom, 'RoomName')}}</option>
                @endif
                @foreach($rooms as $room)
                    <option value="{{$room->_id}}">{{$room->RoomName}}</option>
                @endforeach
            </select>
            <label for="Room">{{__('Room')}}</label>
        </div>

        <div class="md-form">
            <input required type="text" id="URN" name="URN" value="{{$resident->URN}}" class="form-control" autocomplete="off"  placeholder=" ">
            <label for="URN">{{__('URN')}}</label>
        </div>

        <div class="md-form">
            <input required type="text" id="MedicareNumber" name="MedicareNumber" value="{{$resident->MedicareNumber}}" class="form-control" autocomplete="off"  placeholder=" ">
            <label for="PreferredName">{{__('Medicare Number')}}</label>
        </div>


        <div class="md-form">
            <input required type="text" id="PensionNumber" name="PensionNumber" value="{{$resident->PensionNumber}}" class="form-control  " autocomplete="off"  placeholder=" ">
            <label for="PensionNumber">{{__('Pension Number')}}</label>
        </div>

        <div class="md-form">
            <input required type="text" id="DVANumber" name="DVANumber" value="{{$resident->DVANumber}}" class="form-control  " autocomplete="off"  placeholder=" ">
            <label for="DVANumber">{{__('DVA Number')}}</label>
        </div>
    </div>

    <div class="col-4">

        <div class="md-form">

            <input required type="text" id="ProviderName" readonly name="ProviderName" value="{{$resident->ProviderName}}" class="form-control  bg-light"
                   autocomplete="off"  placeholder="">
            <label for="Client">{{__('Provider')}}</label>

        </div>

        <div class="md-form">
            <input required type="text" id="FacilityName" readonly name="FacilityName" value="{{$resident->FacilityName}}" class="form-control  bg-light"
                   autocomplete="off"  placeholder="">
            <label for="FacilityName">{{__('Facility')}}</label>

        </div>
        <div class="form-control text-center border-top-0 pl-0 pr-1" style="height: 250px;">
            <ul class="nav {{-- nav-tabs  md-tabs nav-justified--}} bg-light border-bottom mb-3 p-0" id="myTab" role="tablist">
                <li class="nav-item p-1 m-0">
                    <a class="nav-link active show m-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">View</a>
                </li>
                <li class="nav-item  p-1  m-0" >
                    <a class="nav-link  m-0" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Upload</a>
                </li>

            </ul>
            <div class="tab-content p-1 pb-3" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @if(empty($residentfile))
                        <img  src="http://via.placeholder.com/150x150" />
                    @else
                        @if($residentfile)
                            <img  src="{{$residentfile->AWSFile}}" />
                         @else
                            @if($resident->Photo)
                                <img  height="100px" src="{{$resident->Photo}}" {{--src="data:image/png;base64,{{$resident->Photo}}"--}}/>
                            @else
                                <img  src="http://via.placeholder.com/150x150" />
                            @endif
                        @endif
                    @endif
                </div>
                <div class="tab-pane fade  text-center    " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="dropzone" data-width="150" data-ajax="false"  {{--data-save="false" data-canvas="false"--}} data-height="150" {{--data-image="{{url('resident/preview_photo/'.$resident->_id)}}"--}}  data-url="canvas.php" style="width: 100%; margin: 0 auto;" >
                        <input type="file" name="Photo"{{-- required="required" --}}/>
                    </div>
                </div>
            </div>


        </div>


      {{--  <div class="md-form">
            <input  type="text" id="Doctor" name="Doctor" value="{{$resident->DoctorName}}" class="form-control  "
                    autocomplete="off"  placeholder=" ">
            <label for="Doctor">{{__('Doctor')}}</label>

        </div>

        <div class="md-form">
            <input  type="text" id="Pharmacy" name="Pharmacy" value="{{$resident->PharmacyName}}" class="form-control  "
                    autocomplete="off"  placeholder=" ">
            <label for="Pharmacy">{{__('Pharmacy')}}</label>
        </div>
--}}
        <div class="md-form" >
            <textarea  class="md-textarea form-control" name="Notes" id="Notes" row="1"  placeholder=" ">{{$resident->Notes}}</textarea>
            <label for="Notes">{{__('Notes')}}</label>
        </div>

    </div>

</div>
    <div class="row">
        <div class="col">
            <a href="{{url('resident')}}" class="btn mdb-color">Back</a>
            <button type="submit" class="btn btn-green">Save Changes</button>
        </div>
    </div>
</form>

