@extends('layouts.poc')

@section('style')
    <style>
        #search {
            height: 40px;
            margin-top: 5px;
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 pb-2 pt-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-list"></i> {{__('Evaluation')}}</strong></h3>
                <a href="{{url('resident/view/'.$resident->_id)}}" class="float-right"><i class="fa fa-chevron-circle-left fa-2x"></i></a>
            </div>


        </div>
    </div>

    <div class="container-fluid">
        <div class="row bg-white pl-2 pt-2 pb-2 mb-3">

            @include('resident.header', ['resident' => $resident])

        </div>

        <div class="row bg-white">
            <div class="col pt-3">
                <div class="col-4 pl-0">
                    <input autocomplete="off" type="text" class="form-control" id="search" name="search" placeholder="Search Evaluation"/>
                </div>
                @if(sizeof($assessment) > 0)
                    <table class="table table-striped table-bordered assessment">
                        <thead>
                        <tr>
                            <th class="w-50">{{__('Evaluation')}}</th>
                            <th>{{__('Created By')}}</th>
                            <th>{{__('Updated At')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($assessment as $assm)
                            <tr  @if($assm->Archive == 1)style="text-decoration: line-through; color: silver;"@endif>
                                <td>{{array_get($assm->Form, 'FormName')}}</td>
                                <td>{{array_get($assm->CreatedBy, 'FullName')}}</td>
                                <td>{{ $assm->updated_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                @else
                    <table class="table table-striped table-bordered mt-1">
                        <body>
                        <tr>
                            <td>{{__('No available evaluation.')}}</td>
                        </tr>
                        </body>
                    </table>
                @endif
            </div>
        </div>
    </div>
    </div>

@endsection

@section('script')
    <script>

        $('input[name="search"]').on('keyup', function() {

            var input, filter, tr, td, i;

            input  = $(this);
            filter = input.val().toUpperCase();
            tr     = $("table.assessment tr");

            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0]; // <-- change number if you want other column to search
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        })
    </script>
@endsection

