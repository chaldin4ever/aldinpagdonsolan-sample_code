@extends('layouts.poc')

@section('style')
    <style>
        .white-skin input[type=checkbox].filled-in:checked+label:after {
            background-color: rgb(90, 118, 147);
            border-color: rgb(90, 118, 147);
        }
        .white-skin body{
        'Helvetica Light', Helvetica, Arial, sans-serif
        }

        .nav-link, .navbar {
            padding: 1.2rem .2rem;
            font-weight: normal;
        }

        table.table th {font-weight: bold;}
        .table-bordered th, .table-bordered td {
            border: 1px solid #dee2e6 !important;
        }

    </style>
@endsection
@section('content')

    <div class="container-fluid mt-1">
        <div class="row  pl-4 mb-1  " >
            <div class="col pl-0">
                <h3 class="p-2 float-left"><strong><i class="fa fa-users"></i> {{__('Resident Details')}}</strong></h3>
                <a href="{{url('resident/view/'.$resident->_id)}}" class="float-right pt-2" style="font-size:0.85rem">Back to resident view</a>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row bg-white pl-2 pt-2 pb-2">

            @include('resident.header', ['resident' => $resident])


        </div>
        <div class="content ml-3 mr-3 pl-2 pb-5">

            <div class="row bg-white p-2 pt-4 mt-2">
                <!-- Nav tabs -->
                <div class="col">
                    <ul class="nav nav-tabs nav-justified p-0" style="background-color: #718da9;">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab">{{__('Resident Details')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">{{__('Emergency Contact')}}</a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content card">
                        <!--Panel 1-->
                        <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                            <br>
                            @include('resident.details_resident')
                        </div>
                        <!--/.Panel 1-->
                        <!--Panel 2-->
                        <div class="tab-pane fade" id="panel2" role="tabpanel">

                            <div class="row text-right">
                                <div class="col">
                                    <a href="{{url('resident/edit/'.$resident->_id.'?view=contact')}}" class="btn mdb-color float-right m-2"><i class="fa fa-edit"></i> Edit</a></div>
                            </div>
                            @for($num=1; $num<=4; $num++)
                                @php $contact = array_get($resident_contact, $num); $residentId = $resident->_id; @endphp

                                @include('resident.edit_resident_contact', compact('num', 'contact', 'residentId'))
                            @endfor
                        </div>
                        <!--/.Panel 2-->

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('form#contacts button#save').remove();

            $('form#contacts input').attr('disabled', true);
        })
    </script>
@endsection

