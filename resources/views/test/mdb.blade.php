<html lang="en" class="full-height">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link href="{{ asset('mdb/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mdb/css/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mdb/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/_mdb.style.css') }}" rel="stylesheet">
</head>


<!--Main Navigation-->
<header>

    <nav class="navbar fixed-top navbar-expand-lg navbar-light  scrolling-navbar">
        <a class="navbar-brand" href="#">
            <img src="{{asset('images/oidesk-logo.png')}}" height="30px"/>
            <strong>OI DESK</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <form class="search-form" role="search">
                        <div class="form-group md-form mt-0 pt-1 waves-light">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                    </form>
                </li>
                
            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fa fa-instagram"></i></a>
                </li>
            </ul>
        </div>
    </nav>

</header>
<!--Main Navigation-->

<!--Main Layout-->
<main class="text-center py-5">

    <div class="container">

                @yield('content')

    </div>

</main>
<!--Main Layout-->

            


    <script src="{{asset('mdb/js/jquery-3.2.1.min.js')}}" ></script>
    <script src="{{asset('mdb/js/popper.min.js')}}" ></script>
    
    <script src="{{asset('mdb/js/bootstrap.min.js')}}" ></script>
    <script src="{{asset('mdb/js/mdb.min.js')}}" ></script>
    
</body>

</html>